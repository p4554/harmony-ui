export const environment = {
	production: true,
	hmr: false,
	restApiUrl: 'http://3.108.30.2:5051',
	IDLE_TIMEOUT: 60 * 1000 //2hours
};
