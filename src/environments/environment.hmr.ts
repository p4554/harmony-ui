export const environment = {
	production: false,
	hmr: true,
	restApiUrl: 'http://3.108.30.2:5051',
	IDLE_TIMEOUT: 60 * 1000 //2hours
};
