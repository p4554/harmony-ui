import { AuthConfig } from 'angular-oauth2-oidc';
 
export const authConfig: AuthConfig = {
    issuer: 'https://login.microsoftonline.com/197fad14-00eb-4029-8f07-a373a5c8af99', // Azure AD Tenant ID
    redirectUri: window.location.origin,
    clientId: '15bcb157-dba0-4659-9aaa-fb04b8b08b6f', // Application ID from Azure AD
    scope: 'openid profile email',
    oidc: true
};