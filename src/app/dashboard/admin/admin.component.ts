import { Component } from '@angular/core';

@Component({
	selector: 'app-admin',
	templateUrl: './admin.component.html',
	styleUrls: ['./admin.component.scss'],
})
export class AdminComponent {
	linechart = true;
	barchart = false;
	bubblechart = false;
	doughnutchart = false;

	constructor() {}

	changeChart(param) {
		if (param === 'line') {
			this.linechart = true;
			this.barchart = false;
			this.bubblechart = false;
			this.doughnutchart = false;
		} else if (param === 'bar') {
			this.linechart = false;
			this.barchart = true;
			this.bubblechart = false;
			this.doughnutchart = false;
		} else if (param === 'bubble') {
			this.barchart = false;
			this.linechart = false;
			this.bubblechart = true;
			this.doughnutchart = false;
		} else if (param === 'doughnut') {
			this.barchart = false;
			this.linechart = false;
			this.bubblechart = false;
			this.doughnutchart = true;
		}
	}
}
