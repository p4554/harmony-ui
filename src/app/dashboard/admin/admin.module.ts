import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/shared.module';

import { CardsModule } from '../../components/cards/cards.module';
import { AdminComponent } from './admin.component';
@NgModule({
  declarations: [AdminComponent],
  exports: [AdminComponent],
  imports: [CommonModule, SharedModule, CardsModule, RouterModule],
})
export class AdminModule {}
