import { Component, OnInit } from '@angular/core';
import { CommonDataService } from './../../_services/common-data.service';

export interface userData {
	username: string;
	unit: string;
	created_on: string;
	last_login: string;
	roles: string[];
}
@Component({
	selector: 'app-user',
	templateUrl: './user.component.html',
	styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
	breadCrumData = [
		{
			label: 'Admin',
			command: (event?: any) => {
				console.log('Admin', event);
			},
		},
		{
			label: 'Users',
			command: (event?: any) => {
				console.log('Users', event);
			},
		},
	];
	tabledata: {
		head: string[];
		body: any;
	};
	serachUser: string;

	constructor(private commonDataService: CommonDataService) {}
	ngOnInit(): void {
		let tableCellData = ['username', 'unit', 'created_on', 'last_login', 'roles'];

		this.commonDataService.getusersData().subscribe((data: userData[]) => {
			console.log(data)
			let tableBodyData = [];
			data.forEach((element: userData, i: number) => {
				let obj = {};
				obj['#'] = i + 1;
				tableCellData.forEach((val) => {
					obj[val] = element[val];
				});
				tableBodyData.push(obj);
			});
			this.tabledata = {
				head: ['#', 'Username', 'Business Unit', 'Created On', 'Last Login', 'Roles Assigned', 'Action'],
				body: tableBodyData,
			};
		});
	}

	handleAddUserClick() {
		console.log('Add User');
	}

	handleDownoadClick() {
		console.log('handleDownoadClick');
	}

	HandleUserSearch() {
		console.log(this.serachUser);
	}
}
