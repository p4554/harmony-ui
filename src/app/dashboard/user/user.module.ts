import { NgModule } from '@angular/core';
import { BreadcrumbsModule } from '@app/components/breadcrumbs/breadcrumbs.module';
import { SharedModule } from '@app/shared.module';
import { CommonModule } from '@angular/common';

import { UserComponent } from './user.component';

@NgModule({
  declarations: [UserComponent],
  imports: [
    SharedModule,
    BreadcrumbsModule,
    CommonModule
  
  ],
  exports: [UserComponent],
})
export class UserModule {}
