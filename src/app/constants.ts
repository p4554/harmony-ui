export abstract class Constants {
	static readonly GREY_UP_ARROW = '/assets/images/svg/up-arrow-grey.svg';
	static readonly BLACK_UP_ARROW = '/assets/images/svg/up-arrow-black.svg';
	static readonly BLACK_DOWN_ARROW = '/assets/images/svg/down-arrow-black.svg';
}
