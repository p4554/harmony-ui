import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NonOutlinedButtonComponent } from './non-outlined-button.component';

describe('NonOutlinedButtonComponent', () => {
  let component: NonOutlinedButtonComponent;
  let fixture: ComponentFixture<NonOutlinedButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NonOutlinedButtonComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NonOutlinedButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
