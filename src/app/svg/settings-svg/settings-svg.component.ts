import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'app-settings-svg',
	templateUrl: './settings-svg.component.html',
	styleUrls: ['./settings-svg.component.scss'],
})
export class SettingsSvgComponent implements OnInit {
	@Input() color: string;
	@Input() expanded: boolean;
	fillColor = '#707070';
	clickedMenu = '';

	constructor(private _router: Router) {}

	ngOnInit(): void {}

	settings() {
		this._router.navigateByUrl('/home/settings-home');
	}
}
