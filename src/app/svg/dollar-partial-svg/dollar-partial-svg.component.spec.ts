import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DollarPartialSvgComponent } from './dollar-partial-svg.component';

describe('DollarPartialSvgComponent', () => {
  let component: DollarPartialSvgComponent;
  let fixture: ComponentFixture<DollarPartialSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DollarPartialSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DollarPartialSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
