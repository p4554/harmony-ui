import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
	selector: 'app-down-arrow-svg',
	templateUrl: './down-arrow-svg.component.html',
	styleUrls: ['./down-arrow-svg.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DownArrowSvgComponent {
	@Input() color: string;
}
