import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfiguredSourceSvgComponent } from './configured-source-svg.component';

describe('ConfiguredSourceSvgComponent', () => {
  let component: ConfiguredSourceSvgComponent;
  let fixture: ComponentFixture<ConfiguredSourceSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfiguredSourceSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConfiguredSourceSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
