import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SourceOnboardSvgComponent } from './source-onboard-svg.component';

describe('SourceOnboardSvgComponent', () => {
  let component: SourceOnboardSvgComponent;
  let fixture: ComponentFixture<SourceOnboardSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SourceOnboardSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SourceOnboardSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
