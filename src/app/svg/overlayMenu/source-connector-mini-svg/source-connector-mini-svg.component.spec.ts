import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SourceConnectorMiniSvgComponent } from './source-connector-mini-svg.component';

describe('SourceConnectorMiniSvgComponent', () => {
  let component: SourceConnectorMiniSvgComponent;
  let fixture: ComponentFixture<SourceConnectorMiniSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SourceConnectorMiniSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SourceConnectorMiniSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
