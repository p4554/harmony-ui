import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SourceLibrarySvgComponent } from './source-library-svg.component';

describe('SourceLibrarySvgComponent', () => {
  let component: SourceLibrarySvgComponent;
  let fixture: ComponentFixture<SourceLibrarySvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SourceLibrarySvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SourceLibrarySvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
