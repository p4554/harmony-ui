import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SourceConnectorSvgComponent } from './source-connector-svg.component';

describe('SourceConnectorSvgComponent', () => {
  let component: SourceConnectorSvgComponent;
  let fixture: ComponentFixture<SourceConnectorSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SourceConnectorSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SourceConnectorSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
