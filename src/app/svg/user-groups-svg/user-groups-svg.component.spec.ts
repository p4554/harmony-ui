import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserGroupsSvgComponent } from './user-groups-svg.component';

describe('UserGroupsSvgComponent', () => {
  let component: UserGroupsSvgComponent;
  let fixture: ComponentFixture<UserGroupsSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserGroupsSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserGroupsSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
