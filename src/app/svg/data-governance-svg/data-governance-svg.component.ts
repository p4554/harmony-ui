import { Component ,Input } from '@angular/core';

@Component({
  selector: 'app-data-governance-svg',
  templateUrl: './data-governance-svg.component.html',
  styleUrls: ['./data-governance-svg.component.scss']
})
export class DataGovernanceSvgComponent {

    @Input() props
}
