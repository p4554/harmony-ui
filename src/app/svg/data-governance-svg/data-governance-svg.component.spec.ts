import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataGovernanceSvgComponent } from './data-governance-svg.component';

describe('DataGovernanceSvgComponent', () => {
  let component: DataGovernanceSvgComponent;
  let fixture: ComponentFixture<DataGovernanceSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataGovernanceSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DataGovernanceSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
