import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TenantSvgComponent } from './tenant-svg.component';

describe('TenantSvgComponent', () => {
  let component: TenantSvgComponent;
  let fixture: ComponentFixture<TenantSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TenantSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TenantSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
