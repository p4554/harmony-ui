import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'app-brands-svg',
	templateUrl: './brands-svg.component.html',
	styleUrls: ['./brands-svg.component.scss'],
})
export class BrandsSvgComponent implements OnInit {
	@Input() color: string;
	@Input() expanded: boolean;
	fillColor = '#707070';
	clickedMenu = '';

	constructor(private _router: Router) {}

	ngOnInit(): void {}

	list() {
		this._router.navigateByUrl('/home/view-brands');
	}
}
