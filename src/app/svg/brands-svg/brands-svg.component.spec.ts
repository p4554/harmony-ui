import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BrandsSvgComponent } from './brands-svg.component';

describe('BrandsSvgComponent', () => {
  let component: BrandsSvgComponent;
  let fixture: ComponentFixture<BrandsSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BrandsSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BrandsSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
