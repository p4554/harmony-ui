import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesSvgComponent } from './sales-svg.component';

describe('SalesSvgComponent', () => {
  let component: SalesSvgComponent;
  let fixture: ComponentFixture<SalesSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalesSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SalesSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
