import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'app-sales-svg',
	templateUrl: './sales-svg.component.html',
	styleUrls: ['./sales-svg.component.scss'],
})
export class SalesSvgComponent implements OnInit {
	@Input() color: string;
	@Input() expanded: boolean;
	@Input() isMenuOpen: boolean;

	@Input() clickedItem: string;

	fillColor = '#707070';

	constructor(private _router: Router) {}

	ngOnInit(): void {}

	listSales() {
		this.clickedItem = 'list-sales';
		this._router.navigateByUrl('/home/search-sales');
	}

	saleReturns() {
		this.clickedItem = 'sale-returns';
		this._router.navigateByUrl('/home/search-return-sales');
	}

	stockIssues() {
		this.clickedItem = 'stock-issues';
		this._router.navigateByUrl('/home/search-stock-issues');
	}

	toggleSubMenu() {
		this.expanded = !this.expanded;
	}
}
