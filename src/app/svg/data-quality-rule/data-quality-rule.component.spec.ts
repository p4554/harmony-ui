import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataQualityRuleComponent } from './data-quality-rule.component';

describe('DataQualityRuleComponent', () => {
  let component: DataQualityRuleComponent;
  let fixture: ComponentFixture<DataQualityRuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataQualityRuleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DataQualityRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
