import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PencilISvgComponent } from './pencil-i-svg.component';

describe('PencilISvgComponent', () => {
  let component: PencilISvgComponent;
  let fixture: ComponentFixture<PencilISvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PencilISvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PencilISvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
