import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessRuleProductSvgComponent } from './business-rule-product-svg.component';

describe('BusinessRuleProductSvgComponent', () => {
  let component: BusinessRuleProductSvgComponent;
  let fixture: ComponentFixture<BusinessRuleProductSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessRuleProductSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BusinessRuleProductSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
