import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorsSvgComponent } from './vendors-svg.component';

describe('VendorsSvgComponent', () => {
  let component: VendorsSvgComponent;
  let fixture: ComponentFixture<VendorsSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VendorsSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VendorsSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
