import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'app-vendors-svg',
	templateUrl: './vendors-svg.component.html',
	styleUrls: ['./vendors-svg.component.scss'],
})
export class VendorsSvgComponent implements OnInit {
	@Input() color: string;
	@Input() expanded: boolean;
	@Input() isMenuOpen: boolean;
	@Input() clickedItem: string;

	fillColor = '#707070';

	constructor(private _router: Router) {}

	ngOnInit(): void {}

	list() {
		this.clickedItem = 'list-vendors';
		this._router.navigateByUrl('/home/view-vendors');
	}

	payments() {
		this.clickedItem = 'vendor-payments';
		this._router.navigateByUrl('/home/payments');
	}

	pendignPayments() {
		this.clickedItem = 'vendor-statements';
		this._router.navigateByUrl('/home/pending-payments');
	}

	statements() {
		this.clickedItem = 'back-order';
		this._router.navigateByUrl('/home/vpurchase-accounts-statement');
	}

	toggleSubMenu() {
		this.expanded = !this.expanded;
	}
}

// name: 'List',
// // 			link: '/home/enquiry/open-enquiry/O/weekly',
// // 			icon: '/assets/images/svg/money.svg',
// // 		},
// // 		{
// // 			name: 'Back Orders',
// // 			link: '/home/back-order',
// // 			icon: '/assets/images/svg/money.svg',
