import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserAccessSvgComponent } from './user-access-svg.component';

describe('UserAccessSvgComponent', () => {
  let component: UserAccessSvgComponent;
  let fixture: ComponentFixture<UserAccessSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserAccessSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserAccessSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
