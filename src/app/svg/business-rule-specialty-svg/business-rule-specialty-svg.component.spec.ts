import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessRuleSpecialtySvgComponent } from './business-rule-specialty-svg.component';

describe('BusinessRuleSpecialtySvgComponent', () => {
  let component: BusinessRuleSpecialtySvgComponent;
  let fixture: ComponentFixture<BusinessRuleSpecialtySvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessRuleSpecialtySvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BusinessRuleSpecialtySvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
