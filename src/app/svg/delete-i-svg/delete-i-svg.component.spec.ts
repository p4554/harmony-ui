import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteISvgComponent } from './delete-i-svg.component';

describe('DeleteISvgComponent', () => {
  let component: DeleteISvgComponent;
  let fixture: ComponentFixture<DeleteISvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteISvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DeleteISvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
