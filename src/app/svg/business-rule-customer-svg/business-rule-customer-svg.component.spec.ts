import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessRuleCustomerSvgComponent } from './business-rule-customer-svg.component';

describe('BusinessRuleCustomerSvgComponent', () => {
  let component: BusinessRuleCustomerSvgComponent;
  let fixture: ComponentFixture<BusinessRuleCustomerSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessRuleCustomerSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BusinessRuleCustomerSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
