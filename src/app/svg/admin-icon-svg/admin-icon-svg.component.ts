import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-admin-icon-svg',
  templateUrl: './admin-icon-svg.component.html',
  styleUrls: ['./admin-icon-svg.component.scss']
})
export class AdminIconSvgComponent {

  @Input() props
}
