import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminIconSvgComponent } from './admin-icon-svg.component';

describe('AdminIconSvgComponent', () => {
  let component: AdminIconSvgComponent;
  let fixture: ComponentFixture<AdminIconSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminIconSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminIconSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
