import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserZoneSvgComponent } from './user-zone-svg.component';

describe('UserZoneSvgComponent', () => {
  let component: UserZoneSvgComponent;
  let fixture: ComponentFixture<UserZoneSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserZoneSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserZoneSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
