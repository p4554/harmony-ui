import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SessionManagerSvgComponent } from './session-manager-svg.component';

describe('SessionManagerSvgComponent', () => {
  let component: SessionManagerSvgComponent;
  let fixture: ComponentFixture<SessionManagerSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SessionManagerSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SessionManagerSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
