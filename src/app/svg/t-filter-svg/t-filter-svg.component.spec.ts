import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TFilterSvgComponent } from './t-filter-svg.component';

describe('TFilterSvgComponent', () => {
  let component: TFilterSvgComponent;
  let fixture: ComponentFixture<TFilterSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TFilterSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TFilterSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
