import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessRuleAlignmentSvgComponent } from './business-rule-alignment-svg.component';

describe('BusinessRuleAlignmentSvgComponent', () => {
  let component: BusinessRuleAlignmentSvgComponent;
  let fixture: ComponentFixture<BusinessRuleAlignmentSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessRuleAlignmentSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BusinessRuleAlignmentSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
