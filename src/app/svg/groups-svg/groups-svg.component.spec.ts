import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupsSvgComponent } from './groups-svg.component';

describe('GroupsSvgComponent', () => {
  let component: GroupsSvgComponent;
  let fixture: ComponentFixture<GroupsSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupsSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GroupsSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
