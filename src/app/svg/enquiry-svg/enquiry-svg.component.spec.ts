import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnquirySvgComponent } from './enquiry-svg.component';

describe('EnquirySvgComponent', () => {
  let component: EnquirySvgComponent;
  let fixture: ComponentFixture<EnquirySvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EnquirySvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EnquirySvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
