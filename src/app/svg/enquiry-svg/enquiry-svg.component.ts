import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'app-enquiry-svg',
	templateUrl: './enquiry-svg.component.html',
	styleUrls: ['./enquiry-svg.component.scss'],
})
export class EnquirySvgComponent implements OnInit {
	@Input() color: string;
	@Input() expanded: boolean;
	@Input() isMenuOpen: boolean;
	@Input() clickedItem: string;

	fillColor = '#707070';

	constructor(private _router: Router) {}

	ngOnInit(): void {}

	enquiryListRoute() {
		this.clickedItem = 'list-enquiry';
		this._router.navigateByUrl('/home/enquiry/open-enquiry/O/weekly');
	}

	backOrderRoute() {
		this.clickedItem = 'back-order';
		this._router.navigateByUrl('/home/back-order');
	}

	toggleSubMenu() {
		this.expanded = !this.expanded;
	}
}

// name: 'List',
// // 			link: '/home/enquiry/open-enquiry/O/weekly',
// // 			icon: '/assets/images/svg/money.svg',
// // 		},
// // 		{
// // 			name: 'Back Orders',
// // 			link: '/home/back-order',
// // 			icon: '/assets/images/svg/money.svg',
