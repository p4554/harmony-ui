import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-data-engineering-svg',
  templateUrl: './data-engineering-svg.component.html',
  styleUrls: ['./data-engineering-svg.component.scss']
})
export class DataEngineeringSvgComponent {

  @Input() props
}
