import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataEngineeringSvgComponent } from './data-engineering-svg.component';

describe('DataEngineeringSvgComponent', () => {
  let component: DataEngineeringSvgComponent;
  let fixture: ComponentFixture<DataEngineeringSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataEngineeringSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DataEngineeringSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
