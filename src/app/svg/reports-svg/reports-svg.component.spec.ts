import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsSvgComponent } from './reports-svg.component';

describe('ReportsSvgComponent', () => {
  let component: ReportsSvgComponent;
  let fixture: ComponentFixture<ReportsSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportsSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReportsSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
