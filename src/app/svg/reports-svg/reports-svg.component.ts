import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'app-reports-svg',
	templateUrl: './reports-svg.component.html',
	styleUrls: ['./reports-svg.component.scss'],
})
export class ReportsSvgComponent implements OnInit {
	@Input() color: string;
	@Input() expanded: boolean;
	fillColor = '#707070';
	clickedMenu = '';

	constructor(private _router: Router) {}

	ngOnInit(): void {}

	reports() {
		this._router.navigateByUrl('/home/reports-home');
	}
}
