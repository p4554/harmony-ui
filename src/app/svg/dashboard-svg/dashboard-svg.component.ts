import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'app-dashboard-svg',
	templateUrl: './dashboard-svg.component.html',
	styleUrls: ['./dashboard-svg.component.scss'],
})
export class DashboardSvgComponent implements OnInit {
	@Input() color: string;
	// @Input() expanded: boolean;
	expanded = ''
	constructor(private _router: Router) {}

	ngOnInit(): void {}

	route() {
		this._router.navigateByUrl('/home/admin-dashboard');
	}
}
