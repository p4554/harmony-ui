import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeIconSvgComponent } from './home-icon-svg.component';

describe('HomeIconSvgComponent', () => {
  let component: HomeIconSvgComponent;
  let fixture: ComponentFixture<HomeIconSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeIconSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HomeIconSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
