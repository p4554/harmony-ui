import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RuleLibrarySvgComponent } from './rule-library-svg.component';

describe('RuleLibrarySvgComponent', () => {
  let component: RuleLibrarySvgComponent;
  let fixture: ComponentFixture<RuleLibrarySvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RuleLibrarySvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RuleLibrarySvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
