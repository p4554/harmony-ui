import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-ds-config-svg',
  templateUrl: './ds-config-svg.component.html',
  styleUrls: ['./ds-config-svg.component.scss']
})
export class DsConfigSvgComponent {

  @Input() props
}
