import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DsConfigSvgComponent } from './ds-config-svg.component';

describe('DsConfigSvgComponent', () => {
  let component: DsConfigSvgComponent;
  let fixture: ComponentFixture<DsConfigSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DsConfigSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DsConfigSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
