import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessRuleSvgComponent } from './business-rule-svg.component';

describe('BusinessRuleSvgComponent', () => {
  let component: BusinessRuleSvgComponent;
  let fixture: ComponentFixture<BusinessRuleSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessRuleSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BusinessRuleSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
