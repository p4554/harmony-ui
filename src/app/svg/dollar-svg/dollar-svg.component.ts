import { Component, Input, OnInit } from '@angular/core';

@Component({
	selector: 'app-dollar-svg',
	templateUrl: './dollar-svg.component.html',
	styleUrls: ['./dollar-svg.component.scss'],
})
export class DollarSvgComponent implements OnInit {
	@Input() color: string;

	constructor() {}

	ngOnInit(): void {}
}
