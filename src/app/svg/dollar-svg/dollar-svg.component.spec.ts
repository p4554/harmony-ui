import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DollarSvgComponent } from './dollar-svg.component';

describe('DollarSvgComponent', () => {
  let component: DollarSvgComponent;
  let fixture: ComponentFixture<DollarSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DollarSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DollarSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
