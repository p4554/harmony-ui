import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StocksSvgComponent } from './stocks-svg.component';

describe('StocksSvgComponent', () => {
  let component: StocksSvgComponent;
  let fixture: ComponentFixture<StocksSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StocksSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StocksSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
