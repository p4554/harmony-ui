import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'app-stocks-svg',
	templateUrl: './stocks-svg.component.html',
	styleUrls: ['./stocks-svg.component.scss'],
})
export class StocksSvgComponent implements OnInit {
	@Input() color: string;
	@Input() expanded: boolean;
	fillColor = '#707070';
	clickedMenu = '';

	constructor(private _router: Router) {}

	ngOnInit(): void {}

	list() {
		this._router.navigateByUrl('/home/view-products');
	}
}
