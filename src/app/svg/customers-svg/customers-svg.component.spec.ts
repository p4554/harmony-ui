import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomersSvgComponent } from './customers-svg.component';

describe('CustomersSvgComponent', () => {
  let component: CustomersSvgComponent;
  let fixture: ComponentFixture<CustomersSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomersSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CustomersSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
