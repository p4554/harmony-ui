import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'app-customers-svg',
	templateUrl: './customers-svg.component.html',
	styleUrls: ['./customers-svg.component.scss'],
})
export class CustomersSvgComponent implements OnInit {
	@Input() color: string;
	@Input() expanded: boolean;
	@Input() isMenuOpen: boolean;
	@Input() clickedItem: string;

	fillColor = '#707070';

	constructor(private _router: Router) {}

	ngOnInit(): void {}

	List() {
		this.clickedItem = 'list-customers';
		this._router.navigateByUrl('/home/view-customers');
	}

	statements() {
		this.clickedItem = 'customer-statements';
		this._router.navigateByUrl('/home/customer-accounts-statement');
	}

	receivedPayments() {
		this.clickedItem = 'received-payments';
		this._router.navigateByUrl('/home/receivables');
	}

	pendingReceivables() {
		this.clickedItem = 'pending-receivables';
		this._router.navigateByUrl('/home/pending-receivables');
	}

	toggleSubMenu() {
		this.expanded = !this.expanded;
	}
}

// name: 'List',
// // 			link: '/home/enquiry/open-enquiry/O/weekly',
// // 			icon: '/assets/images/svg/money.svg',
// // 		},
// // 		{
// // 			name: 'Back Orders',
// // 			link: '/home/back-order',
// // 			icon: '/assets/images/svg/money.svg',
