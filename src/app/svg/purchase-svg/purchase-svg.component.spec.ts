import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseSvgComponent } from './purchase-svg.component';

describe('PurchaseSvgComponent', () => {
  let component: PurchaseSvgComponent;
  let fixture: ComponentFixture<PurchaseSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PurchaseSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PurchaseSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
