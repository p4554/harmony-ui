import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'app-purchase-svg',
	templateUrl: './purchase-svg.component.html',
	styleUrls: ['./purchase-svg.component.scss'],
})
export class PurchaseSvgComponent implements OnInit {
	@Input() color: string;
	@Input() expanded: boolean;
	fillColor = '#707070';
	clickedMenu = '';

	constructor(private _router: Router) {}

	ngOnInit(): void {}

	list() {
		this._router.navigateByUrl('/home/search-purchase-order');
	}
}
