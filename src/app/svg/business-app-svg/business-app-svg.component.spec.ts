import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessAppSvgComponent } from './business-app-svg.component';

describe('BusinessAppSvgComponent', () => {
  let component: BusinessAppSvgComponent;
  let fixture: ComponentFixture<BusinessAppSvgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessAppSvgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BusinessAppSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
