import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-business-app-svg',
  templateUrl: './business-app-svg.component.html',
  styleUrls: ['./business-app-svg.component.scss']
})
export class BusinessAppSvgComponent {
  @Input() props
}
