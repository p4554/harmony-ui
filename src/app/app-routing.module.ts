import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './dashboard/admin/admin.component';
import { UserComponent } from './dashboard/user/user.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';

import { LoginComponent } from './login/login.component';
import { AdvancedSearchComponent } from './pages/advanced-search/advanced-search.component';
import { DataCatalogComponent } from './pages/data-governance/data-catalog/data-catalog.component';
import { DataLineageComponent } from './pages/data-governance/data-lineage/data-lineage.component';
import { DataProfilingComponent } from './pages/data-governance/data-profiling/data-profiling.component';
import { QualityRulesComponent } from './pages/data-governance/quality-rules/quality-rules.component';
import { DevProgressComponent } from './pages/dev-progress/dev-progress.component';
import { GroupsComponent } from './pages/setting/groups/groups.component';
import { LovMasterComponent } from './pages/setting/lov-master/lov-master.component';
import { SessionManagerComponent } from './pages/setting/session-manager/session-manager.component';
import { TenantsComponent } from './pages/setting/tenants/tenants.component';
import { UserAccessComponent } from './pages/setting/user-access/user-access.component';
import { UserGroupsComponent } from './pages/setting/user-groups/user-groups.component';
import { UserZoneComponent } from './pages/setting/user-zone/user-zone.component';
import { UsersComponent } from './pages/setting/users/users.component';
import { RuleLibraryComponent } from './pages/data-governance/rule-library/rule-library.component';
import { SourceConnectorComponent } from './pages/source-connector/source-connector.component';
import { AuthGuard, loginAuthGuard } from './_helpers/auth.guard';
import { BusinessRuleComponent } from './pages/data-governance/business-rule/business-rule.component';
import { FileMonitoringComponent } from './pages/data-governance/file-monitoring/file-monitoring.component';
import { DocumentLibraryComponent } from './pages/data-governance/document-library/document-library.component';
import { NotificationComponent } from './pages/notification/notification.component';
import { InboxComponent } from './pages/inbox/inbox.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
		path: 'login',
		// component: LoginComponent,
		loadChildren: () => import('./login/login.module').then((m) => m.LoginModule),

	},
  {
    path: 'forgot-password',
    component: ForgotPasswordComponent,
  },
  {
    path: 'admin-dashboard',
    component: AdminComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'user-dashboard',
    component: UserComponent,
    canActivate : [AuthGuard]
  },
  // {
  //   path: 'login',
  //   component: LoginComponent,
  // },
  {
    path: 'source-connector',
    component: SourceConnectorComponent,
    canActivate : [AuthGuard]
  },{
    path: 'data-quality-rule',
    component: QualityRulesComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'dev-progress',
    component: DevProgressComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'data-profiling',
    component: DataProfilingComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'data-lineage',
    component: DataLineageComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'data-catalog',
    component: DataCatalogComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'Tenants',
    component: TenantsComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'users',
    component: UsersComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'groups',
    component: GroupsComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'user-access',
    component: UserAccessComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'user-groups',
    component: UserGroupsComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'lov-master',
    component: LovMasterComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'session-manager',
    component: SessionManagerComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'user-zone',
    component: UserZoneComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'rule-library',
    component: RuleLibraryComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'business-rule',
    component: BusinessRuleComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'file-monitoring',
    component: FileMonitoringComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'document-library',
    component: DocumentLibraryComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'notification',
    component: NotificationComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'inbox',
    component: InboxComponent,
    canActivate : [AuthGuard]
  },
  {
    path: '',
    component: LoginComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
