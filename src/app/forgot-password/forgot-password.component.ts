import { Component ,OnInit} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CommonDataService } from '@app/_services/common-data.service';
import { LoaderService } from '@app/_services/loading.service';
import { NavigationService } from '@app/_services/navigation.service';
import { EMAIL_REGEX } from '@app/util/helper/patterns';
import { patternValidator } from '@app/util/validators/pattern-validator';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit{
  resetFrom: FormGroup;
  user: any;
  submitted:boolean
  apiResponse:boolean = false;
	errMsg = {require:"This field is required", email :"Please enter a valid email address"}
  constructor(
    private navigationService:NavigationService,
    private commonDataService:CommonDataService,
    public loaderService: LoaderService,
  ){
  }
  ngOnInit():void{
    this.resetFrom = new FormGroup({
			username: new FormControl('', [Validators.required,patternValidator(EMAIL_REGEX)]),
		});
    this.user = this.resetFrom.controls['username']
  }
  handleClick = async () => {
		if(this.resetFrom.valid){
      console.log(this.user.value)
      this.loaderService.showLoader()
      this.apiResponse = true
      this.commonDataService.getForgotEmail(this.user.value).subscribe(response=>{
        this.loaderService.hideLoader()
      
        console.log(response)
      // if(response === 'Please check your email, We have send a mail.'){
      // }else {
      // }
      }) 
		}else{
      this.submitted = true;
    }
	};
  handleReturnSignInClick(){
    this.navigationService.redirectToLogin()
  }
}
