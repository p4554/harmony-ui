import { Component } from '@angular/core';
import {AdminApiService} from '../../_services/admin-api.service'
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { CUSTOM_CHAR_REGEX } from '@app/util/helper/patterns';
import { LoaderService } from '@app/_services/loading.service';
import { editDuplicateRecordCheckerAdmin } from '@app/util/helper/arrayHandlers';
@Component({
  selector: 'app-edit-group',
  templateUrl: './edit-group.component.html',
  styleUrls: ['./edit-group.component.scss']
})
export class EditGroupComponent {
  editGroupForm: FormGroup;
   group_type;
   tableData:any;
   groupTypeData=[
    'Technical',
    'Functional'
  ]
  group_id;
 constructor(
  private ref: DynamicDialogRef,
  private AdminApiService: AdminApiService,
  private fb: FormBuilder,
  private toastService: MessageService,
  private config: DynamicDialogConfig,
  public loaderService: LoaderService,
 ){}
 async ngOnInit(){
    const data = this.config.data.editData;
    this.tableData = this.config.data.tableData;

    const groupName:any =data.USRGRP_NM;
    const groupDescription:any=data.USRGRP_DESC;
    this.group_id=data.USRGRP_ID;
    this.editGroupForm = this.fb.group({
    groupName: [groupName,[Validators.required,Validators.pattern(CUSTOM_CHAR_REGEX)]],
    description: [groupDescription, [Validators.required,Validators.pattern(CUSTOM_CHAR_REGEX)]],
    groupType: [, Validators.required],
    active: [, Validators.required],
  }); 
  const typeMappings = {
    T: 'Technical',
    F: 'Functional'
  };
  
  this.editGroupForm.patchValue({
    groupType: typeMappings[data.GRP_TYP],
    active: data.ACTV_FLG
  });
  
 }

 cancelButtonHandler(){
  this.ref.close();
  }
 async onSaveHandler(){
    if(this.editGroupForm.valid){
      let formValue = this.editGroupForm.value;
      let groupTypeValue;
      if(formValue.groupType==='Technical'){
        groupTypeValue='T';
        formValue.groupType='T';
      }
     else if(formValue.groupType==='Functional'){
      groupTypeValue='F';
        formValue.groupType='F';
        }
        const formProperty = ['groupName','groupType']
        const tableProperty = ['USRGRP_NM','GRP_TYP']
        const duplicateFound = editDuplicateRecordCheckerAdmin(formValue,formProperty,this.tableData,tableProperty,this.group_id,'USRGRP_ID')
      if(duplicateFound){
        this.toastService.add({key: 'wrn',closable: false,sticky: false,severity: '',summary: '',detail:'User is Already Exists.Try Another',});
      }
      else{
       const postData={
        userGroupName:formValue.groupName,
        userGroupDesc:formValue.description,
        groupType:groupTypeValue,
        activeFlag:formValue.active,
        updatedBy:localStorage.getItem('userName'), 
        userGroupId:this.group_id
        }    
      try {
        const response = await this.AdminApiService.updateGroup(postData).toPromise();
  
        this.toastService.clear(); 
        console.log(response);
  
        if(response.status==200){
          this.ref.close('saved');
          this.toastService.add({
            key: 'su',
            closable: false,
            sticky: false,
            severity: '',
            summary: '',
            detail: `Data Successfully Updated!`,
          });
        }
        else{
          this.ref.close();
          this.toastService.clear();
          this.toastService.add({
            key: 'wrn',
            closable: false,
            sticky: false,
            severity: '',
            summary: '',
            detail: `Connection Error Try Again`,
          });
        }
      } catch (error) {
        console.log(error);
      } 
    }   
    }
    else{
      this.editGroupForm.markAllAsTouched();
    } 
 }
}
