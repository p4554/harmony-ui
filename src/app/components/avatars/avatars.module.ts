import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AvatarModule } from 'primeng/avatar';
import { AvatarsComponent } from './avatars.component';
@NgModule({
  declarations: [AvatarsComponent],
  imports: [CommonModule, AvatarModule],
  exports: [AvatarsComponent],
})
export class AvatarsModule {}
