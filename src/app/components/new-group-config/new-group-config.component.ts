import { Component } from '@angular/core';
import {AdminApiService} from '../../_services/admin-api.service'
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import {  DynamicDialogRef } from 'primeng/dynamicdialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { MessageService } from 'primeng/api';
import { LoaderService } from '@app/_services/loading.service';
@Component({
  selector: 'app-new-group-config',
  templateUrl: './new-group-config.component.html',
  styleUrls: ['./new-group-config.component.scss']
})
export class NewGroupConfigComponent {
  groupNameData:any=[]
  NewGroupConfigForm: FormGroup;
  
 constructor(
  private ref: DynamicDialogRef,
  private AdminApiService: AdminApiService,
  private fb: FormBuilder,
  private toastService: MessageService,
  public loaderService: LoaderService,
 ){}
 async ngOnInit(){
    this.NewGroupConfigForm = this.fb.group({
    userName: ['', Validators.required],
    groupName: ['', Validators.required],
    active: ['', Validators.required],
  });
  this.loaderService.showLoader();
  await this.getUserNameData();
  await this.getGroupNameData();
  setTimeout(() => {
    this.loaderService.hideLoader();
  }, 300); 
 }
 userNameData:any;
 mappedUserNameData:any;
 mappedGroupNameData:any;
 userName:any;
 async getUserNameData(){
     this.userNameData=await lastValueFrom(
    this.AdminApiService.userName()
   )
   this.mappedUserNameData = this.userNameData.map(user => {
    return {
      user_name: user.USER_NM,
      userId: user.USER_ID
    };
  });
   this.userName = getMappedArray(
    getUniqueValues(this.userNameData,'USER_NM'),
    'label',
    'value'
  );
}
groupname_data:any;
 async getGroupNameData(){
     this.groupname_data=await lastValueFrom(
    this.AdminApiService.groupName()
   )
   this.mappedGroupNameData = this.groupname_data.map(user => {
    return {
      group_name: user.USRGRP_NM,
      groupId: user.USRGRP_ID,
    };
  });
   this.groupNameData = getMappedArray(
    getUniqueValues(this.groupname_data,'USRGRP_NM'),
    'label',
    'value'
  );
 }
 cancelButtonHandler(){
  this.ref.close();
  }
  selectedUserId:number;
  selectedGroupId:number;
 async onSaveHandler(){
    if(this.NewGroupConfigForm.valid){
      let formValue = this.NewGroupConfigForm.value;
      
      const selectedUser = this.mappedUserNameData.find(user => user.user_name === formValue.userName);
         this.selectedUserId = selectedUser.userId;
      const selectedGroup = this.mappedGroupNameData.find(user => user.group_name === formValue.groupName);
      this.selectedGroupId=selectedGroup.groupId;
    const postData= {
            userId:this.selectedUserId,
            userGroupId:this.selectedGroupId,
            activeFlag:formValue.active,
            createdBy: localStorage.getItem('userName'),
           updatedBy: localStorage.getItem('userName')
      }   
      await this.AdminApiService.addUserGroup(postData).subscribe((response)=>{
        console.log(response);
        if(response.status==200){
          this.ref.close('saved');
          this.toastService.add({
            key: 'su',
            closable: false,
            sticky: false,
            severity: '',
            summary: '',
            detail: `New Configuration Created`,
          });
        }
        else{
          this.ref.close();
          this.toastService.clear();
              this.toastService.add({
                key: 'wrn',
                closable: false,
                sticky: false,
                severity: '',
                summary: '',
                detail: `Connection Error Try Again`,
              });
        }
      }) 
    }
    else{         
      this.NewGroupConfigForm.markAllAsTouched();
    } 
 }
}
