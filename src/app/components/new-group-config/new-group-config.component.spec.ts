import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewGroupConfigComponent } from './new-group-config.component';

describe('NewGroupConfigComponent', () => {
  let component: NewGroupConfigComponent;
  let fixture: ComponentFixture<NewGroupConfigComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewGroupConfigComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NewGroupConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
