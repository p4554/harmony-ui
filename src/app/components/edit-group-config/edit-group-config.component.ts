import { Component } from '@angular/core';
import { AdminApiService } from '../../_services/admin-api.service'
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { MessageService } from 'primeng/api';
import { LoaderService } from '@app/_services/loading.service';
@Component({
  selector: 'app-edit-group-config',
  templateUrl: './edit-group-config.component.html',
  styleUrls: ['./edit-group-config.component.scss']
})
export class EditGroupConfigComponent {
  editGroupConfigForm: FormGroup;

  constructor(
    private ref: DynamicDialogRef,
    private AdminApiService: AdminApiService,
    private fb: FormBuilder,
    private toastService: MessageService,
    private config: DynamicDialogConfig,
    public loaderService: LoaderService,
  ) { }
  usrGroupId: number;
  usr_name: any;
  grp_name: any;
  atv_status: any
  async ngOnInit() {
    const data = this.config.data;
    this.usrGroupId = data.USRGRP_USR_ID;
    this.usr_name = data.USER_NM;
    this.grp_name = data.USRGRP_NM;
    this.atv_status = data.ACTV_FLG;
    this.editGroupConfigForm = this.fb.group({
      username: [this.usr_name, Validators.required],
      groupname: [this.grp_name, Validators.required],
      active: [this.atv_status, Validators.required],
    });
    this.loaderService.showLoader();
    await this.getUserNameData();
    await this.getGroupNameData();
    setTimeout(() => {
      this.loaderService.hideLoader();
    }, 300);
  }
  username_id: any = [];
  username_data: any;
  userNameData: any;
  mappedUserNameData: any;
  mappedGroupNameData: any;
  userName: any;
  async getUserNameData() {
    this.userNameData = await lastValueFrom(
      this.AdminApiService.userName()
    )
    this.mappedUserNameData = this.userNameData.map(user => {
      return {
        user_name: user.USER_NM,
        userId: user.USER_ID
      };
    });
    this.userName = getMappedArray(
      getUniqueValues(this.userNameData, 'USER_NM'),
      'label',
      'value'
    );
  }
  groupname_data: any;
  groupNameData: any = []
  async getGroupNameData() {
    this.groupname_data = await lastValueFrom(
      this.AdminApiService.groupName()
    )
    this.mappedGroupNameData = this.groupname_data.map(user => {
      return {
        group_name: user.USRGRP_NM,
        groupId: user.USRGRP_ID,
      };
    });
    this.groupNameData = getMappedArray(
      getUniqueValues(this.groupname_data, 'USRGRP_NM'),
      'label',
      'value'
    );
  }
  cancelButtonHandler() {
    this.ref.close();
  }
  selectedUserId: number;
  selectedGroupId: number;
  async onSaveHandler() {
    if (this.editGroupConfigForm.valid) {
      let formValue = this.editGroupConfigForm.value;

      const selectedUser = this.mappedUserNameData.find(user => user.user_name === formValue.username);
      this.selectedUserId = selectedUser.userId;
      const selectedGroup = this.mappedGroupNameData.find(user => user.group_name === formValue.groupname);
      this.selectedGroupId = selectedGroup.groupId;
      const postData = {
        userId: this.selectedUserId,
        userGroupId: this.selectedGroupId,
        activeFlag: formValue.active,
        updatedBy: localStorage.getItem('userName'),
        userGroupUserId: this.usrGroupId
      }

      await this.AdminApiService.updateUserGroup(postData).subscribe((response) => {
        console.log(response);
        if (response.status == 200) {
          this.ref.close('saved');
          this.toastService.add({
            key: 'su',
            closable: false,
            sticky: false,
            severity: '',
            summary: '',
            detail: `Configuration Successfully Updated`,
          });
        }
        else {
          this.ref.close();
          this.toastService.clear();
          this.toastService.add({
            key: 'wrn',
            closable: false,
            sticky: false,
            severity: '',
            summary: '',
            detail: `Connection Error Try Again`,
          });
        }
      })
    }
    else {
      this.editGroupConfigForm.markAllAsTouched();
    }
  }
}
