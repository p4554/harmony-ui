import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditGroupConfigComponent } from './edit-group-config.component';

describe('EditGroupConfigComponent', () => {
  let component: EditGroupConfigComponent;
  let fixture: ComponentFixture<EditGroupConfigComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditGroupConfigComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditGroupConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
