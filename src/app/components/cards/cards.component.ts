import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss'],
})
export class CardsComponent {
  @Input() cardData: {
    title: string;
    specificData?: {
      active?: number;
      locked?: number;
      roles?: number;
      pending?: number;
    };
    description: string;
    avatar: string;
  };
  @Output() clickedCard = new EventEmitter();

  handleClick() {
    this.clickedCard.emit(this.cardData);
  }
}
