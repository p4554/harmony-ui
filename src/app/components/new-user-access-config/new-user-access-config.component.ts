import { Component } from '@angular/core';
import {AdminApiService} from '../../_services/admin-api.service'
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import {  getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { MessageService } from 'primeng/api';
import { LoaderService } from '@app/_services/loading.service';
@Component({
  selector: 'app-new-user-access-config',
  templateUrl: './new-user-access-config.component.html',
  styleUrls: ['./new-user-access-config.component.scss']
})
export class NewUserAccessConfigComponent {
  group:any;
tenantNameData;
tenant;
moduleData;
modulename;
moduleMapData;
tenantMapData;
groupMapData;
tableData:any;
  groupData;
  NewUserConfigForm: FormGroup;  
 constructor(
  private ref: DynamicDialogRef,
  private AdminApiService: AdminApiService,
  private fb: FormBuilder,
  private toastService: MessageService,
  public loaderService: LoaderService,
 ){}
 async ngOnInit(){
    this.NewUserConfigForm = this.fb.group({
    groupname: ['', Validators.required],
    tenantName: ['', Validators.required],
    module: ['', Validators.required],
    active: ['', Validators.required],
  });
  this.loaderService.showLoader();
  await this.getGroupData();
  await this.getTenantNameData();
  await this.getModuleData();
  setTimeout(() => {
    this.loaderService.hideLoader();
  }, 300);
 }

 async getGroupData(){
   this.group=await lastValueFrom(
    this.AdminApiService.groupName()
   )
   this.groupData = getMappedArray(
    getUniqueValues(this.group,'USRGRP_NM'),
    'label',
    'value'
  );
  this.groupMapData = this.group.map(user => {
    return {
      label: user.USRGRP_NM,
      id: user.USRGRP_ID,
    };
  });
 }
 async getTenantNameData(){
   this.tenant=await lastValueFrom(
    this.AdminApiService.tenantName()
   )
   this.tenantNameData = getMappedArray(
    getUniqueValues(this.tenant,'TNANT_NM'),
    'label',
    'value'
  );
  this.tenantMapData = this.tenant.map(user => {
    return {
      label: user.TNANT_NM,
      id: user.TNANT_ID,
    };
  });
 }
 async getModuleData(){
   this.modulename=await lastValueFrom(
    this.AdminApiService.userModule()
   )
   this.moduleData = getMappedArray(
    getUniqueValues(this.modulename,'LOV_DESC'),
    'label',
    'value'
  );
  this.moduleMapData = this.modulename.map(user => {
    return {
      label: user.LOV_DESC,
      id: user.LOV_SEQ,
    };
  });
 }
 cancelButtonHandler(){
  this.ref.close();
  }
  selectedGroupId:number;
  selectedTenantId:number;
  selectedModuleId:number;
 async onSaveHandler(){
    if(this.NewUserConfigForm.valid){
      let formValue = this.NewUserConfigForm.value;
      const selectedGroup = this.groupMapData.find(user => user.label === formValue.groupname);
      this.selectedGroupId = selectedGroup.id;
      const selectedTenant = this.tenantMapData.find(user => user.label === formValue.tenantName);
      this.selectedTenantId = selectedTenant.id;
      const selectedModule = this.moduleMapData.find(user => user.label === formValue.module);
      this.selectedModuleId = selectedModule.id;
     const postData={
      moduleId: this.selectedModuleId,
      tenantId: this.selectedTenantId,
      userGroupId: this.selectedGroupId,
      activeFlag: formValue.active,
      createdBy: localStorage.getItem('userName'),
      updatedBy: localStorage.getItem('userName')
     }     
      await this.AdminApiService.addUserAccess(postData).subscribe((response)=>{
        console.log(response);
        if(response.status==200){
          this.ref.close('saved');
          this.toastService.add({
            key: 'su',
            closable: false,
            sticky: false,
            severity: '',
            summary: '',
            detail: `New Configuration is Created`,
          });
        }
        else{
          this.ref.close();
          this.toastService.clear();
              this.toastService.add({
                key: 'wrn',
                closable: false,
                sticky: false,
                severity: '',
                summary: '',
                detail: `Connection Error Try Again`,
              });
        }
      })           
       
    }
    else{
      this.NewUserConfigForm.markAllAsTouched();
    } 
 }
}
