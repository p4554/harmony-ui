import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewUserAccessConfigComponent } from './new-user-access-config.component';

describe('NewUserAccessConfigComponent', () => {
  let component: NewUserAccessConfigComponent;
  let fixture: ComponentFixture<NewUserAccessConfigComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewUserAccessConfigComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NewUserAccessConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
