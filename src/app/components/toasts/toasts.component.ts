import { Component, ViewChild } from '@angular/core';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-toasts',
  templateUrl: './toasts.component.html',
  styleUrls: ['./toasts.component.scss']
})
export class ToastsComponent {




constructor(
  private toastService: MessageService,
){

}

 public triggerToast(key,message){
  this.toastService.clear()
  // console.log('key=>',key,'message=>',message)
  this.toastService.add({key:key, closable:false, sticky:false,  severity:'', summary:'', detail:message});
  
  }
}
