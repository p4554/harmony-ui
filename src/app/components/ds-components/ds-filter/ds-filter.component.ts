import {
	ChangeDetectionStrategy,
	ChangeDetectorRef,
	Component,
	EventEmitter,
	Input,
	OnInit,
	Output,
	ViewChild,
} from '@angular/core';

@Component({
	selector: 'app-ds-filter',
	templateUrl: './ds-filter.component.html',
	styleUrls: ['./ds-filter.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DsFilterComponent implements OnInit {
	@Input() props: any;
	@Input() sc_table_filter_active: boolean;
	@Output() checkboxChangedEmitter = new EventEmitter();
	@Output() resetEmitter = new EventEmitter();


	selectedValues: any[] = [];
	updatedProps: any;
	constructor(private _cdr: ChangeDetectorRef) { }
	ngOnInit() {
		this.sc_table_filter_active = false;
		this.updatedProps = this.props.map((item) => ({ ...item, checked: true }));
		this._cdr.detectChanges();
	}

	reset() {
		this.selectedValues = [];
		this.resetEmitter.emit();
	}

	onCheckboxChange() {
		this.checkboxChangedEmitter.emit(this.selectedValues);
	}

	isResetActive() {
		console.log('this.sc_table_filter_active', this.sc_table_filter_active)
		console.log('this.selectedValues.length', this.selectedValues.length)
		return this.sc_table_filter_active || this.selectedValues.length
	}
}
