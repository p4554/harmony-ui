import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DsFilterComponent } from './ds-filter.component';

describe('DsFilterComponent', () => {
  let component: DsFilterComponent;
  let fixture: ComponentFixture<DsFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DsFilterComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DsFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
