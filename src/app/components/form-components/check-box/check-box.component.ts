import { Component, Input, OnInit, Output , EventEmitter} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-check-box',
  templateUrl: './check-box.component.html',
  styleUrls: ['./check-box.component.scss']
})
export class CheckBoxComponent implements OnInit{

    @Input() props:any
    @Output() checkBoxOnchange = new EventEmitter()
   selectedValues: any[];

    ngOnInit(){
    }
    handleChange(){
      this.checkBoxOnchange.emit(this.selectedValues)
    }

    resetSelection() {
    this.selectedValues = [];
  }

}
