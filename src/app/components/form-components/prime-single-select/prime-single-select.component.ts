import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
export interface singleSelect {
	options?: any;
	optionLabel?: string;
	placeholder?: string;
	formControl: any;
	inputLabel?: string;
	width?: string;
}
@Component({
	selector: 'app-prime-single-select',
	templateUrl: './prime-single-select.component.html',
	styleUrls: ['./prime-single-select.component.scss'],
})
export class PrimeSingleSelectComponent implements OnInit {
	@Input() control: any;
	@Input() options: any;

	@Output() changeEvent = new EventEmitter();

	ngOnInit() {
		// debugger;
		// console.log(this.control);
		// console.log(this.options);
	}
	handleChange() {
		this.changeEvent.emit();
	}
}

// this.dropdown ={
//   // Share information about selected data sources to
//   inputLabel: '',
//   placeholder: 'Select and Search...',
//   optionLabel : 'email',
//   formControl : 'formControlName',
//   options: [
//     // { email: 'steny@gmail.com', code: 'st' },
//     // { email: 'yuvan@gmail.com', code: 'yu' },
//     // { email: 'geenesg@gmail.com', code: 'ge' },
//     // { email: 'vinoth@gmail.com', code: 'vi' },
//     // { email: 'test@gmail.com', code: 'tt' },
//   ]
// }
