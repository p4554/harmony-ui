import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimeSingleSelectComponent } from './prime-single-select.component';

describe('PrimeSingleSelectComponent', () => {
  let component: PrimeSingleSelectComponent;
  let fixture: ComponentFixture<PrimeSingleSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrimeSingleSelectComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PrimeSingleSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
