import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimeInputTextboxComponent } from './prime-input-textbox.component';

describe('PrimeInputTextboxComponent', () => {
  let component: PrimeInputTextboxComponent;
  let fixture: ComponentFixture<PrimeInputTextboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrimeInputTextboxComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PrimeInputTextboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
