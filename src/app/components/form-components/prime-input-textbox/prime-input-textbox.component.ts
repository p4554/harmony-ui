import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-prime-input-textbox',
  templateUrl: './prime-input-textbox.component.html',
  styleUrls: ['./prime-input-textbox.component.scss']
})
export class PrimeInputTextboxComponent {
  @Input() props:any

  @Output() changeEmitter = new EventEmitter();

  selectedValues:any

  
  handleChange(){
      this.changeEmitter.emit(this.selectedValues)
  }

  resetSelection() {
		this.selectedValues = '';
	}
}
