import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListboxsComponent } from './listboxs.component';

describe('ListboxsComponent', () => {
  let component: ListboxsComponent;
  let fixture: ComponentFixture<ListboxsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListboxsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListboxsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
