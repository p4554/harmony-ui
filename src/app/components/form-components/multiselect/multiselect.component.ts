import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-multiselect',
  templateUrl: './multiselect.component.html',
  styleUrls: ['./multiselect.component.scss']
})
export class MultiselectComponent {

  @Input() props
  @Output() changeEmitter = new EventEmitter()

//   props Interface =  { 
//     formGroup : "",
//     label : ""
//     formControl : ""
//     formGroup : ""
//     options : ""
//     optionLabel : ""
//     disabled : ""
//     errorMsg :
// }
  handleChange(){
   console.log(this.props.formControl.value)
   this.changeEmitter.emit(this.props.formControl.value)
  }

}
