import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
export interface singleSelect  {
      options ?:any,
      optionLabel?:string,
      placeholder ?: string,
      formControl :any,
      inputLabel?: string,
      width? : string
  }
@Component({
  selector: 'app-single-select',
  templateUrl: './single-select.component.html',
  styleUrls: ['./single-select.component.scss']
})
export class SingleSelectComponent implements OnInit{

  @Input() props :any

  @Output() changeEvent = new EventEmitter()

  ngOnInit(){
  }
  handleChange(){
      this.changeEvent.emit(this.props.formControl)
  }
}


