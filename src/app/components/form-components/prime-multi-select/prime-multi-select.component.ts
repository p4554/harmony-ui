import { Component, EventEmitter, Input, Output , OnChanges} from '@angular/core';

@Component({
	selector: 'app-prime-multi-select',
	templateUrl: './prime-multi-select.component.html',
	styleUrls: ['./prime-multi-select.component.scss'],
})
export class PrimeMultiSelectComponent implements OnChanges {
	@Output() changeEmitter = new EventEmitter();
	@Output() panelHideEmitter = new EventEmitter();
	@Input() options;

	selectedValues: any[] = [];

	ngOnChanges():void{
			if(this.selectedValues.length){
				this.selectedValues = this.selectedValues.filter(record=>{
					return this.options.some(option=> option.value === record)
				})
			}
	}

	handleChange() {
		console.log(this.selectedValues)
		// debugger;
		this.changeEmitter.emit(this.selectedValues);
	}

	panelHideHandler(){
		this.panelHideEmitter.emit()

	}

	resetSelection() {
		this.selectedValues = [];
	}
}
