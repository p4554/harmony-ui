import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimeMultiSelectComponent } from './prime-multi-select.component';

describe('PrimeMultiSelectComponent', () => {
  let component: PrimeMultiSelectComponent;
  let fixture: ComponentFixture<PrimeMultiSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrimeMultiSelectComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PrimeMultiSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
