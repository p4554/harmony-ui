import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewUserZoneConfigComponent } from './new-user-zone-config.component';

describe('NewUserZoneConfigComponent', () => {
  let component: NewUserZoneConfigComponent;
  let fixture: ComponentFixture<NewUserZoneConfigComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewUserZoneConfigComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NewUserZoneConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
