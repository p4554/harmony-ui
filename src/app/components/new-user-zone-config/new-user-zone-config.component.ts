import { Component } from '@angular/core';
import {AdminApiService} from '../../_services/admin-api.service'
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { MessageService } from 'primeng/api';
import { LoaderService } from '@app/_services/loading.service';
@Component({
  selector: 'app-new-user-zone-config',
  templateUrl: './new-user-zone-config.component.html',
  styleUrls: ['./new-user-zone-config.component.scss']
})
export class NewUserZoneConfigComponent {
  NewUserZoneConfigForm: FormGroup;
  
 constructor(
  private ref: DynamicDialogRef,
  private AdminApiService: AdminApiService,
  private fb: FormBuilder,
  private toastService: MessageService,
  public loaderService: LoaderService,
 ){}
 async ngOnInit(){
    this.NewUserZoneConfigForm = this.fb.group({
    groupName: ['', Validators.required],
    dataZone: ['', Validators.required],
    active: ['', Validators.required],
  });
  this.loaderService.showLoader();
  await this.getGroupnameData();
  await this.getZoneDataData();
  setTimeout(() => {
    this.loaderService.hideLoader();
  }, 300);
 }
groupName:any;
group_data:any;
zoneName:any;
mappedGroupNameData:any;
mappedZoneData:any;
 async getGroupnameData(){
   this.group_data=await lastValueFrom(
    this.AdminApiService.groupName()
   )
  this.groupName = getMappedArray(
    getUniqueValues(this.group_data, 'USRGRP_NM'),
    'label',
    'value'
  );
  this.mappedGroupNameData = this.group_data.map(user => {
    return {
      groupName: user.USRGRP_NM,
      groupId: user.USRGRP_ID,
    };
  });
 }
 async getZoneDataData(){
   this.group_data=await lastValueFrom(
    this.AdminApiService.dataZone()
   )
  this.zoneName = getMappedArray(
    getUniqueValues(this.group_data, 'DATA_ZONE_NM'),
    'label',
    'value'
  );
  this.mappedZoneData = this.group_data.map(user => {
    return {
      zone: user.DATA_ZONE_NM,
      zoneId: user.DATA_ZONE_ID,
    };
  });
 }
 cancelButtonHandler(){
  this.ref.close();
  }
  selectedGroupId:number;
  selectedZoneId:number;
 async onSaveHandler(){
    if(this.NewUserZoneConfigForm.valid){
      console.log("click");
      let formValue = this.NewUserZoneConfigForm.value;
      const selectedGroup = this.mappedGroupNameData.find(user => user.groupName === formValue.groupName);
         this.selectedGroupId = selectedGroup.groupId;
         const selectedZone = this.mappedZoneData.find(user => user.zone === formValue.dataZone);
         this.selectedZoneId = selectedZone.zoneId; 
      const postData={
        layerId: this.selectedZoneId,
        userGroupId: this.selectedGroupId,
        activeFlag: formValue.active,
        createdBy: localStorage.getItem('userName'),
        updatedBy: localStorage.getItem('userName')
     }
      await this.AdminApiService.addUserZone(postData).subscribe((response)=>{
        console.log(response);
        if(response.status==200){
          this.ref.close('saved');
          this.toastService.add({
            key: 'su',
            closable: false,
            sticky: false,
            severity: '',
            summary: '',
            detail: `New Configuration Created`,
          });
        }
        else{
          this.ref.close();
          this.toastService.clear();
              this.toastService.add({
                key: 'wrn',
                closable: false,
                sticky: false,
                severity: '',
                summary: '',
                detail: `Connection Error Try Again`,
              });
        }
      }) 
    }
    else{
      this.NewUserZoneConfigForm.markAllAsTouched();
    } 
 }
}
