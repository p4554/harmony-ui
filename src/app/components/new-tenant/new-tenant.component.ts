import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import { QueryService } from '@app/_services/query.service';
import { FormBuilder, FormGroup, Validators, NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { format } from 'date-fns';
import { MessageService } from 'primeng/api';
import { LoaderService } from '@app/_services/loading.service';
import { CUSTOM_CHAR_REGEX } from '@app/util/helper/patterns';
import { patternValidator } from '@app/util/validators/pattern-validator';
import { addDuplicateRecordCheckerAdmin, filterIncludedValuesFromArray, getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
import { AdminApiService } from '@app/_services/admin-api.service';
@Component({
  selector: 'app-new-tenant',
  templateUrl: './new-tenant.component.html',
  styleUrls: ['./new-tenant.component.scss'],
})

export class NewTenantComponent {
  region = []
  country = []
  language = [];
  region_data;
  country_data: any
  language_data: any
  selectedFile: boolean = false;
  newTenantForm: FormGroup;

  tableData: any;
  imageFile: any;

  constructor(
    private ref: DynamicDialogRef,
    private queryService: QueryService,
    private fb: FormBuilder,
    private toastService: MessageService,
    public loaderService: LoaderService,
    private config: DynamicDialogConfig,
    private adminApiService: AdminApiService
  ) { }

  async ngOnInit() {
    this.newTenantForm = this.fb.group({
      tenantName: ['', [Validators.required, Validators.pattern(CUSTOM_CHAR_REGEX)]],
      tenantRegion: ['', Validators.required],
      tenantCountry: ['', Validators.required],
      tenantProject: ['', [Validators.required, Validators.pattern(CUSTOM_CHAR_REGEX)]],
      tenantProductId: ['', [Validators.required, Validators.pattern(CUSTOM_CHAR_REGEX)]],
      tenantLanguage: ['', Validators.required],
      tenantImage: [null, Validators.required]
    });
    this.loaderService.showLoader();
    this.tableData = this.config.data.tableData;
    await Promise.all([
      this.getRegionData(),
      this.getCountryData(),
      this.getLanguageData(),
    ]);

    setTimeout(() => {
      this.loaderService.hideLoader();
    }, 300);
  }



  async getRegionData() {
    this.region_data = await lastValueFrom(
      this.adminApiService.region()
    )
    for (let i = 0; i < this.region_data.length; i++) {
      this.region[i] = this.region_data[i].LOV_DESC
    }

  }
  async getCountryData() {
    this.country_data = await lastValueFrom(
      this.adminApiService.country()
    )
    for (let i = 0; i < this.country_data.length; i++) {
      this.country[i] = this.country_data[i].LOV_DESC
    }

  }
  async getLanguageData() {
    this.language_data = await lastValueFrom(
      this.adminApiService.language()
    )

    for (let i = 0; i < this.language_data.length; i++) {
      this.language[i] = this.language_data[i].LOV_DESC
    }
  }


  cancelButtonHandler() {

    this.ref.close();
  }
  onFileSelected(e) {
    console.log(e)
    this.imageFile = e.target.files[0];
    // this.newTenantForm.get('tenantImage').setValue(this.imageFile)
  }


  async onSaveHandler() {
    const formData = new FormData();

    formData.append('tenantImage', this.newTenantForm.get('tenantImage').value);


    // const result = await lastValueFrom(this.commonDataService.UploadTenantImage(formData));

    if (this.newTenantForm.valid) {
      let formValue = this.newTenantForm.value;
      const formProperty = ['tenantName', 'tenantCountry', 'tenantProductId']
      const tableProperty = ['TNANT_NM', 'CNTRY_NM', 'PROD_ID']
      const duplicateFound = addDuplicateRecordCheckerAdmin(formValue, formProperty, this.tableData, tableProperty)
      if (duplicateFound) {
        this.toastService.add({ key: 'wrn', closable: false, sticky: false, severity: '', summary: '', detail: 'Tenant is Already Exists.Try Another', });
      }
      else {
        formData.append('tenant_name', formValue.tenantName);
        formData.append('tenant_region', formValue.tenantRegion);
        formData.append('tenant_country', formValue.tenantCountry);
        formData.append('tenant_project', formValue.tenantProject);
        formData.append('tenant_productId', formValue.tenantProductId);
        formData.append('tenant_language', formValue.tenantLanguage);
        formData.append('user_name', localStorage.getItem('userName'));

        try {
          const response = await lastValueFrom(this.adminApiService.createTenant(formData));

          this.toastService.clear();
          console.log(response);

          if (response.status == 200) {
            this.ref.close('saved');
            this.loaderService.showLoader();
            this.toastService.add({
              key: 'su',
              closable: false,
              sticky: false,
              severity: '',
              summary: '',
              detail: `New Tenant ${formValue.tenantName} created`,
            });
          }
          else {
            this.ref.close();
            this.toastService.clear();
            this.toastService.add({
              key: 'wrn',
              closable: false,
              sticky: false,
              severity: '',
              summary: '',
              detail: `Connection Error Try Again`,
            });
          }
        } catch (error) {
          console.log(error);
        }
      }
    }
    else {
      this.newTenantForm.markAllAsTouched();


      this.toastService.add({
        key: 'wrn',
        closable: false,
        sticky: false,
        severity: '',
        summary: '',
        detail: `File couldn't upload. Try again`,
      });


    }
  }

}
