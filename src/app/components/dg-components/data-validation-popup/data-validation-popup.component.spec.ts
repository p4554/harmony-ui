import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataValidationPopupComponent } from './data-validation-popup.component';

describe('DataValidationPopupComponent', () => {
  let component: DataValidationPopupComponent;
  let fixture: ComponentFixture<DataValidationPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataValidationPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DataValidationPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
