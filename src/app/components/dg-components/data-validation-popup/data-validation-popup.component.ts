import { ChangeDetectorRef, Component , EventEmitter, Input, OnInit, Output , ViewChild} from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { CUSTOM_CHAR_REGEX } from '@app/util/helper/patterns';
import { patternValidator } from '@app/util/validators/pattern-validator';
import { MessageService } from 'primeng/api';
import { lastValueFrom, map } from 'rxjs';
import { filterIncludedValuesFromArray } from '@app/util/helper/arrayHandlers';
import { PrimeMultiSelectComponent } from '@app/components/form-components/prime-multi-select/prime-multi-select.component';
import { DataQualityApiService } from '@app/_services/dataQuality-api.service';

@Component({
  selector: 'app-data-validation-popup',
  templateUrl: './data-validation-popup.component.html',
  styleUrls: ['./data-validation-popup.component.scss'],
  providers: [MessageService]
})
export class DataValidationPopupComponent implements OnInit{
  @Input() popupModelData: any;
  @Output() submitResponse = new EventEmitter();
  @Output() cancelResponse = new EventEmitter();
  @ViewChild('filter') attributeFilter:PrimeMultiSelectComponent
  dataProviderOptions: any;
  dataZoneOptions:any;
  functionOptions:any;
  dqSeverityOption: any;
  statusOption: any;
  datasetNameOption: any;
  metricNameOption: any;
  thresholdValueOption: any;
  filterClauseOption: any;
  groupByOption: any;
  attributeOption_orig:any[] = []
  attributeOption_copy:any[] = []
  isSubmitted:boolean = false
  formFieldApiStatus = {
    dataZone : false,
    dataProvider : false,
    datasetName : false,
    attribute : false,
    dateFormat : false,
    function : false,
    dqSeverity : false,
    status : false,
    ruleType:false
  }
  formGroup : FormGroup
  formProps: any
  attributeFromProps :any
  ruleTypeOptions: any;
  ruleType:string = '' 
  dateFormatOption: any;
  tablePageCount:number = 0;
  constructor( 
    private fb:FormBuilder,
    private DataQualityApiService: DataQualityApiService,
    private toastService : MessageService,
    private cdr: ChangeDetectorRef
    ){}
 
 async ngOnInit(){
      // console.log(this.popupModelData)
    this.formGroup = this.fb.group({
      dataZone : ['', Validators.required],
      dataProvider : ['', Validators.required],
      datasetName : ['' , Validators.required],
      attribute :  this.fb.array([])
    })
    this.formPropsValueSetter()
    this.dataZoneOptionGetter()
    this.ruleTypeOptionGetter()
    this.dqSeverityOptionGetter()
    this.statusOptionGetter()
   
  }
  async dataZoneOptionGetter(){
  
    this.formFieldApiStatus.dataZone = true
    this.dataZoneOptions = await lastValueFrom(this.DataQualityApiService.dataZone().pipe(
      map((array)=>{
        return array.map((item)=>({
          label:item.DATA_ZONE_NM,
          value : item.DATA_ZONE_ID
        }))
      })
    ))
    this.formProps.dataZoneProps.options = this.dataZoneOptions.sort((a,b)=> a.label.localeCompare(b.label))
  
    this.formFieldApiStatus.dataZone = false
  }
  async dataProviderOptionsGetter(zoneId){
    this.formFieldApiStatus.dataProvider = true
    const postData={
      zoneId:zoneId[0]
     }
    this.dataProviderOptions = await lastValueFrom(this.DataQualityApiService.dataProvider(postData).pipe(
      map((array)=>{
        return array.map((item)=>{
          if (item.DATA_PROVIDER !== null && item.DATA_PROVIDER !== '') {
            return {
              label:item.DATA_PROVIDER,
              value : item.DATA_PROVIDER
            };
          } else {
            return null;
          }
        }).filter(item => item !== null && item !==''&& item !==' ');
      })
    ))
    this.formProps.dataProviderProps.options = this.dataProviderOptions.sort((a,b)=> a.label.localeCompare(b.label))
    this.formFieldApiStatus.dataProvider = false
    
  }
  async dataSetNameOptionGetter(param){
    // console.log(param)
    this.formFieldApiStatus.datasetName = true

    const data={
      providerName:param[0],
      zoneId:param[1]
      }
    this.datasetNameOption = await lastValueFrom(this.DataQualityApiService.dataSetName(data).pipe(
      map((array)=>{
        return array.map((item)=>({
          label:item.ENT_NM,
          value : item.ENT_ID
        }))
      })
    ))
    this.formProps.datasetNameProps.options = this.datasetNameOption.sort((a,b)=> a.label.localeCompare(b.label))
    // console.log(this.datasetNameOption)
    this.formFieldApiStatus.datasetName = false
    
  }
  async attributeOptionsGetter(entId){
    this.formFieldApiStatus.attribute = true
    const data={
      entId:entId[0]
    }
    this.attributeOption_orig = await lastValueFrom(this.DataQualityApiService.dataSetAttribute(data).pipe(
      map((array)=>{
        return array.map((item,i:number)=>({
          label:item.COL_NM,
          value : item.COL_ID
        }))
      })
    ))
    // this.formProps.dataProviderProps.options = this.attributeOption
    this.attributeOption_orig = this.attributeOption_orig.sort((a,b)=> a.label.localeCompare(b.label)).map((item,i:number)=>({
      label:item.label,
      value : item.value,
      index : i
    }))
    this.attributeOption_copy =  this.attributeOption_orig
      console.log(this.attributeOption_copy)
    this.formFieldApiStatus.attribute = false
    this.attributeFromProps = this.dynamicAttributeFormArraySetter(this.attributeOption_copy)
  }
  
  async ruleTypeOptionGetter(){
  
    this.formFieldApiStatus.ruleType = true
    this.ruleTypeOptions = await lastValueFrom(this.DataQualityApiService.ruleType().pipe(
      map((array)=>{
        return array.map((item)=>({
          label:item.VISUAL_RULE_DESC,
          value : item.MSTR_RULE_ID
        }))
      })
    ))
    this.formFieldApiStatus.ruleType = false
    this.ruleTypeOptions.sort((a,b)=> a.label.localeCompare(b.label))
  }
  async dqSeverityOptionGetter(){
    this.formFieldApiStatus.dqSeverity = true
    this.dqSeverityOption = await lastValueFrom(this.DataQualityApiService.DQSeverity().pipe(
      map((array)=>{
        return array.map((item)=>({
          label:item.ACTION_DESC,
          value : item.LOV_CD
        }))
      })
    ))
    this.formFieldApiStatus.dqSeverity = false
  }
  async statusOptionGetter(){
    this.formFieldApiStatus.status = true
    this.statusOption = await lastValueFrom(this.DataQualityApiService.status().pipe(
      map((array)=>{
        return array.map((item)=>({
          label:item.ACTION_DESC,
          value : item.LOV_CD
        }))
      })
    ))
    this.formFieldApiStatus.status = false
  }
  async dateFormatOptionsGetter(){
    this.formFieldApiStatus.dateFormat = true
	
    this.dateFormatOption = await lastValueFrom(this.DataQualityApiService.dateFormat().pipe(
      map((array)=>{
        return array.map((item)=>({
          label:item.LOV_CD,
          value : item.LOV_CD
        }))
      })
    ))
    this.dateFormatOption.sort((a,b)=> a.label.localeCompare(b.label))
    this.formFieldApiStatus.dateFormat = false
  }
  onchangeHandler(control , type){
    if(type === 'dataZone'){
      // dataZone onchange 
      let zoneId = control.value .value
      this.formGroup.patchValue({
        dataProvider : '',
        datasetName : '',
      })
      this.attributeOption_copy =[]
      this.attributeFormArray.clear()
      this.dataProviderOptionsGetter([zoneId])
      
        this.attributeFilter.resetSelection()
        this.cdr.detectChanges();
      
    }else if(type === 'dataProvider'){
      // dataProvider onchange
      this.dataSetNameOptionGetter([control.value.value,this.formGroup.controls['dataZone'].value.value])
      this.formGroup.patchValue({
        datasetName : '',
      })
      this.attributeOption_copy = []
      this.attributeFormArray.clear()
      this.attributeFilter.resetSelection()
      this.cdr.detectChanges();
    } else if(type === 'dataSetName'){
      // datasetName onchange
      let entId = control.value.value
      this.attributeOptionsGetter([entId])
      this.dateFormatOptionsGetter()
      this.attributeFilter.resetSelection()
      this.cdr.detectChanges();
    }
  }
  formPropsValueSetter(){
    this.formProps = {
      dataZoneProps: {
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['dataZone'],
        options: this.dataZoneOptions,
        errorMsg: 'This field is required.',
        isSubmitted: false,
      },
      dataProviderProps :{
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['dataProvider'],
        options: this.dataProviderOptions,
        errorMsg: 'This field is required.',
        isSubmitted: false,
      },
      datasetNameProps: {
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['datasetName'],
        options: this.datasetNameOption,
        errorMsg: 'This field is required.',
        isSubmitted: false,
      },
      dqSeverityProps: {
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['dqSeverity'],
        options: this.dqSeverityOption,
        errorMsg: 'This field is required.',
        isSubmitted: false,
      },
      statusProps :{
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['status'],
        options: this.statusOption,
        errorMsg: 'This field is required.',
        isSubmitted: false,
      }
    }
  }
  get attributeFormArray(): FormArray {
    return this.formGroup.get('attribute') as FormArray;
  }
  dynamicAttributeFormArraySetter(attributeArr){
    attributeArr.forEach(element=>{
      let obj = this.fb.group({
        colId:[element.value],
        attribute : [element.label],
        ruleType : [''],
        value_1 : [''],
        value_2 : [''],
        dqSeverity : [''],
        status : [''],
      });
      this.attributeFormArray.push(obj)
      // console.log(this.attributeFormArray)
    })
    // console.log(this.formGroup)

  }

// checking formArray having atLest one formGroup value 
formArrayValueChecker():boolean {
  if(this.attributeFormArray.controls.length){
    for(let formGroup of this.attributeFormArray.controls){
      if(formGroup.get('ruleType').value){
        return true
      }
    }
    return false
  }
  return false
}

  submitHandler() {
    if(this.formGroup.valid && this.formArrayValueChecker()){
      this.submitResponse.emit(this.formGroup.value);

    }else{
      // form submit status change
      Object.keys(this.formProps).forEach(property=>{
        this.formProps[property].isSubmitted = true
      })
      this.isSubmitted = true
      if(this.attributeFormArray.controls.length && !this.formArrayValueChecker()){
        this.toastService.add({key:'wrn', closable:false, sticky:false,  severity:'', summary:'', detail:'please Provide Atleast One Attribute Values'});
        this.isSubmitted = false
      }
      }
  }
rejectHandler() {
    this.popupModelData.addDisplayModal = false
    this.cancelResponse.emit();
  }
  ruleTypeChangeHandler(attribute,control){
    console.log('formGroup=>',attribute.label,'control=>',control)
  }
  value_1ChangeHandler(formGroup,control){
    console.log('formGroup=>',formGroup,'control=>',control)
  }
  statusChangeHandler(formGroup,control){
    console.log('formGroup=>',formGroup,'control=>',control)
  }
  dqSeverityChangeHandler(formGroup,control){
    console.log('formGroup=>',formGroup,'control=>',control)
  }
  attributeChangeHandler(formGroup, field?){
    if(field && field === 'ruleType'){
      formGroup.controls['value_1'].reset()
      formGroup.controls['value_2'].reset()
    }
    const ruleType = formGroup.get('ruleType')
    const value_1 = formGroup.get('value_1')
    const dqSeverity = formGroup.get('dqSeverity')
    const status = formGroup.get('status')
    // setting validation to the specific arrayControl
    ruleType.setValidators([Validators.required])
    value_1.setValidators([Validators.required,patternValidator(CUSTOM_CHAR_REGEX)])
    dqSeverity.setValidators([Validators.required])
    status.setValidators([Validators.required])

// calling the validity of the formArray control
    ruleType.updateValueAndValidity()
    value_1.updateValueAndValidity()
    dqSeverity.updateValueAndValidity()
    status.updateValueAndValidity()

    if(formGroup.controls['ruleType'].value.label === 'RANGE CHECK'){
      const value_2 = formGroup.get('value_2')
      value_2.setValidators([Validators.required,patternValidator(CUSTOM_CHAR_REGEX)])
      value_2.updateValueAndValidity()
    }
  //  console.log("original =>",this.formGroup)   
  }
  filteredArr:string[] = []
  multiselectOnchange(filteredArr,column){
    console.log('filteredArr=>',filteredArr,'column=>',column)
    this.attributeOption_copy = filteredArr.length
      ? filterIncludedValuesFromArray(this.attributeOption_orig, filteredArr, column)
      : this.attributeOption_orig;
      this.tablePageCount = 0
  }
}
