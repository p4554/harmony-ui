import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlSumPopupComponent } from './control-sum-popup.component';

describe('ControlSumPopupComponent', () => {
  let component: ControlSumPopupComponent;
  let fixture: ComponentFixture<ControlSumPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ControlSumPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ControlSumPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
