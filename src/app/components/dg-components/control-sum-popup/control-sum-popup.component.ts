import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { groupByValueToArrayConverter, replaceParamsinJSON } from '@app/util/helper/harmonyUtils';
import { CUSTOM_CHAR_REGEX } from '@app/util/helper/patterns';
import { patternValidator } from '@app/util/validators/pattern-validator';
import { lastValueFrom, map } from 'rxjs';
import { MessageService } from 'primeng/api';
import { DataQualityApiService } from '@app/_services/dataQuality-api.service';

@Component({
  selector: 'app-control-sum-popup',
  templateUrl: './control-sum-popup.component.html',
  styleUrls: ['./control-sum-popup.component.scss']
})
export class ControlSumPopupComponent implements OnInit{
  @Input() popupModelData: any;
  @Output() submitResponse = new EventEmitter()
  T_dataZoneOptions: any []= [];
  dataSetNameOptions:  any []= [];
  groupByOptions: any []= [];
  dqSeverityOptions: any;
  statusOptions: any;
  formGroup: FormGroup
  formProps:any
  dataProviderOptions: any;
  metricNameOptions: any;
  formFieldApiStatus = {
        dataZone : false,
        T_dataProvider : false,
        T_dataSetName : false,
        T_metricName : false,
        T_groupBy : false,
        S_dataProvider : false,
        S_dataSetName : false,
        S_metricName : false,
        S_groupBy : false,
        dqSeverity : false,
        status : false,
}
  constructor(
    private fb:FormBuilder,
    private DataQualityApiService: DataQualityApiService,
    private toastService: MessageService,
  ){}

    ngOnInit(): void {
      this.formControlSetter()
      this.formPropsValueSetter()
      this.dataZoneOptionGetter()
      this.dqSeverityOptionGetter()
      this.statusOptionGetter()
      if(this.popupModelData.editValue){
        let currObj = this.popupModelData.editValue
        // console.log(currObj)
        let T_zoneId = [currObj.TARGET_DATA_LAYER_ID]
        let T_ent_Id = [currObj.TARGET_ENT_ID]
        let T_dataSetParam = [currObj.TARGET_DATA_PROVIDER,currObj.TARGET_DATA_LAYER_ID]
        let S_zoneId = [currObj.SOURCE_DATA_LAYER_ID]
        let S_ent_Id = [currObj.SOURCE_ENT_ID]
        let S_dataSetParam = [currObj.SOURCE_DATA_PROVIDER,currObj.SOURCE_DATA_LAYER_ID]
        this.dataProviderOptionGetter([...T_zoneId] , "target")
        this.dataSetNameOptionGetter([...T_dataSetParam] , "target")
        this.groupByNameOptionGetter([...T_ent_Id] , "target")
        this.metricNameOptionGetter([...T_ent_Id] , "target")
        this.dataProviderOptionGetter([...S_zoneId] , "source")
        this.dataSetNameOptionGetter([...S_dataSetParam] , "source")
        this.groupByNameOptionGetter([...S_ent_Id] , "source")
        this.metricNameOptionGetter([...S_ent_Id] , "source")
        const S_groupByValue = groupByValueToArrayConverter(currObj.SOURCE_GROUP_BY,currObj.SOURCE_GROUP_BY_ID)
        const T_groupByValue = groupByValueToArrayConverter(currObj.TARGET_GROUP_BY,currObj.TARGET_GROUP_BY_ID)
        // console.log(T_groupByValue)
        this.formGroup.patchValue({
          T_dataZone :  {label:currObj.TARGET_DATA_ZONE, value :currObj.TARGET_DATA_LAYER_ID},
          T_dataProvider : {label:currObj.TARGET_DATA_PROVIDER, value :currObj.TARGET_DATA_PROVIDER},
          T_dataSetName :  {label:currObj.TARGET_DATASET_NAME, value :currObj.TARGET_ENT_ID},
          T_metricName :  {label:currObj.TARGET_METRIC_NAME, value :currObj.TARGET_COL_ID},
          T_groupBy : T_groupByValue,
          T_filterClause :currObj.TARGET_FILTER,
          S_dataZone : {label:currObj.SOURCE_DATA_ZONE, value :currObj.SOURCE_DATA_LAYER_ID},
          S_dataProvider : {label:currObj.SOURCE_DATA_PROVIDER, value :currObj.SOURCE_DATA_PROVIDER},
          S_dataSetName : {label:currObj.SOURCE_DATASET_NAME, value :currObj.SOURCE_ENT_ID},
          S_metricName : {label:currObj.SOURCE_METRIC_NAME, value :currObj.SOURCE_COL_ID},
          S_groupBy : S_groupByValue,
          S_filterClause : currObj.SOURCE_FILTER,
          dqSeverity : {label:currObj.RULE_SEVERITY, value :currObj.RULE_SEVERITY},
          status : {label:currObj.STATUS, value :currObj.STATUS}
         })
      }
    }
    async dataZoneOptionGetter (){
      this.formFieldApiStatus.dataZone = true
      this.T_dataZoneOptions = await lastValueFrom(this.DataQualityApiService.dataZone().pipe(
        map((array)=>{
          return array.map((item)=>({
            label:item.DATA_ZONE_NM,
            value : item.DATA_ZONE_ID
          }))
        })
      ))
      this.formProps.T_dataZoneProps.options = this.T_dataZoneOptions.sort((a,b)=>a.label.localeCompare(b.label))
      this.formProps.S_dataZoneProps.options = this.T_dataZoneOptions.sort((a,b)=>a.label.localeCompare(b.label))
      this.formFieldApiStatus.dataZone = false
    }
    async dataProviderOptionGetter (zoneId , type){
      if(type === "target"){
        this.formFieldApiStatus.T_dataProvider = true
      }else {
        this.formFieldApiStatus.S_dataProvider = true
      }
      const postData={
        zoneId:zoneId[0]
       }
      this.dataProviderOptions = await lastValueFrom(this.DataQualityApiService.dataProvider(postData).pipe(
        map((array)=>{
          return array.map((item)=>{
            if (item.DATA_PROVIDER !== null && item.DATA_PROVIDER !== '') {
              return {
                label:item.DATA_PROVIDER,
                value : item.DATA_PROVIDER
              };
            } else {
              return null;
            }
          }).filter(item => item !== null && item !==''&& item !==' '); 
        })
      ))
    
      if(type === "target"){
        this.formProps.T_dataProviderProps.options = this.dataProviderOptions.sort((a,b)=>a.label.localeCompare(b.label))
        this.formFieldApiStatus.T_dataProvider = false
       
      }else {
        this.formProps.S_dataProviderProps.options = this.dataProviderOptions.sort((a,b)=>a.label.localeCompare(b.label))
        this.formFieldApiStatus.S_dataProvider = false
      }
    }
    async dataSetNameOptionGetter (dataSetParam , type){
      if(type === "target"){
        this.formFieldApiStatus.T_dataSetName = true
      }else {
        this.formFieldApiStatus.S_dataSetName = true
      }
      const data={
        providerName:dataSetParam[0],
        zoneId:dataSetParam[1]
        }
      this.dataSetNameOptions = await lastValueFrom(this.DataQualityApiService.dataSetName(data).pipe(
        map((array)=>{
          return array.map((item)=>({
            label:item.ENT_NM,
            value : item.ENT_ID
          }))
        })
      ))
    
      if(type === "target"){
        this.formProps.T_dataSetNameProps.options = this.dataSetNameOptions.sort((a,b)=>a.label.localeCompare(b.label))
        this.formFieldApiStatus.T_dataSetName = false
      }else {
        this.formProps.S_dataSetNameProps.options = this.dataSetNameOptions.sort((a,b)=>a.label.localeCompare(b.label))
        this.formFieldApiStatus.S_dataSetName = false
      }
    }
    async metricNameOptionGetter (entId , type){
      if(type === "target"){
        this.formFieldApiStatus.T_metricName = true
      }else {
        this.formFieldApiStatus.S_metricName = true
      }
      const postData={
        entId: entId[0]
     }
      this.metricNameOptions = await lastValueFrom(this.DataQualityApiService.maticName(postData).pipe(
        map((array)=>{
          return array.map((item)=>({
            label:item.COL_NM,
            value : item.COL_ID
          }))
        })
      ))
      if(type === "target"){
        this.formProps.T_metricNameProps.options = this.metricNameOptions.sort((a,b)=>a.label.localeCompare(b.label))
        this.formFieldApiStatus.T_metricName = false
      }else {
        this.formProps.S_metricNameProps.options = this.metricNameOptions.sort((a,b)=>a.label.localeCompare(b.label))
        this.formFieldApiStatus.S_metricName = false
      }
    }
    async groupByNameOptionGetter (entId , type){
      if(type === "target"){
        this.formFieldApiStatus.T_groupBy = true
      }else {
        this.formFieldApiStatus.S_groupBy = true
      }
      const postData={
        entId: entId[0]
     }
      this.groupByOptions = await lastValueFrom(this.DataQualityApiService.groupBy(postData).pipe(
        map((array)=>{
          return array.map((item)=>({
            label:item.COL_NM,
            value : item.COL_ID
          }))
        })
      ))
      if(type === "target"){
        this.formProps.T_groupByProps.options = this.groupByOptions.sort((a,b)=>a.label.localeCompare(b.label))
        this.formFieldApiStatus.T_groupBy = false
      }else {
        this.formProps.S_groupByProps.options = this.groupByOptions.sort((a,b)=>a.label.localeCompare(b.label))
        this.formFieldApiStatus.S_groupBy = false
      }
     
    }
    async dqSeverityOptionGetter(){
      this.formFieldApiStatus.dqSeverity = true
      this.dqSeverityOptions = await lastValueFrom(this.DataQualityApiService.DQSeverity().pipe(
        map((array)=>{
          return array.map((item)=>({
            label:item.ACTION_DESC,
            value : item.LOV_CD
          }))
        })
      ))
      this.formProps.dqSeverityProps.options = this.dqSeverityOptions
      this.formFieldApiStatus.dqSeverity = false
    }
    async statusOptionGetter(){
      this.formFieldApiStatus.status = true
      this.statusOptions = await lastValueFrom(this.DataQualityApiService.status().pipe(
        map((array)=>{
          return array.map((item)=>({
            label:item.ACTION_DESC,
            value : item.LOV_CD
          }))
        })
      ))
      this.formProps.statusProps.options = this.statusOptions
      this.formFieldApiStatus.status = false
    }
  
    formControlSetter = ()=>{
      this.formGroup = this.fb.group({
        T_dataZone : ['',Validators.required],
        T_dataProvider : ['',Validators.required],
        T_dataSetName : ['',Validators.required],
        T_metricName : ['',Validators.required],
        T_groupBy : ['',Validators.required],
        T_filterClause : ['',[ patternValidator(CUSTOM_CHAR_REGEX)]],
        S_dataZone : ['',Validators.required],
        S_dataProvider : ['',Validators.required],
        S_dataSetName : ['',Validators.required],
        S_metricName : ['',Validators.required],
        S_groupBy : ['',Validators.required],
        S_filterClause : ['',[ patternValidator(CUSTOM_CHAR_REGEX)]],
        dqSeverity : ['',Validators.required],
        status : ['',Validators.required]
      })
      this.formPropsValueSetter()
    }
    formPropsValueSetter(){
      this.formProps = {
        T_dataZoneProps: {
          formGroup: this.formGroup,
          formControl : this.formGroup.controls['T_dataZone'],
          options:  [],
          errorMsg: 'This field is required.',
          isSubmitted: false,
        },
        T_dataProviderProps: {
          formGroup: this.formGroup,
          formControl : this.formGroup.controls['T_dataProvider'],
          options: [],
          errorMsg: 'This field is required.',
          isSubmitted: false,
        },
        T_dataSetNameProps: {
          formGroup: this.formGroup,
          formControl : this.formGroup.controls['T_dataSetName'],
          options: [],
          errorMsg: 'This field is required.',
          isSubmitted: false,
        },
        T_metricNameProps: {
          formGroup: this.formGroup,
          formControl : this.formGroup.controls['T_metricName'],
          options: [],
          errorMsg: 'This field is required.',
          isSubmitted: false,
        },
        T_groupByProps: {
          formGroup: this.formGroup,
          formControl : this.formGroup.controls['T_groupBy'],
          options: [],
          errorMsg: 'This field is required.',
          isSubmitted: false,
        },
        T_filterClauseProps: {
          type : "text",
          inputLabel: '',
          placeholder: '',
          formControl : this.formGroup.controls['T_filterClause'],
          formGroup : this.formGroup,
          isSubmitted : false,
          disable : false,
          errorMsg :"This field is required.",
          patternErrMsg : "This (',\\,`) characters not allowed"
        },
        S_dataZoneProps: {
          formGroup: this.formGroup,
          formControl : this.formGroup.controls['S_dataZone'],
          options: [],
          errorMsg: 'This field is required.',
          isSubmitted: false,
        },
        S_dataProviderProps: {
          formGroup: this.formGroup,
          formControl : this.formGroup.controls['S_dataProvider'],
          options: [],
          errorMsg: 'This field is required.',
          isSubmitted: false,
        },
        S_dataSetNameProps: {
          formGroup: this.formGroup,
          formControl : this.formGroup.controls['S_dataSetName'],
          options: [],
          errorMsg: 'This field is required.',
          isSubmitted: false,
        },
        S_metricNameProps: {
          formGroup: this.formGroup,
          formControl : this.formGroup.controls['S_metricName'],
          options: [],
          errorMsg: 'This field is required.',
          isSubmitted: false,
        },
        S_groupByProps: {
          formGroup: this.formGroup,
          formControl : this.formGroup.controls['S_groupBy'],
          options: [],
          errorMsg: 'This field is required.',
          isSubmitted: false,
        },
        S_filterClauseProps: {
          type : "text",
          inputLabel: '',
          placeholder: '',
          formControl : this.formGroup.controls['S_filterClause'],
          formGroup : this.formGroup,
          isSubmitted : false,
          disable : false,
          errorMsg :"This field is required.",
          patternErrMsg : "This (',\\,`) characters not allowed"

        },
        dqSeverityProps: {
          formGroup: this.formGroup,
          formControl : this.formGroup.controls['dqSeverity'],
          options: [],
          errorMsg: 'This field is required.',
          isSubmitted: false,
        },
        statusProps: {
          formGroup: this.formGroup,
          formControl : this.formGroup.controls['status'],
          options: [],
          errorMsg: 'This field is required.',
          isSubmitted: false,
        },
      }
    }

    groupByValueDUplicateChecker(t_grpBy:any[],S_grpBy:any[]):boolean{       
      return false
    }
  
    submitHandler() {
      if(this.formGroup.valid){
        const formValue = this.formGroup.value
        // console.log(formValue)
        if((formValue.T_dataZone.label !== formValue.S_dataZone.label)
         || (formValue.T_dataProvider.label !== formValue.S_dataProvider.label) 
         ||  (formValue.T_dataSetName.label !== formValue.S_dataSetName.label)
          || (formValue.T_metricName.label !== formValue.S_metricName.label)
          || JSON.stringify(formValue.T_groupBy.sort((a,b)=>a.label.localeCompare(b.label))) !== JSON.stringify(formValue.S_groupBy.sort((a,b)=>a.label.localeCompare(b.label)))
          ){
            this.submitResponse.emit(this.formGroup.value);
        
        }else{
          this.toastService.clear()
          this.toastService.add({key:'wrn', closable:false, sticky:false,  severity:'', summary:'', detail:'Target and Source Values Cant be Same'});
        }
      }else{
        // form submit status change
        Object.keys(this.formProps).forEach(property=>{
          this.formProps[property].isSubmitted = true
        })
      }
    }
    rejectHandler() {
      this.popupModelData.displayModal = false
     
    }
  // // target
  // // dataZone drpdwn onchange
  dataZoneValChangeHandler(control,type){
    this.dataProviderOptionGetter([control.value.value],type)
    if(type === 'target'){
       this.formGroup.patchValue({
      T_dataProvider : '',
      T_dataSetName : '',
      T_metricName : '',
      T_groupBy :'',   
     })
    }else{
      this.formGroup.patchValue({
        S_dataProvider : '',
        S_dataSetName : '',
        S_metricName : '',
        S_groupBy : '',    
       })
    }
  
  }
  // data provider drpdwn change
  dataProviderValChangeHandler(control,type){
    if(type === 'target'){
      this.dataSetNameOptionGetter([control.value.value,this.formGroup.controls['T_dataZone'].value.value],type)
      this.formGroup.patchValue({
        T_dataSetName : '',
        T_metricName : '',
        T_groupBy :'',   
       })
    
    }else {
      this.dataSetNameOptionGetter([control.value.value,this.formGroup.controls['S_dataZone'].value.value],type)
   
      this.formGroup.patchValue({
        S_dataSetName : '',
        S_metricName : '',
        S_groupBy : '',    
       })
    }
   
  }
  // datasetName drpdwn onchange
  datasetNameValChangeHandler(control,type){
    this.metricNameOptionGetter([control.value.value],type)
    this.groupByNameOptionGetter([control.value.value],type)
    if(type === 'target'){
      this.formGroup.patchValue({
     T_metricName : '',
     T_groupBy :'',   
    })
   }else{
     this.formGroup.patchValue({
       S_metricName : '',
       S_groupBy : '',    
      })
   }
  }
  metricNameValChangeHandler(control,type){
  }


}
