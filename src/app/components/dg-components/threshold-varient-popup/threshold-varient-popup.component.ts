import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CUSTOM_CHAR_REGEX } from '@app/util/helper/patterns';
import { patternValidator } from '@app/util/validators/pattern-validator';
import { QueryService } from '@app/_services/query.service';
import { lastValueFrom, map } from 'rxjs';
import { MessageService } from 'primeng/api';
import { DataQualityApiService } from '@app/_services/dataQuality-api.service';
@Component({
  selector: 'app-threshold-varient-popup',
  templateUrl: './threshold-varient-popup.component.html',
  styleUrls: ['./threshold-varient-popup.component.scss'],
  providers: [MessageService]
})
export class ThresholdVarientPopupComponent implements OnInit {
  @Input() popupModelData: any;
  @Output() submitResponse = new EventEmitter();
  @Output() cancelResponse = new EventEmitter();
  dataProviderOptions: any;
  dataZoneOptions: any;
  functionOptions: any;
  dqSeverityOption: any;
  statusOption: any;
  datasetNameOption: any;
  metricNameOption: any;
  thresholdValueOption: any;
  filterClauseOption: any;
  groupByOption: any;

  formFieldApiStatus = {
    dataZone: false,
    dataProvider: false,
    datasetName: false,
    metricName: false,
    groupBy: false,
    function: false,
    dqSeverity: false,
    status: false,
  }
  formGroup: FormGroup
  formProps: any
  // @Output() rejectEmitter = new EventEmitter();

  constructor(
    private fb: FormBuilder,
    private queryService: QueryService,
    private toastService: MessageService,
    private dataQualityApiService: DataQualityApiService
  ) { }

  async ngOnInit() {
    // console.log(this.popupModelData)
    this.formGroup = this.fb.group({
      dataZone: ['', Validators.required],
      dataProvider: ['', Validators.required],
      datasetName: ['', Validators.required],
      metricName: ['', Validators.required],
      thresholdValue: ['', [Validators.required, patternValidator(CUSTOM_CHAR_REGEX)]],
      filterClause: ['', [patternValidator(CUSTOM_CHAR_REGEX)]],
      groupBy: ['', Validators.required],
      function: ['', Validators.required],
      dqSeverity: ['', Validators.required],
      status: ['', Validators.required],
    })
    this.formPropsValueSetter()
    this.dataZoneOptionGetter()
    this.FunctionOptionGetter()
    this.dqSeverityOptionGetter()
    this.statusOptionGetter()

    if (this.popupModelData.editValue) {
      let currObj = this.popupModelData.editValue
      let zoneId = [currObj.DATA_LAYER_ID]
      // dataSetName Parameter
      let dataSetParam = [currObj.DATA_PROVIDER, currObj.DATA_LAYER_ID]
      let ent_Id = [currObj.ENT_ID]
      // console.log(currObj)
      this.dataProviderOptionsGetter([...zoneId])
      this.dataSetNameOptionGetter([...dataSetParam])
      this.metricNameOptionGetter([...ent_Id])
      this.groupByOptionGetter([...ent_Id])
      // groupBy Value process
      let groupByNameArr = currObj.GROUP_BY.split(',')
      let groupByValueArr = currObj.GROUP_BY_ID.split(',')
      // console.log("groupByNameArr=>",groupByNameArr,"groupByValueArr=>",groupByValueArr)
      let groupByValue = []
      if (groupByValueArr.length === groupByNameArr.length) {
        groupByNameArr.forEach((name, i) => {
          groupByValue.push({ label: name, value: groupByValueArr[i] })
        })
      }


      this.formGroup.patchValue({
        dataZone: { label: currObj.DATA_ZONE, value: currObj.DATA_LAYER_ID },
        dataProvider: { label: currObj.DATA_PROVIDER, value: currObj.DATA_PROVIDER },
        datasetName: { label: currObj.DATASET_NAME, value: currObj.ENT_ID },
        metricName: { label: currObj.METRIC_NAME, value: currObj.COL_ID },
        thresholdValue: currObj.THRESHOLD_VALUE,
        filterClause: currObj.FILTER_CLAUSE || '',
        groupBy: groupByValue,
        function: { label: currObj.FUNCTION, value: currObj.MSTR_RULE_ID },
        dqSeverity: { label: currObj.RULE_SEVERITY, value: currObj.RULE_SEVERITY },
        status: { label: currObj.STATUS, value: currObj.STATUS },
      })

    }
  }
  async dataZoneOptionGetter() {

    this.formFieldApiStatus.dataZone = true
    // const query = await lastValueFrom(this.queryService.fetchQuery('threshold_zone'));
    this.dataZoneOptions = await lastValueFrom(this.dataQualityApiService.dataZone().pipe(
      map((array) => {
        return array.map((item) => ({
          label: item.DATA_ZONE_NM,
          value: item.DATA_ZONE_ID
        }))
      })
    ))
    this.formProps.dataZoneProps.options = this.dataZoneOptions.sort((a, b) => a.label.localeCompare(b.label))

    this.formFieldApiStatus.dataZone = false
  }
  async FunctionOptionGetter() {
    this.formFieldApiStatus.function = true
    this.functionOptions = await lastValueFrom(this.dataQualityApiService.thFunction().pipe(
      map((array) => {
        return array.map((item) => ({
          label: item.FUNC,
          value: item.MSTR_RULE_ID
        }))
      })
    ))

    this.formProps.functionProps.options = this.functionOptions.sort((a, b) => a.label.localeCompare(b.label))

    this.formFieldApiStatus.function = false
  }
  async dqSeverityOptionGetter() {

    this.formFieldApiStatus.dqSeverity = true

    this.dqSeverityOption = await lastValueFrom(this.dataQualityApiService.DQSeverity().pipe(
      map((array) => {
        return array.map((item) => ({
          label: item.ACTION_DESC,
          value: item.LOV_CD
        }))
      })
    ))

    this.formProps.dqSeverityProps.options = this.dqSeverityOption.sort((a, b) => a.label.localeCompare(b.label))

    this.formFieldApiStatus.dqSeverity = false
  }

  async statusOptionGetter() {

    this.formFieldApiStatus.status = true

    this.statusOption = await lastValueFrom(this.dataQualityApiService.status().pipe(
      map((array) => {
        return array.map((item) => ({
          label: item.ACTION_DESC,
          value: item.LOV_CD
        }))
      })
    ))

    this.formProps.statusProps.options = this.statusOption

    this.formFieldApiStatus.status = false
  }

  formPropsValueSetter() {
    this.formProps = {
      dataZoneProps: {
        formGroup: this.formGroup,
        formControl: this.formGroup.controls['dataZone'],
        options: this.dataZoneOptions,
        errorMsg: 'This field is required.',
        isSubmitted: false,
      },
      dataProviderProps: {
        formGroup: this.formGroup,
        formControl: this.formGroup.controls['dataProvider'],
        options: this.dataProviderOptions,
        errorMsg: 'This field is required.',
        isSubmitted: false,
      },
      datasetNameProps: {
        formGroup: this.formGroup,
        formControl: this.formGroup.controls['datasetName'],
        options: this.datasetNameOption,
        errorMsg: 'This field is required.',
        isSubmitted: false,
      },
      metricNameProps: {
        formGroup: this.formGroup,
        formControl: this.formGroup.controls['metricName'],
        options: this.metricNameOption,
        errorMsg: 'This field is required.',
        isSubmitted: false,
      },
      thresholdValueProps: {
        type: "text",
        inputLabel: '',
        placeholder: '',
        formControl: this.formGroup.controls['thresholdValue'],
        formGroup: this.formGroup,
        isSubmitted: false,
        disable: false,
        errorMsg: "This field is required.",
        patternErrMsg: "This (',\\,`,;) characters not allowed"
      },
      filterClauseProps: {
        type: "text",
        inputLabel: '',
        placeholder: '',
        formControl: this.formGroup.controls['filterClause'],
        formGroup: this.formGroup,
        isSubmitted: false,
        disable: false,
        errorMsg: "This field is required.",
        patternErrMsg: "This (',\\,`,;) characters not allowed"
      },
      groupByProps: {
        formGroup: this.formGroup,
        formControl: this.formGroup.controls['groupBy'],
        options: this.groupByOption,
        errorMsg: 'This field is required.',
        isSubmitted: false,
      },
      functionProps: {
        formGroup: this.formGroup,
        formControl: this.formGroup.controls['function'],
        options: this.functionOptions,
        errorMsg: 'This field is required.',
        isSubmitted: false,
      },
      dqSeverityProps: {
        formGroup: this.formGroup,
        formControl: this.formGroup.controls['dqSeverity'],
        options: this.dqSeverityOption,
        errorMsg: 'This field is required.',
        isSubmitted: false,
      },
      statusProps: {
        formGroup: this.formGroup,
        formControl: this.formGroup.controls['status'],
        options: this.statusOption,
        errorMsg: 'This field is required.',
        isSubmitted: false,
      },
    }
  }
  // dataZone Onchange  
  dataZoneValChangeHandler(control) {
    // DataProvide DropDown Api Call 
    this.dataProviderOptionsGetter([control.value.value])

    // value reset for dependent form field
    this.formGroup.patchValue({
      dataProvider: '',
      datasetName: '',
      metricName: '',
      groupBy: '',
    })

  }
  async dataProviderOptionsGetter(zoneId) {
    this.formFieldApiStatus.dataProvider = true
     const postData={
      zoneId:zoneId[0]
     }
         this.dataProviderOptions = await lastValueFrom(this.dataQualityApiService.dataProvider(postData).pipe(
      map((array:any) => {
        return array.map((item) => {
          if (item.DATA_PROVIDER !== null && item.DATA_PROVIDER !== '' && item !== ' ') {
            return {
              label: item.DATA_PROVIDER,
              value: item.DATA_PROVIDER
            };
          } else {
            return null;
          }
        }).filter(item => item !== null);
      })
    ))
    this.formProps.dataProviderProps.options = this.dataProviderOptions.sort((a, b) => a.label.localeCompare(b.label))
    this.formFieldApiStatus.dataProvider = false

  }
  // dataProvider Onchange
  dataProviderValChangeHandler(control) {

    this.dataSetNameOptionGetter([control.value.value, this.formGroup.controls['dataZone'].value.value])
    // value reset on dependent form field
    this.formGroup.patchValue({
      datasetName: '',
      metricName: '',
      groupBy: '',
    })
  }
  async dataSetNameOptionGetter(param) {
    this.formFieldApiStatus.datasetName = true
   const data={
    providerName:param[0],
    zoneId:param[1]
    }

    this.datasetNameOption = await lastValueFrom(this.dataQualityApiService.dataSetName(data).pipe(
      map((array:any) => {
        return array.map((item) => ({
          label: item.ENT_NM,
          value: item.ENT_ID
        }))
      })
    ))

    this.formProps.datasetNameProps.options = this.datasetNameOption.sort((a, b) => a.label.localeCompare(b.label))
    // console.log(this.datasetNameOption)
    this.formFieldApiStatus.datasetName = false
  }

  // dataSet Name Onchange
  dataSetNameValChangeHandler(control) {
    this.metricNameOptionGetter([control.value.value])
    this.groupByOptionGetter([control.value.value])
    // value reset on dependent form field
    this.formGroup.patchValue({
      metricName: '',
      groupBy: '',
    })
  }

  // metric option api call 
  async metricNameOptionGetter(entId) {
    this.formFieldApiStatus.metricName = true
 const postData={
    entId: entId[0]
 }

    this.metricNameOption = await lastValueFrom(this.dataQualityApiService.maticName(postData).pipe(
      map((array:any) => {
        return array.map((item) => ({
          label: item.COL_NM,
          value: item.COL_ID
        }))
      })
    ))

    this.formProps.metricNameProps.options = this.metricNameOption.sort((a, b) => a.label.localeCompare(b.label))
    // console.log(this.metricNameOption)

    this.formFieldApiStatus.metricName = false
  }
  async groupByOptionGetter(entId) {
    this.formFieldApiStatus.groupBy = true

    const postData={
      entId: entId[0]
   }
  
    this.groupByOption = await lastValueFrom(this.dataQualityApiService.groupBy(postData).pipe(
      map((array:any) => {
        return array.map((item) => ({
          label: item.COL_NM,
          value: item.COL_ID
        }))
      })
    ))
    this.formProps.groupByProps.options = this.groupByOption.sort((a, b) => a.label.localeCompare(b.label))
    this.formFieldApiStatus.groupBy = false
  }
  submitHandler() {
    if (this.formGroup.valid) {
      this.submitResponse.emit(this.formGroup.value);
    } else {
      // form submit status change
      Object.keys(this.formProps).forEach(property => {
        this.formProps[property].isSubmitted = true
      })
    }
  }
  rejectHandler() {
    this.popupModelData.displayModal = false
    this.cancelResponse.emit();
  }

}
