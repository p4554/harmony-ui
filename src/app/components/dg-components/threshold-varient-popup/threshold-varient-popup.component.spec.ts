import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThresholdVarientPopupComponent } from './threshold-varient-popup.component';

describe('ThresholdVarientPopupComponent', () => {
  let component: ThresholdVarientPopupComponent;
  let fixture: ComponentFixture<ThresholdVarientPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThresholdVarientPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ThresholdVarientPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
