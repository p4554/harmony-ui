import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlTotalPopupComponent } from './control-total-popup.component';

describe('ControlTotalPopupComponent', () => {
  let component: ControlTotalPopupComponent;
  let fixture: ComponentFixture<ControlTotalPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ControlTotalPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ControlTotalPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
