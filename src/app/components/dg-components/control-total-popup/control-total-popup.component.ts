import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { groupByValueToArrayConverter, replaceParamsinJSON } from '@app/util/helper/harmonyUtils';
import { CUSTOM_CHAR_REGEX } from '@app/util/helper/patterns';
import { patternValidator } from '@app/util/validators/pattern-validator';
import { DataQualityApiService } from '@app/_services/dataQuality-api.service';
import { LoaderService } from '@app/_services/loading.service';
import { lastValueFrom, map } from 'rxjs';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-control-total-popup',
  templateUrl: './control-total-popup.component.html',
  styleUrls: ['./control-total-popup.component.scss'],
  providers: [MessageService]
})
export class ControlTotalPopupComponent implements OnInit{
  @Input() popupModelData: any;
  @Output() submitResponse = new EventEmitter()
  T_dataZoneOptions: any []= [];
  dataSetNameOptions:  any []= [];
  groupByOptions: any []= [];
  dqSeverityOptions: any;
  statusOptions: any;
  

  constructor( 
     private fb:FormBuilder,
    private DataQualityApiService: DataQualityApiService,
    public loaderService : LoaderService,
    private toastService: MessageService,
    ){
  }
  formGroup: FormGroup
  formProps:any
  formFieldApiStatus = {
      dataZone : false,
      T_dataSetName : false,
      T_groupBy : false,
      S_dataSetName : false,
      S_groupBy : false,
      dqSeverity : false,
      status : false,
  }
  ngOnInit(){ 
    this.formControlSetter()
    this.dataZoneOptionGetter()
    this.dqSeverityOptionGetter()
    this.statusOptionGetter()
    if(this.popupModelData.editValue){
      let currObj = this.popupModelData.editValue
      let T_zoneId = [currObj.TARGET_DATA_LAYER_ID]
      let T_ent_Id = [currObj.TARGET_ENT_ID]
      let S_zoneId = [currObj.SOURCE_DATA_LAYER_ID]
      let S_ent_Id = [currObj.SOURCE_ENT_ID]
      this.dataSetNameOptionGetter([...T_zoneId] , "target")
      this.groupByNameOptionGetter([...T_ent_Id] , "target")
      this.dataSetNameOptionGetter([...S_zoneId] , "source")
      this.groupByNameOptionGetter([...S_ent_Id] , "source")

      const S_groupByValue = groupByValueToArrayConverter(currObj.SOURCE_GROUP_BY,currObj.SOURCE_GROUP_BY_ID)
      const T_groupByValue = groupByValueToArrayConverter(currObj.TARGET_GROUP_BY,currObj.TARGET_GROUP_BY_ID)
      console.log(T_groupByValue)

      this.formGroup.patchValue({
        T_dataZone :  {label:currObj.TARGET_DATA_ZONE, value :currObj.TARGET_DATA_LAYER_ID},
        T_dataSetName :  {label:currObj.TARGET_DATASET_NAME, value :currObj.TARGET_ENT_ID},
        T_groupBy : T_groupByValue,
        T_filterClause :currObj.TARGET_FILTER,
        S_dataZone : {label:currObj.SOURCE_DATA_ZONE, value :currObj.SOURCE_DATA_LAYER_ID},
        S_dataSetName : {label:currObj.SOURCE_DATASET_NAME, value :currObj.SOURCE_ENT_ID},
        S_groupBy : S_groupByValue,
        S_filterClause : currObj.SOURCE_FILTER,
        dqSeverity : {label:currObj.RULE_SEVERITY, value :currObj.RULE_SEVERITY},
        status : {label:currObj.STATUS, value :currObj.STATUS}
       })
    }
  }
  async dataZoneOptionGetter (){
  
    this.formFieldApiStatus.dataZone = true
    this.T_dataZoneOptions = await lastValueFrom(this.DataQualityApiService.dataZone().pipe(
      map((array)=>{
        return array.map((item)=>({
          label:item.DATA_ZONE_NM,
          value : item.DATA_ZONE_ID
        }))
      })
    ))
    this.formProps.T_dataZoneProps.options = this.T_dataZoneOptions.sort((a,b)=> a.label.localeCompare(b.label))
    this.formProps.S_dataZoneProps.options = this.T_dataZoneOptions.sort((a,b)=> a.label.localeCompare(b.label))
    this.formFieldApiStatus.dataZone = false
  } 
  async dataSetNameOptionGetter (zoneId , type){

    if(type === "target"){
      this.formFieldApiStatus.T_dataSetName = true
    }else {
      this.formFieldApiStatus.S_dataSetName = true
    }
    const data={
      zoneId:zoneId[0]
      }
      console.log(data)
    this.dataSetNameOptions = await lastValueFrom(this.DataQualityApiService.controlDataset(data).pipe(
      map((array)=>{
        return array.map((item)=>({
          label:item.ENT_NM,
          value : item.ENT_ID
        }))
      })
    ))
  
    if(type === "target"){
      this.formProps.T_dataSetNameProps.options = this.dataSetNameOptions.sort((a,b)=> a.label.localeCompare(b.label))
      this.formFieldApiStatus.T_dataSetName = false
    }else {
      this.formProps.S_dataSetNameProps.options = this.dataSetNameOptions.sort((a,b)=> a.label.localeCompare(b.label))
      this.formFieldApiStatus.S_dataSetName = false
    }
  
  }
  async groupByNameOptionGetter (entId , type){
    
    if(type === "target"){
      this.formFieldApiStatus.T_groupBy = true
    }else {
      this.formFieldApiStatus.S_groupBy = true
    }
    const postData={
      entId: entId[0]
   }
    this.groupByOptions = await lastValueFrom(this.DataQualityApiService.groupBy(postData).pipe(
      map((array)=>{
        return array.map((item)=>({
          label:item.COL_NM,
          value : item.COL_ID
        }))
      })
    ))
    if(type === "target"){
      this.formProps.T_groupByProps.options = this.groupByOptions.sort((a,b)=> a.label.localeCompare(b.label))
      this.formFieldApiStatus.T_groupBy = false
    }else {
      this.formProps.S_groupByProps.options = this.groupByOptions.sort((a,b)=> a.label.localeCompare(b.label))
      this.formFieldApiStatus.S_groupBy = false
    }
   
  }
  async dqSeverityOptionGetter(){
    this.formFieldApiStatus.dqSeverity = true
    this.dqSeverityOptions = await lastValueFrom(this.DataQualityApiService.DQSeverity().pipe(
      map((array)=>{
        return array.map((item)=>({
          label:item.ACTION_DESC,
          value : item.LOV_CD
        }))
      })
    ))
    this.formProps.dqSeverityProps.options = this.dqSeverityOptions
    this.formFieldApiStatus.dqSeverity = false
  }
  async statusOptionGetter(){
    this.formFieldApiStatus.status = true
    this.statusOptions = await lastValueFrom(this.DataQualityApiService.status().pipe(
      map((array)=>{
        return array.map((item)=>({
          label:item.ACTION_DESC,
          value : item.LOV_CD
        }))
      })
    ))
    this.formProps.statusProps.options = this.statusOptions
    this.formFieldApiStatus.status = false
  }

  
  formControlSetter = ()=>{
    this.formGroup = this.fb.group({
      T_dataZone : ['',Validators.required],
      T_dataSetName : ['',Validators.required],
      T_groupBy : ['',Validators.required],
      T_filterClause : ['',[ patternValidator(CUSTOM_CHAR_REGEX)]],
      S_dataZone : ['',Validators.required],
      S_dataSetName : ['',Validators.required],
      S_groupBy : ['',Validators.required],
      S_filterClause : ['',[ patternValidator(CUSTOM_CHAR_REGEX)]],
      dqSeverity : ['',Validators.required],
      status : ['',Validators.required]
    })
    this.formPropsValueSetter()
  }
  formPropsValueSetter(){
    this.formProps = {
      T_dataZoneProps: {
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['T_dataZone'],
        options:  [],
        errorMsg: 'This field is required.',
        patternErrMsg : "This (',\\,`) characters not allowed",
        isSubmitted: false,
      },
      T_dataSetNameProps: {
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['T_dataSetName'],
        options: [],
        errorMsg: 'This field is required.',
        isSubmitted: false,
      },
      T_groupByProps: {
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['T_groupBy'],
        options: [],
        errorMsg: 'This field is required.',
        isSubmitted: false,
      },
      T_filterClauseProps: {
        type : "text",
        inputLabel: '',
        placeholder: '',
        formControl : this.formGroup.controls['T_filterClause'],
        formGroup : this.formGroup,
        isSubmitted : false,
        disable : false,
        errorMsg :"This field is required.",
        patternErrMsg : "This (',\\,`) characters not allowed"
      },
      S_dataZoneProps: {
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['S_dataZone'],
        options: [],
        errorMsg: 'This field is required.',
        isSubmitted: false,
      },
      S_dataSetNameProps: {
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['S_dataSetName'],
        options: [],
        errorMsg: 'This field is required.',
        isSubmitted: false,
      },
      S_groupByProps: {
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['S_groupBy'],
        options: [],
        errorMsg: 'This field is required.',
        isSubmitted: false,
      },
      S_filterClauseProps: {
        type : "text",
        inputLabel: '',
        placeholder: '',
        formControl : this.formGroup.controls['S_filterClause'],
        formGroup : this.formGroup,
        isSubmitted : false,
        disable : false,
        errorMsg :"This field is required.",
        patternErrMsg : "This (',\\,`) characters not allowed",
      },
      dqSeverityProps: {
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['dqSeverity'],
        options: [],
        errorMsg: 'This field is required.',
        isSubmitted: false,
      },
      statusProps: {
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['status'],
        options: [],
        errorMsg: 'This field is required.',
        isSubmitted: false,
      },
    }
  }

  submitHandler() {
    if(this.formGroup.valid){
      const formValue = this.formGroup.value
      console.log(formValue)
      if((formValue.T_dataZone.label !== formValue.S_dataZone.label)
       ||  (formValue.T_dataSetName.label !== formValue.S_dataSetName.label)
        || (JSON.stringify(formValue.T_groupBy.sort((a,b)=>a.label.localeCompare(b.label))) !== JSON.stringify(formValue.S_groupBy.sort((a,b)=>a.label.localeCompare(b.label))))
        ){
          this.submitResponse.emit(this.formGroup.value);
          // this.popupModelData.displayModal = false
      }else{
        this.toastService.clear()
        this.toastService.add({key:'wrn', closable:false, sticky:false,  severity:'', summary:'', detail:'TargetDataset and SourceDataset Values Cant be Same'});
      }
      // this.submitResponse.emit(this.formGroup.value);
      // this.popupModelData.displayModal = false

    }else{
      // form submit status change
      Object.keys(this.formProps).forEach(property=>{
        this.formProps[property].isSubmitted = true
      })
    }
  }
  rejectHandler() {
    this.popupModelData.displayModal = false
  }

  T_dataZoneValChangeHandler(control){
  
    this.dataSetNameOptionGetter([control.value.value],"target")

    this.formGroup.patchValue({
      T_dataSetName : '',
      T_groupBy : '',
     })
  }
  S_dataZoneValChangeHandler(control){
    console.log(control)
    this.formGroup.patchValue({
      S_dataSetName : '',
      S_groupBy : '',
     })
    this.dataSetNameOptionGetter([control.value.value],"source")
  }
  T_datasetValChangeHandler(control){
    this.groupByNameOptionGetter([control.value.value],"target")
    this.formGroup.patchValue({
      T_groupBy : '',
     })
  }
  S_datasetValChangeHandler(control){
    this.groupByNameOptionGetter([control.value.value],"source")
    this.formGroup.patchValue({
      S_groupBy : '',
     })
  }
}
