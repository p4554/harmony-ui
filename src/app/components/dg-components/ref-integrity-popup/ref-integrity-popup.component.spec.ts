import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RefIntegrityPopupComponent } from './ref-integrity-popup.component';

describe('RefIntegrityPopupComponent', () => {
  let component: RefIntegrityPopupComponent;
  let fixture: ComponentFixture<RefIntegrityPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RefIntegrityPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RefIntegrityPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
