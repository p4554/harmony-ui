import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { lastValueFrom, map } from 'rxjs';
import { MessageService } from 'primeng/api';
import { DataQualityApiService } from '@app/_services/dataQuality-api.service';

@Component({
  selector: 'app-ref-integrity-popup',
  templateUrl: './ref-integrity-popup.component.html',
  styleUrls: ['./ref-integrity-popup.component.scss'],
  providers:[MessageService]
})
export class RefIntegrityPopupComponent implements OnInit {
  @Input() popupModelData: any;
  @Output() submitResponse = new EventEmitter
  dataZoneOptions:any[] = []
  P_dataProviderOptions: any [] = [];
  P_datasetNameOptions: any [] = [];
  P_dataSetAttributeOptions: any [] = [];
  C_dataZoneOptions: any [] = [];
  C_dataProviderOptions: any [] = [];
  C_datasetNameOptions: any [] = [];
  C_dataSetAttributeOptions: any [] = [];
  statusOptions: any [] = [];
  dqSeverityOptions: any[] = [];

  constructor(
    private fb:FormBuilder,
    private DataQualityApiService: DataQualityApiService,
    private toastService: MessageService,
  ){
    
  }
  formGroup: FormGroup

  formProps:any
  formFieldApiStatus = {
    dataZone : false,
    P_dataProvider : false,
    P_datasetName : false,
    P_dataSetAttribute : false,
    C_dataProvider : false,
    C_datasetName : false,
    C_dataSetAttribute : false,
    dqSeverity : false,
    status : false,
  
  }

  
  ngOnInit(){
    this.formControlSetter()
    this.dataZoneOptionGetter()
    this.dqSeverityOptionGetter()
    this.statusOptionGetter()

    if(this.popupModelData.editValue){
      let currObj = this.popupModelData.editValue
      let P_zoneId = [currObj.PARENT_DATA_LAYER_ID]
      let P_ent_Id = [currObj.PARENT_ENT_ID]
      let P_dataSetParam = [currObj.PARENT_DATA_PROVIDER,currObj.PARENT_DATA_LAYER_ID]
      let C_zoneId = [currObj.CHILD_DATA_LAYER_ID]
      let C_ent_Id = [currObj.CHILD_ENT_ID]
      let C_dataSetParam = [currObj.CHILD_DATA_PROVIDER,currObj.CHILD_DATA_LAYER_ID]
      this.dataProviderOptionsGetter([...P_zoneId] , "parent")
      this.dataSetNameOptionGetter([...P_dataSetParam] , "parent")
      this.dataSetAttributeOptionGetter([...P_ent_Id] , "parent")
      this.dataProviderOptionsGetter([...C_zoneId] , "child")
      this.dataSetNameOptionGetter([...C_dataSetParam] , "child")
      this.dataSetAttributeOptionGetter([...C_ent_Id] , "child")
      // console.log(currObj)

      this.formGroup.patchValue({
        P_dataZone : {label:currObj.PARENT_DATA_ZONE, value :currObj.PARENT_DATA_LAYER_ID},
        P_dataProvider : {label:currObj.PARENT_DATA_PROVIDER, value :currObj.PARENT_DATA_PROVIDER},
        P_datasetName : {label:currObj.PARENT_DATASET_NAME, value :currObj.PARENT_ENT_ID},
        P_dataSetAttribute : {label:currObj.PARENT_DATASET_ATTRIBUTE, value :currObj.PARENT_COL_ID},
        C_dataZone : {label:currObj.CHILD_DATA_ZONE, value :currObj.CHILD_DATA_LAYER_ID},
        C_dataProvider : {label:currObj.CHILD_DATA_PROVIDER, value :currObj.CHILD_DATA_PROVIDER},
        C_datasetName : {label:currObj.CHILD_DATASET_NAME, value :currObj.CHILD_ENT_ID},
        C_dataSetAttribute : {label:currObj.CHILD_DATASET_ATTRIBUTE, value :currObj.CHILD_COL_ID},
        dqSeverity : {label:currObj.RULE_SEVERITY, value :currObj.RULE_SEVERITY},
        status : {label:currObj.STATUS, value :currObj.STATUS},
       })
    }
  }
  formControlSetter = ()=>{
    this.formGroup = this.fb.group({
      P_dataZone : ['', Validators.required],
      P_dataProvider : ['', Validators.required],
      P_datasetName : ['' , Validators.required],
      P_dataSetAttribute : ['', Validators.required],
      C_dataZone : ['', Validators.required],
      C_dataProvider : ['', Validators.required],
      C_datasetName : ['' , Validators.required],
      C_dataSetAttribute : ['', Validators.required],
      dqSeverity : ['', Validators.required],
      status : ['', Validators.required],
    })
    this.formPropsValueSetter()
  }
  async dataZoneOptionGetter(){

    this.formFieldApiStatus.dataZone = true
    this.dataZoneOptions = await lastValueFrom(this.DataQualityApiService.dataZone().pipe(
      map((array)=>{
        return array.map((item)=>({
          label:item.DATA_ZONE_NM,
          value : item.DATA_ZONE_ID
        }))
      })
    ))
    
    this.formProps.P_dataZoneProps.options = this.dataZoneOptions.sort((a,b)=>a.label.localeCompare(b.label))
    this.formProps.C_dataZoneProps.options = this.dataZoneOptions.sort((a,b)=>a.label.localeCompare(b.label))
   
    this.formFieldApiStatus.dataZone = false
  }
  async dqSeverityOptionGetter(){
 
    this.formFieldApiStatus.dqSeverity = true
    this.dqSeverityOptions = await lastValueFrom(this.DataQualityApiService.DQSeverity().pipe(
      map((array)=>{
        return array.map((item)=>({
          label:item.ACTION_DESC,
          value : item.LOV_CD
        }))
      })
    ))
    this.formProps.dqSeverityProps.options = this.dqSeverityOptions
    
    this.formFieldApiStatus.dqSeverity = false
  }
  async statusOptionGetter(){

    this.formFieldApiStatus.status = true
    this.statusOptions = await lastValueFrom(this.DataQualityApiService.status().pipe(
      map((array)=>{
        return array.map((item)=>({
          label:item.ACTION_DESC,
          value : item.LOV_CD
        }))
      })
    ))
    this.formProps.statusProps.options = this.statusOptions
  
    this.formFieldApiStatus.status = false
  }

  async dataProviderOptionsGetter(zoneId , relation){
    if(relation === "parent"){
      this.formFieldApiStatus.P_dataProvider = true
    }else {
      this.formFieldApiStatus.C_dataProvider = true
    }
    const postData={
      zoneId:zoneId[0]
     }
    let  dataProvider = await lastValueFrom(this.DataQualityApiService.dataProvider(postData).pipe(
      map((array)=>{
        return array.map((item)=>{
          if (item.DATA_PROVIDER !== null && item.DATA_PROVIDER !== '') {
            return {
              label:item.DATA_PROVIDER,
              value : item.DATA_PROVIDER
            };
          } else {
            return null;
          }
        }).filter(item => item !== null && item !==''&& item !==' ');
      })
    ))
    // console.log(dataProvider , "relation=>",relation)
      if(relation === "parent"){
        this.formFieldApiStatus.P_dataProvider = false
        console.log(dataProvider)
        this.formProps.P_dataProviderProps.options = dataProvider.sort((a,b)=>a.value.localeCompare(b.value))
      }else {
        this.formFieldApiStatus.C_dataProvider = false
        this.formProps.C_dataProviderProps.options = dataProvider.sort((a,b)=>a.value.localeCompare(b.value))
      }
  }

  async dataSetNameOptionGetter(dataSetParam , relation){
    if(relation === "parent"){
      this.formFieldApiStatus.P_datasetName = true
    }else {
      this.formFieldApiStatus.C_datasetName = true
    }
    const data={
      providerName:dataSetParam[0],
      zoneId:dataSetParam[1]
      }
    const datasetNameOption = await lastValueFrom(this.DataQualityApiService.dataSetName(data).pipe(
      map((array)=>{
        return array.map((item)=>({
          label:item.ENT_NM,
          value : item.ENT_ID
        }))
      })
    ))
    if(relation === "parent"){
      this.formProps.P_datasetNameProps.options = datasetNameOption.sort((a,b)=>a.label.localeCompare(b.label))
      this.formFieldApiStatus.P_datasetName = false
    }else {
      this.formProps.C_datasetNameProps.options = datasetNameOption.sort((a,b)=>a.label.localeCompare(b.label))
      this.formFieldApiStatus.C_datasetName = false
    }

  }
  async dataSetAttributeOptionGetter(entId , relation){

    if(relation === "parent"){
      this.formFieldApiStatus.P_dataSetAttribute = true
    }else {
      this.formFieldApiStatus.C_dataSetAttribute = true
    }
    const data={
      entId:entId[0]
    }
    const datasetAttributeOption = await lastValueFrom(this.DataQualityApiService.dataSetAttribute(data).pipe(
      map((array)=>{
        return array.map((item)=>({
          label:item.COL_NM,
          value : item.COL_ID
        }))
      })
    ))
    
    if(relation === "parent"){
      this.formProps.P_dataSetAttributeProps.options  = [...datasetAttributeOption.sort((a,b)=>a.label.localeCompare(b.label))]
      this.formFieldApiStatus.P_dataSetAttribute = false
    }else {
      this.formProps.C_dataSetAttributeProps.options = [...datasetAttributeOption.sort((a,b)=>a.label.localeCompare(b.label))]
      this.formFieldApiStatus.C_dataSetAttribute = false
    }
    console.log(this.formProps.C_dataSetAttributeProps)
    
  }
  P_dataZoneOnchange(control){
    // console.log(control.value.value)
    this.dataProviderOptionsGetter([control.value.value],"parent")
    this.formGroup.patchValue({
      P_dataProvider : '',
      P_datasetName : '',
      P_dataSetAttribute : '',
     })
  
  }
  P_dataProviderOnchange(control){
   let P_dataSetParam =  [control.value.value,this.formGroup.controls['P_dataZone'].value.value]
    this.dataSetNameOptionGetter(P_dataSetParam,"parent")
    this.formGroup.patchValue({
      P_datasetName : '',
      P_dataSetAttribute : '',
     })

  }
  P_datasetNameOnchange(control){
    // console.log([control.value.value])
    this.dataSetAttributeOptionGetter([control.value.value],"parent")
    this.formGroup.patchValue({
      P_dataSetAttribute : '',
     })
  }
  C_dataZoneOnchange(control){
    this.dataProviderOptionsGetter([control.value.value],"child")
    this.formGroup.patchValue({
      C_dataProvider : '',
      C_datasetName : '',
      C_dataSetAttribute : '',
     })
  }
  C_dataProviderOnchange(control){
    let C_dataSetParam =  [control.value.value,this.formGroup.controls['C_dataZone'].value.value]
    this.dataSetNameOptionGetter(C_dataSetParam,"child")
    this.formGroup.patchValue({
      C_datasetName : '',
      C_dataSetAttribute : '',
     })
  }
  C_datasetNameOnchange(control){
    this.dataSetAttributeOptionGetter([control.value.value],"child")
    this.formGroup.patchValue({
      C_dataSetAttribute : '',
     })
  }
  submitHandler() {

    if(this.formGroup.valid){

      const formValue = this.formGroup.value
      console.log(formValue)
      if((formValue.P_dataZone.label !== formValue.C_dataZone.label)
       || (formValue.P_dataProvider.label !== formValue.C_dataProvider.label) 
       ||  (formValue.P_datasetName.label !== formValue.C_datasetName.label)
        || (formValue.P_dataSetAttribute.label !== formValue.C_dataSetAttribute.label)
        ){
          this.submitResponse.emit(this.formGroup.value);
      }else{
        this.toastService.clear()
        this.toastService.add({key:'wrn', closable:false, sticky:false,  severity:'', summary:'', detail:'Parent and Child Values Cant be Same'});
      }
    }else{
      // form submit status change
      Object.keys(this.formProps).forEach(property=>{
        this.formProps[property].isSubmitted = true
      })
    }
  }
  rejectHandler() {
    this.popupModelData.displayModal = false
   
  }
  formPropsValueSetter(){
    this.formProps = {
      P_dataZoneProps: {
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['P_dataZone'],
        options: [],
        errorMsg: 'This field is required.',
        isSubmitted:false,
      },
      P_dataProviderProps: {
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['P_dataProvider'],
        options: [],
        errorMsg: 'This field is required.',
        isSubmitted:false,
      },
      P_datasetNameProps: {
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['P_datasetName'],
        options: [],
        errorMsg: 'This field is required.',
        isSubmitted:false,
      },
      P_dataSetAttributeProps: {
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['P_dataSetAttribute'],
        options: [],
        errorMsg: 'This field is required.',
        isSubmitted:false,
      },
      C_dataZoneProps: {
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['C_dataZone'],
        options: [],
        errorMsg: 'This field is required.',
        isSubmitted:false,
      },
      C_dataProviderProps: {
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['C_dataProvider'],
        options: [],
        errorMsg: 'This field is required.',
        isSubmitted:false,
      },
      C_datasetNameProps: {
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['C_datasetName'],
        options: [],
        errorMsg: 'This field is required.',
        isSubmitted:false,
      },
      C_dataSetAttributeProps: {
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['C_dataSetAttribute'],
        options: [],
        errorMsg: 'This field is required.',
        isSubmitted:false,
      },
      dqSeverityProps: {
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['dqSeverity'],
        options: [],
        errorMsg: 'This field is required.',
        isSubmitted:false,
      },
      statusProps: {
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['status'],
        options: [],
        errorMsg: 'This field is required.',
        isSubmitted:false,
      },
    }
    }

}

