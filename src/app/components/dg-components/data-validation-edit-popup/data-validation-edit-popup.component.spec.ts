import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataValidationEditPopupComponent } from './data-validation-edit-popup.component';

describe('DataValidationEditPopupComponent', () => {
  let component: DataValidationEditPopupComponent;
  let fixture: ComponentFixture<DataValidationEditPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataValidationEditPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DataValidationEditPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
