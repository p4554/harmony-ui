import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CUSTOM_CHAR_REGEX } from '@app/util/helper/patterns';
import { patternValidator } from '@app/util/validators/pattern-validator';
import { DataQualityApiService } from '@app/_services/dataQuality-api.service';
import { lastValueFrom, map } from 'rxjs';

@Component({
  selector: 'app-data-validation-edit-popup',
  templateUrl: './data-validation-edit-popup.component.html',
  styleUrls: ['./data-validation-edit-popup.component.scss']
})
export class DataValidationEditPopupComponent {
  @Input() popupModelData: any;
  @Output() submitResponse = new EventEmitter();
  @Output() cancelResponse = new EventEmitter();
  dataProviderOptions: any;
  dataZoneOptions:any;
  attributeOption:any;
  dateFormatOption:any []=[];
  dqSeverityOption: any;
  statusOption: any;
  datasetNameOption: any;


  formFieldApiStatus = {
    dataZone : false,
    dataProvider : false,
    datasetName : false,
    attribute : false,
    dateFormat : false,
    ruleType :false,
    dqSeverity : false,
    status : false,
  }
  formGroup : FormGroup
  formProps: any
  ruleTypeOptions: any;

  constructor( 
    private fb:FormBuilder,
   private DataQualityApiService: DataQualityApiService,
  
    ){}
    async ngOnInit(){
      console.log(this.popupModelData.editValue)
    this.formGroup = this.fb.group({
      dataZone : ['', Validators.required],
      dataProvider : ['', Validators.required],
      datasetName : ['' , Validators.required],
      attribute : ['', Validators.required],
      ruleType : ['', Validators.required],
      value_1 : ['' , [Validators.required, patternValidator(CUSTOM_CHAR_REGEX)]],
      value_2 : [''],
      dqSeverity : ['', Validators.required],
      status : ['', Validators.required],
    })

    let currObj = this.popupModelData.editValue
    let zoneId = [currObj.DATA_LAYER_ID]
    this.formPropsValueSetter()
    this.dataZoneOptionGetter()   
    this.dataProviderOptionsGetter([...zoneId])
    this.dqSeverityOptionGetter()
    this.statusOptionGetter()
      
      // dataSetName Parameter
      let dataSetParam = [currObj.DATA_PROVIDER,currObj.DATA_LAYER_ID]
      let ent_Id = [currObj.ENT_ID]
       this.dataSetNameOptionGetter([...dataSetParam])
       this.attributeOptionsGetter([...ent_Id])
       this.ruleTypeOptionGetter()
       this.datasetNameOption = await this.dateFormatOptionsGetter()
       // checking the value1 is date format
       console.log(this.datasetNameOption)
      for(let element of this.datasetNameOption){
        if(element.value === currObj.VALUE_1){
          this.formGroup.patchValue({
                    value_1 :{label:currObj.VALUE_1, value :currObj.VALUE_1},
                  })
          break
        }else {
          this.formGroup.patchValue({
            value_1 :currObj.VALUE_1,
          })
        }

      }
       this.formGroup.patchValue({
        dataZone : {label:currObj.DATA_ZONE, value :currObj.DATA_LAYER_ID},
        dataProvider :{label:currObj.DATA_PROVIDER, value :currObj.DATA_PROVIDER},
        datasetName :{label:currObj.DATASET_NAME, value :currObj.ENT_ID},
        attribute : {label:currObj.ATTRIBUTE, value :currObj.COL_ID},
        ruleType : {label:currObj.DQ_RULE_TYPE, value :currObj.MSTR_RULE_ID},
        value_2 : currObj.VALUE_2,
        dqSeverity :{label:currObj.RULE_SEVERITY, value :currObj.RULE_SEVERITY},
        status :{label:currObj.STATUS, value :currObj.STATUS},
       })
      //  value_1 :{label:currObj.DATASET_NAME, value :currObj.ENT_ID},
       
    
  }

  async dataZoneOptionGetter(){
  
    this.formFieldApiStatus.dataZone = true
    this.dataZoneOptions = await lastValueFrom(this.DataQualityApiService.dataZone().pipe(
      map((array)=>{
        return array.map((item)=>({
          label:item.DATA_ZONE_NM,
          value : item.DATA_ZONE_ID
        }))
      })
    ))
    this.formProps.dataZoneProps.options = this.dataZoneOptions.sort((a,b)=> a.label.localeCompare(b.label))
  
    this.formFieldApiStatus.dataZone = false
  }
  async dataProviderOptionsGetter(zoneId){
    this.formFieldApiStatus.dataProvider = true
    const postData={
      zoneId:zoneId[0]
     }

    this.dataProviderOptions = await lastValueFrom(this.DataQualityApiService.dataProvider(postData).pipe(
      map((array)=>{
        return array.map((item)=>{
          if (item.DATA_PROVIDER !== null && item.DATA_PROVIDER !== '') {
            return {
              label:item.DATA_PROVIDER,
              value : item.DATA_PROVIDER
            };
          } else {
            return null;
          }
        }).filter(item => item !== null && item !==''&& item !==' '); // Filter out null items
      })
    ))
    this.formProps.dataProviderProps.options = this.dataProviderOptions.sort((a,b)=> a.label.localeCompare(b.label))
    console.log(this.dataProviderOptions)
    this.formFieldApiStatus.dataProvider = false
  }
  async dataSetNameOptionGetter(param){
    // console.log(param)
    this.formFieldApiStatus.datasetName = true
    const data={
      providerName:param[0],
      zoneId:param[1]
      }
    
    this.datasetNameOption = await lastValueFrom(this.DataQualityApiService.dataSetName(data).pipe(
      map((array)=>{
        return array.map((item)=>({
          label:item.ENT_NM,
          value : item.ENT_ID
        }))
      })
    ))
    this.formProps.datasetNameProps.options = this.datasetNameOption.sort((a,b)=> a.label.localeCompare(b.label))
    console.log(this.datasetNameOption)
    this.formFieldApiStatus.datasetName = false
    
  }
  async attributeOptionsGetter(entId){
    this.formFieldApiStatus.attribute = true
    const data={
      entId:entId[0]
    }
    this.attributeOption = await lastValueFrom(this.DataQualityApiService.dataSetAttribute(data).pipe(
      map((array)=>{
        return array.map((item)=>({
          label:item.COL_NM,
          value : item.COL_ID
        }))
      })
    ))
    this.formProps.attributeProps.options = this.attributeOption.sort((a,b)=> a.label.localeCompare(b.label))

    this.formFieldApiStatus.attribute = false
  
  }
  
  async ruleTypeOptionGetter(){
    this.formFieldApiStatus.ruleType = true
    this.ruleTypeOptions = await lastValueFrom(this.DataQualityApiService.ruleType().pipe(
      map((array)=>{
        return array.map((item)=>({
          label:item.VISUAL_RULE_DESC,
          value : item.MSTR_RULE_ID
        }))
      })
    ))
    this.formFieldApiStatus.ruleType = false
    console.log(this.ruleTypeOptions)
    this.formProps.ruleTypeProps.options = this.ruleTypeOptions.sort((a,b)=> a.label.localeCompare(b.label))
  }
  async dqSeverityOptionGetter(){
    this.formFieldApiStatus.dqSeverity = true
    this.dqSeverityOption = await lastValueFrom(this.DataQualityApiService.DQSeverity().pipe(
      map((array)=>{
        return array.map((item)=>({
          label:item.ACTION_DESC,
          value : item.LOV_CD
        }))
      })
    ))
    this.formFieldApiStatus.dqSeverity = false
    this.formProps.dqSeverityProps.options = this.dqSeverityOption
  }
  async statusOptionGetter(){
    this.formFieldApiStatus.status = true
    this.statusOption = await lastValueFrom(this.DataQualityApiService.status().pipe(
      map((array)=>{
        return array.map((item)=>({
          label:item.ACTION_DESC,
          value : item.LOV_CD
        }))
      })
    ))
    this.formFieldApiStatus.status = false
    this.formProps.statusProps.options = this.statusOption
  }
  async dateFormatOptionsGetter(): Promise<any[]>{
    this.formFieldApiStatus.dateFormat = true
	
   let dateFormatOption = await lastValueFrom(this.DataQualityApiService.dateFormat().pipe(
      map((array)=>{
        return array.map((item)=>({
          label:item.LOV_CD,
          value : item.LOV_CD
        }))
      })
    ))
    this.formProps.dateFormatProps.options = dateFormatOption.sort((a,b)=> a.label.localeCompare(b.label))
    
    this.formFieldApiStatus.dateFormat = false
    return dateFormatOption
  }

valueChangeHandler(control,field){
console.log(control)
  if(field === 'dataZone'){
    this.formGroup.patchValue({
      dataProvider :'',
      datasetName :'',
      attribute : '',
      ruleType :'',
      value_1 : '',
      value_2 : '',
     })
     this.dataProviderOptionsGetter([control.value.value])
  } else if(field === 'dataProvider'){
    this.formGroup.patchValue({
      datasetName :'',
      attribute : '',
      ruleType :'',
      value_1 : '',
      value_2 : '',
     })

     this.dataSetNameOptionGetter([control.value.value ,this.formGroup.get('dataZone').value.value])
  }else if(field === 'dataSetName'){
    this.formGroup.patchValue({
      attribute : '',
      ruleType :'',
      value_1 : '',
      value_2 : '',
     })
     this.attributeOptionsGetter([control.value.value])
  }else if(field === 'attribute'){
    this.formGroup.patchValue({
      ruleType :'',
      value_1 : '',
      value_2 : '',
     })
  }else if(field === 'ruleType'){
    this.formGroup.patchValue({
      value_1 : '',
      value_2 : '',
     })
     if(control.value.label === 'RANGE CHECK'){
      let value_2 = this.formGroup.get('value_2')
      value_2.setValidators([Validators.required, patternValidator(CUSTOM_CHAR_REGEX)])
      value_2.updateValueAndValidity()
     }else {
      let value_2 = this.formGroup.get('value_2')
      value_2.setValidators([])
      value_2.updateValueAndValidity()
     }
     
  }

}
  submitHandler() {
    if(this.formGroup.valid){
      console.log(this.formGroup.value)
      this.submitResponse.emit(this.formGroup.value);
    }else{
      // form submit status change
      Object.keys(this.formProps).forEach(property=>{
        this.formProps[property].isSubmitted = true
      })
    }
  }
rejectHandler() {
    this.popupModelData.editDisplayModal = false
    this.cancelResponse.emit();
  }

  formPropsValueSetter(){
    this.formProps = {
      dataZoneProps: {
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['dataZone'],
        options: [],
        errorMsg: 'This field is required.',
        isSubmitted: false,
      },
      dataProviderProps :{
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['dataProvider'],
        options: [],
        errorMsg: 'This field is required.',
        isSubmitted: false,
      },
      datasetNameProps: {
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['datasetName'],
        options: [],
        errorMsg: 'This field is required.',
        isSubmitted: false,
      },
      attributeProps :{
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['attribute'],
        options: [],
        errorMsg: 'This field is required.',
        isSubmitted: false,
      },
      ruleTypeProps :{
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['ruleType'],
        options: [],
        errorMsg: 'This field is required.',
        isSubmitted: false,
      },
      dateFormatProps: {
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['value_1'],
        options: [],
        errorMsg: 'This field is required.',
        isSubmitted: false,
      },
      value_1TextBoxProps : {
        type : "text",
        inputLabel: '',
        placeholder: 'Enter value',
        formControl : this.formGroup.controls['value_1'],
        formGroup : this.formGroup,
        isSubmitted : false,
        disable : false,
        errorMsg :"This field is required.",
        patternErrMsg : "This (',\\,`,;) characters not allowed"
      },
      value_1RangeTextBoxProps : {
        type : "text",
        inputLabel: 'Start value',
        placeholder: '',
        formControl : this.formGroup.controls['value_1'],
        formGroup : this.formGroup,
        isSubmitted : false,
        disable : false,
        errorMsg :"This field is required.",
        patternErrMsg : "This (',\\,`,;) characters not allowed"
      },
      value_2RangeTextBoxProps : {
        type : "text",
        inputLabel: 'End value',
        placeholder: '',
        formControl : this.formGroup.controls['value_2'],
        formGroup : this.formGroup,
        isSubmitted : false,
        disable : false,
        errorMsg :"This field is required.",
        patternErrMsg : "This (',\\,`,;) characters not allowed"
      },
      dqSeverityProps: {
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['dqSeverity'],
        options: [],
        errorMsg: 'This field is required.',
        isSubmitted: false,
      },
      statusProps :{
        formGroup: this.formGroup,
        formControl : this.formGroup.controls['status'],
        options: [],
        errorMsg: 'This field is required.',
        isSubmitted: false,
      },
    }
  }
}

