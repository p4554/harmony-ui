import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThresholdTrendPopupComponent } from './threshold-trend-popup.component';

describe('ThresholdTrendPopupComponent', () => {
  let component: ThresholdTrendPopupComponent;
  let fixture: ComponentFixture<ThresholdTrendPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThresholdTrendPopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ThresholdTrendPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
