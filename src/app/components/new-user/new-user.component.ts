import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { lastValueFrom } from 'rxjs';
import { CUSTOM_CHAR_REGEX } from '@app/util/helper/patterns';
import { getMappedArray, getUniqueValues, addDuplicateRecordCheckerAdmin } from '@app/util/helper/arrayHandlers';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoaderService } from '@app/_services/loading.service';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
import { AdminApiService } from '@app/_services/admin-api.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.scss']
})
export class NewUserComponent {
  @Output() onSave: EventEmitter<any> = new EventEmitter();

  city: any
  managerData: []

  // activeUser:string; 
  newUserForm: FormGroup;
  tableData: any;

  constructor(
    private AdminApiService: AdminApiService,
    private fb: FormBuilder,
    private toastService: MessageService,
    public loaderService: LoaderService,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig
  ) { }


  async ngOnInit() {
    this.newUserForm = this.fb.group({
      userFirstName: ['', [Validators.required, Validators.pattern(CUSTOM_CHAR_REGEX)]],
      userLastName: ['', [Validators.required, Validators.pattern(CUSTOM_CHAR_REGEX)]],
      userName: ['', [Validators.required, Validators.pattern(CUSTOM_CHAR_REGEX)]],
      userEmail: [
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/),
        ]),
      ],
      password: ['', [Validators.required, Validators.pattern(CUSTOM_CHAR_REGEX)]],
      manager: ['', Validators.required],
      activeUser: ['', Validators.required],
      userType: ['', Validators.required],
      admin: ['', Validators.required],
      userImage: [null, Validators.required],
    });
    this.loaderService.showLoader();
    this.tableData = this.config.data.tableData;
    await Promise.all([
      this.getMangerData()
    ]);
    setTimeout(() => {
      this.loaderService.hideLoader();
    }, 300);
  }

  ReportingManger: any;
  mappedReportingManagerData:any
  async getMangerData() {
    this.ReportingManger = await lastValueFrom(
      this.AdminApiService.reportingManager()
    )
    this.managerData = getMappedArray(
      getUniqueValues(this.ReportingManger, 'USER_NM'),
      'label',
      'value'
    );
    this.mappedReportingManagerData = this.ReportingManger.map(user => {
      return {
        mangerName: user.USER_NM,
        managerId: user.USER_ID,
      };
    });
  }
  imageFile: any;
  onFileSelected(e) {

    this.imageFile = e.target.files[0];
    this.newUserForm.get('userImage').setValue(this.imageFile)
    console.log(e)
  }

  cancelButtonHandler() {                                           
    this.ref.close();
  }
  localUserFlag: any;
  ldapUserFlag: any;
  selectedManagerId: any;
  async onSaveHandler() {
    
    const formData = new FormData();
   const image= this.newUserForm.get('userImage').value;
   formData.append('userImage', this.newUserForm.get('userImage').value);

    if (this.newUserForm.valid) {
      let formValue = this.newUserForm.value;
      if (formValue.userType == 'ldap') {
        this.ldapUserFlag = 'true';
        this.localUserFlag = 'false';
        formValue.userType = true;
      }
      else if (formValue.userType == 'local') {
        this.ldapUserFlag = 'false';
        this.localUserFlag = 'true';
        formValue.userType = false;
      }
      const formProperty = ['userName', 'userEmail','userType']
      const tableProperty = ['USER_NM', 'USER_EMAIL_ID','LDAP_USER_FLG']
      const duplicateFound = addDuplicateRecordCheckerAdmin(formValue, formProperty, this.tableData, tableProperty)
      if (duplicateFound) {
        this.toastService.add({ key: 'wrn', closable: false, sticky: false, severity: '', summary: '', detail: 'User is Already Exists.Try Another', });
      } else {
        const selectedManager = this.mappedReportingManagerData.find(user => user.mangerName === formValue.manager);
        this.selectedManagerId = selectedManager.managerId;
        const reportingId=this.selectedManagerId;
        formData.append('reportingManager',reportingId);
        formData.append('lastName',formValue.userLastName);
        formData.append('firstName',formValue.userFirstName);
        formData.append('ldapFlag',this.ldapUserFlag);
        formData.append('userName', formValue.userName);
        formData.append('adminFlag', formValue.admin);
        formData.append('localFlag', this.ldapUserFlag); 
        formData.append('userEmailId',formValue.userEmail); 
        formData.append('userPassword', formValue.password); 
        formData.append('activeFlag', formValue.activeUser);  
        formData.append('createdBy', localStorage.getItem('userName'))
        formData.append('updatedBy',localStorage.getItem('userName'))
        

        await this.AdminApiService.addUser(formData).subscribe((response) => {
          console.log(response);
          if (response.status == 200) {
            this.ref.close('saved');
            this.toastService.add({
              key: 'su',
              closable: false,
              sticky: false,
              severity: '',
              summary: '',
              detail: `New User ${formValue.userName} created`,
            });
          }
          else {
            this.ref.close();
            this.toastService.clear();
            this.toastService.add({
              key: 'wrn',
              closable: false,
              sticky: false,
              severity: '',
              summary: '',
              detail: `Connection Error Try Again`,
            });
          }
        })
      }

    }
    else {
      this.newUserForm.markAllAsTouched();
    }


  }
}
