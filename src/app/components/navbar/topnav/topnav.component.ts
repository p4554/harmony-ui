import { Component, Input, OnInit, Renderer2 } from '@angular/core';
// import { MenuItem } from 'primeng/api/menuitem';
import { MenuItem } from 'primeng/api';
import { NavigationService } from '@app/_services/navigation.service';
import { LocalStorageService } from '@app/_services/local-storage.service';
import { AuthenticationService } from '@app/_services/authentication.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient } from '@angular/common/http';
import { MessageService } from 'primeng/api';
import { CommonDataService } from '@app/_services/common-data.service';
import { AdminApiService } from '@app/_services/admin-api.service';
@Component({
  selector: 'app-top-nav',
  templateUrl: './topnav.component.html',
  styleUrls: ['./topnav.component.scss'],
})
export class TopnavComponent implements OnInit {
  avatarUrl: any;
  downArrow: any;
  menuItemsLogout: MenuItem[];
  menuIsActive: boolean;
  active_url: string;
  isActive = false;

  d_g_menu: any = {
    overlayVisible: false,
  };
  // overlayVisible

  dashboard_fc = '#707070';

  showDropdown = true;

  showFlyoutMenu: boolean;

  overlayVisible: boolean;
  dataGovernanceOverlayVisible: boolean;
  dataEngineeringOverlayVisible: boolean;
  adminOverlayVisible: boolean;
  notificationOverlayVisible: boolean;
  inboxOverlayVisible: boolean;

  username: string = localStorage.getItem('userName');
  notificationData: any = [];

  isDefineMenuOpen = false;

  @Input() isMenuOpen: boolean;
  clickedMenu_data$: Observable<any>;

  home_fc = '#000';
  define_fc = '#fff';

  define_arrow = false;
  home_arrow = false;

  clickedItem: any;
  inboxData: any = [];
  selectedMenuItem: string;
  notificationCount: number = 5;
  menuItems: any = [
    {
      label: 'Data Quality',
      url: 'data-quality-rule',
      children: [
        {
          label: 'Data Quality Rules',
          details: 'Configure data quality rules for processing',
          icon: '/assets/svg/dataQualityRules.svg',
          url: 'data-quality-rule',
        },
        {
          label: 'Refrential Integrity',
          details: 'Define rule to check data integrity between two tables',
          icon: '/assets/svg/referential-integrity.svg',
          url: 'data-quality-rule',
        },
        {
          label: 'Control Sum',
          details:
            'Define rule to compare sum of Metric columns between two tables',
          icon: '/assets/svg/control-sum.svg',
          url: 'data-quality-rule',
        },
        {
          label: 'Data Validation',
          details: 'Define various rules at column level to check domain value',
          icon: '/assets/svg/data-validation.svg',
          url: 'data-quality-rule',
        },
        {
          label: 'Control Total',
          details: 'Define rule to compare count of rows between two tables',
          icon: '/assets/svg/control-total.svg',
          url: 'data-quality-rule',
        },
        {
          label: 'Threshold Variation',
          details:
            'Define rule to identify if a metric value is under the defined threshold value',
          icon: '/assets/svg/threshold-variation.svg',
          url: 'data-quality-rule',
        },
        {
          label: 'Threshold Trend',
          details:
            ' Define rule to identify if a metric value is under the defined threshold value for a defined period.',
          icon: '/assets/svg/threshold-trend.svg',
          url: 'data-quality-rule',
        },
        {
          label: 'Data Lineage',
          details:
            ' Define rule to identify if a metric value is under the defined threshold value for a defined period.',
          icon: '/assets/svg/threshold-trend.svg',
          url: 'data-lineage',
        },
      ],
    },
    {
      label: 'Business Rule',
      children: [
        {
          label: 'Data Quality Rules',
          details: 'Configure data quality rules for processing',
          icon: '/assets/svg/dataQualityRules.svg',
        },
        {
          label: 'Refrential Integrity',
          details: 'Define rule to check data integrity between two tables',
          icon: '/assets/svg/referential-integrity.svg',
        },
      ],
      url: 'business-rule',
    },
    {
      label: 'Data Quality Report',
      children: [{ label: 'Projects' }, { label: 'Sandboxes' }],
    },
    {
      label: 'Rule Library',
      url: 'rule-library',
    },
    {
      label: 'Audit Report',
    },
    {
      label: 'Data Profiling',
      url: 'data-profiling',
    },
    {
      label: 'File Monitoring',
      url: 'file-monitoring',
    },
    {
      label: 'Document Library',
    },
  ];

  dataEngineeringMenuItems: any = [
    {
      label: 'Data Lineage',
      details:
        ' Define rule to identify if a metric value is under the defined threshold value for a defined period.',
      icon: '/assets/svg/threshold-trend.svg',
      url: 'data-lineage',
    },
    {
      label: 'Data Catalog',
      details:
        ' Define rule to identify if a metric value is under the defined threshold value for a defined period.',
      icon: '/assets/svg/threshold-trend.svg',
      url: 'data-catalog',
    },
  ];
  adminMenuItems: any = [
    {
      label: 'Tenants',
      url: 'Tenants',
    },
    {
      label: 'Users',

      url: 'users',
    },
    {
      label: 'Groups',

      url: 'groups',
    },
    {
      label: 'User Access configuration',

      url: 'user-access',
    },
    {
      label: 'User Groups Configuration',

      url: 'user-groups',
    },
    {
      label: 'Lov Master',

      url: 'lov-master',
    },
    {
      label: 'User Zone Configuration',

      url: 'user-zone',
    },
    {
      label: 'Session Manager',

      url: 'session-manager',
    },
    {
      label: 'Clear Cache',

      url: ' ',
    },
  ];

  constructor(
    private navigationService: NavigationService,
    private localStorageService: LocalStorageService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute,
    private _router: Router,
    private renderer: Renderer2,
    private http: HttpClient,
    private toastService: MessageService,
    private commonDataService: CommonDataService,
    private AdminApiService: AdminApiService
  ) {}

  topnavData = [
    {
      label: 'Home',
      activeState: false,
    },
    {
      label: 'Data Source Configuration',
      activeState: false,
    },
    {
      label: 'Data Governance',
      activeState: false,
    },
    {
      label: 'Data Engineering',
      activeState: false,
    },
    {
      label: 'Business Application',
      activeState: false,
    },
    {
      label: 'Admin',
      activeState: false,
    },
  ];

  async ngOnInit() {
    this.avatarUrl = '../../../assets/avatar.svg';
    this.downArrow = '../../../assets/svgs/arrow-down.svg';

    this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe(() => {
        const url = this.router.url.slice(1);
        this.active_url = url;
        console.log('router Url', this.active_url);
        if (this.active_url === 'admin-dashboard') {
          this.topnavData[0].activeState = true;
        } else if (this.active_url === 'source-connector') {
          this.topnavData[1].activeState = true;
          this.isActive = true;
        } else {
          this.dataEngineeringMenuItemscheckUrl(this.dataEngineeringMenuItems);
          this.MenuItemscheckUrl(this.menuItems);
          this.AdminMenuItemscheckUrl(this.adminMenuItems);
        }
      });

    this.menuItemsLogout = [
      {
        items: [
          {
            label: 'logout',
            icon: 'pi pi-fw pi-sign-out',
            command: () => {
              console.log('New clicked');

              this.logout();
            },
          },
          {
            label: 'Dev Progress',
            icon: 'pi pi-fw pi-sign-out',
            command: () => {
              this.router.navigateByUrl('dev-progress');
            },
          },
        ],
      },
    ];
  }

  dataEngineeringMenuItemscheckUrl(data: any) {
    for (const item of data) {
      if (item.url === this.active_url) {
        this.topnavData[3].activeState = true;
        item.active = true;
        break;
      }
    }
  }
  MenuItemscheckUrl(data: any) {
    for (const item of data) {
      if (item.url === this.active_url) {
        this.topnavData[2].activeState = true;
        item.active = true;
        break;
      }
    }
  }
  AdminMenuItemscheckUrl(data: any) {
    for (const item of data) {
      if (item.url === this.active_url) {
        this.topnavData[5].activeState = true;
        item.active = true;
        break;
      }
    }
  }

  toggleDropdown() {
    this.showDropdown = !this.showDropdown;
  }

  handleMenuMouseHover(arg: number) {
    if (arg === 3) {
      this.dataEngineeringOverlayVisible = true;
    }
    if (arg === 5) {
      this.adminOverlayVisible = true;
    }
    if (arg === 1) {
      this.overlayVisible = true;
    }
    if (arg === 2) {
      this.dataGovernanceOverlayVisible = true;
    }
    // console.log('mouseHover');
  }

  handleMenuMouseOut(arg: number) {
    // if (!this.menuIsActive ) {
    //   this.topnavData[arg].activeState = false;
    // }
    if (arg === 3) {
      this.dataEngineeringOverlayVisible = false;
    }
    if (arg === 1) {
      this.overlayVisible = false;
    }
    if (arg === 2) {
      this.dataGovernanceOverlayVisible = false;
    }
  }

  handleTopNavMenuClick(data: any) {
    if (data.label === 'Home') {
      this.homeNavigation();
      this.adminMenuItems.forEach((i) => (i.active = false));
      this.dataEngineeringMenuItems.forEach((i) => (i.active = false));
      this.menuItems.forEach((i) => (i.active = false));
    }
  }

  dataSoureConfigHandler() {
    this.overlayVisible = true;
  }

  toggleActive() {
    this.isActive = true;
  }

  handleSourceConnectorPageClick() {
    this.toggleActive();
    this.overlayVisible = false;
    for (let i = 0; i < 6; i++) {
      this.topnavData[i].activeState = false;
    }
    this.topnavData[1].activeState = true;
    this.router.navigateByUrl('source-connector');
    // Set all the other menuItems  to inactive
    this.menuItems.forEach((item) => {
      if (item !== this.menuItems) {
        item.active = false;
        item.openSubMenu = false;
        this.menuItems.activeState = false;
      }
    });

    // Set all the other dataEngineeringMenuItems to inactive
    this.dataEngineeringMenuItems.forEach((item) => {
      if (item !== this.dataEngineeringMenuItems) {
        item.active = false;
        item.openSubMenu = false;
        this.dataEngineeringMenuItems.activeState = false;
      }
    });

    // Set all the other adminMenuItems to inactive
    this.adminMenuItems.forEach((item) => {
      if (item !== this.adminMenuItems) {
        item.active = false;
        item.openSubMenu = false;
        this.adminMenuItems.activeState = false;
      }
    });
  }

  d_GovernaceClickHandler() {
    this.dataGovernanceOverlayVisible = true;
    // this.d_g_menu.overlayVisible = !this.d_g_menu.overlayVisible
    // console.log("d_GovernaceClickHandler=>",this.d_g_menu.overlayVisible)
  }
  async mainMenuChangeHandler(param) {
    if (param === 'home') {
      for (let i = 0; i < 6; i++) {
        this.topnavData[i].activeState = false;
      }
      this.isActive = false;
      this.topnavData[0].activeState = true;
    }

    if (param === 'notification') {
      await this.commonDataService
        .fetchNotifications()
        .toPromise()
        .then((data: any) => {
          this.notificationData = data;
        });
      this.notificationOverlayVisible = true;
    }
    if (param === 'inbox') {
      await this.commonDataService
        .fetchInbox()
        .toPromise()
        .then((data: any) => {
          this.inboxData = data;
        });
      this.inboxOverlayVisible = true;
    }
  }

  mainMenuChangeBackHandler(param) {
    if (param === 'admin') {
      this.adminOverlayVisible = false;
      // if (!this.menuIsActive) {
      //   this.topnavData[5].activeState = false;
      // }
    }
  }

  homeNavigation() {
    this.overlayVisible = false;
    this.d_g_menu.overlayVisible = false;
    this.router.navigateByUrl('admin-dashboard');
  }

  logout() {
    this.localStorageService.setLoggedOut();
    this.navigationService.redirectToLogin();
  }

  onMenuItemClick(menuItem: any, param) {
    console.log('menuItem=>', menuItem);
    // Set the clicked item to active
    menuItem.active = true;
    this.menuIsActive = menuItem.active;
    menuItem.openSubMenu = true;
    //set main menu active

    if (param === 'dataEngineering') {
      this.dataEngineeringOverlayVisible = true;
      for (let i = 0; i < 6; i++) {
        this.topnavData[i].activeState = false;
      }
      this.topnavData[3].activeState = true;
    }

    if (param === 'Data Governance') {
      this.dataGovernanceOverlayVisible = true;
      for (let i = 0; i < 6; i++) {
        this.topnavData[i].activeState = false;
      }
      this.topnavData[2].activeState = true;
    }

    if (param === 'admin') {
      this.adminOverlayVisible = true;
      for (let i = 0; i < 6; i++) {
        this.topnavData[i].activeState = false;
      }
      this.topnavData[5].activeState = true;
    }

    if (param === 'Data Source Configuration') {
      this.overlayVisible = true;
      for (let i = 0; i < 6; i++) {
        this.topnavData[i].activeState = false;
      }
      this.topnavData[1].activeState = true;
    }

    // Set all other items to inactive
    if (menuItem.label == 'Clear Cache') {
      const flushCachePromise = this.AdminApiService.flushCache().toPromise();

      flushCachePromise
        .then((response: any) => {
          console.log('Cache cleared successfully.');
          this.toastService.clear();
          this.toastService.add({
            key: 'su',
            closable: false,
            sticky: false,
            severity: '',
            summary: '',
            detail: 'Cache cleared successfully',
          });
        })
        .catch((error: any) => {
          console.error('Error clearing cache:', error);
          this.toastService.clear();
          this.toastService.add({
            key: 'wrn',
            closable: false,
            sticky: false,
            severity: '',
            summary: '',
            detail: 'Connection Error. Please try again.',
          });
        });
    }

    // Set all the other menuItems  to inactive
    this.menuItems.forEach((item) => {
      if (item !== menuItem) {
        item.active = false;
        item.openSubMenu = false;
      }
    });

    // Set all the other dataEngineeringMenuItems to inactive
    this.dataEngineeringMenuItems.forEach((item) => {
      if (item !== menuItem) {
        item.active = false;
        item.openSubMenu = false;
      }
    });

    // Set all the other adminMenuItems to inactive
    this.adminMenuItems.forEach((item) => {
      if (item !== menuItem) {
        item.active = false;
        item.openSubMenu = false;
      }
    });

    if (menuItem.label === 'Home') {
      this._router.navigate(['/admin-home-page']);
    }

    if (menuItem.label === 'Audit Report') {
      const url =
        'https://prod-useast-a.online.tableau.com/#/site/datazymes/views/JobStatusReport/Dashboard1?:iid=1';
      window.open(url, '_blank', 'noopener');
    } else if (menuItem.label === 'Data Quality Report') {
      const url =
        'https://prod-useast-b.online.tableau.com/t/datazymes2021/views/DataQualityDashboard/Homepage?:embed=y&amp;:toolbar=n#3';
      window.open(url, '_blank', 'noopener');
    } else {
      this._router.navigate([menuItem.url]);
    }
    this.dataGovernanceOverlayVisible = false;
    this.dataEngineeringOverlayVisible = false;
    this.overlayVisible = false;
    this.adminOverlayVisible = false;
    this.isActive = false;
  }

  onSubMenuItemClick(childItem, event) {
    event.stopPropagation();

    // Set all other sub menu items to inactive
    this.menuItems.forEach((item) => {
      item.openSubMenu = false;
    });

    if (childItem.label === 'Data Sources')
      this._router.navigate(['/data-soruce-page']);
  }

  onItemClick(item: any) {
    this.menuItems.forEach((otherItem) => (otherItem.active = false));
    item.active = true;
  }

  selectMenuItem(menuItem: string) {
    this.selectedMenuItem = menuItem;
  }

  toggleDefineMenu() {
    this.isDefineMenuOpen = !this.isDefineMenuOpen;
  }
  gotoRoute(url) {
    this._router.navigateByUrl(url);
    this.dataGovernanceOverlayVisible = false;
    this.dataEngineeringOverlayVisible = false;
    this.overlayVisible = false;
    this.adminOverlayVisible = false;
  }
  notificationClickHandler() {
    this._router.navigate(['/notification']);
    this.notificationOverlayVisible = false;
  }
  inboxClickHandler() {
    this._router.navigate(['/inbox']);
    this.inboxOverlayVisible = false;
  }
  setclickhandler() {
    for (let i = 0; i < 6; i++) {
      this.topnavData[i].activeState = false;
    }
    this.adminMenuItems.forEach((i) => (i.active = false));
    this.dataEngineeringMenuItems.forEach((i) => (i.active = false));
    this.menuItems.forEach((i) => (i.active = false));
  }
  inbox;
}
