import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidenavComponent } from './sidenav/sidenav.component';
import { TopnavComponent } from './topnav/topnav.component';
import { SharedModule } from 'src/app/shared.module';


@NgModule({
	declarations: [SidenavComponent, TopnavComponent],
	exports: [SidenavComponent, TopnavComponent],
	imports: [CommonModule, SharedModule,BrowserAnimationsModule],
})
export class NavbarModule {}
