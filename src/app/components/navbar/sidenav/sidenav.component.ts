import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NavigationService } from '@app/_services/navigation.service';
import { MenuItem } from 'primeng/api/menuitem';

@Component({
	selector: 'app-sidenav',
	templateUrl: './sidenav.component.html',
	styleUrls: ['./sidenav.component.scss'],
})
export class SidenavComponent {
	constructor(private _router: Router, private navigationService: NavigationService) {}

	ngOnInit() {}

	navigateToUserDashboard() {
		this.navigationService.goToUserDashboard();
	}
}
