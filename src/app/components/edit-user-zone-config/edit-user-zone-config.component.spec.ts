import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditUserZoneConfigComponent } from './edit-user-zone-config.component';

describe('EditUserZoneConfigComponent', () => {
  let component: EditUserZoneConfigComponent;
  let fixture: ComponentFixture<EditUserZoneConfigComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditUserZoneConfigComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditUserZoneConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
