import { Component } from '@angular/core';
import {AdminApiService} from '../../_services/admin-api.service'
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {  getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { MessageService } from 'primeng/api';
import { LoaderService } from '@app/_services/loading.service';
@Component({
  selector: 'app-edit-user-zone-config',
  templateUrl: './edit-user-zone-config.component.html',
  styleUrls: ['./edit-user-zone-config.component.scss']
})
export class EditUserZoneConfigComponent {
  
  EditUserZoneConfigForm: FormGroup;
  
  constructor(
  private ref: DynamicDialogRef,
  private AdminApiService: AdminApiService,
  private fb: FormBuilder,
  private toastService: MessageService,
  private config: DynamicDialogConfig,
  public loaderService: LoaderService,
 ){}
 user_group_lyr_id:number;
 grp_name:any;
 data_zone:any;
 act_flg:any
 async ngOnInit(){
   const data = this.config.data;
   this.user_group_lyr_id=data.USRGRP_LYR_ID;
    this.EditUserZoneConfigForm = this.fb.group({
    groupName: [, Validators.required],
    dataZone: [, Validators.required],
    active: [, Validators.required],
  });
  this.EditUserZoneConfigForm.patchValue({
    groupName : data.USRGRP_NM,
    dataZone : data.DATA_LYR_NAME,
    active : data.ACTV_FLG,
  }); 
  this.loaderService.showLoader();
  await this.getGroupnameData();
  await this.getZoneDataData();
  setTimeout(() => {
    this.loaderService.hideLoader();
  }, 300);
 }
group_name:any;
group_data:any;
zone_name:any;
mappedGroupNameData:any;
mappedZoneData:any;
 async getGroupnameData(){
   this.group_data=await lastValueFrom(
    this.AdminApiService.groupName()
   )
  this.group_name = getMappedArray(
    getUniqueValues(this.group_data, 'USRGRP_NM'),
    'label',
    'value'
  );
  this.mappedGroupNameData = this.group_data.map(user => {
    return {
      group: user.USRGRP_NM,
      groupId: user.USRGRP_ID,
    };
  });
 }
 async getZoneDataData(){
   this.group_data=await lastValueFrom(
    this.AdminApiService.dataZone()
   )
  this.zone_name = getMappedArray(
    getUniqueValues(this.group_data, 'DATA_ZONE_NM'),
    'label',
    'value'
  );
  this.mappedZoneData = this.group_data.map(user => {
    return {
      zone: user.DATA_ZONE_NM,
      zoneId: user.DATA_ZONE_ID,
    };
  });
 }
 cancelButtonHandler(){
  this.ref.close();
  }
  selectedGroupId:number;
  selectedZoneId:number;
 async onSaveHandler(){
    if(this.EditUserZoneConfigForm.valid){
      let formValue = this.EditUserZoneConfigForm.value;
      const selectedGroup = this.mappedGroupNameData.find(user => user.group === formValue.groupName);
         this.selectedGroupId = selectedGroup.groupId;
         const selectedZone = this.mappedZoneData.find(user => user.zone === formValue.dataZone);
         this.selectedZoneId = selectedZone.zoneId;       
      const postData={
            layerId: this.selectedZoneId,
            userGroupId: this.selectedGroupId,
            activeFlag: formValue.active,
            updatedBy: localStorage.getItem('userName'),
            userGroupLayerId:this.user_group_lyr_id
      }
      await this.AdminApiService.updateUserZone(postData).subscribe((response)=>{
        console.log(response);
        if(response.status==200){
          this.ref.close('saved');
          this.toastService.add({
            key: 'su',
            closable: false,
            sticky: false,
            severity: '',
            summary: '',
            detail: `Configuration Successfully Updated`,
          });
        }
        else{
          this.ref.close();
          this.toastService.clear();
              this.toastService.add({
                key: 'wrn',
                closable: false,
                sticky: false,
                severity: '',
                summary: '',
                detail: `Connection Error Try Again`,
              });
        }
      }) 
    }
    else{
      this.EditUserZoneConfigForm.markAllAsTouched();
    } 
 }
}
