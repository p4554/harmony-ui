import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-popup-model',
  templateUrl: './popup-model.component.html',
  styleUrls: ['./popup-model.component.scss'],
})
export class PopupModelComponent implements OnInit{
  @Input() popupModelData: any;
  @Output() submitResoponse = new EventEmitter();
  @Output() cancelResoponse = new EventEmitter();
  // @Output() recjectEmitter = new EventEmitter();


  constructor(){
    console.log(this.popupModelData)
  }
  ngOnInit(): void {
   
  }
 
  handleChange() {
  
  }
  submitHandler() {
    this.submitResoponse.emit();
  }
  rejectHandler() {
    this.popupModelData.displayModal = false
    this.cancelResoponse.emit();
  }
  
}
