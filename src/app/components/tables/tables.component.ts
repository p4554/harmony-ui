import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MenuItem } from 'primeng/api';

// export interface tableHeadInput {
//   1:string,
//   input:{
//     type:string,
//     props:any
//   }
// }

@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.scss'],
})
export class TablesComponent implements OnInit {
  @Input() tableData: {
    head: any[];
    body: any[];
  };
  @Input() menuData: MenuItem[];

  @Output() handleEditEmitter = new EventEmitter<any>();
  @Output() handleDeleteEmitter = new EventEmitter<any>();
  menuItems: MenuItem[];

  isEdit:boolean = false
  constructor() {}
  selectedItem: any;
  tableBodyKeys: string[] = [];

  ngOnInit() {
    console.log(this.tableData)
    if (this.menuData) {
      this.menuItems = this.menuData;
    }
    if (this.tableData.body.length > 0) {
      this.tableBodyKeys = Object.keys(this.tableData.body[0]);
    }
  }

  
  handleEdit(row,index){
    console.log(index)
    this.isEdit = true  
    this.handleEditEmitter.emit(index)
  }

  handleDelete(row,index:number){
    this.handleDeleteEmitter.emit(index)
  }

}
