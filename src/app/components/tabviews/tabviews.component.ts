import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-tabviews',
  templateUrl: './tabviews.component.html',
  styleUrls: ['./tabviews.component.scss'],
})
export class TabviewsComponent {
  @Input() tabviewData: { header: string, content: string , count ?: number }[]
  @Output() setCurrentTab = new EventEmitter<any>();
  handleChange(e) {
    this.setCurrentTab.emit(this.tabviewData[e.index]);
  }
}
