import { Component } from '@angular/core';
import {AdminApiService} from '../../_services/admin-api.service'
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import {  DynamicDialogRef } from 'primeng/dynamicdialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { CUSTOM_CHAR_REGEX } from '@app/util/helper/patterns';
import { LoaderService } from '@app/_services/loading.service';
import { addDuplicateRecordCheckerAdmin } from '@app/util/helper/arrayHandlers';
@Component({
  selector: 'app-new-group',
  templateUrl: './new-group.component.html',
  styleUrls: ['./new-group.component.scss']
})
export class NewGroupComponent {
  
  group_type;
  groupTypeData=[
    'Technical',
    'Functional'
  ]
  NewGroupForm: FormGroup;
  
 constructor(
  private ref: DynamicDialogRef,
  private AdminApiService: AdminApiService,
  private fb: FormBuilder,
  private toastService: MessageService,
  public loaderService: LoaderService,
 ){}
 async ngOnInit(){
    this.NewGroupForm = this.fb.group({
    groupName: ['', [Validators.required,Validators.pattern(CUSTOM_CHAR_REGEX)]],
    description: ['', [Validators.required,Validators.pattern(CUSTOM_CHAR_REGEX)]],
    groupType: ['', Validators.required],
    active: ['', Validators.required],
  }); 
  await this.getTableData()
 }
 table_data;
 async getTableData(){
    this.table_data = await lastValueFrom(
      this.AdminApiService.groupTable()
    );
 }
 cancelButtonHandler(){
  this.ref.close();
  }
 async onSaveHandler(){
    if(this.NewGroupForm.valid){
       let groupTypeValue;
      let formValue = this.NewGroupForm.value;
      if(formValue.groupType==='Technical'){
        groupTypeValue='T';
        formValue.groupType='T';
      }
     else if(formValue.groupType==='Functional'){
      groupTypeValue='F';
        formValue.groupType='F';
        }
      const formProperty = ['groupName','groupType']
      const tableProperty = ['USRGRP_NM','GRP_TYP']
      const duplicateFound = addDuplicateRecordCheckerAdmin(formValue,formProperty,this.table_data,tableProperty)
      if(duplicateFound){
        this.toastService.add({key: 'wrn',closable: false,sticky: false,severity: '',summary: '',detail:'User is Already Exists.Try Another',});
      }
      else{
      const postData={
        userGroupName:formValue.groupName,
        userGroupDesc:formValue.description,
        groupType:groupTypeValue,
       activeFlag:formValue.active,
        createdBy:localStorage.getItem('userName'),
        updatedBy:localStorage.getItem('userName')
        }
      
      try {
        const response = await this.AdminApiService.addGroup(postData).toPromise();
  
        this.toastService.clear(); 
        console.log(response);
  
        if(response.status==200){
          this.ref.close('saved');
          this.toastService.add({
            key: 'su',
            closable: false,
            sticky: false,
            severity: '',
            summary: '',
            detail: `New Group ${formValue.groupName} created`,
          });
        }
        else{
          this.ref.close();
          this.toastService.clear();
          this.toastService.add({
            key: 'wrn',
            closable: false,
            sticky: false,
            severity: '',
            summary: '',
            detail: `Connection Error Try Again`,
          });
        }
      } catch (error) {
        console.log(error);
      }    
    }
  }
    else{
      this.NewGroupForm.markAllAsTouched();
    } 
 }
}
