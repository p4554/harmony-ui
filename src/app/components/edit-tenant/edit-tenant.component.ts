import { Component, OnInit, EventEmitter, Output, ChangeDetectorRef } from '@angular/core';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { FormBuilder, FormGroup, Validators, NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { format } from 'date-fns';
import { CUSTOM_CHAR_REGEX } from '@app/util/helper/patterns';
import { MessageService } from 'primeng/api';
import { LoaderService } from '@app/_services/loading.service';
import { addDuplicateRecordChecker, editDuplicateRecordChecker, editDuplicateRecordCheckerAdmin, filterExactValuesFromText, filterIncludedValuesFromArray, getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { AdminApiService } from '@app/_services/admin-api.service';

@Component({
  selector: 'app-edit-tenant',
  templateUrl: './edit-tenant.component.html',
  styleUrls: ['./edit-tenant.component.scss']
})
export class EditTenantComponent {
  region = []
  country = []
  language = [];
  region_data;
  country_data: any
  language_data: any
  editTenantForm: FormGroup;
  tenant_id: any;
  tableData: any;
  formData = new FormData();
  imageFile: any;
  imageUrl: string
  imageInputFlag = false;

  constructor(
    private ref: DynamicDialogRef,
    private fb: FormBuilder,
    private toastService: MessageService,
    private config: DynamicDialogConfig,
    public loaderService: LoaderService,
    private cdRef: ChangeDetectorRef,
    private adminApiService: AdminApiService
  ) { }
  async ngOnInit() {
    this.imageUrl = this.config.data.editData.IMG_URL
    const data = this.config.data.editData;
    this.tableData = this.config.data.tableData;
    let tenantName: any = data.TNANT_NM;
    let tenantProject: any = data.PROJ_NM;
    let tenantProductId: any = data.PROD_ID;
    this.tenant_id = data.TNANT_ID;
    this.editTenantForm = this.fb.group({
      tenantName: [tenantName, [Validators.required, Validators.pattern(CUSTOM_CHAR_REGEX)]],
      tenantRegion: [, Validators.required],
      tenantCountry: [, Validators.required],
      tenantProject: [tenantProject, [Validators.required, Validators.pattern(CUSTOM_CHAR_REGEX)]],
      tenantProductId: [tenantProductId, [Validators.required, Validators.pattern(CUSTOM_CHAR_REGEX)]],
      tenantLanguage: [, Validators.required],
      // tenantImage: ['', Validators.required]
    });
    await this.getRegionData();
    await this.getCountryData();
    await this.getLanguageData();
    this.editTenantForm.patchValue({
      tenantRegion: data.RGN_NM,
      tenantCountry: data.CNTRY_NM,
      tenantLanguage: data.LANG_NM,
      // tenantImage:data.IMG_URL
    });

  }

  async getRegionData() {
    this.region_data = await lastValueFrom(
      this.adminApiService.region()
    )
    for (let i = 0; i < this.region_data.length; i++) {
      this.region[i] = this.region_data[i].LOV_DESC
    }

  }
  async getCountryData() {
    this.country_data = await lastValueFrom(
      this.adminApiService.country()
    )
    for (let i = 0; i < this.country_data.length; i++) {
      this.country[i] = this.country_data[i].LOV_DESC
    }

  }
  async getLanguageData() {
    this.language_data = await lastValueFrom(
      this.adminApiService.language()
    )

    for (let i = 0; i < this.language_data.length; i++) {
      this.language[i] = this.language_data[i].LOV_DESC
    }
  }
  cancelButtonHandler() {
    this.ref.close();
  }

  onFileSelected(e) {
    this.imageFile = e.target.files[0];
    this.editTenantForm.get('tenantImage').setValue(this.imageFile)
  }

  imgDeleteHandler() {
    this.editTenantForm.addControl('tenantImage', this.fb.control('', [Validators.required]))
    this.imageInputFlag = true;
  }



  async onSaveHandler() {

    if (this.editTenantForm.valid) {
      let formValue = this.editTenantForm.value;
      const formProperty = ['tenantName', 'tenantCountry', 'tenantProductId']
      const tableProperty = ['TNANT_NM', 'CNTRY_NM', 'PROD_ID']
      const duplicateFound = editDuplicateRecordCheckerAdmin(formValue, formProperty, this.tableData, tableProperty, this.tenant_id, 'TNANT_ID')
      if (duplicateFound) {
        this.toastService.add({ key: 'wrn', closable: false, sticky: false, severity: '', summary: '', detail: 'Tenant Name is Exists.Try Another', });
      }
      else {
        const formData = new FormData();
        if (this.imageInputFlag
          && this.editTenantForm.contains('tenantImage')) {
          formData.append('imageReplace', 'true')
          formData.append('tenantImage', this.editTenantForm.get('tenantImage').value);
        } else {
          formData.append('imageReplace', 'false')
        };
        formData.append('existing_file_name', this.imageUrl)
        formData.append('tenant_id', this.tenant_id);
        formData.append('tenant_name', formValue.tenantName);
        formData.append('tenant_region', formValue.tenantRegion);
        formData.append('tenant_country', formValue.tenantCountry);
        formData.append('tenant_project', formValue.tenantProject);
        formData.append('tenant_productId', formValue.tenantProductId);
        formData.append('tenant_language', formValue.tenantLanguage);
        formData.append('user_name', localStorage.getItem('userName'));


        const response = await lastValueFrom(this.adminApiService.editTenant(formData))


        console.log(response);
        if (response.status == 200) {
          this.ref.close('saved');
          this.toastService.add({
            key: 'su',
            closable: false,
            sticky: false,
            severity: '',
            summary: '',
            detail: `Data Successfully Updated!`,
          });
        }
        else {
          this.ref.close();
          this.toastService.clear();
          this.toastService.add({
            key: 'wrn',
            closable: false,
            sticky: false,
            severity: '',
            summary: '',
            detail: `Connection Error Try Again`,
          });
        }
      }
    } else {
      this.editTenantForm.markAllAsTouched();
    }
  }
}
