import { Component,OnInit,EventEmitter,Output} from '@angular/core';
import { lastValueFrom } from 'rxjs';
import { AdminApiService } from '@app/_services/admin-api.service';
import {editDuplicateRecordCheckerAdmin, getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { format } from 'date-fns';
import { LoaderService } from '@app/_services/loading.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent {
  @Output() onSave: EventEmitter<any> = new EventEmitter();

  city:any 
  managerData:[]
  ReportingManger:any
  user_id;
  newUserForm: FormGroup;
  type:string;
  local:string;
  tableData: any;
  imageUrl: string
  admin:string;
  imageInputFlag = false;
  existing_file_name:any;
  constructor(
    private AdminApiService: AdminApiService,
    private fb: FormBuilder,
    private toastService: MessageService,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    public loaderService: LoaderService,
  ){}


async ngOnInit(){
  const data = this.config.data.editData;
  this.tableData = this.config.data.tableData;
  console.log('data',data)
  this.user_id=data.USER_ID;
  let first_name=data.FIRST_NM;
  let last_name=data.LAST_NM;
  let user_name=data.USER_NM;
  let user_email=data.USER_EMAIL_ID;
  let pasw=data.USER_PASSWD;
  let ldap_user_flg=data.LDAP_USER_FLG;
  let local_user_flag=data.LOCL_USER_FLG;
  let admin_flag=data.ADMN_FLG;
  let active_flag=data.ACTV_FLG;
  let report_manager=data.REPORTING_MANAGER;
  this.existing_file_name=data.IMG_URL;
  if(ldap_user_flg == true){

    this.type = 'ldap';

  }else{

    this.type = 'local';
  }

  if(local_user_flag == true){

    this.local = 'true';

  }else{

    this.local = 'false';
  }
  if(admin_flag == true){
      this.admin='true';
  }
  else{
    this.admin='false';
  }

  console.log(data.ADMN_FLG)
  this.newUserForm = this.fb.group({
    userFirstName: [first_name, Validators.required],
    userLastName: [last_name, Validators.required],
    userName: [user_name, Validators.required],
    userEmail: [user_email, 
      Validators.compose([
        Validators.required,
        Validators.pattern(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/),
      ]),
    ],
    password: [pasw, Validators.required],
    manager: [, Validators.required],
    activeUser:[active_flag,Validators.required],
    userType:[this.type,Validators.required],
    admin:[this.admin,Validators.required],
    userImage: [, Validators.required],
  });
  await this.getMangerData();

  this.newUserForm.patchValue({

    manager : data.REPORTING_MANAGER,
    
  }); 

  }
  mappedReportingManagerdata:any;
 async getMangerData(){
     this.ReportingManger=await lastValueFrom(
      this.AdminApiService.reportingManager()
     )
     console.log('Reporting',this.ReportingManger)
     this.managerData = getMappedArray(
      getUniqueValues(this.ReportingManger, 'USER_NM'),
      'label',
      'value'
    );
    this.mappedReportingManagerdata = this.ReportingManger.map(user => {
      return {
        manganame: user.USER_NM,
        managerId: user.USER_ID,
      };
    });
    } 


    cancelButtonHandler(){
      this.ref.close();
    }
    localUserFlag:any;
    ldapUserFlag:any;
    selectedManagerId:any;
    imageFile:any;

    onFileSelected(e) {
      this.imageFile = e.target.files[0];
      this.newUserForm.get('userImage').setValue(this.imageFile)
    }
    imgDeleteHandler() {
      this.newUserForm.addControl('userImage', this.fb.control('', [Validators.required]))
      this.imageInputFlag = true;
    }
  async onSaveHandler(){
    
    let formValue = this.newUserForm.value;
      if (this.newUserForm.valid){
        const formData = new FormData();
        if (formValue.userType == 'ldap') {
          this.ldapUserFlag = 'true';
          this.localUserFlag = 'false';
          formValue.userType = true;
        }
        else if (formValue.userType == 'local') {
          this.ldapUserFlag = 'false';
          this.localUserFlag = 'true';
          formValue.userType = false;
        }
        const selectedManager = this.mappedReportingManagerdata.find(user => user.manganame === formValue.manager);
        this.selectedManagerId = selectedManager.managerId;
        const formProperty = ['userName','userEmail','userType']
        const tableProperty = ['USER_NM','USER_EMAIL_ID','LDAP_USER_FLG']
        const duplicateFound = editDuplicateRecordCheckerAdmin(formValue,formProperty,this.tableData,tableProperty,this.user_id,'USER_ID')
        console.log('duplicate',duplicateFound)
        if(duplicateFound){
          this.toastService.add({key: 'wrn',closable: false,sticky: false,severity: '',summary: '',detail:'User is Already Exists.Try Another',});
        }else{
        if(formValue.userType=='ldap'){
          this.ldapUserFlag='true';
          this.localUserFlag='false';
        }
        else if(formValue.userType=='local'){
          this.ldapUserFlag='false';
          this.localUserFlag='true';
        }
        if (this.imageInputFlag
          && this.newUserForm.contains('userImage')) {
          formData.append('imageReplace', 'true')
          formData.append('userImage', this.newUserForm.get('userImage').value);
        } else {
          formData.append('imageReplace', 'false')
        };   
        formData.append('existing_file_name',this.existing_file_name);
        formData.append('reportingManager',this.selectedManagerId);
        formData.append('lastName',formValue.userLastName);
        formData.append('firstName',formValue.userFirstName);
        formData.append('ldapFlag',this.ldapUserFlag);
        formData.append('userName', formValue.userName);
        formData.append('adminFlag', formValue.admin);
        formData.append('localFlag', this.ldapUserFlag); 
        formData.append('userEmailId',formValue.userEmail); 
        formData.append('userPassword', formValue.password); 
        formData.append('activeFlag', formValue.activeUser);  
        formData.append('updatedBy',localStorage.getItem('userName'));
        formData.append('userId',this.user_id);


        await this.AdminApiService.updateUser(formData).subscribe((response)=>{
          console.log(response);
          if(response.status==200){
            this.ref.close('saved');
            this.toastService.add({
              key: 'su',
              closable: false,
              sticky: false,
              severity: '',
              summary: '',
              detail: `User update successfully`,
            });
          }
          else{
            this.ref.close();
            this.toastService.clear();
                this.toastService.add({
                  key: 'wrn',
                  closable: false,
                  sticky: false,
                  severity: '',
                  summary: '',
                  detail: `Connection Error Try Again`,
                });
          }
        }) 
      }
    }
      else{
        this.newUserForm.markAllAsTouched();
      }

    }
}
