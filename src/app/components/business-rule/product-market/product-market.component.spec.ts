import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductMarketComponent } from './product-market.component';

describe('ProductMarketComponent', () => {
  let component: ProductMarketComponent;
  let fixture: ComponentFixture<ProductMarketComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductMarketComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProductMarketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
