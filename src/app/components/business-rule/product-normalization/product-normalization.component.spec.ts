import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductNormalizationComponent } from './product-normalization.component';

describe('ProductNormalizationComponent', () => {
  let component: ProductNormalizationComponent;
  let fixture: ComponentFixture<ProductNormalizationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductNormalizationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProductNormalizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
