import { Component,ViewChild,Output,Input,EventEmitter} from '@angular/core';
import { QueryService } from '@app/_services/query.service';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import { LoaderService } from '@app/_services/loading.service';
import { BusinessRuleApiService } from '@app/_services/business-rule-api.service';
import { getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { tableFilter } from '@app/util/helper/harmonyUtils';
import { PrimeMultiSelectComponent } from '@app/components/form-components/prime-multi-select/prime-multi-select.component';
import { DialogService  } from 'primeng/dynamicdialog';
import { EditNormalizationRuleComponent} from '../../business-rule-components/edit-normalization-rule/edit-normalization-rule.component'
import { MessageService } from 'primeng/api';
@Component({
  selector: 'app-product-normalization',
  templateUrl: './product-normalization.component.html',
  styleUrls: ['./product-normalization.component.scss']
})
export class ProductNormalizationComponent {
  @ViewChild('productCodeFilter') productCodeFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('ruleNameFilter') ruleNameFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('productUnitFilter') productUnitFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('businessUnitFilter') businessUnitFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('therapeuticFilter') therapeuticFilterFiltersComponent: PrimeMultiSelectComponent;
  @ViewChild('normalFactorFilter') normalFactorFilterComponent: PrimeMultiSelectComponent;  
  @ViewChild('startDateFilter') startDateFilterComponent: PrimeMultiSelectComponent;  
  @ViewChild('endDateFilter') endDateFilterComponent: PrimeMultiSelectComponent;  

  @Output() Data: EventEmitter<{ type: string, payload: any }> = new EventEmitter<{ type: string, payload: any }>();
  @Output() ruleCountEmitter:EventEmitter<number> = new EventEmitter<number>() 

  @Input() savedData:string;


  tablePageNumber=0;
  tableData:any;
  tableDataCopy:any;
  uniqueValues:any;
  filterComponents: any;
Column={
   productCode:"PROD_CD",
   ruleName:"BR_NM",
   productUnit:"PROD_UNIT",
   businessUnit: "BU_CD",
   therapeutic: "TC_CD",
   normalFactor: "NORM_FACTR",
   startDate:"START_DATE",
   endDate:"END_DATE"
}
columnArray=[
  this.Column.productCode,
  this.Column.ruleName,
  this.Column.productUnit,
  this.Column.businessUnit,
  this.Column.therapeutic,
  this.Column.normalFactor,
  this.Column.startDate,
  this.Column.endDate,
]
activeFilter={
  productCode: { valArr:[],column:'PROD_CD'},
  ruleName: { valArr:[],column:'BR_NM'},
  productUnit:{valArr:[],column:'PROD_UNIT'},
  businessUnit: { valArr:[],column:'BU_CD'},
  therapeutic:{ valArr:[],column:'TC_CD'},
  normalFactor:{ valArr:[],column:'NORM_FACTR'},
  startDate:{ valArr:[],column:'START_DATE'},
  endDate:{ valArr:[],column:'END_DATE'}
}
  constructor(
    private BusinessRuleApiService: BusinessRuleApiService,
    public loaderService: LoaderService,
    private queryService: QueryService,
    private dialogService: DialogService,
    private toastService: MessageService,
  ) { }
  ngOnChanges(){

    if(this.savedData=='savedNormal'){
     this.getResponseHandler();
    }
  }
  async ngOnInit() {
    this.loaderService.showLoader();

      await this.getResponseHandler();

   
   }
   async getResponseHandler(){
    this.tableData = await lastValueFrom(
      this.BusinessRuleApiService.normalizationFactorTable()
    );
    console.log(this.tableData)
     this.tableData.reverse();
    this.tableDataCopy=[...this.tableData]
    this.tablePageNumber = 0
    this.uniqueValues=this.getDistinctValuesForColumns(this.tableData,this.columnArray)
    this.uniqueValues=this.getDistinctValuesForColumns(this.tableData,this.columnArray)
     setTimeout(() => {
      this.filterComponents = [
        { selected: 'productCodeFilterSelected', component: this.productCodeFilterComponent },
        { selected: 'ruleNameFilterSelected', component: this.ruleNameFilterComponent },
        { selected: 'productUnitFilterSelected', component: this.productUnitFilterComponent },
        { selected: 'businessUnitFilterSelected', component: this.businessUnitFilterComponent },
        { selected: 'therapeuticFiltersSelected', component: this.therapeuticFilterFiltersComponent },
        { selected: 'normalFactorFilterSelected', component: this.normalFactorFilterComponent },
        { selected: 'startDateFilterSelected', component: this.startDateFilterComponent },
        { selected: 'endDateFilterSelected', component: this.endDateFilterComponent },
      ];
    }, 300);
    if (this.activeFilter) {
      this.tableData = tableFilter(this.activeFilter,this.tableDataCopy);
    }
    this.loaderService.hideLoader();
    this.Data.emit({type:'market',payload:this.tableDataCopy})
   this.ruleCountEmitter.emit( this.tableData.length);
   }
   getDistinctValuesForColumns = (tableData: any[], columns: string[]) => {
		const result: any = [];
		columns.forEach((column) => {
		  const uniqueValues = getUniqueValues(tableData, column);
		  const mappedArray = getMappedArray(uniqueValues);
		  result[column] = [...mappedArray];
		});
		return result;
	  };
    filterChangeHandler(filterArray, column) {
      const filterArrayCopy = filterArray;
      if (column === 'PROD_CD') {
        this.activeFilter.productCode.valArr = filterArrayCopy;
      }
      else if (column === 'BR_NM') {
        this.activeFilter.ruleName.valArr = filterArrayCopy;
      }
      else if (column === 'PROD_UNIT') {
        this.activeFilter.productUnit.valArr = filterArrayCopy;
      }
      else if (column === 'BU_CD') {
        this.activeFilter.businessUnit.valArr = filterArrayCopy;
      }
      else if (column === 'TC_CD') {
        this.activeFilter.therapeutic.valArr = filterArrayCopy;
      }
      else if (column === 'NORM_FACTR') {
        this.activeFilter.normalFactor.valArr = filterArrayCopy;
      }
      else if (column === 'START_DATE') {
        this.activeFilter.startDate.valArr = filterArrayCopy;
      }
      else if (column === 'END_DATE') {
        this.activeFilter.endDate.valArr = filterArrayCopy;
      }
      const result=tableFilter(this.activeFilter,this.tableDataCopy)
  
      if (result.length){
      this.tableData=result
      this.tablePageNumber = 0
     }
     else{
      if(!Object.keys(this.activeFilter).some(field=>this.activeFilter[field].valArr.length)){
        this.tableData = this.tableDataCopy
      }else {
        this.tableData = result
      }
     }
     this.setFilterOptions(this.activeFilter,result)  
     this.ruleCountEmitter.emit( this.tableData.length);

    }
    setFilterOptions(fieldObject,result){

      const filterFields = Object.keys(fieldObject)
    
      for(let field of filterFields){
        if(!fieldObject[field].valArr.length){
        this.uniqueValues[fieldObject[field].column] = getMappedArray(getUniqueValues(result, fieldObject[field].column));
        }
      }
      }
      resetAll() {
        this.filterComponents.forEach((item) => {
          item.component.resetSelection();
        });
        this.tableData = this.tableDataCopy;
        this.activatedFilterReset(this.activeFilter)
        this.uniqueValues = this.getDistinctValuesForColumns(this.tableData, this.columnArray);
        this.ruleCountEmitter.emit( this.tableData.length);
      }
      activatedFilterReset(fieldObject:any){
        const filterFields = Object.keys(fieldObject)
        filterFields.forEach(key=> fieldObject[key].valArr = [])
      }
  
      exportExcel(){
        
      }
  editHandler(value:string){
    this.loaderService.showLoader();

    const dialogRef = this.dialogService.open(EditNormalizationRuleComponent, {
      header: 'Edit Normalization Factor Rule',
      width: '60%',
      height: '80%',
      data: {
        editData: value,
        tableData: this.tableDataCopy
      },
    });
    dialogRef.onClose.subscribe(result => {
      if (result === 'saved') {
        this.getResponseHandler();
      }
    });
    window.addEventListener('popstate', () => {
      dialogRef.close();
    });
  }
  deletePopupModel: any;
  tableSelectedRowIndex: number;
  deleteHandler(index: number) {
    this.deletePopupModel = {
      displayModal: true,
      header: 'Delete Normalization Factor Rule',
      icon: 'pi-exclamation-triangle',
      content: {
        header: `Normalization Factor Rule`,
        text: 'This action will delete the product permanently',
      },
      deleteBtnProps: {
        label: 'Delete',
        click: '',
      },
      cancelBtnProps: {
        label: 'Cancel',
        click: '',
      },
    };

    this.tableSelectedRowIndex = index;
  }
  deletePopupDeleteHandler = () => {
    let brProductId = this.tableData[this.tableSelectedRowIndex].BR_PROD_ID;
    const postData = {
  
      productId:brProductId
    };
  this.deletePopupModel.displayModal = false;
  this.loaderService.showLoader();
  this.BusinessRuleApiService.deleteNormalizationFactor(postData).subscribe((response) => {
      this.loaderService.hideLoader();
      if (response.status == 200) {
        this.toastService.clear();
        this.toastService.add({
          key: 'err',
          closable: false,
          sticky: false,
          severity: '',
          summary: '',
          detail: `Normalization Factor Rule Deleted Successfully`,
        });
        this.getResponseHandler();
      } else {
        console.log(response);
        this.toastService.clear();
        this.toastService.add({
          key: 'wrn',
          closable: false,
          sticky: false,
          severity: '',
          summary: '',
          detail: `Connection Error TryAgain`,
        });
      }
    });
  };
}
