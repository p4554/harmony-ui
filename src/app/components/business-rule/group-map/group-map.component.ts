import { Component,ViewChild,Output,Input,EventEmitter} from '@angular/core';
import { QueryService } from '@app/_services/query.service';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import { LoaderService } from '@app/_services/loading.service';
import { BusinessRuleApiService } from '@app/_services/business-rule-api.service';
import { getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { tableFilter } from '@app/util/helper/harmonyUtils';
import { PrimeMultiSelectComponent } from '@app/components/form-components/prime-multi-select/prime-multi-select.component';
import { DialogService } from 'primeng/dynamicdialog';
import {EditGroupMapRuleComponent} from '../../business-rule-components/edit-group-map-rule/edit-group-map-rule.component';
import { MessageService } from 'primeng/api';
@Component({
  selector: 'app-group-map',
  templateUrl: './group-map.component.html',
  styleUrls: ['./group-map.component.scss']
})
export class GroupMapComponent {
  @ViewChild('groupFilter') groupFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('codeFilter') codeFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('ruleNameFilter') ruleNameFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('businessUnitFilter') businessUnitFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('businessProcessFilter') businessProcessFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('ruleTypeFilter') ruleTypeFilterComponent: PrimeMultiSelectComponent; 
  @ViewChild('functionAreaFilter') functionAreaFilterComponent: PrimeMultiSelectComponent;   
  @ViewChild('startDateFilter') startDateFilterComponent: PrimeMultiSelectComponent;  
  @ViewChild('endDateFilter') endDateFilterComponent: PrimeMultiSelectComponent;  
  @ViewChild('statusFilter') statusFilterComponent: PrimeMultiSelectComponent;
  @Output() Data: EventEmitter<{ type: string, payload: any }> = new EventEmitter<{ type: string, payload: any }>();
  @Input() savedData:string;

  tablePageNumber=0;
  tableData:any;
  tableDataCopy:any;
  uniqueValues:any;
  filterComponents: any;
Column={
   group:"SPCLTY_GRP",
   code:"SPCLTY_CD",
   ruleName: "BR_NM",
   businessUnit: "BU_CD",
   businessProcess: "FUNC_AREA_CD",
   ruleType:"ACTION",
   functionArea:"FUNC_AREA_CD",
   startDate:"START_DATE",
   endDate:"END_DATE",
   status:"RULE_STATUS"
}
columnArray=[
  this.Column.group,
  this.Column.code,
  this.Column.ruleName,
  this.Column.businessUnit,
  this.Column.businessProcess,
  this.Column.ruleType,
  this.Column.functionArea,
  this.Column.startDate,
  this.Column.endDate,
  this.Column.status,
]
activeFilter={
  group: { valArr:[],column:'SPCLTY_GRP'},
  code: { valArr:[],column:'SPCLTY_CD'},
  ruleName: { valArr:[],column:'BR_NM'},
  businessUnit:{ valArr:[],column:'BU_CD'},
  businessProcess:{ valArr:[],column:'FUNC_AREA_CD'},
  ruleType:{valArr:[],column:'ACTION'},
  functionArea: { valArr:[],column:'FUNC_AREA_CD'},
  startDate:{ valArr:[],column:'START_DATE'},
  endDate:{ valArr:[],column:'END_DATE'},
  status:{ valArr:[],column:'RULE_STATUS'}
}
  constructor(
    private BusinessRuleApiService: BusinessRuleApiService,
    public loaderService: LoaderService,
    private queryService: QueryService,
    private dialogService: DialogService,
    private toastService: MessageService,

  ) { }
  ngOnChanges(){

    if(this.savedData=='savedGroupMapRule'){
     this.getResponseHandler();
    }
  }
  async ngOnInit() {
    this.loaderService.showLoader();

      await this.getResponseHandler();
   
   }
   async getResponseHandler(){
    this.tableData = await lastValueFrom(
      this.BusinessRuleApiService.groupMapTable()
    );
    console.log(this.tableData)
    this.tableDataCopy=[...this.tableData]
    this.tablePageNumber = 0
    this.uniqueValues=this.getDistinctValuesForColumns(this.tableData,this.columnArray)
    setTimeout(() => {
     this.filterComponents = [
       { selected: 'groupFilterSelected', component: this.groupFilterComponent },
       { selected: 'codeFilterSelected', component: this.codeFilterComponent },
       { selected: 'ruleNameFilterSelected', component: this.ruleNameFilterComponent },
       { selected: 'businessUnitFilterSelected', component: this.businessUnitFilterComponent },
       { selected: 'businessProcessFilterSelected', component: this.businessProcessFilterComponent },
       { selected: 'ruleTypeFilterSelected', component: this.ruleTypeFilterComponent },
       { selected: 'functionAreaFilterSelected', component: this.functionAreaFilterComponent },
       { selected: 'startDateFilterSelected', component: this.startDateFilterComponent },
       { selected: 'endDateFilterSelected', component: this.endDateFilterComponent },
       { selected: 'statusFilterSelected', component: this.statusFilterComponent },
     ];
   }, 300);
   
   if (this.activeFilter) {
     this.tableData = tableFilter(this.activeFilter,this.tableDataCopy);
   }
   this.loaderService.hideLoader();   
   this.Data.emit({type:'groupMap',payload:this.tableDataCopy})

  }
   getDistinctValuesForColumns = (tableData: any[], columns: string[]) => {
		const result: any = [];
		columns.forEach((column) => {
		  const uniqueValues = getUniqueValues(tableData, column);
		  const mappedArray = getMappedArray(uniqueValues);
		  result[column] = [...mappedArray];
		});
		return result;
	  };
    filterChangeHandler(filterArray, column) {
      const filterArrayCopy = filterArray;
      if (column === 'SPCLTY_GRP') {
        this.activeFilter.group.valArr = filterArrayCopy;
      }
      else if (column === 'SPCLTY_CD') {
        this.activeFilter.code.valArr = filterArrayCopy;
      }
      else if (column === 'BR_NM') {
        this.activeFilter.ruleName.valArr = filterArrayCopy;
      }
      else if (column === 'BU_CD') {
        this.activeFilter.businessUnit.valArr = filterArrayCopy;
      }
      else if (column === 'FUNC_AREA_CD') {
        this.activeFilter.businessProcess.valArr = filterArrayCopy;
      }
      else if (column === 'ACTION') {
        this.activeFilter.ruleType.valArr = filterArrayCopy;
      }
      else if (column === 'FUNC_AREA_CD') {
        this.activeFilter.functionArea.valArr = filterArrayCopy;
      }
      else if (column === 'START_DATE') {
        this.activeFilter.startDate.valArr = filterArrayCopy;
      }
      else if (column === 'END_DATE') {
        this.activeFilter.endDate.valArr = filterArrayCopy;
      } 
      else if (column === 'RULE_STATUS') {
        this.activeFilter.status.valArr = filterArrayCopy;
      } 
      const result=tableFilter(this.activeFilter,this.tableDataCopy)
  
      if (result.length){
      this.tableData=result
      this.tablePageNumber = 0
     }
     else{
      if(!Object.keys(this.activeFilter).some(field=>this.activeFilter[field].valArr.length)){
        this.tableData = this.tableDataCopy
      }else {
        this.tableData = result
      }
     }
     this.setFilterOptions(this.activeFilter,result)  
    }
    setFilterOptions(fieldObject,result){

      const filterFields = Object.keys(fieldObject)
    
      for(let field of filterFields){
        if(!fieldObject[field].valArr.length){
        this.uniqueValues[fieldObject[field].column] = getMappedArray(getUniqueValues(result, fieldObject[field].column));
        }
      }
      }
      resetAll() {
        this.filterComponents.forEach((item) => {
          item.component.resetSelection();
        });
        this.tableData = this.tableDataCopy;
        this.activatedFilterReset(this.activeFilter)
        this.uniqueValues = this.getDistinctValuesForColumns(this.tableData, this.columnArray);
      }
      activatedFilterReset(fieldObject:any){
        const filterFields = Object.keys(fieldObject)
        filterFields.forEach(key=> fieldObject[key].valArr = [])
      }
  
      exportExcel(){
        
      }
  editHandler(value:string){
    this.loaderService.showLoader();

    const dialogRef = this.dialogService.open(EditGroupMapRuleComponent, {
      header: 'Edit Specialty Group Map  Rule',
      width: '60%',
      height: '80%',
      data: {
        editData: value,
        tableData: this.tableDataCopy
      },
    });
    dialogRef.onClose.subscribe(result => {
      if (result === 'saved') {
        this.getResponseHandler();
      }
    });
    window.addEventListener('popstate', () => {
      dialogRef.close();
    });
  }
  deletePopupModel: any;
  tableSelectedRowIndex: number;
  deleteHandler(index: number) {
    this.deletePopupModel = {
      displayModal: true,
      header: 'Delete Specialty Group Map Rule',
      icon: 'pi-exclamation-triangle',
      content: {
        header: `Specialty Group Map Rule`,
        text: 'This action will delete the specialty permanently',
      },
      deleteBtnProps: {
        label: 'Delete',
        click: '',
      },
      cancelBtnProps: {
        label: 'Cancel',
        click: '',
      },
    };

    this.tableSelectedRowIndex = index;
  }
  deletePopupDeleteHandler = () => {
    let brSpecialtyId = this.tableData[this.tableSelectedRowIndex].BR_SPCLTY_ID;
    const postData = {
      specialtyId:brSpecialtyId
    }; 
    let deleteData = {
      query: `delete from HRMNY_DB.HRMNY_CNF_ZN.BR_SPCLTY where BR_SPCLTY_ID =${brSpecialtyId}`,
      flag: 'd',
    };
    this.deletePopupModel.displayModal = false;
    this.loaderService.showLoader();
    this.BusinessRuleApiService.deleteGroupMap(postData).subscribe((response) => {
      this.loaderService.hideLoader();
      if (response.status == 200) {
        this.toastService.clear();
        this.toastService.add({
          key: 'err',
          closable: false,
          sticky: false,
          severity: '',
          summary: '',
          detail: `Specialty Group Map Rule Deleted Successfully`,
        });
        this.getResponseHandler();
      } else {
        console.log(response);
        this.toastService.clear();
        this.toastService.add({
          key: 'wrn',
          closable: false,
          sticky: false,
          severity: '',
          summary: '',
          detail: `Connection Error TryAgain`,
        });
      }
    });
  };
}
