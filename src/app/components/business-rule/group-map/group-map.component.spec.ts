import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupMapComponent } from './group-map.component';

describe('GroupMapComponent', () => {
  let component: GroupMapComponent;
  let fixture: ComponentFixture<GroupMapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupMapComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GroupMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
