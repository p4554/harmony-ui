import { Component,ViewChild,Output,EventEmitter,Input} from '@angular/core';
import { QueryService } from '@app/_services/query.service';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import { LoaderService } from '@app/_services/loading.service';
import { BusinessRuleApiService } from '@app/_services/business-rule-api.service';
import { getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { tableFilter } from '@app/util/helper/harmonyUtils';
import { PrimeMultiSelectComponent } from '@app/components/form-components/prime-multi-select/prime-multi-select.component';
import { DialogService  } from 'primeng/dynamicdialog';
import { EditCustomerTerritoryRuleComponent} from '../../business-rule-components/edit-customer-territory-rule/edit-customer-territory-rule.component'
import { MessageService } from 'primeng/api';
@Component({
  selector: 'app-customer-territory',
  templateUrl: './customer-territory.component.html',
  styleUrls: ['./customer-territory.component.scss']
})
export class CustomerTerritoryComponent {
  @ViewChild('customerIdFilter') customerIdFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('ruleNameFilter') ruleNameFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('businessUnitFilter') businessUnitFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('territoryIdFilter') territoryIdFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('cycleFilter') cycleFilterComponent: PrimeMultiSelectComponent;  
  @ViewChild('startDateFilter') startDateFilterComponent: PrimeMultiSelectComponent;  
  @ViewChild('endDateFilter') endDateFilterComponent: PrimeMultiSelectComponent;  
  @ViewChild('actionFilter') actionFilterComponent: PrimeMultiSelectComponent;
  @Output() Data: EventEmitter<{ type: string, payload: any }> = new EventEmitter<{ type: string, payload: any }>();
  @Input() savedData:string;

  tablePageNumber=0;
  tableData:any;
  tableDataCopy:any;
  uniqueValues:any;
  filterComponents: any;
Column={
   customerId:"CUST_ID",
   ruleName:"CUST_BR_NM",
   businessUnit: "CUST_BU_CD",
   territoryId: "CUST_TERR_ID",
   cycle: "CUST_ALIGN_CYCLE",
   startDate:"CUST_START_DATE",
   endDate:"CUST_END_DATE",
   action:"CUST_RULE_STATUS",
}
columnArray=[
  this.Column.customerId,
  this.Column.ruleName,
  this.Column.businessUnit,
  this.Column.territoryId,
  this.Column.cycle,
  this.Column.startDate,
  this.Column.endDate,
  this.Column.action,
]
activeFilter={
  customerId: { valArr:[],column:'CUST_ID'},
  ruleName: { valArr:[],column:'CUST_BR_NM'},
  businessUnit: { valArr:[],column:'CUST_BU_CD'},
  territoryId:{ valArr:[],column:'CUST_TERR_ID'},
  cycle:{ valArr:[],column:'CUST_ALIGN_CYCLE'},
  startDate:{ valArr:[],column:'CUST_START_DATE'},
  endDate:{ valArr:[],column:'CUST_END_DATE'},
  action:{valArr:[],column:'CUST_RULE_STATUS'},
}
  constructor(
    private BusinessRuleApiService: BusinessRuleApiService,
    public loaderService: LoaderService,
    private queryService: QueryService,
    private dialogService: DialogService,
    private toastService: MessageService,

  ) { }
  ngOnChanges(){

    if(this.savedData=='savedCustomer'){
     this.getResponseHandler();
    }
  }
  async ngOnInit() {
    this.loaderService.showLoader();

      await this.getResponseHandler();

   }
   async getResponseHandler(){
    this.tableData = await lastValueFrom(
      this.BusinessRuleApiService.customerTerritoryTable()
    );
    console.log(this.tableData)
    this.tableDataCopy=[...this.tableData]
    this.tablePageNumber = 0
    this.uniqueValues=this.getDistinctValuesForColumns(this.tableData,this.columnArray)
    setTimeout(() => {
     this.filterComponents = [
       { selected: 'customerIdFilterSelected', component: this.customerIdFilterComponent },
       { selected: 'ruleNameFilterSelected', component: this.ruleNameFilterComponent },
       { selected: 'businessUnitFilterSelected', component: this.businessUnitFilterComponent },
       { selected: 'territoryIdFilterSelected', component: this.territoryIdFilterComponent },
       { selected: 'cycleFilterSelected', component: this.cycleFilterComponent },
       { selected: 'startDateFilterSelected', component: this.startDateFilterComponent },
       { selected: 'endDateFilterSelected', component: this.endDateFilterComponent },
       { selected: 'actionFilterSelected', component: this.actionFilterComponent },
     ];
   }, 300);
   
   if (this.activeFilter) {
     this.tableData = tableFilter(this.activeFilter,this.tableDataCopy);
   }
   this.Data.emit({type:'customer',payload:this.tableDataCopy})
   this.loaderService.hideLoader(); 
  }
   getDistinctValuesForColumns = (tableData: any[], columns: string[]) => {
		const result: any = [];
		columns.forEach((column) => {
		  const uniqueValues = getUniqueValues(tableData, column);
		  const mappedArray = getMappedArray(uniqueValues);
		  result[column] = [...mappedArray];
		});
		return result;
	  };
    filterChangeHandler(filterArray, column) {
      const filterArrayCopy = filterArray;
      if (column === 'CUST_ID') {
        this.activeFilter.customerId.valArr = filterArrayCopy;
      }
      else if (column === 'CUST_BR_NM') {
        this.activeFilter.ruleName.valArr = filterArrayCopy;
      }
      else if (column === 'CUST_BU_CD') {
        this.activeFilter.businessUnit.valArr = filterArrayCopy;
      }
      else if (column === 'CUST_TERR_ID') {
        this.activeFilter.territoryId.valArr = filterArrayCopy;
      }
      else if (column === 'CUST_ALIGN_CYCLE') {
        this.activeFilter.cycle.valArr = filterArrayCopy;
      }
      else if (column === 'CUST_START_DATE') {
        this.activeFilter.startDate.valArr = filterArrayCopy;
      }
      else if (column === 'CUST_END_DATE') {
        this.activeFilter.endDate.valArr = filterArrayCopy;
      } else if (column === 'CUST_RULE_STATUS') {
        this.activeFilter.action.valArr = filterArrayCopy;
      }
      const result=tableFilter(this.activeFilter,this.tableDataCopy)
  
      if (result.length){
      this.tableData=result
      this.tablePageNumber = 0
     }
     else{
      if(!Object.keys(this.activeFilter).some(field=>this.activeFilter[field].valArr.length)){
        this.tableData = this.tableDataCopy
      }else {
        this.tableData = result
      }
     }
     this.setFilterOptions(this.activeFilter,result)  
    }
    setFilterOptions(fieldObject,result){

      const filterFields = Object.keys(fieldObject)
    
      for(let field of filterFields){
        if(!fieldObject[field].valArr.length){
        this.uniqueValues[fieldObject[field].column] = getMappedArray(getUniqueValues(result, fieldObject[field].column));
        }
      }
      }
      resetAll() {
        this.filterComponents.forEach((item) => {
          item.component.resetSelection();
        });
        this.tableData = this.tableDataCopy;
        this.activatedFilterReset(this.activeFilter)
        this.uniqueValues = this.getDistinctValuesForColumns(this.tableData, this.columnArray);
      }
      activatedFilterReset(fieldObject:any){
        const filterFields = Object.keys(fieldObject)
        filterFields.forEach(key=> fieldObject[key].valArr = [])
      }
  
      exportExcel(){
        
      }
  editHandler(value:string){
    this.loaderService.showLoader();
    const dialogRef = this.dialogService.open(EditCustomerTerritoryRuleComponent, {
      header: 'Edit Customer Territory Alignment Rule',
      width: '60%',
      height: '80%',
      data: {
        editData: value,
        tableData: this.tableDataCopy
      },
    });
    dialogRef.onClose.subscribe(result => {
      if (result === 'saved') {
        this.getResponseHandler();
      }
    });
    window.addEventListener('popstate', () => {
      dialogRef.close();
    });
  }
  deletePopupModel: any;
  tableSelectedRowIndex: number;
  deleteHandler(index: number) {
    this.deletePopupModel = {
      displayModal: true,
      header: 'Delete Customer Territory Alignment Rule',
      icon: 'pi-exclamation-triangle',
      content: {
        header: `Customer Territory Alignment Rule`,
        text: 'This action will delete the alignment permanently',
      },
      deleteBtnProps: {
        label: 'Delete',
        click: '',
      },
      cancelBtnProps: {
        label: 'Cancel',
        click: '',
      },
    };

    this.tableSelectedRowIndex = index;
  }
  deletePopupDeleteHandler = () => {
    let brAlignmentId = this.tableData[this.tableSelectedRowIndex].BR_ALIGN_ID;
    const postData = {
      alignmentId:brAlignmentId
    };
    this.deletePopupModel.displayModal = false;
    this.loaderService.showLoader();
    this.BusinessRuleApiService.deleteCustomerTerritory(postData).subscribe((response) => {
      this.loaderService.hideLoader();
      if (response.status == 200) {
        this.toastService.clear();
        this.toastService.add({
          key: 'err',
          closable: false,
          sticky: false,
          severity: '',
          summary: '',
          detail: `Customer Territory Alignment Rule Deleted Successfully`,
        });
        this.getResponseHandler();
      } else {
        console.log(response);
        this.toastService.clear();
        this.toastService.add({
          key: 'wrn',
          closable: false,
          sticky: false,
          severity: '',
          summary: '',
          detail: `Connection Error TryAgain`,
        });
      }
    });
  };
}
