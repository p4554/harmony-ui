import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerTerritoryComponent } from './customer-territory.component';

describe('CustomerTerritoryComponent', () => {
  let component: CustomerTerritoryComponent;
  let fixture: ComponentFixture<CustomerTerritoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomerTerritoryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CustomerTerritoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
