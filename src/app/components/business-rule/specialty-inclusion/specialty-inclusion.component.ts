import { Component,ViewChild,Output,EventEmitter,Input} from '@angular/core';
import { QueryService } from '@app/_services/query.service';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import { LoaderService } from '@app/_services/loading.service';
import { BusinessRuleApiService } from '@app/_services/business-rule-api.service';
import { getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { tableFilter } from '@app/util/helper/harmonyUtils';
import { PrimeMultiSelectComponent } from '@app/components/form-components/prime-multi-select/prime-multi-select.component';
import { DialogService  } from 'primeng/dynamicdialog';
import {EditInclusionRuleComponent} from '../../business-rule-components/edit-inclusion-rule/edit-inclusion-rule.component'
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-specialty-inclusion',
  templateUrl: './specialty-inclusion.component.html',
  styleUrls: ['./specialty-inclusion.component.scss']
})
export class SpecialtyInclusionComponent {
  @ViewChild('typeFilter') typeFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('codeFilter') codeFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('ruleNameFilter') ruleNameFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('businessUnitFilter') businessUnitFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('businessProcessFilter') businessProcessFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('ruleTypeFilter') ruleTypeFilterComponent: PrimeMultiSelectComponent; 
  @ViewChild('functionAreaFilter') functionAreaFilterComponent: PrimeMultiSelectComponent;   
  @ViewChild('startDateFilter') startDateFilterComponent: PrimeMultiSelectComponent;  
  @ViewChild('endDateFilter') endDateFilterComponent: PrimeMultiSelectComponent;  
  @ViewChild('statusFilter') statusFilterComponent: PrimeMultiSelectComponent;
  @Output() Data: EventEmitter<{ type: string, payload: any }> = new EventEmitter<{ type: string, payload: any }>();
  @Input() savedData:string;
  
  tablePageNumber=0;
  tableData:any;
  tableDataCopy:any;
  uniqueValues:any;
  filterComponents: any;
Column={
   type:"SPCLTY_TYP",
   code:"INC_SPCLTY_CD",
   ruleName: "INC_BR_NM",
   businessUnit: "INC_BU_CD",
   businessProcess: "INC_FUNC_AREA_CD",
   ruleType:"INC_ACTION",
   functionArea:"INC_FUNC_AREA_CD",
   startDate:"INC_START_DATE",
   endDate:"INC_END_DATE",
   status:"INC_RULE_STATUS"
}
columnArray=[
  this.Column.type,
  this.Column.code,
  this.Column.ruleName,
  this.Column.businessUnit,
  this.Column.businessProcess,
  this.Column.ruleType,
  this.Column.functionArea,
  this.Column.startDate,
  this.Column.endDate,
  this.Column.status,
]
activeFilter={
  type: { valArr:[],column:'SPCLTY_TYP'},
  code: { valArr:[],column:'INC_SPCLTY_CD'},
  ruleName: { valArr:[],column:'INC_BR_NM'},
  businessUnit:{ valArr:[],column:'INC_BU_CD'},
  businessProcess:{ valArr:[],column:'INC_FUNC_AREA_CD'},
  ruleType:{valArr:[],column:'INC_ACTION'},
  functionArea: { valArr:[],column:'INC_FUNC_AREA_CD'},
  startDate:{ valArr:[],column:'INC_START_DATE'},
  endDate:{ valArr:[],column:'INC_END_DATE'},
  status:{ valArr:[],column:'INC_RULE_STATUS'}
}
  constructor(
    private BusinessRuleApiService: BusinessRuleApiService,
    public loaderService: LoaderService,
    private queryService: QueryService,
    private dialogService: DialogService,
    private toastService: MessageService,

  ) { }
  ngOnChanges(){

    if(this.savedData=='savedInclusions'){
     this.getResponseHandler();
    }
  }
  async ngOnInit() {
    this.loaderService.showLoader();

      await this.getResponseHandler();

   
   }
   async getResponseHandler(){
    this.tableData = await lastValueFrom(
      this.BusinessRuleApiService.inclusionTable()
    );
    console.log(this.tableData)
    this.tableDataCopy=[...this.tableData]
    this.tablePageNumber = 0
    this.uniqueValues=this.getDistinctValuesForColumns(this.tableData,this.columnArray)
    setTimeout(() => {
     this.filterComponents = [
       { selected: 'typeFilterSelected', component: this.typeFilterComponent },
       { selected: 'codeFilterSelected', component: this.codeFilterComponent },
       { selected: 'ruleNameFilterSelected', component: this.ruleNameFilterComponent },
       { selected: 'businessUnitFilterSelected', component: this.businessUnitFilterComponent },
       { selected: 'businessProcessFilterSelected', component: this.businessProcessFilterComponent },
       { selected: 'ruleTypeFilterSelected', component: this.ruleTypeFilterComponent },
       { selected: 'functionAreaFilterSelected', component: this.functionAreaFilterComponent },
       { selected: 'startDateFilterSelected', component: this.startDateFilterComponent },
       { selected: 'endDateFilterSelected', component: this.endDateFilterComponent },
       { selected: 'statusFilterSelected', component: this.statusFilterComponent },
     ];
   }, 300);
   
   if (this.activeFilter) {
     this.tableData = tableFilter(this.activeFilter,this.tableDataCopy);
   }
   this.loaderService.hideLoader();  
   this.Data.emit({type:'inclusion',payload:this.tableDataCopy})
  }
   getDistinctValuesForColumns = (tableData: any[], columns: string[]) => {
		const result: any = [];
		columns.forEach((column) => {
		  const uniqueValues = getUniqueValues(tableData, column);
		  const mappedArray = getMappedArray(uniqueValues);
		  result[column] = [...mappedArray];
		});
		return result;
	  };
    filterChangeHandler(filterArray, column) {
      const filterArrayCopy = filterArray;
      if (column === 'SPCLTY_TYP') {
        this.activeFilter.type.valArr = filterArrayCopy;
      }
      else if (column === 'INC_SPCLTY_CD') {
        this.activeFilter.code.valArr = filterArrayCopy;
      }
      else if (column === 'INC_BR_NM') {
        this.activeFilter.ruleName.valArr = filterArrayCopy;
      }
      else if (column === 'INC_BU_CD') {
        this.activeFilter.businessUnit.valArr = filterArrayCopy;
      }
      else if (column === 'INC_FUNC_AREA_CD') {
        this.activeFilter.businessProcess.valArr = filterArrayCopy;
      }
      else if (column === 'INC_ACTION') {
        this.activeFilter.ruleType.valArr = filterArrayCopy;
      }
      else if (column === 'INC_FUNC_AREA_CD') {
        this.activeFilter.functionArea.valArr = filterArrayCopy;
      }
      else if (column === 'INC_START_DATE') {
        this.activeFilter.startDate.valArr = filterArrayCopy;
      }
      else if (column === 'INC_END_DATE') {
        this.activeFilter.endDate.valArr = filterArrayCopy;
      } 
      else if (column === 'INC_RULE_STATUS') {
        this.activeFilter.status.valArr = filterArrayCopy;
      } 
      const result=tableFilter(this.activeFilter,this.tableDataCopy)
      console.log('result',result)
      if (result.length){
      this.tableData=result
      this.tablePageNumber = 0
     }
     else{
      if(!Object.keys(this.activeFilter).some(field=>this.activeFilter[field].valArr.length)){
        this.tableData = this.tableDataCopy
      }else {
        this.tableData = result
      }
     }
     this.setFilterOptions(this.activeFilter,result)  
    }
    setFilterOptions(fieldObject,result){

      const filterFields = Object.keys(fieldObject)
    
      for(let field of filterFields){
        if(!fieldObject[field].valArr.length){
        this.uniqueValues[fieldObject[field].column] = getMappedArray(getUniqueValues(result, fieldObject[field].column));
        }
      }
      }
      resetAll() {
        this.filterComponents.forEach((item) => {
          item.component.resetSelection();
        });
        this.tableData = this.tableDataCopy;
        this.activatedFilterReset(this.activeFilter)
        this.uniqueValues = this.getDistinctValuesForColumns(this.tableData, this.columnArray);
      }
      activatedFilterReset(fieldObject:any){
        const filterFields = Object.keys(fieldObject)
        filterFields.forEach(key=> fieldObject[key].valArr = [])
      }
  
      
  editHandler(value:string){
    this.loaderService.showLoader();

    const dialogRef = this.dialogService.open(EditInclusionRuleComponent, {
      header: 'Edit Specialty Inclusion Rule',
      width: '60%',
      height: '80%',
      data: {
        editData: value,
        tableData: this.tableDataCopy
      },
    });
    dialogRef.onClose.subscribe(result => {
      if (result === 'saved') {
        this.getResponseHandler();
      }
    });
    window.addEventListener('popstate', () => {
      dialogRef.close();
    });
  }
  deletePopupModel: any;
  tableSelectedRowIndex: number;
  deleteHandler(index: number) {
    this.deletePopupModel = {
      displayModal: true,
      header: 'Delete Specialty Inclusion Rule',
      icon: 'pi-exclamation-triangle',
      content: {
        header: `Specialty Inclusion Rule`,
        text: 'This action will delete the specialty permanently',
      },
      deleteBtnProps: {
        label: 'Delete',
        click: '',
      },
      cancelBtnProps: {
        label: 'Cancel',
        click: '',
      },
    };

    this.tableSelectedRowIndex = index;
  }
  deletePopupDeleteHandler = () => {
    let brSpecialtyId = this.tableData[this.tableSelectedRowIndex].BR_SPCLTY_ID;
    const postData = {
      specialtyId:brSpecialtyId
    };   
    this.deletePopupModel.displayModal = false;
    this.loaderService.showLoader();
    this.BusinessRuleApiService.deleteSpecialtyInclusion(postData).subscribe((response) => {
      this.loaderService.hideLoader();
      if (response.status == 200) {
        this.toastService.clear();
        this.toastService.add({
          key: 'err',
          closable: false,
          sticky: false,
          severity: '',
          summary: '',
          detail: `Specialty Inclusion Rule Deleted Successfully`,
        });
        this.getResponseHandler();
      } else {
        console.log(response);
        this.toastService.clear();
        this.toastService.add({
          key: 'wrn',
          closable: false,
          sticky: false,
          severity: '',
          summary: '',
          detail: `Connection Error TryAgain`,
        });
      }
    });
  };
}
