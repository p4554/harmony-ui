import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecialtyInclusionComponent } from './specialty-inclusion.component';

describe('SpecialtyInclusionComponent', () => {
  let component: SpecialtyInclusionComponent;
  let fixture: ComponentFixture<SpecialtyInclusionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpecialtyInclusionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SpecialtyInclusionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
