import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZipTerritoryComponent } from './zip-territory.component';

describe('ZipTerritoryComponent', () => {
  let component: ZipTerritoryComponent;
  let fixture: ComponentFixture<ZipTerritoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZipTerritoryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ZipTerritoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
