import { Component,ViewChild,Output,EventEmitter,Input} from '@angular/core';
import { QueryService } from '@app/_services/query.service';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import { LoaderService } from '@app/_services/loading.service';
import { BusinessRuleApiService } from '@app/_services/business-rule-api.service';
import { getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { tableFilter } from '@app/util/helper/harmonyUtils';
import { PrimeMultiSelectComponent } from '@app/components/form-components/prime-multi-select/prime-multi-select.component';
import { DialogService  } from 'primeng/dynamicdialog';
import {EditUpdateRuleComponent} from '../../business-rule-components/edit-update-rule/edit-update-rule.component'
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-specialty-update',
  templateUrl: './specialty-update.component.html',
  styleUrls: ['./specialty-update.component.scss']
})
export class SpecialtyUpdateComponent {
  @ViewChild('codeFilter') codeFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('descFilter') descFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('ruleNameFilter') ruleNameFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('businessUnitFilter') businessUnitFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('businessProcessFilter') businessProcessFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('ruleTypeFilter') ruleTypeFilterComponent: PrimeMultiSelectComponent; 
  @ViewChild('functionAreaFilter') functionAreaFilterComponent: PrimeMultiSelectComponent;   
  @ViewChild('startDateFilter') startDateFilterComponent: PrimeMultiSelectComponent;  
  @ViewChild('endDateFilter') endDateFilterComponent: PrimeMultiSelectComponent;  
  @ViewChild('statusFilter') statusFilterComponent: PrimeMultiSelectComponent;
  @Output() Data: EventEmitter<{ type: string, payload: any }> = new EventEmitter<{ type: string, payload: any }>();
  @Input() savedData:string;

  tablePageNumber=0;
  tableData:any;
  tableDataCopy:any;
  uniqueValues:any;
  filterComponents: any;
Column={
   code:"UDT_SPCLTY_CD",
   desc:"SPCLTY_DESC",
   ruleName: "UDT_BR_NM",
   businessUnit: "UDT_BU_CD",
   businessProcess: "UDT_FUNC_AREA_CD",
   ruleType:"UDT_ACTION",
   functionArea:"UDT_FUNC_AREA_CD",
   startDate:"UDT_START_DATE",
   endDate:"UDT_END_DATE",
   status:"UDT_RULE_STATUS"
}
columnArray=[
  this.Column.code,
  this.Column.desc,
  this.Column.ruleName,
  this.Column.businessUnit,
  this.Column.businessProcess,
  this.Column.ruleType,
  this.Column.functionArea,
  this.Column.startDate,
  this.Column.endDate,
  this.Column.status,
]
activeFilter={
  code: { valArr:[],column:'UDT_SPCLTY_CD'},
  desc: { valArr:[],column:'SPCLTY_DESC'},
  ruleName: { valArr:[],column:'UDT_BR_NM'},
  businessUnit:{ valArr:[],column:'UDT_BU_CD'},
  businessProcess:{ valArr:[],column:'UDT_FUNC_AREA_CD'},
  ruleType:{valArr:[],column:'UDT_ACTION'},
  functionArea: { valArr:[],column:'UDT_FUNC_AREA_CD'},
  startDate:{ valArr:[],column:'UDT_START_DATE'},
  endDate:{ valArr:[],column:'UDT_END_DATE'},
  status:{ valArr:[],column:'UDT_RULE_STATUS'}
}
  constructor(
    private BusinessRuleApiService: BusinessRuleApiService,
    public loaderService: LoaderService,
    private queryService: QueryService,
    private dialogService: DialogService,
    private toastService: MessageService,
  ) { }
  ngOnChanges(){

    if(this.savedData=='savedUpdateRule'){
     this.getResponseHandler();
    }
  }
  async ngOnInit() {
    this.loaderService.showLoader();

      await this.getResponseHandler();

   
   }
   async getResponseHandler(){
    this.tableData = await lastValueFrom(
      this.BusinessRuleApiService.specialtyUpdateTable()
    );
    console.log(this.tableData)
    this.tableDataCopy=[...this.tableData]
    this.tablePageNumber = 0
    this.uniqueValues=this.getDistinctValuesForColumns(this.tableData,this.columnArray)
    setTimeout(() => {
     this.filterComponents = [
       { selected: 'codeFilterSelected', component: this.codeFilterComponent },
       { selected: 'descFilterSelected', component: this.descFilterComponent },
       { selected: 'ruleNameFilterSelected', component: this.ruleNameFilterComponent },
       { selected: 'businessUnitFilterSelected', component: this.businessUnitFilterComponent },
       { selected: 'businessProcessFilterSelected', component: this.businessProcessFilterComponent },
       { selected: 'ruleTypeFilterSelected', component: this.ruleTypeFilterComponent },
       { selected: 'functionAreaFilterSelected', component: this.functionAreaFilterComponent },
       { selected: 'startDateFilterSelected', component: this.startDateFilterComponent },
       { selected: 'endDateFilterSelected', component: this.endDateFilterComponent },
       { selected: 'statusFilterSelected', component: this.statusFilterComponent },
     ];
   }, 300);
   
   if (this.activeFilter) {
     this.tableData = tableFilter(this.activeFilter,this.tableDataCopy);
   }
   this.loaderService.hideLoader();  
   this.Data.emit({type:'update',payload:this.tableDataCopy})

  }
   getDistinctValuesForColumns = (tableData: any[], columns: string[]) => {
		const result: any = [];
		columns.forEach((column) => {
		  const uniqueValues = getUniqueValues(tableData, column);
		  const mappedArray = getMappedArray(uniqueValues);
		  result[column] = [...mappedArray];
		});
		return result;
	  };
    filterChangeHandler(filterArray, column) {
      const filterArrayCopy = filterArray;
      if (column === 'UDT_SPCLTY_CD') {
        this.activeFilter.code.valArr = filterArrayCopy;
      }
      else if (column === 'SPCLTY_DESC') {
        this.activeFilter.desc.valArr = filterArrayCopy;
      }
      else if (column === 'UDT_BR_NM') {
        this.activeFilter.ruleName.valArr = filterArrayCopy;
      }
      else if (column === 'UDT_BU_CD') {
        this.activeFilter.businessUnit.valArr = filterArrayCopy;
      }
      else if (column === 'UDT_FUNC_AREA_CD') {
        this.activeFilter.businessProcess.valArr = filterArrayCopy;
      }
      else if (column === 'UDT_ACTION') {
        this.activeFilter.ruleType.valArr = filterArrayCopy;
      }
      else if (column === 'UDT_FUNC_AREA_CD') {
        this.activeFilter.functionArea.valArr = filterArrayCopy;
      }
      else if (column === 'UDT_START_DATE') {
        this.activeFilter.startDate.valArr = filterArrayCopy;
      }
      else if (column === 'UDT_END_DATE') {
        this.activeFilter.endDate.valArr = filterArrayCopy;
      } 
      else if (column === 'UDT_RULE_STATUS') {
        this.activeFilter.status.valArr = filterArrayCopy;
      } 
      const result=tableFilter(this.activeFilter,this.tableDataCopy)
  
      if (result.length){
      this.tableData=result
      this.tablePageNumber = 0
     }
     else{
      if(!Object.keys(this.activeFilter).some(field=>this.activeFilter[field].valArr.length)){
        this.tableData = this.tableDataCopy
      }else {
        this.tableData = result
      }
     }
     this.setFilterOptions(this.activeFilter,result)  
    }
    setFilterOptions(fieldObject,result){

      const filterFields = Object.keys(fieldObject)
    
      for(let field of filterFields){
        if(!fieldObject[field].valArr.length){
        this.uniqueValues[fieldObject[field].column] = getMappedArray(getUniqueValues(result, fieldObject[field].column));
        }
      }
      }
      resetAll() {
        this.filterComponents.forEach((item) => {
          item.component.resetSelection();
        });
        this.tableData = this.tableDataCopy;
        this.activatedFilterReset(this.activeFilter)
        this.uniqueValues = this.getDistinctValuesForColumns(this.tableData, this.columnArray);
      }
      activatedFilterReset(fieldObject:any){
        const filterFields = Object.keys(fieldObject)
        filterFields.forEach(key=> fieldObject[key].valArr = [])
      }
  
      exportExcel(){
        
      }
  editHandler(value:string){
    this.loaderService.showLoader();

    const dialogRef = this.dialogService.open(EditUpdateRuleComponent, {
      header: 'Edit Specialty Update Rule',
      width: '60%',
      height: '80%',
      data: {
        editData: value,
        tableData: this.tableDataCopy
      },
    });
    dialogRef.onClose.subscribe(result => {
      if (result === 'saved') {
        this.getResponseHandler();
      }
    });
    window.addEventListener('popstate', () => {
      dialogRef.close();
    });
  }
  deletePopupModel: any;
  tableSelectedRowIndex: number;
  deleteHandler(index: number) {
    this.deletePopupModel = {
      displayModal: true,
      header: 'Delete Specialty Update Rule',
      icon: 'pi-exclamation-triangle',
      content: {
        header: `Specialty Update Rule`,
        text: 'This action will delete the specialty permanently',
      },
      deleteBtnProps: {
        label: 'Delete',
        click: '',
      },
      cancelBtnProps: {
        label: 'Cancel',
        click: '',
      },
    };

    this.tableSelectedRowIndex = index;
  }
  deletePopupDeleteHandler = () => {
    let brSpecialtyId = this.tableData[this.tableSelectedRowIndex].BR_SPCLTY_ID;
    const postData = {
      
      specialtyId:brSpecialtyId
    };
    
    this.deletePopupModel.displayModal = false;
    this.loaderService.showLoader();
    this.BusinessRuleApiService.deleteSpecialtyUpdate(postData).subscribe((response) => {
      this.loaderService.hideLoader();
      if (response.status == 200) {
        this.toastService.clear();
        this.toastService.add({
          key: 'err',
          closable: false,
          sticky: false,
          severity: '',
          summary: '',
          detail: `Specialty Update Rule Deleted Successfully`,
        });
        this.getResponseHandler();
      } else {
        console.log(response);
        this.toastService.clear();
        this.toastService.add({
          key: 'wrn',
          closable: false,
          sticky: false,
          severity: '',
          summary: '',
          detail: `Connection Error TryAgain`,
        });
      }
    });
  };
}
