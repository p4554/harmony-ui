import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeTerritoryComponent } from './employee-territory.component';

describe('EmployeeTerritoryComponent', () => {
  let component: EmployeeTerritoryComponent;
  let fixture: ComponentFixture<EmployeeTerritoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeeTerritoryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EmployeeTerritoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
