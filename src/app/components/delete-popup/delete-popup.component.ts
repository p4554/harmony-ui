import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-delete-popup',
  templateUrl: './delete-popup.component.html',
  styleUrls: ['./delete-popup.component.scss']
})
export class DeletePopupComponent implements OnInit{

  @Input() deletePopupModel:any;

  @Output() deleteHandler = new EventEmitter()
  ngOnInit(){
     
  }

  handleDelete(){
    console.log(this.deletePopupModel);
    
    this.deleteHandler.emit()
  }

}
