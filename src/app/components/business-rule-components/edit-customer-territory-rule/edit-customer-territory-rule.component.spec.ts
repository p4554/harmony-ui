import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCustomerTerritoryRuleComponent } from './edit-customer-territory-rule.component';

describe('EditCustomerTerritoryRuleComponent', () => {
  let component: EditCustomerTerritoryRuleComponent;
  let fixture: ComponentFixture<EditCustomerTerritoryRuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditCustomerTerritoryRuleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditCustomerTerritoryRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
