import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditEmployeeTerritoryRuleComponent } from './edit-employee-territory-rule.component';

describe('EditEmployeeTerritoryRuleComponent', () => {
  let component: EditEmployeeTerritoryRuleComponent;
  let fixture: ComponentFixture<EditEmployeeTerritoryRuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditEmployeeTerritoryRuleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditEmployeeTerritoryRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
