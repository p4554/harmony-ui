import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewZipRuleComponent } from './new-zip-rule.component';

describe('NewZipRuleComponent', () => {
  let component: NewZipRuleComponent;
  let fixture: ComponentFixture<NewZipRuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewZipRuleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NewZipRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
