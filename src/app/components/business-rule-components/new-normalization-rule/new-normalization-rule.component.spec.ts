import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewNormalizationRuleComponent } from './new-normalization-rule.component';

describe('NewNormalizationRuleComponent', () => {
  let component: NewNormalizationRuleComponent;
  let fixture: ComponentFixture<NewNormalizationRuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewNormalizationRuleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NewNormalizationRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
