import { Component,OnInit,EventEmitter,Output} from '@angular/core';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import { QueryService } from '@app/_services/query.service';
import { BusinessRuleApiService } from '@app/_services/business-rule-api.service';
import { FormBuilder, FormGroup, Validators,NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { format } from 'date-fns';
import { MessageService } from 'primeng/api';
import { LoaderService } from '@app/_services/loading.service';
import { CUSTOM_CHAR_REGEX } from '@app/util/helper/patterns';
import { patternValidator } from '@app/util/validators/pattern-validator';
import { addDuplicateRecordCheckerAdmin, filterIncludedValuesFromArray, getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
@Component({
  selector: 'app-new-normalization-rule',
  templateUrl: './new-normalization-rule.component.html',
  styleUrls: ['./new-normalization-rule.component.scss']
})
export class NewNormalizationRuleComponent {

  newRuleForm: FormGroup;
  productUnitData: any;
  businessUnitData:any;
  businessUnitOptions:any;
  therapeuticOptions:any;
  therapeuticData:any;
  ProductCodeData:any;
  factorData:any;
  factorOption:any;
  productCodeOptions:any
  productUnitOption:any;
  tableData:any;
  endDate: Date = new Date();

  constructor(
    private ref: DynamicDialogRef,
    private queryService: QueryService,
    private BusinessRuleApiService: BusinessRuleApiService,
    private fb: FormBuilder,
    private toastService: MessageService,
    public loaderService: LoaderService,
    private config: DynamicDialogConfig
    ) {}
    
    async ngOnInit(){
      this.newRuleForm = this.fb.group({
        ruleName: [, [Validators.required,Validators.pattern(CUSTOM_CHAR_REGEX)]],
        businessUnit: ['', Validators.required],
        therapeuticClass: ['', Validators.required],
        productCode: ['', Validators.required],
        NormalizationFactor: ['', Validators.required],
        productUnit: ['', Validators.required],
        startDate: ['', Validators.required],
        EndDate: [, Validators.required]
      });
    // this.tableData = this.config.data.tableData;
   
    this.loaderService.showLoader();
    await Promise.all([
      this.getBusinessUnitData(),
      this.getTherapeuticData(),
      this.getProductCodeData(),
      this.getFactorData(),
      this.getProductUnitData(),
      this.getTablesData()
    ]);
  
    setTimeout(() => {
      this.loaderService.hideLoader();
    }, 300); 
       }
  
      
    
     async getBusinessUnitData(){
       this.businessUnitData=await lastValueFrom(
        this.BusinessRuleApiService.businessUnit()
       )
  
       this.businessUnitOptions = getMappedArray(
        getUniqueValues(this.businessUnitData,'BUSINESS_UNIT'),
        'label',
        'value'
      );
  
     }
   async  getTherapeuticData(){
      this.therapeuticData=await lastValueFrom(
       this.BusinessRuleApiService.therapeuticClass()
      )
      this.therapeuticOptions = getMappedArray(
        getUniqueValues(this.therapeuticData,'BUSINESS_PROCESS'),
        'label',
        'value'
      );
  
     }
     async getProductCodeData(){
      this.ProductCodeData=await lastValueFrom(
       this.BusinessRuleApiService.productCode()
      )
      this.productCodeOptions = getMappedArray(
       getUniqueValues(this.ProductCodeData,'PROD_CODE'),
       'label',
       'value'
     );
    } 
    async  getFactorData(){
     this.factorData=await lastValueFrom(
      this.BusinessRuleApiService.normalizationFactor()
     )
     this.factorOption = getMappedArray(
       getUniqueValues(this.factorData,'NORM_FACTOR'),
       'label',
       'value'
     );
    }
   async getProductUnitData(){
     this.productUnitData = await lastValueFrom(
       this.BusinessRuleApiService.productUnit()
     );
     this.productUnitOption = getMappedArray(
       getUniqueValues(this.productUnitData,'PROD_UNIT'),
       'label',
       'value'
     );
   }
    async getTablesData(){
    this.tableData = await lastValueFrom(
      this.BusinessRuleApiService.normalizationFactorTable()
    );
    }
  
    cancelButtonHandler(){
    
      this.ref.close();
    }
   
    formatDate(dateString) {
      const date = new Date(dateString);
      const year = date.getFullYear();
      const month = (date.getMonth() + 1).toString().padStart(2, '0');
      const day = date.getDate().toString().padStart(2, '0');
      const formattedDate = `${year}-${month}-${day}`;
      return formattedDate;
    }


    async onSaveHandler(){
      
      if (this.newRuleForm.valid) {
        const formattedDate = format(new Date(), 'yyyy-MM-dd');
        let formValue = this.newRuleForm.value;
        const formatStartDate = this.formatDate(formValue.startDate)
        const formatEndDate = this.formatDate(formValue.EndDate)
        const formProperty = ['productCode','productUnit','NormalizationFactor','EndDate']
        const tableProperty = ['PROD_CD','PROD_UNIT','NORM_FACTR','END_DATE']
        const duplicateFound = addDuplicateRecordCheckerAdmin(formValue,formProperty,this.tableData,tableProperty)
        if(duplicateFound){
          this.toastService.add({key: 'wrn',closable: false,sticky: false,severity: '',summary: '',detail:'Normalization Factor Rule is Already Exists.Try Another',});
        }
       else{  
        const postData = {
          ruleName: formValue.ruleName,
          businessUnit: formValue.businessUnit,
          therapeuticClass: formValue.therapeuticClass,
          productCode: formValue.productCode,
          productUnit: formValue.productUnit,
          normalizationFactor: formValue.NormalizationFactor,
          startDate: formatStartDate,
          endDate: formatEndDate,
          insertId: localStorage.getItem("userId"),
          updateId: localStorage.getItem("userId"),
        };
    
        try {
          const response = await this.BusinessRuleApiService.addNormalizationFactor(postData).toPromise();
    
          this.toastService.clear(); 
          console.log(response);
    
          if(response.status==200){
            this.ref.close('savedNormal');
            this.loaderService.showLoader();
            this.toastService.add({
              key: 'su',
              closable: false,
              sticky: false,
              severity: '',
              summary: '',
              detail: `New Normalization Factor Created`,
            });
          }
          else{
            this.ref.close();
            this.toastService.clear();
            this.toastService.add({
              key: 'wrn',
              closable: false,
              sticky: false,
              severity: '',
              summary: '',
              detail: `Connection Error Try Again`,
            });
          }
        } catch (error) {
          console.log(error);
        }     
      } 
    }
      else {
        this.newRuleForm.markAllAsTouched();
      }
      
  
    }
    handleDateOnchange(inputDate: Date):void{
      const today = new Date(inputDate);
      const month = today.getMonth();
      const year = today.getFullYear();
      const date = today.getDate();
      this.endDate.setMonth(month);
      this.endDate.setFullYear(year);
      this.endDate.setUTCDate(date);
      this.newRuleForm.get('EndDate').setValue('');
  }
}
