import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCustomerRuleComponent } from './new-customer-rule.component';

describe('NewCustomerRuleComponent', () => {
  let component: NewCustomerRuleComponent;
  let fixture: ComponentFixture<NewCustomerRuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewCustomerRuleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NewCustomerRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
