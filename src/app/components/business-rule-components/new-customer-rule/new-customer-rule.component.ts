import { Component,OnInit,EventEmitter,Output} from '@angular/core';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import { QueryService } from '@app/_services/query.service';
import { BusinessRuleApiService } from '@app/_services/business-rule-api.service';
import { FormBuilder, FormGroup, Validators,NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { format } from 'date-fns';
import { MessageService } from 'primeng/api';
import { LoaderService } from '@app/_services/loading.service';
import { CUSTOM_CHAR_REGEX } from '@app/util/helper/patterns';
import { patternValidator } from '@app/util/validators/pattern-validator';
import { addDuplicateRecordCheckerAdmin, filterIncludedValuesFromArray, getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
@Component({
  selector: 'app-new-customer-rule',
  templateUrl: './new-customer-rule.component.html',
  styleUrls: ['./new-customer-rule.component.scss']
})
export class NewCustomerRuleComponent {

newCustomerForm: FormGroup;
tableData: any;
businessUnitData:any;
businessUnitOptions:any;
businessProcessOptions:any;
businessProcessData:any;
customerData:any;
customerTypeOption:any;
customerIdOption:any;
ruleTypeData:any;
ruleTypeOption:any;
currentDate: Date = new Date();
endDate: Date = new Date();
constructor(
  private ref: DynamicDialogRef,
  private queryService: QueryService,
  private BusinessRuleApiService: BusinessRuleApiService,
  private fb: FormBuilder,
  private toastService: MessageService,
  public loaderService: LoaderService,
  private config: DynamicDialogConfig
  ) {}
  
  async ngOnInit(){
    this.newCustomerForm = this.fb.group({
      ruleName: ['', [Validators.required,Validators.pattern(CUSTOM_CHAR_REGEX)]],
      businessUnit: ['', Validators.required],
      businessProcess: ['', Validators.required],
      customerType: ['', Validators.required],
      CustomerID: ['', Validators.required],
      ruleType: ['', Validators.required],
      oldValue: ['', [Validators.required,Validators.pattern(CUSTOM_CHAR_REGEX)]],
      newValue: ['', [Validators.required,Validators.pattern(CUSTOM_CHAR_REGEX)]],
      startDate: ['', Validators.required],
      EndDate: [, Validators.required]
    });
  this.loaderService.showLoader();
  await Promise.all([
    this.getBusinessUnitData(),
    this.getBusinessProcessData(),
    this.getCustomerData(),
    this.getRuleTypeData(),
    this.getTableData(),
  ]);

  setTimeout(() => {
    this.loaderService.hideLoader();
  }, 300); 
     }

    
  
     async getBusinessUnitData(){
      this.businessUnitData=await lastValueFrom(
       this.BusinessRuleApiService.businessUnit()
      )
 
      this.businessUnitOptions = getMappedArray(
       getUniqueValues(this.businessUnitData,'BUSINESS_UNIT'),
       'label',
       'value'
     );
 
    }
  async  getBusinessProcessData(){
     this.businessProcessData=await lastValueFrom(
      this.BusinessRuleApiService.getBusinessProcess()
     )
     this.businessProcessOptions = getMappedArray(
       getUniqueValues(this.businessProcessData,'BUSINESS_PROCESS'),
       'label',
       'value'
     );
 
    }
   async getCustomerData(){
      this.customerData=await lastValueFrom(
       this.BusinessRuleApiService.getCustomerData()
      )
 
      this.customerTypeOption = getMappedArray(
       getUniqueValues(this.customerData,'CUST_TYPE'),
       'label',
       'value'
     );
     this.customerIdOption = getMappedArray(
       getUniqueValues(this.customerData,'CUST_ID'),
       'label',
       'value'
     );
    } 
    async  getRuleTypeData(){
     this.ruleTypeData=await lastValueFrom(
      this.BusinessRuleApiService.ruleType()
     )
     this.ruleTypeOption = getMappedArray(
       getUniqueValues(this.ruleTypeData,'ACTION'),
       'label',
       'value'
     );
    }
  async getTableData(){
    this.tableData = await lastValueFrom(
      this.BusinessRuleApiService.getCustomerTable()
    );
  }

  cancelButtonHandler(){
  
    this.ref.close();
  }
 
  formatDate(dateString) {
    const date = new Date(dateString);
    const year = date.getFullYear();
    const month = (date.getMonth() + 1).toString().padStart(2, '0');
    const day = date.getDate().toString().padStart(2, '0');
    const formattedDate = `${year}-${month}-${day}`;
    return formattedDate;
  }
  async onSaveHandler(){
    
    if (this.newCustomerForm.valid) {
      const formattedDate = format(new Date(), 'yyyy-MM-dd');
      let formValue = this.newCustomerForm.value;
      console.log(formValue)
      const formatStartDate = this.formatDate(formValue.startDate)
      const formatEndDate = this.formatDate(formValue.EndDate)
      const formProperty = ['CustomerID','businessProcess','ruleType','customerType','EndDate']
      const tableProperty = ['CUST_ID','CUST_BP_CD','CUST_BR_TYP','CUST_TYP','CUST_EFFCTV_END_DT']
      
      const duplicateFound = addDuplicateRecordCheckerAdmin(formValue,formProperty,this.tableData,tableProperty)
      if(duplicateFound){
        this.toastService.add({key: 'wrn',closable: false,sticky: false,severity: '',summary: '',detail:'Customer Rule is Already Exists.Try Another',});
      }
     else{
      const postData = {
        ruleName: formValue.ruleName,
        businessUnit: formValue.businessUnit,
        businessProcess: formValue.businessProcess,
        customerType: formValue.customerType,
        customerId: formValue.CustomerID,
        ruleType: formValue.ruleType,
        oldValue: formValue.oldValue,
        newValue: formValue.newValue,
        startDate: formatStartDate,
        endDate: formatEndDate,
        insertId: localStorage.getItem("userId"),
        updateId: localStorage.getItem("userId"),
      };
  
      try {
        const response = await this.BusinessRuleApiService.addCustomer(postData).toPromise();
  
        this.toastService.clear(); 
        console.log(response);
  
        if(response.status==200){
          this.ref.close('saved');
          this.toastService.add({
            key: 'su',
            closable: false,
            sticky: false,
            severity: '',
            summary: '',
            detail: `New Customer Rule Created`,
          });
        }
        else{
          this.ref.close();
          this.toastService.clear();
          this.toastService.add({
            key: 'wrn',
            closable: false,
            sticky: false,
            severity: '',
            summary: '',
            detail: `Connection Error Try Again`,
          });
        }
      } catch (error) {
        console.log(error);
      }     
    } 
  }
    else {
      this.newCustomerForm.markAllAsTouched();
    }
    

  }
   
  handleDateOnchange(inputDate: Date):void{
    const today = new Date(inputDate);
    const month = today.getMonth();
    const year = today.getFullYear();
    const date = today.getDate();
    this.endDate.setMonth(month);
    this.endDate.setFullYear(year);
    this.endDate.setUTCDate(date);
    this.newCustomerForm.get('EndDate').setValue('');
}
}
