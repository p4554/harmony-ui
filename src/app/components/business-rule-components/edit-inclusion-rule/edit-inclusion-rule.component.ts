import { Component,OnInit,EventEmitter,Output} from '@angular/core';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import { QueryService } from '@app/_services/query.service';
import { BusinessRuleApiService } from '@app/_services/business-rule-api.service';
import { FormBuilder, FormGroup, Validators,NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { format } from 'date-fns';
import { MessageService } from 'primeng/api';
import { LoaderService } from '@app/_services/loading.service';
import { CUSTOM_CHAR_REGEX } from '@app/util/helper/patterns';
import { patternValidator } from '@app/util/validators/pattern-validator';
import { editDuplicateRecordCheckerAdmin, filterIncludedValuesFromArray, getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
@Component({
  selector: 'app-edit-inclusion-rule',
  templateUrl: './edit-inclusion-rule.component.html',
  styleUrls: ['./edit-inclusion-rule.component.scss']
})
export class EditInclusionRuleComponent {
  newRuleForm: FormGroup;
  functionalData: any;
  businessUnitData:any;
  businessUnitOptions:any;
  businessProcessOption:any;
  businessProcessData:any;
  specialtyTypeData:any;
  specialtyCodeData:any;
  specialtyCodeOption:any;
  specialtyTypeOption:any
  functionalOption:any;
  ruleTypeData:any;
  ruleTypeOption:any;
  tableData:any;
  brSpecialtyId:any;
  endDate: Date = new Date();
  currentDate: Date = new Date();
  constructor(
    private ref: DynamicDialogRef,
    private queryService: QueryService,
    private BusinessRuleApiService: BusinessRuleApiService,
    private fb: FormBuilder,
    private toastService: MessageService,
    public loaderService: LoaderService,
    private config: DynamicDialogConfig
    ) {}
    
    async ngOnInit(){
      this.loaderService.showLoader();
      this.tableData = this.config.data.tableData;
      const editData=this.config.data.editData;
      this.brSpecialtyId=editData.BR_SPCLTY_ID;
      this.newRuleForm = this.fb.group({
        specialtyType: ['', Validators.required],
        specialtyCode: ['', Validators.required],
        ruleType: ['', Validators.required],
        functionalArea: ['', Validators.required],
        startDate: ['', Validators.required],
        EndDate: [, Validators.required]
      });
    await Promise.all([
      this.getSpecialtyTypeData(),
      this.getSpecialtyCodeData(),
      this.getRuleTypeData(),
      this.getFunctionalData(),
    ]);
    const StartDate = new Date(editData.INC_START_DATE);
    const endDate= new Date(editData.INC_END_DATE);
    this.newRuleForm.patchValue({
      specialtyType:editData.SPCLTY_TYP,
      specialtyCode:editData.INC_SPCLTY_CD,
      ruleType:editData.INC_ACTION,
      functionalArea:editData.INC_FUNC_AREA_CD,
      startDate:StartDate,
      EndDate:endDate,
    });
    setTimeout(() => {
      this.loaderService.hideLoader();
    }, 300); 
       }
  
      
    async getSpecialtyTypeData(){
       this.specialtyTypeData=await lastValueFrom(
        this.BusinessRuleApiService.specialtyType()
       )
       this.specialtyTypeOption = getMappedArray(
        getUniqueValues(this.specialtyTypeData,'SPCLT_TYP'),
        'label',
        'value'
      );
     } 
     async  getSpecialtyCodeData(){
      this.specialtyCodeData=await lastValueFrom(
       this.BusinessRuleApiService.specialtyCode()
      )
      this.specialtyCodeOption = getMappedArray(
        getUniqueValues(this.specialtyCodeData,'SPCLT_CD'),
        'label',
        'value'
      );
     }
     async  getRuleTypeData(){
      this.ruleTypeData=await lastValueFrom(
       this.BusinessRuleApiService.ruleType()
      )
      this.ruleTypeOption = getMappedArray(
        getUniqueValues(this.ruleTypeData,'ACTION'),
        'label',
        'value'
      );
     }
    async getFunctionalData(){
      this.functionalData = await lastValueFrom(
        this.BusinessRuleApiService.functionalArea()
      );
      this.functionalOption = getMappedArray(
        getUniqueValues(this.functionalData,'LOV_CD'),
        'label',
        'value'
      );
    }
    cancelButtonHandler(){
    
      this.ref.close();
    }
   
  
    formatDate(dateString) {
      const date = new Date(dateString);
      const year = date.getFullYear();
      const month = (date.getMonth() + 1).toString().padStart(2, '0');
      const day = date.getDate().toString().padStart(2, '0');
      const formattedDate = `${year}-${month}-${day}`;
      return formattedDate;
    }
    async onSaveHandler(){ 
      if (this.newRuleForm.valid) {
        const formattedDate = format(new Date(), 'yyyy-MM-dd');
        let formValue = this.newRuleForm.value;
        const formatStartDate = this.formatDate(formValue.startDate)
        const formatEndDate = this.formatDate(formValue.EndDate)
        const formProperty = ['specialtyType','specialtyCode','ruleType','EndDate']
        const tableProperty = ['SPCLTY_TYP','INC_SPCLTY_CD','INC_ACTION','INC_END_DATE']
        const duplicateFound = editDuplicateRecordCheckerAdmin(formValue,formProperty,this.tableData,tableProperty,this.brSpecialtyId,'BR_SPCLTY_ID')
        if(duplicateFound){
          this.toastService.add({key: 'wrn',closable: false,sticky: false,severity: '',summary: '',detail:'Specialty Inclusion Rule is Already Exists.Try Another',});
        }
       else{
        const postData = {
          functionalArea: formValue.functionalArea,
          specialtyType: formValue.specialtyType,
          specialtyCode: formValue.specialtyCode,
          ruleType: formValue.ruleType,
          startDate: formatStartDate,
          endDate: formatEndDate,
          updateId: localStorage.getItem("userId"),
          specialtyId:this.brSpecialtyId
        };   
    
    
        try {
          const response = await this.BusinessRuleApiService.updateSpecialtyInclusion(postData).toPromise();
    
          this.toastService.clear(); 
          console.log(response);
    
          if(response.status==200){
            this.ref.close('saved');
            this.loaderService.showLoader();
            this.toastService.add({
              key: 'su',
              closable: false,
              sticky: false,
              severity: '',
              summary: '',
              detail: `Specialty Inclusion Rule Updated`,
            });
          }
          else{
            this.ref.close();
            this.toastService.clear();
            this.toastService.add({
              key: 'wrn',
              closable: false,
              sticky: false,
              severity: '',
              summary: '',
              detail: `Connection Error Try Again`,
            });
          }
        } catch (error) {
          console.log(error);
        }     
      } 
    }
      else {
        this.newRuleForm.markAllAsTouched();
      }
      
  
    }
    handleDateOnchange(inputDate: Date):void{
      const today = new Date(inputDate);
      const month = today.getMonth();
      const year = today.getFullYear();
      const date = today.getDate();
      this.endDate.setMonth(month);
      this.endDate.setFullYear(year);
      this.endDate.setUTCDate(date);
      this.newRuleForm.get('EndDate').setValue('');
  }
}
