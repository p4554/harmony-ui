import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditInclusionRuleComponent } from './edit-inclusion-rule.component';

describe('EditInclusionRuleComponent', () => {
  let component: EditInclusionRuleComponent;
  let fixture: ComponentFixture<EditInclusionRuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditInclusionRuleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditInclusionRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
