import { Component,OnInit,EventEmitter,Output} from '@angular/core';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import { QueryService } from '@app/_services/query.service';
import { BusinessRuleApiService } from '@app/_services/business-rule-api.service';
import { FormBuilder, FormGroup, Validators,NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { format } from 'date-fns';
import { MessageService } from 'primeng/api';
import { LoaderService } from '@app/_services/loading.service';
import { CUSTOM_CHAR_REGEX } from '@app/util/helper/patterns';
import { patternValidator } from '@app/util/validators/pattern-validator';
import { editDuplicateRecordCheckerAdmin, filterIncludedValuesFromArray, getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
@Component({
  selector: 'app-edit-update-rule',
  templateUrl: './edit-update-rule.component.html',
  styleUrls: ['./edit-update-rule.component.scss']
})
export class EditUpdateRuleComponent {

  newRuleForm: FormGroup;
  functionalData: any;
  businessUnitData:any;
  businessUnitOptions:any;
  businessProcessOption:any;
  businessProcessData:any;
  specialtyDescriptionData:any;
  specialtyCodeData:any;
  specialtyCodeOption:any;
  specialtyOption:any
  functionalOption:any;
  ruleTypeData:any;
  ruleTypeOption:any;
  tableData:any;
  brSpecialtyId:any;
  endDate: Date = new Date();
  currentDate: Date = new Date();
  constructor(
    private ref: DynamicDialogRef,
    private queryService: QueryService,
    private BusinessRuleApiService: BusinessRuleApiService,
    private fb: FormBuilder,
    private toastService: MessageService,
    public loaderService: LoaderService,
    private config: DynamicDialogConfig
    ) {}
    
    async ngOnInit(){
      this.loaderService.showLoader();
      this.tableData = this.config.data.tableData;
      const editData=this.config.data.editData;
      this.brSpecialtyId=editData.BR_SPCLTY_ID;
      this.newRuleForm = this.fb.group({
        specialtyDescription: ['', Validators.required],
        specialtyCode: ['', Validators.required],
        ruleType: ['', Validators.required],
        functionalArea: ['', Validators.required],
        startDate: ['', Validators.required],
        EndDate: [, Validators.required]
      });
  
    await Promise.all([
      this.getSpecialtyDescriptionData(),
      this.getSpecialtyCodeData(),
      this.getRuleTypeData(),
      this.getFunctionalData(),
    ]);
    const StartDate = new Date(editData.UDT_START_DATE);
    const endDate= new Date(editData.UDT_END_DATE);
    this.newRuleForm.patchValue({
      specialtyDescription:editData.SPCLTY_DESC,
      specialtyCode:editData.UDT_SPCLTY_CD,
      ruleType:editData.UDT_ACTION,
      functionalArea:editData.UDT_FUNC_AREA_CD,
      startDate:StartDate,
      EndDate:endDate,
    });
    setTimeout(() => {
      this.loaderService.hideLoader();
    }, 300); 
       }
  
    async getSpecialtyDescriptionData(){
       this.specialtyDescriptionData=await lastValueFrom(
        this.BusinessRuleApiService.specialtyDesc()
       )
       this.specialtyOption = getMappedArray(
        getUniqueValues(this.specialtyDescriptionData,'SPCLT_DESC'),
        'label',
        'value'
      );
     } 
     async  getSpecialtyCodeData(){
      this.specialtyCodeData=await lastValueFrom(
       this.BusinessRuleApiService.specialtyCode()
      )
      this.specialtyCodeOption = getMappedArray(
        getUniqueValues(this.specialtyCodeData,'SPCLT_CD'),
        'label',
        'value'
      );
     }
     async  getRuleTypeData(){
      this.ruleTypeData=await lastValueFrom(
       this.BusinessRuleApiService.ruleType()
      )
      this.ruleTypeOption = getMappedArray(
        getUniqueValues(this.ruleTypeData,'ACTION'),
        'label',
        'value'
      );
     }
    async getFunctionalData(){
      this.functionalData = await lastValueFrom(
        this.BusinessRuleApiService.functionalArea()
      );
      this.functionalOption = getMappedArray(
        getUniqueValues(this.functionalData,'LOV_CD'),
        'label',
        'value'
      );
    }
  
    cancelButtonHandler(){
    
      this.ref.close();
    }
    formatDate(dateString) {
      const date = new Date(dateString);
      const year = date.getFullYear();
      const month = (date.getMonth() + 1).toString().padStart(2, '0');
      const day = date.getDate().toString().padStart(2, '0');
      const formattedDate = `${year}-${month}-${day}`;
      return formattedDate;
    }
  
    async onSaveHandler(){
      
      if (this.newRuleForm.valid) {
        const formattedDate = format(new Date(), 'yyyy-MM-dd');
        let formValue = this.newRuleForm.value;
        const formatStartDate = this.formatDate(formValue.startDate)
        const formatEndDate = this.formatDate(formValue.EndDate)
        const formProperty = ['specialtyCode','specialtyDescription','ruleType','EndDate']
        const tableProperty = ['UDT_SPCLTY_CD','SPCLTY_DESC','UDT_ACTION','UDT_END_DATE']
        const duplicateFound = editDuplicateRecordCheckerAdmin(formValue,formProperty,this.tableData,tableProperty,this.brSpecialtyId,'BR_SPCLTY_ID')
        if(duplicateFound){
          this.toastService.add({key: 'wrn',closable: false,sticky: false,severity: '',summary: '',detail:'Specialty Update Rule is Already Exists.Try Another',});
        }
       else{  
        const postData = {
          functionalArea: formValue.functionalArea,
          specialtyDesc: formValue.specialtyDescription,
          specialtyCode: formValue.specialtyCode,
          ruleType: formValue.ruleType,
          startDate: formatStartDate,
          endDate: formatEndDate,
          updateId: localStorage.getItem("userId"),
          specialtyId:this.brSpecialtyId
        };  
       
    
        try {
          const response = await this.BusinessRuleApiService.updateSpecialtyUpdate(postData).toPromise();
    
          this.toastService.clear(); 
          console.log(response);
    
          if(response.status==200){
            this.ref.close('saved');
            this.loaderService.showLoader();
            this.toastService.add({
              key: 'su',
              closable: false,
              sticky: false,
              severity: '',
              summary: '',
              detail: `Specialty Update Rule Updated`,
            });
          }
          else{
            this.ref.close();
            this.toastService.clear();
            this.toastService.add({
              key: 'wrn',
              closable: false,
              sticky: false,
              severity: '',
              summary: '',
              detail: `Connection Error Try Again`,
            });
          }
        } catch (error) {
          console.log(error);
        }     
      } 
    }
      else {
        this.newRuleForm.markAllAsTouched();
      }
      
  
    }
    
    handleDateOnchange(inputDate: Date):void{
      const today = new Date(inputDate);
      const month = today.getMonth();
      const year = today.getFullYear();
      const date = today.getDate();
      this.endDate.setMonth(month);
      this.endDate.setFullYear(year);
      this.endDate.setUTCDate(date);
      this.newRuleForm.get('EndDate').setValue('');
  }
}
