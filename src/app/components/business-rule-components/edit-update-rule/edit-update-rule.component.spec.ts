import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditUpdateRuleComponent } from './edit-update-rule.component';

describe('EditUpdateRuleComponent', () => {
  let component: EditUpdateRuleComponent;
  let fixture: ComponentFixture<EditUpdateRuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditUpdateRuleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditUpdateRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
