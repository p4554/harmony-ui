import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewGroupMapRuleComponent } from './new-group-map-rule.component';

describe('NewGroupMapRuleComponent', () => {
  let component: NewGroupMapRuleComponent;
  let fixture: ComponentFixture<NewGroupMapRuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewGroupMapRuleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NewGroupMapRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
