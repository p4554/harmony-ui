import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewUpdateRuleComponent } from './new-update-rule.component';

describe('NewUpdateRuleComponent', () => {
  let component: NewUpdateRuleComponent;
  let fixture: ComponentFixture<NewUpdateRuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewUpdateRuleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NewUpdateRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
