import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewMarketRuleComponent } from './new-market-rule.component';

describe('NewMarketRuleComponent', () => {
  let component: NewMarketRuleComponent;
  let fixture: ComponentFixture<NewMarketRuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewMarketRuleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NewMarketRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
