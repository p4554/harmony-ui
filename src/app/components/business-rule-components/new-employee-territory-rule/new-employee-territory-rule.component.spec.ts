import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewEmployeeTerritoryRuleComponent } from './new-employee-territory-rule.component';

describe('NewEmployeeTerritoryRuleComponent', () => {
  let component: NewEmployeeTerritoryRuleComponent;
  let fixture: ComponentFixture<NewEmployeeTerritoryRuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewEmployeeTerritoryRuleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NewEmployeeTerritoryRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
