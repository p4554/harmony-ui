import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditZipRuleComponent } from './edit-zip-rule.component';

describe('EditZipRuleComponent', () => {
  let component: EditZipRuleComponent;
  let fixture: ComponentFixture<EditZipRuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditZipRuleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditZipRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
