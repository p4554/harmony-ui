import { Component,OnInit,EventEmitter,Output} from '@angular/core';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import { QueryService } from '@app/_services/query.service';
import { BusinessRuleApiService } from '@app/_services/business-rule-api.service';
import { FormBuilder, FormGroup, Validators,NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { format } from 'date-fns';
import { MessageService } from 'primeng/api';
import { LoaderService } from '@app/_services/loading.service';
import { CUSTOM_CHAR_REGEX } from '@app/util/helper/patterns';
import { patternValidator } from '@app/util/validators/pattern-validator';
import { editDuplicateRecordCheckerAdmin, filterIncludedValuesFromArray, getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
@Component({
  selector: 'app-edit-zip-rule',
  templateUrl: './edit-zip-rule.component.html',
  styleUrls: ['./edit-zip-rule.component.scss']
})
export class EditZipRuleComponent {
  newRuleForm: FormGroup;
  productUnitData: any;
  businessUnitData:any;
  businessUnitOptions:any;
  zipOptions:any;
  zipData:any;
  territoryIdData:any;
  cycleData:any;
  cycleOption:any;
  territoryIdOptions:any
  productUnitOption:any;
  tableData:any;
  brAlignmentId:any;
  endDate: Date = new Date();
  currentDate: Date = new Date();
  constructor(
    private ref: DynamicDialogRef,
    private queryService: QueryService,
    private BusinessRuleApiService: BusinessRuleApiService,
    private fb: FormBuilder,
    private toastService: MessageService,
    public loaderService: LoaderService,
    private config: DynamicDialogConfig
    ) {}
    
    async ngOnInit(){
      this.loaderService.showLoader();
      this.tableData = this.config.data.tableData;
      const editData=this.config.data.editData;
      this.brAlignmentId=editData.BR_ALIGN_ID;
      this.newRuleForm = this.fb.group({
        zipCode: ['', Validators.required],
        territoryId: ['', Validators.required],
        cycle: ['', Validators.required],
        startDate: ['', Validators.required],
        EndDate: [, Validators.required]
      });
    this.loaderService.showLoader();
    await Promise.all([
      this.getZipData(),
      this.getTerritoryIdData(),
      this.getCycleData(),
    ]);
    const StartDate = new Date(editData.ZIP_END_DATE);
    const endDate= new Date(editData.ZIP_END_DATE);
    this.newRuleForm.patchValue({
      zipCode:editData.ZIP_CD,
      territoryId:editData.ZIP_TERR_ID,
      cycle:editData.ZIP_ALIGN_CYCLE,
      startDate:StartDate,
      EndDate:endDate,
    });
    setTimeout(() => {
      this.loaderService.hideLoader();
    }, 300); 
       }
  
   async  getZipData(){
      this.zipData=await lastValueFrom(
       this.BusinessRuleApiService.zipCode()
      )
      this.zipOptions = getMappedArray(
        getUniqueValues(this.zipData,'ZIP'),
        'label',
        'value'
      );
  
     }
    async getTerritoryIdData(){
       this.territoryIdData=await lastValueFrom(
        this.BusinessRuleApiService.territoryId()
       )
       this.territoryIdOptions = getMappedArray(
        getUniqueValues(this.territoryIdData,'TERRITORY'),
        'label',
        'value'
      );
     } 
     async  getCycleData(){
      this.cycleData=await lastValueFrom(
       this.BusinessRuleApiService.cycle()
      )
      this.cycleOption = getMappedArray(
        getUniqueValues(this.cycleData,'CYCLE_NM'),
        'label',
        'value'
      );
     }
   
    cancelButtonHandler(){
      this.ref.close();
    }
    formatDate(dateString) {
      const date = new Date(dateString);
      const year = date.getFullYear();
      const month = (date.getMonth() + 1).toString().padStart(2, '0');
      const day = date.getDate().toString().padStart(2, '0');
      const formattedDate = `${year}-${month}-${day}`;
      return formattedDate;
    }
  
    async onSaveHandler(){
      
      if (this.newRuleForm.valid) {
        const formattedDate = format(new Date(), 'yyyy-MM-dd');
        let formValue = this.newRuleForm.value;
        const formatStartDate = this.formatDate(formValue.startDate)
        const formatEndDate = this.formatDate(formValue.EndDate)
        const formProperty = ['zipCode','territoryId','EndDate']
        const tableProperty = ['ZIP_CD','ZIP_TERR_ID','ZIP_END_DATE']
        const duplicateFound = editDuplicateRecordCheckerAdmin(formValue,formProperty,this.tableData,tableProperty,this.brAlignmentId,'BR_ALIGN_ID')
        if(duplicateFound){
          this.toastService.add({key: 'wrn',closable: false,sticky: false,severity: '',summary: '',detail:'Zip Territory Alignment Rule is Already Exists.Try Another',});
        }
       else{
        const postData = {
          zipCode: formValue.zipCode,
          territoryId: formValue.territoryId,
          cycle: formValue.cycle,
          startDate: formatStartDate,
          endDate: formatEndDate,
          updateId: localStorage.getItem("userId"),
          alignmentId:this.brAlignmentId
        };
       
        try {
          const response = await this.BusinessRuleApiService.updateZipTerritory(postData).toPromise();
    
          this.toastService.clear(); 
          console.log(response);
    
          if(response.status==200){
            this.ref.close('saved');
            this.toastService.add({
              key: 'su',
              closable: false,
              sticky: false,
              severity: '',
              summary: '',
              detail: `Zip Territory Alignment Updated`,
            });
          }
          else{
            this.ref.close();
            this.toastService.clear();
            this.toastService.add({
              key: 'wrn',
              closable: false,
              sticky: false,
              severity: '',
              summary: '',
              detail: `Connection Error Try Again`,
            });
          }
        } catch (error) {
          console.log(error);
        }     
      } 
    }
      else {
        this.newRuleForm.markAllAsTouched();
      }
      
  
    }

    handleDateOnchange(inputDate: Date):void{
      const today = new Date(inputDate);
      const month = today.getMonth();
      const year = today.getFullYear();
      const date = today.getDate();
      this.endDate.setMonth(month);
      this.endDate.setFullYear(year);
      this.endDate.setUTCDate(date);
      this.newRuleForm.get('EndDate').setValue('');
  }
}
