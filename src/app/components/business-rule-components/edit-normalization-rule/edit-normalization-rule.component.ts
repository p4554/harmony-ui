import { Component,OnInit,EventEmitter,Output} from '@angular/core';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import { QueryService } from '@app/_services/query.service';
import { BusinessRuleApiService } from '@app/_services/business-rule-api.service';
import { FormBuilder, FormGroup, Validators,NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { format } from 'date-fns';
import { MessageService } from 'primeng/api';
import { LoaderService } from '@app/_services/loading.service';
import { CUSTOM_CHAR_REGEX } from '@app/util/helper/patterns';
import { patternValidator } from '@app/util/validators/pattern-validator';
import { editDuplicateRecordCheckerAdmin, filterIncludedValuesFromArray, getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
@Component({
  selector: 'app-edit-normalization-rule',
  templateUrl: './edit-normalization-rule.component.html',
  styleUrls: ['./edit-normalization-rule.component.scss']
})
export class EditNormalizationRuleComponent {

  newRuleForm: FormGroup;
  productUnitData: any;
  businessUnitData:any;
  businessUnitOptions:any;
  therapeuticOptions:any;
  therapeuticData:any;
  ProductCodeData:any;
  factorData:any;
  factorOption:any;
  productCodeOptions:any
  productUnitOption:any;
  tableData:any;
  brProductId:any;
  endDate: Date = new Date();
  currentDate: Date = new Date();
  constructor(
    private ref: DynamicDialogRef,
    private queryService: QueryService,
    private BusinessRuleApiService: BusinessRuleApiService,
    private fb: FormBuilder,
    private toastService: MessageService,
    public loaderService: LoaderService,
    private config: DynamicDialogConfig
    ) {}
    
    async ngOnInit(){
      this.loaderService.showLoader();
      this.tableData = this.config.data.tableData;
      const editData=this.config.data.editData;
      this.brProductId=editData.BR_PROD_ID;
      this.newRuleForm = this.fb.group({
        productCode: ['', Validators.required],
        NormalizationFactor: ['', Validators.required],
        productUnit: ['', Validators.required],
        startDate: ['', Validators.required],
        EndDate: [, Validators.required]
      });
   
    await Promise.all([
      this.getProductCodeData(),
      this.getFactorData(),
      this.getProductUnitData(),
    ]);
    const StartDate = new Date(editData.START_DATE);
    const endDate= new Date(editData.END_DATE);
    this.newRuleForm.patchValue({
      productCode:editData.PROD_CD,
      NormalizationFactor:editData.NORM_FACTR,
      productUnit:editData.PROD_UNIT,
      startDate:StartDate,
      EndDate:endDate,
    });
    setTimeout(() => {
      this.loaderService.hideLoader();
    }, 300); 
       }
  
      
    
    async getProductCodeData(){
       this.ProductCodeData=await lastValueFrom(
        this.BusinessRuleApiService.productCode()
       )
       this.productCodeOptions = getMappedArray(
        getUniqueValues(this.ProductCodeData,'PROD_CODE'),
        'label',
        'value'
      );
     } 
     async  getFactorData(){
      this.factorData=await lastValueFrom(
       this.BusinessRuleApiService.normalizationFactor()
      )
      this.factorOption = getMappedArray(
        getUniqueValues(this.factorData,'NORM_FACTOR'),
        'label',
        'value'
      );
     }
    async getProductUnitData(){
      this.productUnitData = await lastValueFrom(
        this.BusinessRuleApiService.productUnit()
      );
      this.productUnitOption = getMappedArray(
        getUniqueValues(this.productUnitData,'PROD_UNIT'),
        'label',
        'value'
      );
    }
   
  
    cancelButtonHandler(){
    
      this.ref.close();
    }
   
    formatDate(dateString) {
      const date = new Date(dateString);
      const year = date.getFullYear();
      const month = (date.getMonth() + 1).toString().padStart(2, '0');
      const day = date.getDate().toString().padStart(2, '0');
      const formattedDate = `${year}-${month}-${day}`;
      return formattedDate;
    }


    async onSaveHandler(){
      
      if (this.newRuleForm.valid) {
        const formattedDate = format(new Date(), 'yyyy-MM-dd');
        let formValue = this.newRuleForm.value;
        const formatStartDate = this.formatDate(formValue.startDate)
        const formatEndDate = this.formatDate(formValue.EndDate)
        const formProperty = ['productCode','productUnit','NormalizationFactor','EndDate']
        const tableProperty = ['PROD_CD','PROD_UNIT','NORM_FACTR','END_DATE']
        const duplicateFound = editDuplicateRecordCheckerAdmin(formValue,formProperty,this.tableData,tableProperty,this.brProductId,'BR_PROD_ID')
        if(duplicateFound){
          this.toastService.add({key: 'wrn',closable: false,sticky: false,severity: '',summary: '',detail:'Normalization Factor Rule is Already Exists.Try Another',});
        }
       else{  
        const postData = {
          productCode: formValue.productCode,
          productUnit: formValue.productUnit,
          normalizationFactor: formValue.NormalizationFactor,
          startDate: formatStartDate,
          endDate: formatEndDate,
          updateId: localStorage.getItem("userId"),
          productId:this.brProductId
        };
  
  
        try {
          const response = await this.BusinessRuleApiService.updateNormalizationFactor(postData).toPromise();
    
          this.toastService.clear(); 
          console.log(response);
    
          if(response.status==200){
            this.ref.close('saved');
            this.loaderService.showLoader();
            this.toastService.add({
              key: 'su',
              closable: false,
              sticky: false,
              severity: '',
              summary: '',
              detail: `Normalization Factor Updated`,
            });
          }
          else{
            this.ref.close();
            this.toastService.clear();
            this.toastService.add({
              key: 'wrn',
              closable: false,
              sticky: false,
              severity: '',
              summary: '',
              detail: `Connection Error Try Again`,
            });
          }
        } catch (error) {
          console.log(error);
        }     
      } 
    }
      else {
        this.newRuleForm.markAllAsTouched();
      }
      
  
    }
    handleDateOnchange(inputDate: Date):void{
      const today = new Date(inputDate);
      const month = today.getMonth();
      const year = today.getFullYear();
      const date = today.getDate();
      this.endDate.setMonth(month);
      this.endDate.setFullYear(year);
      this.endDate.setUTCDate(date);
      this.newRuleForm.get('EndDate').setValue('');
  } 
}
