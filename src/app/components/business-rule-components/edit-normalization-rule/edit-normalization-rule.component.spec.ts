import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditNormalizationRuleComponent } from './edit-normalization-rule.component';

describe('EditNormalizationRuleComponent', () => {
  let component: EditNormalizationRuleComponent;
  let fixture: ComponentFixture<EditNormalizationRuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditNormalizationRuleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditNormalizationRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
