import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMarketRuleComponent } from './edit-market-rule.component';

describe('EditMarketRuleComponent', () => {
  let component: EditMarketRuleComponent;
  let fixture: ComponentFixture<EditMarketRuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditMarketRuleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditMarketRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
