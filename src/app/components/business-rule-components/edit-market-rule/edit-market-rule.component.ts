import { Component,OnInit,EventEmitter,Output} from '@angular/core';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import { QueryService } from '@app/_services/query.service';
import { BusinessRuleApiService } from '@app/_services/business-rule-api.service';
import { FormBuilder, FormGroup, Validators,NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { format } from 'date-fns';
import { MessageService } from 'primeng/api';
import { LoaderService } from '@app/_services/loading.service';
import { CUSTOM_CHAR_REGEX } from '@app/util/helper/patterns';
import { patternValidator } from '@app/util/validators/pattern-validator';
import { editDuplicateRecordCheckerAdmin, filterIncludedValuesFromArray, getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
@Component({
  selector: 'app-edit-market-rule',
  templateUrl: './edit-market-rule.component.html',
  styleUrls: ['./edit-market-rule.component.scss']
})
export class EditMarketRuleComponent {

newMarketForm: FormGroup;
marketCodeData: any;
businessUnitData:any;
businessUnitOptions:any;
therapeuticOptions:any;
therapeuticData:any;
ProductCodeData:any;
productCodeOptions:any;
ruleTypeData:any;
ruleTypeOption:any;
marketCodeOption:any;
tableData:any;
brProductId:any;
endDate: Date = new Date();
currentDate: Date = new Date();
constructor(
  private ref: DynamicDialogRef,
  private queryService: QueryService,
  private BusinessRuleApiService: BusinessRuleApiService,
  private fb: FormBuilder,
  private toastService: MessageService,
  public loaderService: LoaderService,
  private config: DynamicDialogConfig
  ) {}
  
  async ngOnInit(){
    this.loaderService.showLoader();
    this.tableData = this.config.data.tableData;
    const editData=this.config.data.editData;
    this.brProductId=editData.BR_PROD_ID;
    this.newMarketForm = this.fb.group({
      productCode: ['', Validators.required],
      marketCode: ['', Validators.required],
      ruleType: ['', Validators.required],
      startDate: ['', Validators.required],
      EndDate: [, Validators.required]
    });
  await Promise.all([
    this.getProductCodeData(),
    this.getRuleTypeData(),
    this.getMarketCodeData(),
  ]);
  const StartDate = new Date(editData.START_DATE);
  const endDate= new Date(editData.END_DATE);
  this.newMarketForm.patchValue({
    productCode:editData.PROD_CD,
    marketCode:editData.MKT_NM,
    ruleType:editData.ACTION,
    startDate:StartDate,
    EndDate:endDate,
  })
  setTimeout(() => {
    this.loaderService.hideLoader();
  }, 300); 
     }

    
  
  async getProductCodeData(){
     this.ProductCodeData=await lastValueFrom(
      this.BusinessRuleApiService.productCode()
     )
     this.productCodeOptions = getMappedArray(
      getUniqueValues(this.ProductCodeData,'PROD_CODE'),
      'label',
      'value'
    );
   } 
   async  getRuleTypeData(){
    this.ruleTypeData=await lastValueFrom(
     this.BusinessRuleApiService.ruleType()
    )
    this.ruleTypeOption = getMappedArray(
      getUniqueValues(this.ruleTypeData,'ACTION'),
      'label',
      'value'
    );
   }
  async getMarketCodeData(){
   this.marketCodeData = await lastValueFrom(
      this.BusinessRuleApiService.marketCode()
    );    this.marketCodeOption = getMappedArray(
      getUniqueValues(this.marketCodeData,'MARKET_CD'),
      'label',
      'value'
    );
  }
  cancelButtonHandler(){
  
    this.ref.close();
  }
 
  formatDate(dateString) {
    const date = new Date(dateString);
    const year = date.getFullYear();
    const month = (date.getMonth() + 1).toString().padStart(2, '0');
    const day = date.getDate().toString().padStart(2, '0');
    const formattedDate = `${year}-${month}-${day}`;
    return formattedDate;
  }
  async onSaveHandler(){
    
    if (this.newMarketForm.valid) {
      const formattedDate = format(new Date(), 'yyyy-MM-dd');
      let formValue = this.newMarketForm.value;
      const formatStartDate = this.formatDate(formValue.startDate)
      const formatEndDate = this.formatDate(formValue.EndDate)
      const formProperty = ['productCode','marketCode','ruleType','EndDate']
      const tableProperty = ['PROD_CD','MKT_NM','ACTION','END_DATE']
      const duplicateFound = editDuplicateRecordCheckerAdmin(formValue,formProperty,this.tableData,tableProperty,this.brProductId,'BR_PROD_ID')
      if(duplicateFound){
        this.toastService.add({key: 'wrn',closable: false,sticky: false,severity: '',summary: '',detail:'Market Definition Rule is Already Exists.Try Another',});
      }
     else{  
      const postData = {
        productCode: formValue.productCode,
        marketName: formValue.marketCode,
        action: formValue.ruleType,
        startDate: formatStartDate,
        endDate: formatEndDate,
        updateId: localStorage.getItem("userId"),
        productId:this.brProductId
      };
    
      try {
        const response = await this.BusinessRuleApiService.updateMarketDefinition(postData).toPromise();
  
        this.toastService.clear(); 
        console.log(response);
  
        if(response.status==200){
          this.ref.close('saved');
          this.loaderService.showLoader();
          this.toastService.add({
            key: 'su',
            closable: false,
            sticky: false,
            severity: '',
            summary: '',
            detail: `Market Definition Rule updated`,
          });
        }
        else{
          this.ref.close();
          this.toastService.clear();
          this.toastService.add({
            key: 'wrn',
            closable: false,
            sticky: false,
            severity: '',
            summary: '',
            detail: `Connection Error Try Again`,
          });
        }
      } catch (error) {
        console.log(error);
      }     
    } 
  }
    else {
      this.newMarketForm.markAllAsTouched();
    }
    

  }
  handleDateOnchange(inputDate: Date):void{
    const today = new Date(inputDate);
    const month = today.getMonth();
    const year = today.getFullYear();
    const date = today.getDate();
    this.endDate.setMonth(month);
    this.endDate.setFullYear(year);
    this.endDate.setUTCDate(date);
    this.newMarketForm.get('EndDate').setValue('');
}
  
}
