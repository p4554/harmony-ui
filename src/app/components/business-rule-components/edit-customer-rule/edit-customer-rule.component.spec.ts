import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCustomerRuleComponent } from './edit-customer-rule.component';

describe('EditCustomerRuleComponent', () => {
  let component: EditCustomerRuleComponent;
  let fixture: ComponentFixture<EditCustomerRuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditCustomerRuleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditCustomerRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
