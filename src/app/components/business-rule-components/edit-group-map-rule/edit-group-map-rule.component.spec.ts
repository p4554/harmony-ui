import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditGroupMapRuleComponent } from './edit-group-map-rule.component';

describe('EditGroupMapRuleComponent', () => {
  let component: EditGroupMapRuleComponent;
  let fixture: ComponentFixture<EditGroupMapRuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditGroupMapRuleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditGroupMapRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
