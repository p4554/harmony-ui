import { Component,OnInit,EventEmitter,Output} from '@angular/core';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import { QueryService } from '@app/_services/query.service';
import { BusinessRuleApiService } from '@app/_services/business-rule-api.service';
import { FormBuilder, FormGroup, Validators,NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { format } from 'date-fns';
import { MessageService } from 'primeng/api';
import { LoaderService } from '@app/_services/loading.service';
import { CUSTOM_CHAR_REGEX } from '@app/util/helper/patterns';
import { patternValidator } from '@app/util/validators/pattern-validator';
import { addDuplicateRecordCheckerAdmin, filterIncludedValuesFromArray, getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
@Component({
  selector: 'app-new-inclusion-rule',
  templateUrl: './new-inclusion-rule.component.html',
  styleUrls: ['./new-inclusion-rule.component.scss']
})
export class NewInclusionRuleComponent {
 
  newRuleForm: FormGroup;
  functionalData: any;
  businessUnitData:any;
  businessUnitOptions:any;
  businessProcessOption:any;
  businessProcessData:any;
  specialtyTypeData:any;
  specialtyCodeData:any;
  specialtyCodeOption:any;
  specialtyTypeOption:any
  functionalOption:any;
  ruleTypeData:any;
  ruleTypeOption:any;
  tableData:any;
  endDate: Date = new Date();

  constructor(
    private ref: DynamicDialogRef,
    private queryService: QueryService,
    private BusinessRuleApiService: BusinessRuleApiService,
    private fb: FormBuilder,
    private toastService: MessageService,
    public loaderService: LoaderService,
    private config: DynamicDialogConfig
    ) {}
    
    async ngOnInit(){
      this.newRuleForm = this.fb.group({
        ruleName: [, [Validators.required,Validators.pattern(CUSTOM_CHAR_REGEX)]],
        businessUnit: ['', Validators.required],
        businessProcess: ['', Validators.required],
        specialtyType: ['', Validators.required],
        specialtyCode: ['', Validators.required],
        ruleType: ['', Validators.required],
        functionalArea: ['', Validators.required],
        startDate: ['', Validators.required],
        EndDate: [, Validators.required]
      });
    // this.tableData = this.config.data.tableData;
    this.loaderService.showLoader();
    await Promise.all([
      this.getBusinessUnitData(),
      this.getBusinessProcessData(),
      this.getSpecialtyTypeData(),
      this.getSpecialtyCodeData(),
      this.getRuleTypeData(),
      this.getFunctionalData(),
      this.getTableData()
    ]);
  
    setTimeout(() => {
      this.loaderService.hideLoader();
    }, 300); 
       }
  
      
    
     async getBusinessUnitData(){
       this.businessUnitData=await lastValueFrom(
        this.BusinessRuleApiService.businessUnit()
       )
  
       this.businessUnitOptions = getMappedArray(
        getUniqueValues(this.businessUnitData,'BUSINESS_UNIT'),
        'label',
        'value'
      );
  
     }
     async  getBusinessProcessData(){
      this.businessProcessData=await lastValueFrom(
       this.BusinessRuleApiService.getBusinessProcess()
      )
      this.businessProcessOption = getMappedArray(
        getUniqueValues(this.businessProcessData,'BUSINESS_PROCESS'),
        'label',
        'value'
      );
  
     }
     async getSpecialtyTypeData(){
      this.specialtyTypeData=await lastValueFrom(
       this.BusinessRuleApiService.specialtyType()
      )
      this.specialtyTypeOption = getMappedArray(
       getUniqueValues(this.specialtyTypeData,'SPCLT_TYP'),
       'label',
       'value'
     );
    } 
    async  getSpecialtyCodeData(){
     this.specialtyCodeData=await lastValueFrom(
      this.BusinessRuleApiService.specialtyCode()
     )
     this.specialtyCodeOption = getMappedArray(
       getUniqueValues(this.specialtyCodeData,'SPCLT_CD'),
       'label',
       'value'
     );
    }
    async  getRuleTypeData(){
     this.ruleTypeData=await lastValueFrom(
      this.BusinessRuleApiService.ruleType()
     )
     this.ruleTypeOption = getMappedArray(
       getUniqueValues(this.ruleTypeData,'ACTION'),
       'label',
       'value'
     );
    }
   async getFunctionalData(){
     this.functionalData = await lastValueFrom(
       this.BusinessRuleApiService.functionalArea()
     );
     this.functionalOption = getMappedArray(
       getUniqueValues(this.functionalData,'LOV_CD'),
       'label',
       'value'
     );
   }
    async getTableData(){
      this.tableData = await lastValueFrom(
        this.BusinessRuleApiService.inclusionTable()
      );
   }  
    cancelButtonHandler(){
    
      this.ref.close();
    }
   
  
    formatDate(dateString) {
      const date = new Date(dateString);
      const year = date.getFullYear();
      const month = (date.getMonth() + 1).toString().padStart(2, '0');
      const day = date.getDate().toString().padStart(2, '0');
      const formattedDate = `${year}-${month}-${day}`;
      return formattedDate;
    }
    async onSaveHandler(){ 
      if (this.newRuleForm.valid) {
        const formattedDate = format(new Date(), 'yyyy-MM-dd');
        let formValue = this.newRuleForm.value;
        const formatStartDate = this.formatDate(formValue.startDate)
        const formatEndDate = this.formatDate(formValue.EndDate)
        const formProperty = ['specialtyType','specialtyCode','ruleType','EndDate']
        const tableProperty = ['SPCLTY_TYP','INC_SPCLTY_CD','INC_ACTION','INC_END_DATE']
        const duplicateFound = addDuplicateRecordCheckerAdmin(formValue,formProperty,this.tableData,tableProperty)
        if(duplicateFound){
          this.toastService.add({key: 'wrn',closable: false,sticky: false,severity: '',summary: '',detail:'Specialty Inclusion Rule is Already Exists.Try Another',});
        }
       else{
        const postData = {
          ruleName: formValue.ruleName,
          businessUnit: formValue.businessUnit,
          functionalArea: formValue.functionalArea,
          specialtyType: formValue.specialtyType,
          specialtyCode: formValue.specialtyCode,
          ruleType: formValue.ruleType,
          startDate: formatStartDate,
          endDate: formatEndDate,
          insertId: localStorage.getItem("userId"),
          updateId: localStorage.getItem("userId"),
        };   
    
        try {
          const response = await this.BusinessRuleApiService.addSpecialtyInclusion(postData).toPromise();
    
          this.toastService.clear(); 
          console.log(response);
    
          if(response.status==200){
            this.ref.close('savedInclusions');
            this.loaderService.showLoader();
            this.toastService.add({
              key: 'su',
              closable: false,
              sticky: false,
              severity: '',
              summary: '',
              detail: `New Specialty Inclusion Rule Created`,
            });
          }
          else{
            this.ref.close();
            this.toastService.clear();
            this.toastService.add({
              key: 'wrn',
              closable: false,
              sticky: false,
              severity: '',
              summary: '',
              detail: `Connection Error Try Again`,
            });
          }
        } catch (error) {
          console.log(error);
        }     
      } 
    }
      else {
        this.newRuleForm.markAllAsTouched();
      }
      
  
    }
    handleDateOnchange(inputDate: Date):void{
      const today = new Date(inputDate);
      const month = today.getMonth();
      const year = today.getFullYear();
      const date = today.getDate();
      this.endDate.setMonth(month);
      this.endDate.setFullYear(year);
      this.endDate.setUTCDate(date);
      this.newRuleForm.get('EndDate').setValue('');
  }
}
