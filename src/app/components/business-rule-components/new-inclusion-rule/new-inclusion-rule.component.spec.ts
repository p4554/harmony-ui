import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewInclusionRuleComponent } from './new-inclusion-rule.component';

describe('NewInclusionRuleComponent', () => {
  let component: NewInclusionRuleComponent;
  let fixture: ComponentFixture<NewInclusionRuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewInclusionRuleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NewInclusionRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
