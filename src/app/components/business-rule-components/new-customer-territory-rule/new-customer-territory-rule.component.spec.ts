import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCustomerTerritoryRuleComponent } from './new-customer-territory-rule.component';

describe('NewCustomerTerritoryRuleComponent', () => {
  let component: NewCustomerTerritoryRuleComponent;
  let fixture: ComponentFixture<NewCustomerTerritoryRuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewCustomerTerritoryRuleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NewCustomerTerritoryRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
