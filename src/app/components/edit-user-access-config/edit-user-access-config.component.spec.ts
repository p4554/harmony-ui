import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditUserAccessConfigComponent } from './edit-user-access-config.component';

describe('EditUserAccessConfigComponent', () => {
  let component: EditUserAccessConfigComponent;
  let fixture: ComponentFixture<EditUserAccessConfigComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditUserAccessConfigComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditUserAccessConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
