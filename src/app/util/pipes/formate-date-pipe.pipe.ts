import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formateDatePipe'
})
export class FormateDatePipePipe implements PipeTransform {

  transform(date: string): string {
    const backendDate = new Date(date);
    return backendDate.toLocaleDateString('en-US', {
      weekday: 'short',
      day: 'numeric',
      month: 'short',
      year: 'numeric',
    });
  }

}
