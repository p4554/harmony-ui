import { replacePlaceholders } from '@app/util/helper/patterns';

export const replaceParamsinJSON = (jsonObj: any, paramArray: string[]) => {
	const newQuery = replacePlaceholders(jsonObj.query, paramArray);
	jsonObj.query = newQuery;

	return jsonObj;
};


// replace words with '' 

export const replaceWordsWithEmptyString = (str:string) => {
	return str.replace(/[a-zA-Z]+/g,'');
};
export const replaceNumberWithEmptyString = (str:string) => {
	return str.replace(/[0-9]+/g,'');
};

export const groupByArrayToValueConverter = (grpByArr:{label:string,value:string}[])=>{
	let value = ''
	if(grpByArr.length !== 0){
		const arr = grpByArr.sort((a,b)=>a.label.localeCompare(b.label))
		arr.forEach((element,i)=> {
			if(arr.length === (i+1)){
			  value = value + element.value
			}else{
			  value = value + element.value + ','
			}
		  })
	}
	  return value
}
export const groupByValueToArrayConverter = (name:string,value:string)=>{
	let groupByNameArr = name.split(',')
	let groupByValueArr = value.split(',')
	let groupBySelectedOption = []
	if(groupByValueArr.length === groupByNameArr.length){
		groupByNameArr.forEach((name,i)=>{
			groupBySelectedOption.push({label:name,value:groupByValueArr[i]})
		})
		}
	return groupBySelectedOption
}

export const editGroupByValueDuplicateChecker= (groupByValue:string,tableArr:any[],tableColumn:string,ruleId:string):boolean=>{
	const groupByDuplicationFlag = tableArr.some(obj =>{

			if(obj.RULE_ID !== ruleId){
				return obj[tableColumn] === groupByValue
			}else{
				return false
			}
      })
	  return !groupByDuplicationFlag
}


export const  tableFilter = (fieldObject:any,original_array:any[]):any[]=>{
	const filterFields = Object.keys(fieldObject)
	let tempTableArr:any[] = [...original_array]
	for(let field of filterFields){
	  if( fieldObject[field].valArr.length){
		  if(!tempTableArr.length){
			return tempTableArr
		  }
		tempTableArr = tempTableArr.filter(record=>{
		  const currentFilteredArr = fieldObject[field].valArr
		//   console.log(record)
		//   console.log(fieldObject[field].column )
		  const currentValue = record[fieldObject[field].column]
		//   console.log('currentFilteredArr=>',currentFilteredArr)
		//   console.log('currentValue=>',currentValue)
		return currentFilteredArr.includes(currentValue) 
		})
	  }
	}
	// console.log('endResult=>',tempTableArr)
	return tempTableArr
  }

