// array handling functions
export const removeFromArray = (main_array: any, remove_object: any, main_array_key: any, remove_object_key: any) => {
	const findIndex = main_array.findIndex((a: any) => a[main_array_key] === remove_object[remove_object_key]);

	findIndex !== -1 && main_array.splice(findIndex, 1);

	return main_array;
};

export const findValueFromArray = (main_array: any, key: any) => {
	const result = main_array.find((o) => o.key === key);
	return result;
};

export const getUniqueValues = (main_array: any, key: any) => {
	const uniqueValues = [...new Set(main_array.map((item) => item[key]).filter((value)=>value!==null && value!=='' && value!==' '))];
	return uniqueValues.sort();
};

export const getMappedArray = (main_array: any, key: string = 'label', value: string = 'value') => {
	const mappedArray = main_array.map((item) => ({
		[key]: item ? item.toString() : '',
		[value]: item ? item.toString() : '',
	}));
  // .sort((a,b)=>a.label.localeCompare(b.label))
	return mappedArray;
};
export const filterIncludedValuesFromArray = (main_array: any, filterArray: any, key: any) => {
	if (filterArray.length === 0) {
		return main_array;
	}
	const result = main_array.filter((data) => filterArray.includes(data[key]));
	return result;
};

export const filterIncludedValuesFromText = (main_array: any, filterVal: any, key: any) => {
	if (filterVal === '') {
		return main_array;
	}

	const result = main_array.filter((row) => row[key].toLowerCase().includes(filterVal.toLowerCase()));
	return result;
};

export const filterExactValuesFromText = (main_array: any, filterVal: any, key: any) => {
	if (filterVal === '') {
		return main_array;
	}

	const result = main_array.filter((row) => row[key] === filterVal);
	return result;
};

export const deepSearch = (obj: any, searchTerm: string): boolean => {
  for (const key in obj) {
    if (typeof obj[key] === 'object' && obj[key] !== null) {
      if (deepSearch(obj[key], searchTerm)) {
        return true;
      }
    } else if (typeof obj[key] === 'string') {
      if (obj[key].toLowerCase().includes(searchTerm)) {
        return true;
      }
    }
  }
  return false;
}

export const  editDuplicateRecordChecker = (fromValue:{}, formProperty:string[],table_data:any[], tableProperty:string[], ruleId:string):boolean =>{
    // console.log('fromValue=>',fromValue,'formProperty=>',formProperty,'tableProperty=>',tableProperty)
    let finalResult = table_data.filter((record)=>{
      if(ruleId !== (record.RULE_ID ? record.RULE_ID : record.CONN_ID)){
        let singleRecordResult:boolean[] = []
        formProperty.forEach((fProp:string,i:number)=>{
         if((record[tableProperty[i]] !== null ? record[tableProperty[i]].toLowerCase():'') === (fromValue[fProp].label?fromValue[fProp].label:fromValue[fProp]).toLowerCase()){
           singleRecordResult.push(true)
         }else{
           singleRecordResult.push(false)
         }
       })
       singleRecordResult = singleRecordResult.filter(record=>!record)
       if(singleRecordResult.length){
         return false
       }else{
         return true
       }
      }else{
        return false
      }
    })
    if(finalResult.length){
      return true
    }else{
      return false
    }
  } 

  // export const editDuplicateRecordCheckerAdmin = (
  //   fromValue: any[],
  //   formProperty: string[],
  //   table_data: any[],
  //   tableProperty: string[],
  //   ruleId: string,
  //   commonProperty: string
  // ): boolean => {
  //   const combinationValues = formProperty.map((prop) => {
  //     const value = fromValue[prop]?.label || fromValue[prop];
  //     return value;
  //   });
  
  //   for (let j = 0; j < table_data.length; j++) {
  //     if (ruleId !== (table_data[j].RULE_ID ? table_data[j].RULE_ID : table_data[j][commonProperty])) {
  //       let isDuplicate = true;
  //       for (let i = 0; i < formProperty.length; i++) {
  //         const formProp = formProperty[i];
  //         const tableProp = tableProperty[i];
  //         const formValue = String(combinationValues[i]).toLowerCase();
  //         const tableValue = String(table_data[j][tableProp]).toLowerCase();
  //         if (formValue !== tableValue) {
  //           isDuplicate = false;
  //           break;
  //         }
  //       }
  //       if (isDuplicate) {
  //         return true;
  //       }
  //     }
  //   }
  
  //   return false;
  // };
  export const editDuplicateRecordCheckerAdmin = (
    fromValue: any[],
    formProperty: string[],
    table_data: any[],
    tableProperty: string[],
    ruleId: string,
    commonProperty: string
  ): boolean => {
    const combinationValues = formProperty.map((prop) => {
      const value = fromValue[prop]?.label || fromValue[prop];
      return value;
    });
  
    for (let j = 0; j < table_data.length; j++) {
      if (ruleId !== (table_data[j].RULE_ID ? table_data[j].RULE_ID : table_data[j][commonProperty])) {
        let isDuplicate = true;
        for (let i = 0; i < formProperty.length; i++) {
          const formProp = formProperty[i];
          const tableProp = tableProperty[i];
          const formValue = combinationValues[i];
          const tableValue = table_data[j][tableProp];
  
          if (formValue instanceof Date && typeof tableValue === 'string') {
            const formattedFormValue = formatDate(formValue);
            const formattedTableValue = formatDate(new Date(tableValue));
  
            if (formattedFormValue !== formattedTableValue) {
              isDuplicate = false;
              break;
            }
          } else if (typeof formValue === 'string' && typeof tableValue === 'string') {
            if (formValue.toLowerCase() !== tableValue.toLowerCase()) {
              isDuplicate = false;
              break;
            }
          } else if (formValue !== tableValue) {
            isDuplicate = false;
            break;
          }
        }
        if (isDuplicate) {
          return true;
        }
      }
    }
  
    return false;
  };
  
   
  
  
  
  export const  addDuplicateRecordChecker = (fromValue:{}, formProperty:string[],table_data:any[], tableProperty:string[]):boolean =>{
   
    let finalResult = table_data.filter((record)=>{
        let singleRecordResult:boolean[] = []
        formProperty.forEach((fProp:string,i:number)=>{
          // console.log(record)
          // console.log(`${record[tableProperty[i]]}`,(record[tableProperty[i]]))
          // console.log(`${fromValue[fProp].label.toLowerCase()}`,(fromValue[fProp].label).toLowerCase())
         if((record[tableProperty[i]] !== null ? record[tableProperty[i]].toLowerCase():'') === (fromValue[fProp].label?fromValue[fProp].label:fromValue[fProp]).toLowerCase()){
           singleRecordResult.push(true)
         }else{
           singleRecordResult.push(false)
         }
       })
       singleRecordResult = singleRecordResult.filter(record=>!record)
       if(singleRecordResult.length){
         return false
       }else{
         return true
       }
    })
    if(finalResult.length){
      return true
    }else{
      return false
    }
  } 
  
  
  // export const addDuplicateRecordCheckerAdmin = (fromValue: {}, formProperty: string[], table_data: any[], tableProperty: string[]): boolean => {
  //   const combinationValues = formProperty.map((prop) => {
  //     const value = fromValue[prop] && fromValue[prop].label ? fromValue[prop].label : fromValue[prop];
  //     return value;
  //   });
  
  //   for (let j = 0; j < table_data.length; j++) {
  //     let matchCount = 0;
  //     for (let i = 0; i < formProperty.length; i++) {
  //       const formProp = formProperty[i];
  //       const tableProp = tableProperty[i];
  //       const formValue = combinationValues[i];
  //       const tableValue = table_data[j][tableProp];
  //       if (typeof formValue === 'string' && typeof tableValue === 'string'){
  //         if (formValue.toLowerCase() == tableValue.toLowerCase()) { 
  //         matchCount++;
  //       }
  //     }
  //     else if (formValue === tableValue){
  //       matchCount++;
  //     }
  //   }
  //     if (matchCount === formProperty.length) {
  //       return true;
  //     }
  //   }
  //   return false;
  // };
  function formatDate(date: Date): string {
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');
    return `${year}-${month}-${day}`;
  }
  export const addDuplicateRecordCheckerAdmin = (fromValue: {}, formProperty: string[], table_data: any[], tableProperty: string[]): boolean => {
    const combinationValues = formProperty.map((prop) => {
      const value = fromValue[prop] && fromValue[prop].label ? fromValue[prop].label : fromValue[prop];
      return value;
    });
  
    for (let j = 0; j < table_data.length; j++) {
      let matchCount = 0;
      for (let i = 0; i < formProperty.length; i++) {
        const formProp = formProperty[i];
        const tableProp = tableProperty[i];
        const formValue = combinationValues[i];
        const tableValue = table_data[j][tableProp];
  
        if (formValue instanceof Date && typeof tableValue === 'string') {
          const formattedFormValue = formatDate(formValue);
          const formattedTableValue = new Date(tableValue).toISOString().substring(0, 10); 
          if (formattedFormValue === formattedTableValue) {
            matchCount++;
          }
        } else if (typeof formValue === 'string' && typeof tableValue === 'string') {
          if (formValue.toLowerCase() === tableValue.toLowerCase()) {
            matchCount++;

          }
        } else if (formValue === tableValue) {
          matchCount++;

        }
      }
  
      if (matchCount === formProperty.length) {
        return true;
      }
    }
  
    return false;
  };
  
  
  

  export const checkingDuplicationBetweentwoArray = (formValue:any[],tableArr:any[]) =>{
    // ,formProperty,tableProperty


    // for(fromElement of formValue){
    //   const singleRecordResult = tableArr.some(element=> element.COL_ID === fromElement.colId && element.DQ_RULE_TYPE ===  fromElement.ruleType.label)
      
    //   console.log('singleRecordResult=>',singleRecordResult)
    //   if(singleRecordResult){
    //     return fromElement.attribute
    //     }
        
    // }

    // return false
  }


 