// export const getFormData = (object) =>
// 	Object.keys(object).reduce((formData, key) => {
// 		debugger;
// 		formData.append(key, object[key]);
// 		return formData;
// 	}, new FormData());

// export const getFormData = (object) => {
// 	const formData = new FormData();
// 	Object.keys(object).forEach((key) => formData.append(key, object[key]));
// 	return formData;
// };

export const convertToFormData = (obj: any, formData: any) => {
	//let formData = new FormData();

	for (const key in obj) {
		switch (typeof obj[key]) {
			case 'boolean':
				formData.append(key, JSON.stringify(obj[key]));

				break;

			case 'object':
				if (obj[key]) {
					if (obj[key] instanceof File) {
						formData.append(key, obj[key]);
					}

					if (obj[key] instanceof FileList) {
						const files = obj[key];

						for (let i = 0; i < files.length; i++) {
							formData.append(key, files[i]);
						}
					} else {
						formData.append(key, JSON.stringify(obj[key]));
					}
				}

				break;

			case 'string':
				formData.append(key, obj[key]);

				break;

			case 'number':
				formData.append(key, obj[key]);

				break;

			default:
				break;
		}
	}

	return formData;
};
