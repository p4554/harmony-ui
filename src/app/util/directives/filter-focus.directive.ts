import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
	selector: '[appFilterOnFocus]',
})
export class FilterOnFocusDirective {
	constructor(private elementRef: ElementRef) {}

	@HostListener('focus', ['$event']) onFocus() {
		if (this.elementRef.nativeElement.value === 'All Customers') {
			this.elementRef.nativeElement.select();
		}
	}
}
