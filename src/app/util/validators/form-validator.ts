export function findInvalidControls(submitForm: any) {
	const invalid = [];
	const controls = submitForm.controls;
	for (const name in controls) {
		if (controls[name].invalid) {
			console.log(controls[name].errors);
			invalid.push(name);
		}
	}
	return invalid;
}

export function changeNotNumberToZero(form: any, param: string) {
	if (form.get(param)?.value === '' || form.get(param)?.value === null || form.get(param)?.value < 0) {
		form.controls[param].setValue(0);
	}
	return;
}
