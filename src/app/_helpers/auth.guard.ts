import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot , NavigationEnd} from '@angular/router';
import { LocalStorageService } from '@app/_services/local-storage.service';

import { AuthenticationService } from '../_services';
import { NavigationService } from '@app/_services/navigation.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
	constructor(
		private router: Router,
		private authenticationService: AuthenticationService,
		private localStorageService: LocalStorageService,
		private navigationService: NavigationService
	) {}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		// console.log(route)
		// console.log(state)
		const user = this.authenticationService.currentUserValue;
		if(user){
		// logged in so return true
			return true
		}else{
			this.navigationService.redirectToLogin();
			return false
		}
	}
}

@Injectable({ providedIn: 'root' })
export class loginAuthGuard implements CanActivate {
	constructor(
		private router: Router,
		private authenticationService: AuthenticationService,
		private localStorageService: LocalStorageService,
		private navigationService: NavigationService
	) {}
	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		console.log(route)
		console.log(state)
		this.router.events.subscribe(event => {
			if (event instanceof NavigationEnd) {
			  // set previousUrl to the URL of the previous route
			  const previousUrl = event.url;
			  console.log(previousUrl)
			}
		  });
		const user = this.authenticationService.currentUserValue;
		if(!user){

			return true
		}else{
			if(state.url !== '/login'){

				this.router.navigateByUrl(state.url)
			}else {
				this.navigationService.goToAdminDashboard()
			}
			
			return false
		}
	}
}
