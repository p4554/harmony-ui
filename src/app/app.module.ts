import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { SharedModule } from './shared.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { NavbarModule } from './components/navbar/navbar.module';

import { AdminModule } from './dashboard/admin/admin.module';
import { UserModule } from './dashboard/user/user.module';
import { LoginModule } from './login/login.module';
import { AdvancedSearchModule } from './pages/advanced-search/advanced-search.module';
import { DataSourcePageModule } from './pages/data-source-page/data-source-page.module';
import { ErrorInterceptor } from './_services/http-error-interceptor';
import { MessageService } from 'primeng/api';
import { NgChartsModule } from 'ng2-charts';
import { DialogService } from 'primeng/dynamicdialog';
import { AuthGuard, loginAuthGuard } from './_helpers/auth.guard';
import { OAuthModule } from 'angular-oauth2-oidc';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,

    FormsModule,
    HttpClientModule,

    BrowserAnimationsModule,
    RouterModule,
    LoginModule,
    AdminModule,
    UserModule,
    DataSourcePageModule,
    AdvancedSearchModule,

    NavbarModule,
    SharedModule,
    NgChartsModule,
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: ['https://api.example.com'],
        sendAccessToken: true
      }
    })
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },MessageService,DialogService,AuthGuard, loginAuthGuard],
  bootstrap: [AppComponent],
})
export class AppModule {}
