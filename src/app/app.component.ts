import { Component } from '@angular/core';
import { LocalStorageService } from './_services/local-storage.service';
import { ActivatedRoute, Router, RouterStateSnapshot } from '@angular/router';
import { UserInactiveService } from './_services/user-inactive.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent {

	isLoggedIn = false;
	isTopNav = false;

	constructor(private localStorageService: LocalStorageService, private router: Router,
		private activatedRoute: ActivatedRoute,
		private userInactiveService: UserInactiveService
	) { }

	ngOnInit() {

		this.localStorageService.isLoggedIn$().subscribe((isLoggedIn) => {
			this.isLoggedIn = isLoggedIn;
		});

		this.localStorageService.topNavVisibility().subscribe(topNavVisibility => {
			this.isTopNav = topNavVisibility
		})


		// if(this.isLoggedIn){
		// 	this.router.navigateByUrl('admin-dashboard')
		// }

		// this.userInactiveService..subscribe((isInactive: boolean) => {
		// 	if (isInactive) {
		// 		// User is inactive, handle the event here
		// 		alert('Session Timeout')
		// 	}
		// });
	}
}
