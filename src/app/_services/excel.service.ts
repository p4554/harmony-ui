import { Injectable } from '@angular/core';
import * as xlsx from 'xlsx';

@Injectable({
  providedIn: 'root'
})
export class ExcelService {

  constructor() { }

  async exportExcel(filename, data) {
	//	const fileName = 'Export_Quality_Rules.xlsx';

		const reportData = JSON.parse(JSON.stringify(data));

		const wb1: xlsx.WorkBook = xlsx.utils.book_new();

		const ws1: xlsx.WorkSheet = xlsx.utils.json_to_sheet([]);

		xlsx.utils.book_append_sheet(wb1, ws1, 'sheet1');

		//start frm A2 here
		xlsx.utils.sheet_add_json(wb1.Sheets['sheet1'], reportData, {
			skipHeader: false,
		});

		xlsx.writeFile(wb1, filename);
	}

}
