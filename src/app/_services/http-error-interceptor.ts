import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, finalize, timeout } from 'rxjs/operators';
import { LoaderService } from './loading.service';
import { MessageService } from 'primeng/api';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(private loaderService: LoaderService, private messageService: MessageService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const timeoutValue = 10000; // Set a timeout of 6 seconds
    // Start the loader spinner
    this.loaderService.showLoader();

    return next.handle(request)
      .pipe(
        // timeout(timeoutValue), // Set the timeout
        catchError((error: HttpErrorResponse) => {
          // Stop the loader spinner from running
          this.loaderService.hideLoader();

          // Handle the HTTP error appropriately
          if (error.status === 0) {
            console.log('Server not responding');
            // Display an error message to the user
          } else {

            console.log(`Error ${error.status}: ${error.message}`);
            // Display an error message to the user
          }

          // Rethrow the error to propagate it to the subscriber
        //  alert('Server Not Responding');
        // this.messageService.add({severity:'success', summary:'Service Message', detail:'Via MessageService'});

          return throwError(error);
        }),
        finalize(() => {
          // Stop the loader spinner from running when the request completes
          this.loaderService.hideLoader();
        })
      );
  }
}
