import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BusinessRuleApiService {
  restApiUrl = environment.restApiUrl;
  constructor(private httpClient: HttpClient) {
  }
  getCustomerTable() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/customerTable`);
  }
  getBusinessProcess() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/businessProcess`);
  }
  getCustomerData() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/customerData`);
  }
  marketTable() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/marketTable`);
  }
  marketCode() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/marketCode`);
  }
  normalizationFactor() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/normalizationFactor`);
  }
  productUnit() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/productUnit`);
  }
  normalizationFactorTable() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/normalizationFactorTable`);
  }
  zipTerritoryTable() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/zipTerritoryTable`);
  }
  zipCode() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/zipCode`);
  }
  customerTerritoryTable() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/customerTerritoryTable`);
  }
  customerId() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/customerId`);
  }
  employeeTerritoryTable() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/employeeTerritoryTable`);
  }
  employeeId() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/employeeId`);
  }
  groupMapTable() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/groupMapTable`);
  }
  specialtyGroup() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/specialtyGroup`);
  }
  specialtyUpdateTable() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/specialtyUpdateTable`);
  }
  specialtyDesc() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/specialtyDesc`);
  }
  inclusionTable() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/inclusionTable`);
  }
  specialtyType() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/specialtyType`);
  }

  ruleType() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/ruleType`);
  }
  businessUnit() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/businessUnit`);
  }
  therapeuticClass() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/therapeuticClass`);
  }
  territoryId() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/territoryId`);
  }
  productCode() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/productCode`);
  }
  cycle() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/cycle`);
  }
  specialtyCode() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/specialtyCode`);
  }
  functionalArea() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/businessRule/functionalArea`);
  }
  addCustomer(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/businessRule/addCustomer`, data);
  }
  updateCustomer(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/businessRule/updateCustomer`, data);
  }
  deleteCustomer(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/businessRule/deleteCustomer`, data);
  }
  addMarketDefinition(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/businessRule/addMarketDefinition`, data);
  }
  updateMarketDefinition(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/businessRule/updateMarketDefinition`, data);
  }
  deleteMarketDefinition(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/businessRule/deleteMarketDefinition`, data);
  }
  addNormalizationFactor(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/businessRule/addNormalizationFactor`, data);
  }
  updateNormalizationFactor(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/businessRule/updateNormalizationFactor`, data);
  }
  deleteNormalizationFactor(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/businessRule/deleteNormalizationFactor`, data);
  }
  addZipTerritory(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/businessRule/addZipTerritory`, data);
  }
  updateZipTerritory(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/businessRule/updateZipTerritory`, data);
  }
  deleteZipTerritory(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/businessRule/deleteZipTerritory`, data);
  }
  addCustomerTerritory(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/businessRule/addCustomerTerritory`, data);
  }
  updateCustomerTerritory(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/businessRule/updateCustomerTerritory`, data);
  } 
  deleteCustomerTerritory(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/businessRule/deleteCustomerTerritory`, data);
  }
  addEmployeeTerritory(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/businessRule/addEmployeeTerritory`, data);
  }
  updateEmployeeTerritory(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/businessRule/updateEmployeeTerritory`, data);
  }
  deleteEmployeeTerritory(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/businessRule/deleteEmployeeTerritory`, data);
  }
  addGroupMap(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/businessRule/addGroupMap`, data);
  }
  updateGroupMap(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/businessRule/updateGroupMap`, data);
  }
  deleteGroupMap(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/businessRule/deleteGroupMap`, data);
  } 
  addSpecialtyUpdate(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/businessRule/addSpecialtyUpdate`, data);
  }
  updateSpecialtyUpdate(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/businessRule/updateSpecialtyUpdate`, data);
  }
  deleteSpecialtyUpdate(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/businessRule/deleteSpecialtyUpdate`, data);
  }
  addSpecialtyInclusion(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/businessRule/addSpecialtyInclusion`, data);
  }
  updateSpecialtyInclusion(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/businessRule/updateSpecialtyInclusion`, data);
  }
  deleteSpecialtyInclusion(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/businessRule/deleteSpecialtyInclusion`, data);
  }
}