import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "@environments/environment";
@Injectable({
    providedIn: 'root',
})

export class DataQualityDropdownApiService {
    restApiUrl = environment.restApiUrl;
    constructor(private httpClient: HttpClient) {
    }


    getDataZone() {
        return this.httpClient.get<any>(`${this.restApiUrl}/v1/api/dataQuality/getDataZone`);
    }


}