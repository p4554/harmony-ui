import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from './authentication.service';

@Injectable({
	providedIn: 'root',
})
export class NavigationService {
	constructor(private _router: Router) {
		// console.log(_router.config[1].path);
	}

	goToDashboard() {}

	goToUserDashboard(): void {
		this._router.navigate(['user-dashboard']);
	}

	goToAdminDashboard(): void {
		this._router.navigate(['admin-dashboard']);
	}
	goToForgotPassword(): void {
		this._router.navigate(['forgot-password']);
	}
	goToDataQualityRule():void {
		this._router.navigateByUrl("data-quality-rule")
	}
	redirectToLogin(): void {
		this._router.navigateByUrl("");
	}
}
