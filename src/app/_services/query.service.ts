import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class QueryService {
  constructor(private http: HttpClient) { }

  fetchQuery(queryName: string) {
    return this.http.get('assets/sql-queries.json').pipe(
      map((queries: any) => queries[queryName])
    );
  }
}
