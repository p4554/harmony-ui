import { Injectable } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { environment } from '@environments/environment';
import { BehaviorSubject, Observable, Subscription, lastValueFrom, timer } from 'rxjs';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class UserInactiveService {


  private IDLE_TIMEOUT = environment.IDLE_TIMEOUT
  private idleTimer: number;
  private timer;

  subscription: Subscription;


  constructor(private router: Router,
    private authService: AuthenticationService,
  ) {

  }

  setUserInterActionTimer() {
    this.subscription = this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.resetTimer();
      }
    });
    this.idleTimer = this.IDLE_TIMEOUT
    this.startTimer();
  }


  startTimer() {
    // console.log('timer start')
    this.timer = setInterval(async () => {
      this.idleTimer = this.idleTimer - 1000;
      // console.log(this.idleTimer)
      if (this.idleTimer <= 0) {
        this.subscription.unsubscribe();
        // const logout = await lastValueFrom(this.authService.logout());
        alert('session TimeOut')
        //  this.navigationService.redirectToLogin();
        // your implementation of logout
        clearInterval(this.timer);
      }
    }, 1000);
  }

  removeTimer() {
    this.subscription?.unsubscribe();
    clearInterval(this.timer);
  }

  resetTimer() {
    this.idleTimer = this.IDLE_TIMEOUT;
  }
}
