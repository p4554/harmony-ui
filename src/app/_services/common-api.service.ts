import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map, shareReplay } from 'rxjs/operators';

import { environment } from '@environments/environment';

@Injectable({
	providedIn: 'root',
})
export class CommonApiService {
	restApiUrl = environment.restApiUrl;

	constructor(private httpClient: HttpClient) { }

	getStates() {
		return this.httpClient.get(this.restApiUrl + '/v1/api/admin/get-states');
	}
}
