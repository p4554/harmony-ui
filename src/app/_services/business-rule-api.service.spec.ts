import { TestBed } from '@angular/core/testing';

import { BusinessRuleApiService } from './business-rule-api.service';

describe('BusinessRuleApiService', () => {
  let service: BusinessRuleApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BusinessRuleApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
