/* eslint-disable @typescript-eslint/member-ordering */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, filter } from 'rxjs/operators';

import { isPlatformBrowser } from '@angular/common';
import { Inject, PLATFORM_ID } from '@angular/core';

import { BehaviorSubject, from, Observable, of } from 'rxjs';

import { CommonApiService } from '@app/_services/common-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Buffer } from 'buffer';

import { switchMap } from 'rxjs/operators';
import { environment } from '@environments/environment';
import { NavigationService } from './navigation.service';

@Injectable({
	providedIn: 'root',
})
export class AuthenticationService {
	restApiUrl = environment.restApiUrl;

	// storage_mode: any;
	device: any;
	error_msg = 'Something went wrong. Contact administrator.';

	private currentMenuSubject = new BehaviorSubject<any>(null);
	public currentMenu: Observable<any> = this.currentMenuSubject.asObservable();

	private currentUserSubject = new BehaviorSubject<any>(null);
	public currentUser: Observable<any> = this.currentUserSubject.asObservable();

	constructor(
		private httpClient: HttpClient,
		private navigationService: NavigationService,
		private _commonApiService: CommonApiService,
		private _router: Router
	) { }

	public get currentUserValue(): any {
		this.setCurrentUser()
		return this.currentUserSubject.value;
	}

	veryfyUser(user: string, pass: string) {
		return this.httpClient.get(`${this.restApiUrl}/users/?username=${user}&password=${pass}`);
	}

	async setCurrentMenu(clickedMenu) {
		localStorage.setItem('currentMenu', clickedMenu);
		this.currentMenuSubject.next(clickedMenu);
	}

	setCurrentUser(): void {
		this.currentUserSubject.next(localStorage.getItem('userName'));
	}


	// this.currentUserSubject.next(this.getLocalStoreItems('currentUser'));
}
