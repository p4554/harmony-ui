import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SourceConfigApiServiceService {

  restApiUrl = environment.restApiUrl;
  constructor(private httpClient: HttpClient) {
  }


  getSourceConnectorTable() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/source/sourceConnectorTable`);
  }
  addSourceConnector(data) {
    return this.httpClient.post(`${this.restApiUrl}/v1/api/source/addSourceConnector`, data)
}
updateSourceConnector(data) {
  return this.httpClient.post(`${this.restApiUrl}/v1/api/source/updateSourceConnector`, data)
}
deleteSourceConnector(data) {
  return this.httpClient.post(`${this.restApiUrl}/v1/api/source/deleteSourceConnector`, data)
}
}
