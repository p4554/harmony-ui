import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "@environments/environment";
@Injectable({
    providedIn: 'root',
})
export class DataQualityApiService {

    restApiUrl = environment.restApiUrl;
    constructor(private httpClient: HttpClient) {
    }

    getAllRules() {
        return this.httpClient.get<any>(`${this.restApiUrl}/v1/api/dataQuality/getAll`);
    }

    getThVariationRules() {
        return this.httpClient.get<any>(`${this.restApiUrl}/v1/api/dataQuality/th-variation-getAll`);
    }
    createThresholdVariationRule(data) {
        return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/dataQuality/th-variation-rule-create`, data);
    }
    updateThresholdVariationRule(data) {
        return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/dataQuality/th-variation-rule-update`, data);
    }
    deleteRule(data) {
        return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/dataQuality/delete-rule`, data);
    }

    // ref integrity methods 
    getReferentialIntegrityRules() {
        return this.httpClient.get<any>(`${this.restApiUrl}/v1/api/dataQuality/ref-integrity-getAll`);
    }
    createReferentialIntegrityRule(data) {
        return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/dataQuality/ref-integrity-rule-create`, data);
    }
    updateReferentialIntegrityRule(data) {
        return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/dataQuality/ref-integrity-rule-update`, data);
    }


    // control total 
    getControlTotalRules() {
        return this.httpClient.get<any>(`${this.restApiUrl}/v1/api/dataQuality/control-total-getAll`);
    }
    createControlTotalRule(data) {
        return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/dataQuality/control-total-rule-create`, data);
    }
    updateControlTotalRule(data) {
        return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/dataQuality/control-total-rule-update`, data);
    }

    // data validation apis

    getDataValidationRules() {
        return this.httpClient.get<any>(`${this.restApiUrl}/v1/api/dataQuality/data-validation-getAll`);
    }
    createDataValidationRule(data) {
        return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/dataQuality/data-validation-rule-create`, data);
    }
    updateDataValidationRule(data) {
        return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/dataQuality/data-validation-rule-update`, data);
    }

    // threshold trend  apis 

    getThTrendRules() {
        return this.httpClient.get<any>(`${this.restApiUrl}/v1/api/dataQuality/th-trend-getAll`);
    }
    createThTrendRules(data) {
        return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/dataQuality/th-trend-rule-create`, data);
    }
    updateThTrendRules(data) {
        return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/dataQuality/th-trend-rule-update`, data);
    }


    // control sum apis


    getControlSumRules() {
        return this.httpClient.get<any>(`${this.restApiUrl}/v1/api/dataQuality/control-sum-getAll`);
    }
    createControlSumRules(data) {
        return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/dataQuality/control-sum-create`, data);
    }
    updateControlSumRules(data) {
        return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/dataQuality/control-sum-update`, data);
    }

    // updating rule status

    updateRuleStatus(data) {
        return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/dataQuality/updateStatus`, data)
    }

    // common apis

    dataZone() {
        return this.httpClient.get<any>(`${this.restApiUrl}/v1/api/dataQuality/dataZone`);
    }
    dateFormat() {
        return this.httpClient.get<any>(`${this.restApiUrl}/v1/api/dataQuality/data-validation-dateFormat`);
    }
    dataSetName(data) {
        return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/dataQuality/dataSetName`,data);
    }
    maticName(data) {
        return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/dataQuality/maticName`,data);
    }
    dataProvider(data) {
        return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/dataQuality/dataProvider`,data);
    }
    groupBy(data) {
        return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/dataQuality/groupBy`,data);
    }
    status() {
        return this.httpClient.get<any>(`${this.restApiUrl}/v1/api/dataQuality/status`);
    }
    DQSeverity() {
        return this.httpClient.get<any>(`${this.restApiUrl}/v1/api/dataQuality/DQSeverity`);
    }
    thFunction() {
        return this.httpClient.get<any>(`${this.restApiUrl}/v1/api/dataQuality/thFunction`);
    }
    timeLineAttribute() {
        return this.httpClient.get<any>(`${this.restApiUrl}/v1/api/dataQuality/timeLineAttribute`);
    }
    dataSetAttribute(data) {
        return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/dataQuality/dataSetAttribute`,data);
    }
    ruleType() {
        return this.httpClient.get<any>(`${this.restApiUrl}/v1/api/dataQuality/data-validation-ruleType`);
    }
    controlDataset(data) {
        return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/dataQuality/control-total-rule-dataSetName`,data);
    }
}