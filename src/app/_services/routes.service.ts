import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable({
	providedIn: 'root',
})
export class RoutesService {
	constructor(private _router: Router) {}

	routeDashboard() {
		this._router.navigateByUrl('/home/admin-dashboard');
	}

	routeEnquiries() {
		this._router.navigateByUrl('/home/enquiry/open-enquiry/O/weekly');
	}

	routeAddEnquiry() {
		this._router.navigateByUrl('/home/enquiry');
	}

	processEnquiry(id) {
		this._router.navigate(['/home/enquiry/process-enquiry', id]);
	}

	// Navigation
	searchSaleOrder() {
		this._router.navigateByUrl('/home/search-sales');
	}

	searchStockIssue() {
		this._router.navigateByUrl('/home/search-stock-issues');
	}
}
