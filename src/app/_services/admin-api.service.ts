import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AdminApiService {
  restApiUrl = environment.restApiUrl;
  constructor(private httpClient: HttpClient) {
  }

  // tenant create 

  flushCache() {
    return this.httpClient.get<any>(`${this.restApiUrl}/v1/api/flushCache`);
  }
  createTenant(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/tenant/create-tenant`, data);
  }
  editTenant(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/tenant/update-tenant`, data);
  }
  addUser(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/user/addUser`, data);
  }
  updateUser(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/user/updateUser`, data);
  }
  tenantTable() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/tenant/tenantTable`);
  }
  region() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/tenant/region`);
  }
  country() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/tenant/country`);
  }
  language() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/tenant/language`);
  }
  deleteTenant(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/tenant/deleteTenant`, data);
  }
  userTable() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/user/userTable`);
  }
  reportingManager() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/user/reportingManager`);
  }
  deleteUser(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/user/deleteUser`, data);
  }
  groupTable() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/group/groupTable`);
  }
  addGroup(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/group/addGroup`, data);
  }
  updateGroup(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/group/updateGroup`, data);
  }
  deleteGroup(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/group/deleteGroup`, data);
  }
  userAccessTable() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/userAccess/userAccessTable`);
  }
  groupName() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/userAccess/groupName`);
  }
  tenantName() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/userAccess/tenantName`);
  }
  userModule() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/userAccess/userModule`);
  }
  addUserAccess(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/userAccess/addUserAccess`, data);
  }
  updateUserAccess(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/userAccess/updateUserAccess`, data);
  }
  deleteUserAccess(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/userAccess/deleteUserAccess`, data);
  }
  userGroupTable() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/userGroup/userGroupTable`);
  }
  userName() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/userGroup/userName`);
  }
  addUserGroup(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/userGroup/addUserGroup`, data);
  }
  updateUserGroup(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/userGroup/updateUserGroup`, data);
  }
  deleteUserGroup(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/userGroup/deleteUserGroup`, data);
  }
  userZoneTable() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/userZone/userZoneTable`);
  }
  dataZone() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/userZone/dataZone`);
  }
  addUserZone(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/userZone/addUserZone`, data);
  }
  updateUserZone(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/userZone/updateUserZone`, data);
  }
  deleteUserZone(data) {
    return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/userZone/deleteUserZone`, data);
  }
  sessionManagerTable() {
    return this.httpClient.get(`${this.restApiUrl}/v1/api/sessionManager/sessionManagerTable`);
  }
}
