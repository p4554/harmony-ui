import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '@environments/environment';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root',
})
export class CommonDataService {
	restApiUrl = environment.restApiUrl;
	constructor(private httpClient: HttpClient) {
	}

	getCustomerData(id: number) {
		return this.httpClient.get(`${this.restApiUrl}/v1/api/admin/get-customer-details/${id}`);
	}

	getOnBoardData() {
		return this.httpClient.get(`${this.restApiUrl}/onboard`);
	}
	getusersData() {
		return this.httpClient.get(`${this.restApiUrl}/usersData`);
	}

	getSourceConnectorTableData(data: any) {
		return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/DBconnector`, data);
	}
	getRuleLibraryData() {
		return this.httpClient.get<any>(`${this.restApiUrl}/v1/api/ruleLibrary/ruleLibraryTable`);
	}
	UploadTenantImage(data) {
		return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/tenant/imageUpload`, data)
	}
	editTenantImage(data) {
		return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/tenant/editImage`, data)
	}
	deleteTenantImage(data) {
		return this.httpClient.post<any>(`${this.restApiUrl}/v1/api/tenant/deleteImage`, data)
	}
	getDatabaseProfilingPath(datazone, dataSetName) {
		return this.httpClient.get(
			`https://harmony.datazymes.com/Home/GetDatabaseProfilingPath?SchemaName=${datazone}&TableName=${dataSetName}`
		);
	}
	getS3ProfilingPath(dataSetName) {
		return this.httpClient.get(
			`https://harmony.datazymes.com/Home/GetS3ObjectProfilingPath?key=${dataSetName}`
		);
	}
	getLocalProfilePath(filename) {

		return this.httpClient.get(
			`https://harmony.datazymes.com/DataGovernance/GetLocalProfilingPath?FileName=${filename}.csv&Html=${filename}`
		);

	}
	getInbox() {
		return this.httpClient.get('https://harmony.datazymes.com/Home/GetInbox');
	}

	getForgotEmail(email) {
		return this.httpClient.get<string>(`https://harmony.datazymes.com/Home/SendPasswordOnMail?Email=${email}`);
	}

	getProfiling() {
		// const data = {
		//   schemaName: 'HRMNY_LND_ZN',
		//   tableName: 'LND_ADDRESS'
		// };

		const data = new FormData();
		data.append('schemaName', 'HRMNY_LND_ZN');
		data.append('tableName', 'LND_ADDRESS');

		return this.httpClient.post<any>('https://harmony.datazymes.com:5000/profiling', data);
	}

	checkIfURLExists() {
		return this.httpClient.get(`https://harmony.datazymes.com/DataProfile/LND_ADDRESS.html`);
	}

	fetchNotifications() {
		return this.httpClient.get(`${this.restApiUrl}/v1/api//DBconnector/getNotifications`);
	}
	fetchInbox() {
		return this.httpClient.get(`${this.restApiUrl}/v1/api/DBconnector/inbox`);
	}
}

// https://harmony.datazymes.com/Home/GetDatabaseProfilingPath?SchemaName=HRMNY_LND_ZN&TableName=LND_ADDRESS

// https://harmony.datazymes.com/Home/GetInbox
