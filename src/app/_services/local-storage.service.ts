import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
	providedIn: 'root',
})
export class LocalStorageService {
	private loggedInSubject = new BehaviorSubject<boolean>(this.isLoggedIn());
	private topNavSubject = new BehaviorSubject<boolean>(this.isTopNav())

	isLoggedIn(): boolean {
		return localStorage.getItem('isLoggedIn') === 'true';
	}
	isTopNav():boolean{
		return localStorage.getItem('topNav') === 'true';
	}

	setLoggedIn(value: boolean): void {
		localStorage.setItem('isLoggedIn', value.toString());
		this.loggedInSubject.next(value);
	}
	setTopNav(value: boolean): void {
		localStorage.setItem('topNav', value.toString());
		this.topNavSubject.next(value);
	}

	topNavVisibility(): BehaviorSubject<boolean> {
		return this.topNavSubject;
	}
	isLoggedIn$(): BehaviorSubject<boolean> {
		return this.loggedInSubject;
	}

	setLoggedOut(): void {
		localStorage.removeItem('isLoggedIn');
		localStorage.removeItem('userName');
		localStorage.removeItem('role');
		localStorage.removeItem('userId');
		localStorage.removeItem('role');
		localStorage.removeItem('topNav');
		localStorage.removeItem('userEmailId');
    localStorage.removeItem('currentMenu');

		this.loggedInSubject.next(false);
		this.topNavSubject.next(false)
	}

	getRole() {
		return localStorage.getItem('role');
	}
}
