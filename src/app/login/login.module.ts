// import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
// import { LoginComponent } from './login.component';
// import { RouterModule } from '@angular/router';
// import { InputTextModule } from 'primeng/inputtext';
// import { ButtonModule } from 'primeng/button';
// import { FormsModule } from '@angular/forms';
// import { HttpClientModule } from '@angular/common/http';
// import { ToastModule } from 'primeng/toast';
// import { ReactiveFormsModule } from '@angular/forms';

// @NgModule({
// 	declarations: [LoginComponent],
// 	imports: [
// 		CommonModule,
// 		RouterModule,
// 		InputTextModule,
// 		ButtonModule,
// 		FormsModule,
// 		HttpClientModule,
// 		ToastModule,
// 		ReactiveFormsModule,
// 	],
// 	exports: [LoginComponent],
// })
// export class LoginModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { RouterModule } from '@angular/router';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ToastModule } from 'primeng/toast';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginPageRoutingModule } from './login-routing.module';
import { SharedModule } from '@app/shared.module';

@NgModule({
	declarations: [LoginComponent],
	imports: [
		CommonModule,
		RouterModule,
		InputTextModule,
		ButtonModule,
		FormsModule,
		HttpClientModule,
		ToastModule,
		ReactiveFormsModule,
		LoginPageRoutingModule,
		SharedModule
	],
	exports: [LoginComponent],
})
export class LoginModule {}
