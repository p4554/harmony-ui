import { Component, OnInit } from '@angular/core';

import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LocalStorageService } from '@app/_services/local-storage.service';
import { AuthenticationService } from '@app/_services/authentication.service';
import { NavigationService } from '@app/_services/navigation.service';
import { CommonDataService } from '@app/_services/common-data.service';
import { LoaderService } from '@app/_services/loading.service';
import { patternValidator } from '@app/util/validators/pattern-validator';
import { EMAIL_REGEX } from '@app/util/helper/patterns';
import { OAuthService, OAuthSuccessEvent } from 'angular-oauth2-oidc';
@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss'],
	providers: [MessageService, OAuthService], 
})
export class LoginComponent implements OnInit {	
	constructor(
		private authenticationService: AuthenticationService,
		private localStorageService: LocalStorageService,
		private toast: MessageService,
		private route: Router, private navigationService: NavigationService,
		private commonDataService:CommonDataService,
		public loaderService: LoaderService,
		public oauthService: OAuthService
	) {
		if (this.localStorageService.isLoggedIn()) {
			if (this.localStorageService.getRole() === 'admin') {
				// Redirect the user to the admin page
       		 this.navigationService.goToAdminDashboard();
			}
		}
	}
	loginResponse:boolean = false
	rememberMeFlag:boolean = true
	user: any;
	pass: any;
	login_from: FormGroup;
	
	passType:string = "password"
	passIcon : string = "pi pi-eye"
	submited:boolean
	errMsg = {require:"This field is required", email :"Please enter a valid email address"}
	
	ngOnInit() {
		this.submited = false;
		this.login_from = new FormGroup({
			username: new FormControl('', [Validators.required, patternValidator(EMAIL_REGEX)]),
			password: new FormControl('', [Validators.required]),
		});
		this.user = this.getUserFromControl();
		this.pass = this.getPasswordFromControl();

		
	}
	// login Handler
	handleClick = async () => {
		this.submited = true;
		if(this.login_from.valid){
			this.loaderService.showLoader()
			const userName = this.user.value.toLowerCase()
			console.log(userName)
			let loginData =  {
				"query":`SELECT FIRST_NM, LAST_NM, IS_DEL_FLG, USER_ID, USER_NM, LDAP_USER_FLG, LOCL_USER_FLG, ADMN_FLG, USER_PASSWD, USER_EMAIL_ID, ACTV_FLG, CREATD_BY, CREATD_DT, UPDTD_BY, UPDTD_DT FROM HRMNY_DB.HRMNY_CNF_ZN.USER_INFO where USER_EMAIL_ID= '${userName}'  AND USER_PASSWD='${this.pass.value}'`,   
				"flag": "s"
		   }
			this.commonDataService.getSourceConnectorTableData(loginData).subscribe(data=>{
				console.log(data)
				this.loaderService.hideLoader()
				if(data.length){
					const userName = `${data[0].FIRST_NM} ${data[0].LAST_NM}`
					localStorage.setItem('userEmailId', JSON.stringify(userName));
					localStorage.setItem('userId', `${data[0].USER_ID}`);
					localStorage.setItem('role', 'admin')
					localStorage.setItem('userName', userName);
					this.localStorageService.setLoggedIn(this.rememberMeFlag);
					this.localStorageService.setTopNav(true);
					this.navigationService.goToAdminDashboard();
					this.authenticationService.setCurrentUser();
				}else{
					this.loginResponse = true
				}
			})
		}
	};
	getUserFromControl() {
		return this.login_from.get('username');
	}
	getPasswordFromControl() {
		return this.login_from.get('password');
	}
	passVisibleHandler(){
		this.passType = "text"
		this.passIcon = "pi pi-eye-slash"
		console.log(this.user)
	}
	passHideHandler(){
		this.passType = "password"
		this.passIcon = "pi pi-eye"
	}

	forGotPasswordClickHandler(){
		this.navigationService.goToForgotPassword()
	}

	ssoLogin(){
		this.oauthService.initLoginFlow();
	}
}


