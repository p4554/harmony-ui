import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RouterModule } from '@angular/router';

import { AutoFocusDirective } from './util/directives/auto-focus.directive';
import { DisableControlDirective } from './util/directives/disable-control.directive';
import { FilterOnFocusDirective } from './util/directives/filter-focus.directive';
import { FocusedDirective } from './util/directives/focused-directive';
import { NumericDirective } from './util/directives/numeric.directive';
import { PreventDoubleClickDirective } from './util/directives/prevent-double-click.directive';
import { SelectOnFocusDirective } from './util/directives/select-focus.directive';
import { CheckBooleanPipe } from './util/pipes/check-boolean.pipe';
import { DayWeekPipe } from './util/pipes/day-week.pipe';
import { FilterPipe } from './util/pipes/filter.pipe';
import { FormatBytePipe } from './util/pipes/format-bytes.pipe';
import { EscapeHtmlPipe } from './util/pipes/keep-html.pipe';
import { CustomPipe } from './util/pipes/keys.pipe';
import { NoNullPipe } from './util/pipes/nonull.pipe';
import { NullToDashPipe } from './util/pipes/null-dash.pipe';
import { NullToNaPipe } from './util/pipes/null-na.pipe';
import { NullToQuotePipe } from './util/pipes/null-quote.pipe';
import { NullToZeroPipe } from './util/pipes/null-zero.pipe';
import { PricePipePipe } from './util/pipes/price-formatter.pipe';
import { SafePipe } from './util/pipes/safe-html.pipe';
import { SanitizerUrlPipe } from './util/pipes/sanitizer-url.pipe';
import { SupDatePipe } from './util/pipes/sup-date.pipe';
import { TruncatePipe } from './util/pipes/truncate.pipe';
import { UrlidPipe } from './util/pipes/url-id.pipe';
import { ZeroToValPipe } from './util/pipes/zerotoval.pipe';
import { FormateDatePipePipe } from './util/pipes/formate-date-pipe.pipe';
// primeng

import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { ToastModule } from 'primeng/toast';
import { CheckboxModule } from 'primeng/checkbox';
import { MenuModule } from 'primeng/menu';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { ListboxModule } from 'primeng/listbox';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { InputSwitchModule } from 'primeng/inputswitch';
import { FileUploadModule } from 'primeng/fileupload';
import {RadioButtonModule} from 'primeng/radiobutton';
import { CalendarModule } from 'primeng/calendar';
// components
import { TablesComponent } from './components/tables/tables.component';
import { TabviewsComponent } from './components/tabviews/tabviews.component';

import { InputTextBoxComponent } from './components/form-components/input-text-box/input-text-box.component';
import { AutoCompletionComponent } from './components/form-components/auto-completion/auto-completion.component';
import { ListboxsComponent } from './components/form-components/listboxs/listboxs.component';
import { PopupModelComponent } from './components/popup-model/popup-model.component';
import { SingleSelectComponent } from './components/form-components/single-select/single-select.component';

import { AvatarsModule } from './components/avatars/avatars.module';
import { MenubarModule } from 'primeng/menubar';
import { SidebarModule } from 'primeng/sidebar';
import { OverlayModule } from 'primeng/overlay';
import { DollarSvgComponent } from './svg/dollar-svg/dollar-svg.component';
import { BusinessRuleSvgComponent } from './svg/business-rule-svg/business-rule-svg.component';

import { DownArrowSvgComponent } from './svg/down-arrow-svg/down-arrow-svg.component';
import {UserSvgComponent} from './svg/user-svg/user-svg.component'
import { HomeIconSvgComponent } from './svg/home-icon-svg/home-icon-svg.component';
import { AdminIconSvgComponent } from './svg/admin-icon-svg/admin-icon-svg.component';
import { BusinessAppSvgComponent } from './svg/business-app-svg/business-app-svg.component';
import { DataGovernanceSvgComponent } from './svg/data-governance-svg/data-governance-svg.component';
import { DsConfigSvgComponent } from './svg/ds-config-svg/ds-config-svg.component';
import { DataEngineeringSvgComponent } from './svg/data-engineering-svg/data-engineering-svg.component';
import { SourceConnectorComponent } from './pages/source-connector/source-connector.component';
import { SourceConnectorSvgComponent } from './svg/overlayMenu/source-connector-svg/source-connector-svg.component';
import { TextAreaComponent } from './components/form-components/text-area/text-area.component';
import { MultiSelectModule } from 'primeng/multiselect';
import { NewTenantComponent} from './components/new-tenant/new-tenant.component'
import {GroupsComponent} from './pages/setting/groups/groups.component'
import { LovMasterComponent } from "./pages/setting/lov-master/lov-master.component";
import { UserAccessComponent} from "./pages/setting/user-access/user-access.component";
import {UserGroupsComponent} from './pages/setting/user-groups/user-groups.component';
import {UserZoneComponent} from './pages/setting/user-zone/user-zone.component';
import {UsersComponent} from './pages/setting/users/users.component'
import {SessionManagerComponent} from './pages/setting/session-manager/session-manager.component'
import {EditTenantComponent} from './components/edit-tenant/edit-tenant.component'
import {RuleLibraryComponent} from './pages/data-governance/rule-library/rule-library.component';
import { BusinessRuleComponent } from './pages/data-governance/business-rule/business-rule.component';
// PrimeNG - Components imports
import { SlideMenuModule } from 'primeng/slidemenu';

import { TieredMenuModule } from 'primeng/tieredmenu';
import { CheckBoxComponent } from './components/form-components/check-box/check-box.component';
import { DeletePopupComponent } from './components/delete-popup/delete-popup.component';
import { DeleteButtonComponent } from './buttons/delete-button/delete-button.component';
import { NonOutlinedButtonComponent } from './buttons/non-outlined-button/non-outlined-button.component';
import { PrimaryButtonComponent } from './buttons/primary-button/primary-button.component';
import { FooterComponent } from './footer/footer.component';
import { MultiselectComponent } from './components/form-components/multiselect/multiselect.component';
import { ProgressSpinnerComponent } from './components/progress-spinner/progress-spinner.component';
import { SourceConnectorMiniSvgComponent } from './svg/overlayMenu/source-connector-mini-svg/source-connector-mini-svg.component';
import { SourceLibrarySvgComponent } from './svg/overlayMenu/source-library-svg/source-library-svg.component';
import { SourceOnboardSvgComponent } from './svg/overlayMenu/source-onboard-svg/source-onboard-svg.component';
import { ConfiguredSourceSvgComponent } from './svg/overlayMenu/configured-source-svg/configured-source-svg.component';
import { DeleteISvgComponent } from './svg/delete-i-svg/delete-i-svg.component';
import { PencilISvgComponent } from './svg/pencil-i-svg/pencil-i-svg.component';
import { DsFilterComponent } from './components/ds-components/ds-filter/ds-filter.component';
import { PrimeMultiSelectComponent } from './components/form-components/prime-multi-select/prime-multi-select.component';

import { QualityRulesComponent } from './pages/data-governance/quality-rules/quality-rules.component';
import { DataQualityRuleComponent } from './svg/data-quality-rule/data-quality-rule.component';
import { RuleLibrarySvgComponent } from './svg/rule-library-svg/rule-library-svg.component';
import { TFilterSvgComponent } from './svg/t-filter-svg/t-filter-svg.component';
import { ThresholdVariationComponent } from './pages/data-governance/threshold-variation/threshold-variation.component';
import { PrimeInputTextboxComponent } from './components/form-components/prime-input-textbox/prime-input-textbox.component';
import { NgChartsModule } from 'ng2-charts';
import { LineChartComponent } from './components/charts/line-chart/line-chart.component';
import { BarChartComponent } from './components/charts/bar-chart/bar-chart.component';
import { BubbleChartComponent } from './components/charts/bubble-chart/bubble-chart.component';
import { DoughnutChartComponent } from './components/charts/doughnut-chart/doughnut-chart.component';
import { ThresholdVarientPopupComponent } from './components/dg-components/threshold-varient-popup/threshold-varient-popup.component';
import { ReferentialIntegrityComponent } from './pages/data-governance/referential-integrity/referential-integrity.component';
import { DevProgressComponent } from './pages/dev-progress/dev-progress.component';
import { DataProfilingComponent } from './pages/data-governance/data-profiling/data-profiling.component';
import { PrimeSingleSelectComponent } from './components/form-components/prime-single-select/prime-single-select.component';
import { ControlTotalComponent } from './pages/data-governance/control-total/control-total.component';
import { ControlTotalPopupComponent } from './components/dg-components/control-total-popup/control-total-popup.component';
import { DataValidationComponent } from './pages/data-governance/data-validation/data-validation.component';
import { ControlSumComponent } from './pages/data-governance/control-sum/control-sum.component';
import { ThresholdTrendComponent } from './pages/data-governance/threshold-trend/threshold-trend.component';
import { DataLineageComponent } from './pages/data-governance/data-lineage/data-lineage.component';
import { DocumentLibraryComponent } from './pages/data-governance/document-library/document-library.component';
import { FileMonitoringComponent } from './pages/data-governance/file-monitoring/file-monitoring.component';
import { DataCatalogComponent } from './pages/data-governance/data-catalog/data-catalog.component';
import { RefIntegrityPopupComponent } from './components/dg-components/ref-integrity-popup/ref-integrity-popup.component';
import {TenantsComponent} from './pages/setting/tenants/tenants.component'
import {TenantSvgComponent} from './svg/tenant-svg/tenant-svg.component'
import { NewUserComponent } from './components/new-user/new-user.component';
import { GroupsSvgComponent } from './svg/groups-svg/groups-svg.component';
import { UserGroupsSvgComponent } from './svg/user-groups-svg/user-groups-svg.component';
import { SessionManagerSvgComponent } from './svg/session-manager-svg/session-manager-svg.component';
import { UserAccessSvgComponent } from './svg/user-access-svg/user-access-svg.component';
import { NewGroupComponent } from './components/new-group/new-group.component';
import { NewUserAccessConfigComponent } from './components/new-user-access-config/new-user-access-config.component';
import { NewGroupConfigComponent } from './components/new-group-config/new-group-config.component';
import { EditGroupComponent } from './components/edit-group/edit-group.component';
import { EditGroupConfigComponent } from './components/edit-group-config/edit-group-config.component';
import { EditUserComponent } from './components/edit-user/edit-user.component';
import { EditUserAccessConfigComponent } from './components/edit-user-access-config/edit-user-access-config.component';
import { UserZoneSvgComponent } from './svg/user-zone-svg/user-zone-svg.component';
import { NewUserZoneConfigComponent } from './components/new-user-zone-config/new-user-zone-config.component';
import { EditUserZoneConfigComponent } from './components/edit-user-zone-config/edit-user-zone-config.component';
import { DataValidationPopupComponent } from './components/dg-components/data-validation-popup/data-validation-popup.component';
import { ThresholdTrendPopupComponent } from './components/dg-components/threshold-trend-popup/threshold-trend-popup.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ControlSumPopupComponent } from './components/dg-components/control-sum-popup/control-sum-popup.component';
import { DataValidationEditPopupComponent } from './components/dg-components/data-validation-edit-popup/data-validation-edit-popup.component';
// import { OverlayPanelComponent } from './components/overlay-panel/overlay-panel.component';
import { TooltipModule } from 'primeng/tooltip';
import { ToastsComponent } from './components/toasts/toasts.component';
import { BusinessRuleCustomerComponent } from './pages/data-governance/business-rule-customer/business-rule-customer.component';
import { BusinessRuleProductComponent } from './pages/data-governance/business-rule-product/business-rule-product.component';
import { BusinessRuleAlignmentComponent } from './pages/data-governance/business-rule-alignment/business-rule-alignment.component';
import { ProductMarketComponent } from './components/business-rule/product-market/product-market.component';
import { ProductNormalizationComponent } from './components/business-rule/product-normalization/product-normalization.component';
import { ZipTerritoryComponent } from './components/business-rule/zip-territory/zip-territory.component';
import { CustomerTerritoryComponent } from './components/business-rule/customer-territory/customer-territory.component';
import { EmployeeTerritoryComponent } from './components/business-rule/employee-territory/employee-territory.component';
import { BusinessRuleSpecialtyComponent } from './pages/data-governance/business-rule-specialty/business-rule-specialty.component';
import { GroupMapComponent } from './components/business-rule/group-map/group-map.component';
import { SpecialtyUpdateComponent } from './components/business-rule/specialty-update/specialty-update.component';
import { SpecialtyInclusionComponent } from './components/business-rule/specialty-inclusion/specialty-inclusion.component';
import { BusinessRuleCustomerSvgComponent } from './svg/business-rule-customer-svg/business-rule-customer-svg.component';
import { BusinessRuleAlignmentSvgComponent } from './svg/business-rule-alignment-svg/business-rule-alignment-svg.component';
import { BusinessRuleProductSvgComponent } from './svg/business-rule-product-svg/business-rule-product-svg.component';
import { BusinessRuleSpecialtySvgComponent } from './svg/business-rule-specialty-svg/business-rule-specialty-svg.component';
import {NewCustomerRuleComponent} from './components/business-rule-components/new-customer-rule/new-customer-rule.component'
import { NewMarketRuleComponent } from './components/business-rule-components/new-market-rule/new-market-rule.component';
import { NewNormalizationRuleComponent } from './components/business-rule-components/new-normalization-rule/new-normalization-rule.component';
import { NewZipRuleComponent } from './components/business-rule-components/new-zip-rule/new-zip-rule.component';
import { NewCustomerTerritoryRuleComponent } from './components/business-rule-components/new-customer-territory-rule/new-customer-territory-rule.component';
import { NewEmployeeTerritoryRuleComponent } from './components/business-rule-components/new-employee-territory-rule/new-employee-territory-rule.component';
import { NewGroupMapRuleComponent } from './components/business-rule-components/new-group-map-rule/new-group-map-rule.component';
import { NewUpdateRuleComponent } from './components/business-rule-components/new-update-rule/new-update-rule.component';
import { NewInclusionRuleComponent } from './components/business-rule-components/new-inclusion-rule/new-inclusion-rule.component';
import { EditCustomerRuleComponent } from './components/business-rule-components/edit-customer-rule/edit-customer-rule.component';
import { EditMarketRuleComponent } from './components/business-rule-components/edit-market-rule/edit-market-rule.component';
import { EditNormalizationRuleComponent } from './components/business-rule-components/edit-normalization-rule/edit-normalization-rule.component';
import { EditZipRuleComponent } from './components/business-rule-components/edit-zip-rule/edit-zip-rule.component';
import { EditCustomerTerritoryRuleComponent } from './components/business-rule-components/edit-customer-territory-rule/edit-customer-territory-rule.component';
import { EditEmployeeTerritoryRuleComponent } from './components/business-rule-components/edit-employee-territory-rule/edit-employee-territory-rule.component';
import { EditGroupMapRuleComponent } from './components/business-rule-components/edit-group-map-rule/edit-group-map-rule.component';
import { EditUpdateRuleComponent } from './components/business-rule-components/edit-update-rule/edit-update-rule.component';
import { EditInclusionRuleComponent } from './components/business-rule-components/edit-inclusion-rule/edit-inclusion-rule.component';
import { NotificationComponent } from './pages/notification/notification.component';
import { InboxComponent } from './pages/inbox/inbox.component';
const primengModules = [
	InputTextModule,
	ButtonModule,
	ToastModule,
	TableModule,
	MenuModule,
	FormsModule,
	CheckboxModule,
	TabViewModule,
	ListboxModule,
	DialogModule,
	DropdownModule,
	AutoCompleteModule,
	AvatarsModule,
	SidebarModule,
	MenubarModule,
	SlideMenuModule,
	TieredMenuModule,
	OverlayModule,
	InputTextareaModule,
	MultiSelectModule,
	ProgressSpinnerModule,
	InputSwitchModule,
	NgChartsModule,
	FileUploadModule,
	RadioButtonModule,
	TooltipModule,
	CalendarModule,
	CalendarModule,
];

const components = [
	NullToQuotePipe,
	NullToZeroPipe,
	NullToDashPipe,
	NullToNaPipe,
	CheckBooleanPipe,
	SupDatePipe,
	CustomPipe,
	NoNullPipe,
	TruncatePipe,
	UrlidPipe,
	SafePipe,
	EscapeHtmlPipe,
	DayWeekPipe,
	ZeroToValPipe,
	PricePipePipe,
	FilterPipe,
	FormatBytePipe,
	SanitizerUrlPipe,
	AutoFocusDirective,
	FocusedDirective,
	PreventDoubleClickDirective,
	SelectOnFocusDirective,
	FilterOnFocusDirective,
	NumericDirective,
	DisableControlDirective,
    FormateDatePipePipe,
	//
	TablesComponent,
	TabviewsComponent,
	ListboxsComponent,
	AutoCompletionComponent,
	InputTextBoxComponent,
	PopupModelComponent,
	SingleSelectComponent,
	DollarSvgComponent,
	DownArrowSvgComponent,
	DsConfigSvgComponent,
	HomeIconSvgComponent,
	DataGovernanceSvgComponent,
	BusinessAppSvgComponent,
	DataEngineeringSvgComponent,
	AdminIconSvgComponent,
	SourceConnectorSvgComponent,
	SourceConnectorMiniSvgComponent,
	SourceLibrarySvgComponent,
	SourceOnboardSvgComponent,
	ConfiguredSourceSvgComponent,
	UserSvgComponent,
	GroupsSvgComponent,
	UserGroupsSvgComponent,
	SessionManagerSvgComponent,
	UserAccessSvgComponent,
	UserZoneSvgComponent,
	RuleLibrarySvgComponent,
	BusinessRuleSvgComponent,
	BusinessRuleCustomerSvgComponent,
	BusinessRuleAlignmentSvgComponent,
	BusinessRuleProductSvgComponent,
	BusinessRuleSpecialtySvgComponent,
	BusinessRuleCustomerSvgComponent,
	BusinessRuleAlignmentSvgComponent,
	BusinessRuleProductSvgComponent,
	BusinessRuleSpecialtySvgComponent,
	// OverlayPanelComponent,
	SourceConnectorComponent,
	TextAreaComponent,
	CheckBoxComponent,
	DeletePopupComponent,
	DeleteButtonComponent,
	NonOutlinedButtonComponent,
	PrimaryButtonComponent,
	FooterComponent,
	MultiselectComponent,
	ProgressSpinnerComponent,
	PencilISvgComponent,
	DeleteISvgComponent,
	DsFilterComponent,
	QualityRulesComponent,
	DataQualityRuleComponent,
	RuleLibrarySvgComponent,
	TFilterSvgComponent,
	ThresholdVarientPopupComponent,
	ThresholdVariationComponent,
	ReferentialIntegrityComponent,
	PrimeInputTextboxComponent,
	LineChartComponent,
	BarChartComponent,
	BubbleChartComponent,
	DoughnutChartComponent,
	DevProgressComponent,
	ControlTotalComponent,
	ControlTotalPopupComponent,
	DataValidationComponent,
	ControlSumComponent,
	ThresholdTrendComponent,
	RefIntegrityPopupComponent,
	TenantsComponent,
	TenantSvgComponent,
	NewTenantComponent,
	EditTenantComponent,
	GroupsComponent,
	LovMasterComponent,
	SessionManagerComponent,
	UserAccessComponent,
	UserGroupsComponent,
	UserZoneComponent,
	UsersComponent,
	NewUserComponent,
	NewGroupComponent,
	UserAccessComponent,
	NewUserAccessConfigComponent,
	NewGroupConfigComponent,
	EditGroupComponent,
	EditGroupConfigComponent,
	EditUserComponent,
	EditUserAccessConfigComponent,
	NewUserZoneConfigComponent,
	EditUserZoneConfigComponent,
	DataValidationPopupComponent,
	ThresholdTrendPopupComponent,
	ForgotPasswordComponent,
	ControlSumPopupComponent,
	DataValidationEditPopupComponent,
	ToastsComponent,
	RuleLibraryComponent,
    BusinessRuleComponent,
	BusinessRuleCustomerComponent,
	BusinessRuleProductComponent,
	BusinessRuleAlignmentComponent,
	FileMonitoringComponent,
	DocumentLibraryComponent,
	ProductMarketComponent,
	ProductNormalizationComponent,
	ZipTerritoryComponent,
	CustomerTerritoryComponent,
	EmployeeTerritoryComponent,
	BusinessRuleSpecialtyComponent,
	GroupMapComponent,
	SpecialtyUpdateComponent,
	SpecialtyInclusionComponent,
	NewCustomerRuleComponent,
	NewMarketRuleComponent,
	NewNormalizationRuleComponent,
	NewZipRuleComponent,
	NewCustomerTerritoryRuleComponent,
	NewEmployeeTerritoryRuleComponent,
	NewGroupMapRuleComponent,
	NewUpdateRuleComponent,
	NewInclusionRuleComponent,
	EditCustomerRuleComponent,
	EditMarketRuleComponent,
    EditNormalizationRuleComponent,
	EditZipRuleComponent,
	EditCustomerTerritoryRuleComponent,
	EditEmployeeTerritoryRuleComponent,
	EditGroupMapRuleComponent,
	EditUpdateRuleComponent,
	EditInclusionRuleComponent,
	NotificationComponent,
	InboxComponent

];

@NgModule({
	declarations: [
		...components,
		SourceConnectorMiniSvgComponent,
		SourceLibrarySvgComponent,
		SourceOnboardSvgComponent,
		ConfiguredSourceSvgComponent,
		PrimeMultiSelectComponent,
		LineChartComponent,
		BarChartComponent,
		BubbleChartComponent,
		DoughnutChartComponent,
		DataProfilingComponent,
		PrimeSingleSelectComponent,
		DataLineageComponent,
		DataCatalogComponent,
	],
	imports: [...primengModules, CommonModule, ReactiveFormsModule, FormsModule, RouterModule],
	exports: [...components, ...primengModules, DownArrowSvgComponent, ReactiveFormsModule],
	entryComponents: [],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class SharedModule {}
