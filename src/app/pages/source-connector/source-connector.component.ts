import {
	AfterViewInit,
	ChangeDetectionStrategy,
	ChangeDetectorRef,
	Component,
	ElementRef,
	OnInit,
	ViewChild,
} from '@angular/core';
import { CommonDataService } from '@app/_services/common-data.service';
import { MessageService } from 'primeng/api';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { format } from 'date-fns';
import { CUSTOM_CHAR_REGEX } from '../../util/helper/patterns';
import { patternValidator } from '../../util/validators/pattern-validator';
import { LoaderService } from '@app/_services/loading.service';
import { QueryService } from '@app/_services/query.service';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import { addDuplicateRecordChecker, editDuplicateRecordChecker, filterExactValuesFromText, filterIncludedValuesFromArray, getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { DatePipe } from '@angular/common';
import { map } from 'rxjs';
import { PrimeMultiSelectComponent } from '@app/components/form-components/prime-multi-select/prime-multi-select.component';
import { DsFilterComponent } from '@app/components/ds-components/ds-filter/ds-filter.component';
import { tableFilter } from '@app/util/helper/harmonyUtils';
import { SourceConfigApiServiceService } from '@app/_services/source-config-api.service.service';

@Component({
	selector: 'app-source-connector',
	templateUrl: './source-connector.component.html',
	styleUrls: ['./source-connector.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	providers: [MessageService],
})
export class SourceConnectorComponent implements OnInit {
	@ViewChild('connectionStatusFilter') connectionStatusFilterComponent: PrimeMultiSelectComponent;
	@ViewChild('connectionNameFilter') connectionNameFilterComponent: PrimeMultiSelectComponent;
	@ViewChild('createdOnFilter') createdOnFilterComponent: PrimeMultiSelectComponent;
	@ViewChild('createdByFilter') createdByFilterComponent: PrimeMultiSelectComponent;
	@ViewChild('dsFilterComponent') dsFilterComponent: DsFilterComponent;

	nameFilterProps;
	popupModelData: any;
	deletePopupModel: any;
	newConnectionsProps: any;
	typeFilterData: any;
	popupModelTrigger: boolean = false;
	isSubmitted: boolean = false;
	isEdited: boolean = false;
	nameFilterArr: string[] = [];
	stsFilterArr: string[] = [];
	crtedOnFilterArr: string[] = [];
	crtedByFilterArr: string[] = [];
	selectedRow: any;
	filterProps: any[] = [];
	newConnectionFromGroup: FormGroup;
	//UniqueValue : any;

	currentDeleteRow: any;

	table_data_orig: any;
	table_data_copy: any;
	sourceTypeDrpdwnOptions = [{ label: 'Amazon S3', value: 'Amazon S3' }, { label: 'SFTP', value: 'SFTP' }, { label: 'Snowflake', value: 'Snowflake' }, { label: 'Amazon Redshift', value: 'Amazon Redshift' }, { label: 'Mysql', value: 'Mysql' }, { label: 'PostgreSql', value: 'PostgreSql' }, { label: 'Microsoft sql Server', value: 'Microsoft sql Server' }, { label: 'Azure Synapses', value: 'Azure Synapses' }, { label: 'Azure Database', value: 'Azure Database' }, { label: 'oracle', value: 'oracle' },]
	filterComponents: any;
	connectionNameFilterOptions: any;
	connectionStatusFilterOptions: any;
	createdOnFilterOptions: any;
	createdByFilterOptions: any;
	connectionTypeFilterOptions: any;
	tablePageCount: number = 0
	mainFilterValue: any[] = []
	uniqueValues: any;
	conn_type_filtered_value: any[] = []
	COLUMNS = {
		name: "CONNECTION_NAME",
		status: "CONNECTION_STATUS",
		creatdDate: "CREATD_DT",
		creatdBy: "CREATD_BY",
	};
	columnsArray = [
		this.COLUMNS.name,
		this.COLUMNS.status,
		this.COLUMNS.creatdDate,
		this.COLUMNS.creatdBy,
	];
	activeFiltered = {
		name: { valArr: [], column: 'CONNECTION_NAME' },
		status: { valArr: [], column: 'CONNECTION_STATUS' },
		creatdDate: { valArr: [], column: 'CREATD_DT' },
		creatdBy: { valArr: [], column: 'CREATD_BY' },
	}
	sc_table_filter_active: boolean;
	constructor(
		private fb: FormBuilder,
		private toastService: MessageService,
		public loaderService: LoaderService,
		private _cdr: ChangeDetectorRef,
		private sourceConfigApiService: SourceConfigApiServiceService
	) { }
	ngOnInit() {
		this.loaderService.showLoader();
		this.getResponseHandler();
		this.newConnectionFromGroup = this.fb.group({
			type: ['', [Validators.required]],
			name: ['', [Validators.required]],
			string: ['', [Validators.required]],
			comment: ['', [Validators.required, patternValidator(CUSTOM_CHAR_REGEX)]],
			active: [false],
		});
		this.popupModelData = {
			displayModal: this.popupModelTrigger,
			header: 'Edit Connector',
			connectionFromProps: {
				name: {
					type: 'text',
					label: '',
					placeholder: 'Enter Connection Name',
					formControl: this.newConnectionFromGroup.controls['name'],
					formGroup: this.newConnectionFromGroup,
					isSubmitted: this.isSubmitted,
					disabled: false,
					errorMsg: 'This field is required.',
				},
				type: {
					label: '',
					placeholder: 'Select...',
					formControl: this.newConnectionFromGroup.controls['type'],
					formGroup: this.newConnectionFromGroup,
					options: this.sourceTypeDrpdwnOptions.sort((a, b) => a.label.localeCompare(b.label)),
					isSubmitted: this.isSubmitted,
					disabled: false,
					errorMsg: 'This field is required.',
				},
				connectionString: {
					type: 'text',
					label: '',
					placeholder: 'Enter Connection',
					formControl: this.newConnectionFromGroup.controls['string'],
					formGroup: this.newConnectionFromGroup,
					isSubmitted: this.isSubmitted,
					disabled: false,
					errorMsg: 'This field is required.',
				},
				comment: {
					type: 'text',
					inputLabel: '',
					placeholder: '',
					formControl: this.newConnectionFromGroup.controls['comment'],
					formGroup: this.newConnectionFromGroup,
					isSubmitted: this.isSubmitted,
					disabled: false,
					errorMsg: {
						require: 'This field is required.',
						pattern: "This (',\\,`) characters not allowed",
					},
				},
				active: {
					formGroup: this.newConnectionFromGroup,
					formControl: this.newConnectionFromGroup.controls['active'],
					label: 'Active',
					disabled: false,
					errorMsg: 'This field is required.',
				},
			},
			saveBtnProps: {
				label: 'Save',
				click: '',
			},
			cancelBtnProps: {
				label: 'Cancel',
				click: '',
			},
		};


	}
	// ngAfterViewInit(){
	// 	this.filterComponents = [
	// 		{ selected: 'connectionNameFilterSelected', component: this.connectionNameFilterComponent },
	// 		{ selected: 'connectionStatusFilterSelected', component: this.connectionStatusFilterComponent },
	// 		{ selected: 'createdOnFilterSelected', component: this.createdOnFilterComponent },
	// 		{ selected: 'createdByFilterSelected', component: this.createdByFilterComponent },
	// 	];

	//   }
	async getResponseHandler() {

		this.table_data_orig = await lastValueFrom(
			this.sourceConfigApiService.getSourceConnectorTable().pipe(
				map((array: any) => {
					const datePipe = new DatePipe('en-US');
					return array.map((item) => ({
						...item,
						CREATD_DT: datePipe.transform(item.CREATD_DT, 'MM/dd/yyyy'),
					}));
				})
			)
		);
		this.mainFilterValue = filterIncludedValuesFromArray(this.table_data_orig, this.conn_type_filtered_value, 'CONNECTION_TYPE')
		this.table_data_copy = tableFilter(this.activeFiltered, this.mainFilterValue)
		this.uniqueValues = this.getDistinctValuesForColumns(this.table_data_copy, this.columnsArray);
		this.connectionTypeFilterOptions = getMappedArray(
			getUniqueValues(this.table_data_orig, 'CONNECTION_TYPE'),
			'label',
			'value'
		);
		this.loaderService.hideLoader();
		setTimeout(() => {
			this.filterComponents = [
				{ selected: 'connectionNameFilterSelected', component: this.connectionNameFilterComponent },
				{ selected: 'connectionStatusFilterSelected', component: this.connectionStatusFilterComponent },
				{ selected: 'createdOnFilterSelected', component: this.createdOnFilterComponent },
				{ selected: 'createdByFilterSelected', component: this.createdByFilterComponent },
			];
		}, 300)

	}


	getDistinctValuesForColumns = (tableData: any[], columns: string[]) => {
		const result: any = [];
		columns.forEach((column) => {
			const uniqueValues = getUniqueValues(tableData, column);
			const mappedArray = getMappedArray(uniqueValues);
			result[column] = [...mappedArray];
		});
		console.log(result)
		return result;
	};
	editHandler(row) {
		this.isEdited = false;
		this.popupModelData.displayModal = true;
		this.popupModelData.header = 'Edit Connector';
		this.selectedRow = row
		let isActive: boolean;
		if (this.selectedRow.CONNECTION_STATUS === 'Inactive') {
			isActive = false;
		} else {
			isActive = true;
		}
		this.newConnectionFromGroup.patchValue({
			type: { label: this.selectedRow.CONNECTION_TYPE, value: this.selectedRow.CONNECTION_TYPE },
			name: this.selectedRow.CONNECTION_NAME,
			string: this.selectedRow.CONN_OBJ,
			comment: this.selectedRow.COMMENTS,
			active: isActive,
		});

		this.isEdited = true;
	}

	deleteHandler(row) {
		this.deletePopupModel = {
			displayModal: true,
			header: 'Delete Connector',
			icon: 'pi-exclamation-triangle',
			content: {
				header: `Delete ${row.CONNECTION_NAME}?`,
				text: 'This action will delete the connector permanently',
			},
			deleteBtnProps: {
				label: 'Delete',
				click: '',
			},
			cancelBtnProps: {
				label: 'Cancel',
				click: '',
			},
		};

		this.currentDeleteRow = row;
	}

	// popup save click handler
	editPopupSubmitHandler() {
		// you have to do isSubmit true
		// Format the date in "yyyy-mm-dd" format
		let formValue = this.newConnectionFromGroup.value;
		console.log(formValue);
		const formattedDate = format(new Date(), 'yyyy-MM-dd');
		console.log(formattedDate);
		// checking duplication
		const formProperty = ['name']
		const tableProperty = ['CONNECTION_NAME']

		if (!this.isEdited) {
			if (this.newConnectionFromGroup.valid) {
				let formValue = this.newConnectionFromGroup.value;
				const duplicateFound = addDuplicateRecordChecker(formValue, formProperty, this.table_data_orig, tableProperty)
				if (duplicateFound) {
					this.toastService.add({ key: 'wrn', closable: false, sticky: false, severity: '', summary: '', detail: 'Connection Name Already Exists.Try Another', });
				} else {
					this.popupModelData.displayModal = false;
					this.loaderService.showLoader();
					const postData = {
						connectionName: formValue.name,
						connectionDesc: formValue.comment,
						connectionObject: formValue.string,
						connectionType: formValue.type.value,
						activeFlag: formValue.active ? 'Y' : 'N',
						createdBy: localStorage.getItem('userName'),
						updatedBy: localStorage.getItem('userName')
					};
					this.sourceConfigApiService.addSourceConnector(postData).subscribe((response: any) => {
						this.toastService.clear();
						this.loaderService.hideLoader();
						console.log(response.status, response);
						if (response.status == 200) {
							this.toastService.add({ key: 'su', closable: false, sticky: false, severity: '', summary: '', detail: `New connector ${formValue.name} created`, });
							this.getResponseHandler();
						} else {
							this.toastService.clear();
							this.toastService.add({ key: 'wrn', closable: false, sticky: false, severity: '', summary: '', detail: `Connection Error Try Again`, });
						}
					});
					this.newConnectionFromGroup.reset();
					this.isEdited = false;
				}

			} else {
				console.log('Form Is not Valid');
				this.isSubmitted = true;
				this.isEdited = false;
				this.popupModelData.connectionFromProps.name.isSubmitted = this.isSubmitted;
				this.popupModelData.connectionFromProps.type.isSubmitted = this.isSubmitted;
				this.popupModelData.connectionFromProps.connectionString.isSubmitted = this.isSubmitted;
				this.popupModelData.connectionFromProps.comment.isSubmitted = this.isSubmitted;
			}
		} else if (this.isEdited) {
			const formattedDate = format(new Date(), 'yyyy-MM-dd');
			if (this.newConnectionFromGroup.valid) {
				let formValue = this.newConnectionFromGroup.value
				const duplicateFound = editDuplicateRecordChecker(formValue, formProperty, this.table_data_orig, tableProperty, this.selectedRow.CONN_ID)
				if (duplicateFound) {
					this.toastService.add({ key: 'wrn', closable: false, sticky: false, severity: '', summary: '', detail: 'Connection Name Already Exists.Try Another', });
				} else {
					this.popupModelData.displayModal = false;
					this.loaderService.showLoader();
					const postData = {
						connectionName: formValue.name,
						connectionDesc: formValue.comment,
						connectionObject: formValue.string,
						connectionType: formValue.type.value,
						activeFlag: formValue.active ? 'Y' : 'N',
						updatedBy: localStorage.getItem('userName'),
						connectionId: this.selectedRow.CONN_ID
					};
					console.log(postData)
					this.sourceConfigApiService.updateSourceConnector(postData).subscribe((response: any) => {
						this.toastService.clear();
						this.loaderService.hideLoader();
						if (response.status == 200) {
							this.toastService.add({ key: 'su', closable: false, sticky: false, severity: '', summary: '', detail: `${formValue.name} updated Sucessfully` });
							this.getResponseHandler();
						} else {
							console.log(response);
							this.toastService.clear();
							this.toastService.add({ key: 'wrn', closable: false, sticky: false, severity: '', summary: '', detail: `Connection Error Try Again`, });
						}
					});
					this.newConnectionFromGroup.reset();
				}

			} else {

				this.isSubmitted = true;
				this.popupModelData.connectionFromProps.name.isSubmitted = this.isSubmitted;
				this.popupModelData.connectionFromProps.type.isSubmitted = this.isSubmitted;
				this.popupModelData.connectionFromProps.connectionString.isSubmitted = this.isSubmitted;
				this.popupModelData.connectionFromProps.comment.isSubmitted = this.isSubmitted;
			}

		}
	}
	editPopupCancelHandler() {
		this.newConnectionFromGroup.reset();
		console.log('editPopupCancelHandler');
	}
	deletePopupDeleteHandler = () => {
		console.log(this.currentDeleteRow);
		let connId = this.currentDeleteRow.CONN_ID;
		let connName = this.currentDeleteRow.CONNECTION_NAME;
		const postData = {
			connectionId: connId
		}
		this.deletePopupModel.displayModal = false;
		this.loaderService.showLoader();
		this.sourceConfigApiService.deleteSourceConnector(postData).subscribe((response: any) => {
			this.loaderService.hideLoader();
			if (response.status == 200) {
				this.toastService.clear();
				this.toastService.add({
					key: 'err',
					closable: false,
					sticky: false,
					severity: '',
					summary: '',
					detail: `${connName} Deleted Successfully`,
				});
				this.getResponseHandler();
			} else {
				console.log(response);
				this.toastService.clear();
				this.toastService.add({
					key: 'wrn',
					closable: false,
					sticky: false,
					severity: '',
					summary: '',
					detail: `Connection Error TryAgain`,
				});
			}
		});
		console.log('deletePopupDeleteHandler');
	};
	handleNewConnectionClick() {
		this.isSubmitted = false;
		this.isEdited = false;
		this.popupModelData.connectionFromProps.name.isSubmitted = this.isSubmitted;
		this.popupModelData.connectionFromProps.type.isSubmitted = this.isSubmitted;
		this.popupModelData.connectionFromProps.connectionString.isSubmitted = this.isSubmitted;
		this.popupModelData.connectionFromProps.comment.isSubmitted = this.isSubmitted;
		this, this.newConnectionFromGroup.reset();
		this.popupModelData.displayModal = true;
		this.popupModelData.header = 'New Connector';

		console.log(this.popupModelData);
	}

	changeHandler(filterArray, column) {
		console.log(filterArray)
		let tempFilterArray = filterArray
		if (column === 'CONNECTION_NAME') {
			this.activeFiltered.name.valArr = tempFilterArray
		} else if (column === 'CONNECTION_STATUS') {
			this.activeFiltered.status.valArr = tempFilterArray
		} else if (column === 'CREATD_DT') {
			this.activeFiltered.creatdDate.valArr = tempFilterArray
		} else if (column === 'CREATD_BY') {
			this.activeFiltered.creatdBy.valArr = tempFilterArray
		}
		console.log('filter has value=>', Object.keys(this.activeFiltered).some(field => this.activeFiltered[field].valArr.length))
		// set reset button state tru or false based on the filter result
		if (Object.keys(this.activeFiltered).some(field => this.activeFiltered[field].valArr.length)) {
			console.log('true')
			this.sc_table_filter_active = true;

		} else {
			this.sc_table_filter_active = false;
			console.log('false')
		}
		const result = tableFilter(this.activeFiltered, this.mainFilterValue)
		if (result.length) {
			this.table_data_copy = result
		} else {
			if (!Object.keys(this.activeFiltered).some(field => this.activeFiltered[field].valArr.length)) {
				this.table_data_copy = this.mainFilterValue
			} else {
				this.table_data_copy = result;
			}

		}
		this.setFilterOptions(this.activeFiltered, result)

		console.log(this.activeFiltered)
	}
	setFilterOptions(fieldObject, result) {

		const filterFields = Object.keys(fieldObject)

		for (let field of filterFields) {
			if (!fieldObject[field].valArr.length) {
				this.uniqueValues[fieldObject[field].column] = getMappedArray(getUniqueValues(result, fieldObject[field].column));
			}
		}
	}

	activatedFilterReset(fieldObject: any) {
		this.sc_table_filter_active = false;
		const filterFields = Object.keys(fieldObject)
		filterFields.forEach(key => fieldObject[key].valArr = [])
	}

	resetAll() {


		console.log(this.filterComponents);
		this.filterComponents.forEach((item) => {
			console.log(item)
			item.component.resetSelection();
		});
		this.mainFilterValue = this.table_data_orig
		this.table_data_copy = this.table_data_orig;
		// resetting local filter value to empty
		this.activatedFilterReset(this.activeFiltered)
		this.uniqueValues = this.getDistinctValuesForColumns(this.table_data_copy, this.columnsArray);
	}

	mainFilterOnchange(filterArray: any) {
		console.log(filterArray)
		this.conn_type_filtered_value = filterArray
		this.mainFilterValue = filterIncludedValuesFromArray(this.table_data_orig, this.conn_type_filtered_value, 'CONNECTION_TYPE');
		this.changeHandler([], '');
	}
}
