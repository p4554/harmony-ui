import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { replacePlaceholders } from '@app/util/helper/patterns';
import { CommonDataService } from '@app/_services/common-data.service';
import { LoaderService } from '@app/_services/loading.service';
import { QueryService } from '@app/_services/query.service';
import { lastValueFrom, map } from 'rxjs';
import { query } from '@angular/animations';
import { replaceParamsinJSON } from '@app/util/helper/harmonyUtils';
import { MessageService } from 'primeng/api/messageservice';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { s3OptionsValue } from './s3.options';
import { ToastsComponent } from '@app/components/toasts/toasts.component';

@Component({
	selector: 'app-data-profiling',
	templateUrl: './data-profiling.component.html',
	styleUrls: ['./data-profiling.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DataProfilingComponent implements AfterViewInit {
	@ViewChild('toast') toast: ToastsComponent;
	form: FormGroup;
	dataZoneOptions: any;
	dataProviderOptions: any;
	datasetNameOptions: any;
	filePath: any;
	fileUploadUrl = 'https://harmony.datazymes.com/Home/UploadLocalProfiling'

	s3Options: any[] = []
	isLoaded = false;
	// url = 'https://harmony.datazymes.com/DataProfile/LND_ADDRESS.html';

	safeUrl: SafeResourceUrl;


	databaseOptions = [
		{ label: 'Database', value: 'database' },
		{ label: 'S3', value: 's3' },
		{ label: 'Local', value: 'local' },
	];

	dataZoneName = ''
	datasetName = ''
	dataProvideName = ''
	s3SelectedName = ''
	database = ''
	localFileName = ''
	uploadedFiles: any[] = [];
	localFileHtmlTemplate: Object;

	ngAfterViewInit(): void {
		console.log(this.toast)
	}
	constructor(
		private fb: FormBuilder,
		private queryService: QueryService,
		private commonDataService: CommonDataService,
		public loaderService: LoaderService,
		private _cdr: ChangeDetectorRef,
		private sanitizer: DomSanitizer
	) {
		this.getDataZone();
		this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
			`https://harmony.datazymes.com/DataProfile/DQ/DQ10061.html`
		)

		//this.getDatasetName();
		this.form = this.fb.group({
			database: ['', Validators.required],
			data_zone: [''],
			data_provider: [''],
			dataset_name: [''],
			s3: ['']
		});


		this.s3Options = s3OptionsValue.map((name) => ({ label: name, value: name }))
	}

	// this.getDataProvider();
	async getDataZone() {
		const query = await lastValueFrom(this.queryService.fetchQuery('data_profile_data_zone'));

		this.loaderService.showLoader()
		this.dataZoneOptions = await lastValueFrom(
			this.commonDataService.getSourceConnectorTableData(query).pipe(
				map((array) => {
					return array.map((item) => ({
						label: item.DATA_ZONE_NM,
						value: item.DATA_ZONE_ID,
					}));
				})
			)
		);
		// this._cdr.detectChanges();
		this.dataZoneOptions = this.dataZoneOptions.sort((a, b) => a.label.localeCompare(b.label))
		this.loaderService.hideLoader()

		//let test = await lastValueFrom(this.commonDataService.getProfiling());
		// this.buildProfilePage();
	}

	buildProfilePage() {
		this.commonDataService.checkIfURLExists().subscribe((res: any) => {
			console.log(res.status_code);
			debugger;
		});

		this.commonDataService.getProfiling().subscribe((response) => {
			if (response.status === 200) {
				//  test = TableName;
				let TableName = 'LND_ADDRESS';
				const path = 'https://harmony.datazymes.com/DataProfile/' + TableName + '.html';
				const file = new Blob([response.body], { type: 'text/html' });
				const fileURL = URL.createObjectURL(file);
				const a = document.createElement('a');
				a.href = fileURL;
				a.download = TableName + '.html';
				document.body.appendChild(a);
				a.click();
			}
		});
	}

	async getDataProvider(paramArray) {
		this.loaderService.showLoader()
		const jsonStr = await lastValueFrom(this.queryService.fetchQuery('data_profile_data_provider'));
		const newJSONStr = replaceParamsinJSON(jsonStr, paramArray);
		this.dataProviderOptions = await lastValueFrom(
			this.commonDataService.getSourceConnectorTableData(newJSONStr).pipe(
				map((array) => {
					return array.map((item) => {
						if (item.DATA_PROVIDER !== null && item.DATA_PROVIDER !== '') {
							return {
								label: item.DATA_PROVIDER,
								value: item.DATA_PROVIDER
							};
						} else {
							return null;
						}
					}).filter(item => item !== null && item !== '' && item !== ' ');
				})
			))
		this.loaderService.hideLoader()
		// this._cdr.detectChanges();
		this.dataProviderOptions = this.dataProviderOptions.sort((a, b) => a.label.localeCompare(b.label))
	}

	async getDatasetName(paramArray) {
		const jsonStr = await lastValueFrom(this.queryService.fetchQuery('data_profile_dataset_name'));

		const newJSONStr = replaceParamsinJSON(jsonStr, paramArray);
		this.loaderService.showLoader()
		this.datasetNameOptions = await lastValueFrom(
			this.commonDataService.getSourceConnectorTableData(newJSONStr).pipe(
				map((array) => {
					return array.map((item) => ({
						label: item.ENT_NM,
						value: item.ENT_ID,
					}));
				})
			)
		);
		this.datasetNameOptions = this.datasetNameOptions.sort((a, b) => a.label.localeCompare(b.label))
		this.loaderService.hideLoader()
		// this._cdr.detectChanges();
	}

	getControlByName(controlName: string) {
		return this.form.get(controlName);
	}

	changeEventHandler(param) {
		if (param === 'datazone') {
			let paramArray: string[] = [this.form.value.data_zone];

			// reset the depended dropdown value
			this.form.patchValue({
				data_provider: '',
				dataset_name: '',
			})

			console.log(paramArray);
			this.getDataProvider(paramArray);
		} else if (param === 'dataprovider') {
			let paramArray: string[] = [this.form.value.data_provider, this.form.value.data_zone];
			this.getDatasetName(paramArray);
			// reset the depended dropdown value
			this.form.patchValue({
				dataset_name: '',
			})
		} else if (param === 'dataname') {
			// let paramArray: string[] = [this.form.value.data_zone, this.form.value.data_name];
			// this.getDatasetName(paramArray);
		}
	}


	databaseOnchange() {
		const database = this.getControlByName('database').value
		const dataZone = this.getControlByName('data_zone')
		const dataProvider = this.getControlByName('data_provider')
		const datasetName = this.getControlByName('dataset_name')
		const s3 = this.getControlByName('s3')
		if (database === 'database') {
			dataZone.setValidators([Validators.required])
			dataZone.updateValueAndValidity();
			dataProvider.setValidators([Validators.required])
			dataProvider.updateValueAndValidity();
			datasetName.setValidators([Validators.required])
			datasetName.updateValueAndValidity();
			s3.clearValidators()
			s3.updateValueAndValidity();
			s3.reset()
		} else if (database === 's3') {
			dataZone.clearValidators()
			dataZone.updateValueAndValidity();
			dataProvider.clearValidators()
			dataProvider.updateValueAndValidity();
			datasetName.clearValidators()
			datasetName.updateValueAndValidity();
			s3.setValidators([Validators.required])
			s3.updateValueAndValidity();
			dataZone.reset()
			dataProvider.reset()
			datasetName.reset()
		} else {
			dataZone.clearValidators()
			dataZone.updateValueAndValidity();
			dataProvider.clearValidators()
			dataProvider.updateValueAndValidity();
			datasetName.clearValidators()
			datasetName.updateValueAndValidity();
			s3.clearValidators()
			s3.updateValueAndValidity();
			dataZone.reset()
			dataProvider.reset()
			datasetName.reset()
			s3.reset()
		}
	}


	submit() {
		this.isLoaded = true;
	}

	onUpload(event) {
		for (let file of event.files) {
			this.uploadedFiles.push(file);
		}

		console.log(event)
	}

	async runProfile() {
		if (((this.form.value.database !== 'local') && this.form.valid) || (this.form.value.database === 'local' && this.localFileName)) {
			const formValue = this.form.value
			this.isLoaded = true
			this.database = formValue.database
			// database
			if (formValue.database === 'database') {
				const dataZone = this.dataZoneOptions.filter(obj => obj.value === formValue.data_zone).shift().label
				const datasetName = this.datasetNameOptions.filter(obj => obj.value === formValue.dataset_name).shift().label
				this.dataZoneName = dataZone
				this.datasetName = datasetName
				this.dataProvideName = formValue.data_provider
				this.s3SelectedName = ''
				console.log('dataZone=>', dataZone, 'datasetName=>', datasetName)
				this.filePath = await lastValueFrom(this.commonDataService.getDatabaseProfilingPath(dataZone, datasetName));

			} else
				// s3
				if (formValue.database === 's3') {
					this.s3SelectedName = formValue.s3
					this.dataZoneName = ''
					this.datasetName = ''
					this.dataProvideName = ''
					this.filePath = await lastValueFrom(this.commonDataService.getS3ProfilingPath(formValue.s3));


				} else {
					this.dataZoneName = ''
					this.datasetName = ''
					this.dataProvideName = ''
					this.s3SelectedName = ''
					this.localFileHtmlTemplate = await lastValueFrom(this.commonDataService.getLocalProfilePath(this.localFileName));

				}

			console.log(this.filePath)
			if (this.filePath) {
				this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
					`https://harmony.datazymes.com/DataProfile/${this.filePath}.html`
				);
			}


			this._cdr.detectChanges()
		}
	}
	fileUploadComplete(fileName) {
		console.log("file uploaded sucessfully", fileName)
		this.toast.triggerToast('succ', 'file uploaded sucessfully')
		this.localFileName = fileName

	}
	fileUploadError(error) {
		console.log(this.toast)
		if (error) {
			this.toast.triggerToast('wrn', error.error.statusText)

		}
		console.log(error)
	}
}
