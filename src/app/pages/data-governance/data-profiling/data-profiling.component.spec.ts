import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataProfilingComponent } from './data-profiling.component';

describe('DataProfilingComponent', () => {
  let component: DataProfilingComponent;
  let fixture: ComponentFixture<DataProfilingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataProfilingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DataProfilingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
