import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QualityRulesComponent } from './quality-rules.component';

describe('QualityRulesComponent', () => {
  let component: QualityRulesComponent;
  let fixture: ComponentFixture<QualityRulesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QualityRulesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QualityRulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
