import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild, ElementRef, Renderer2, AfterViewChecked } from '@angular/core';
import { CommonDataService } from '@app/_services/common-data.service';
import { LoaderService } from '@app/_services/loading.service';
import { ExcelService } from '@app/_services/excel.service';
import { QueryService } from '@app/_services/query.service';
import { map } from 'rxjs';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import {
  filterIncludedValuesFromArray,
  getMappedArray,
  getUniqueValues,
  filterIncludedValuesFromText,
  filterExactValuesFromText,
} from '@app/util/helper/arrayHandlers';
import { PrimeMultiSelectComponent } from '@app/components/form-components/prime-multi-select/prime-multi-select.component';
import { MessageService } from 'primeng/api';
import { format } from 'date-fns';
import { replaceNumberWithEmptyString, tableFilter } from '@app/util/helper/harmonyUtils';
import { DataQualityApiService } from '@app/_services/dataQuality-api.service';


@Component({
  selector: 'app-quality-rules',
  templateUrl: './quality-rules.component.html',
  styleUrls: ['./quality-rules.component.scss'],
  providers: [MessageService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QualityRulesComponent implements OnInit, AfterViewInit {
  @ViewChild('allDataZoneFilter') allDataZoneFilter: PrimeMultiSelectComponent;
  @ViewChild('allDataNameSetFilter') allDataNameSetFilter: PrimeMultiSelectComponent;
  @ViewChild('allFiledNameFilter') allFiledNameFilter: PrimeMultiSelectComponent;
  @ViewChild('allDataTypeFilter') dataTypeFilter: PrimeMultiSelectComponent;
  @ViewChild('allRuleNameFilter') ruleNameFilter: PrimeMultiSelectComponent;
  @ViewChild('allRuleTypeFilter') ruleTypeFilter: PrimeMultiSelectComponent;
  @ViewChild('allRuleDescFilter') ruleDescFilter: PrimeMultiSelectComponent;


  COLUMNS = {
    Datazone: "DATAZONE",
    DataSetName: "DATASET_NM",
    FieldName: "FIELD_NM",
    DataType: "DATA_TYP",
    RuleName: "RULE_NM",
    RuleType: "RULE_TYP",
    RuleDesc: "RULE_DESC",
    Status: "STATUS",
  };
  columnsArray = [
    this.COLUMNS.Datazone,
    this.COLUMNS.DataSetName,
    this.COLUMNS.FieldName,
    this.COLUMNS.DataType,
    this.COLUMNS.RuleName,
    this.COLUMNS.RuleType,
    this.COLUMNS.Status,
  ];
  showRuleSelector: any = '';
  showRuleDropDownOption = [
    { label: "Show All", value: "" },
    { label: "Show Enabled Rule", value: "Active" },
    { label: "Show Disabled Rule", value: "In Active" },
  ];

  uniqueValues: any;
  tablePageNumber: number = 0
  table_data_copy: any[];
  tabs = [
    "All",
    "Threshold Variation",
    "Referential Integrity",
    "Data Validation",
    "Control Total",
    "Threshold Trend",
    "Control Sum",
  ];
  activeFiltered = {
    dataZone: { valArr: [], column: 'DATAZONE' },
    dataSetName: { valArr: [], column: 'DATASET_NM' },
    fieldName: { valArr: [], column: 'FIELD_NM' },
    dataType: { valArr: [], column: 'DATA_TYP' },
    ruleName: { valArr: [], column: 'RULE_NM' },
    ruleType: { valArr: [], column: 'RULE_TYP' },
  }
  rules_record_count: number;
  currentTab: string = "All";
  tab_label = 'Data Quality'
  table_data_orig: any[] = [];

  mainFilterValue: any[] = []
  buttonProps = {
    label: "Export",
    click: "",
  };

  filterComponents: any;
  // enable disable count
  count: { enable: number, disable: number, total: number }
  constructor(
    private queryService: QueryService,
    private commonDataService: CommonDataService,
    public loaderService: LoaderService,
    private _cdr: ChangeDetectorRef,
    private toastService: MessageService,
    private excelservice: ExcelService,
    private elementRef: ElementRef, private renderer: Renderer2,
    private dataQualityApiService: DataQualityApiService
  ) { }

  ngOnInit() {
    this.getResponseHandler();

  }

  async getResponseHandler() {
    // const query = await lastValueFrom(this.queryService.fetchQuery("D_G_ruleConnection"));
    // console.log(query);
    // this.table_data_orig = await lastValueFrom(this.queryService.fetchQuery("testData"));
    this.loaderService.showLoader();
    this.table_data_orig = await lastValueFrom(
      this.dataQualityApiService.getAllRules().pipe(
        map((array: any) => {
          return array.map((item) => ({
            ...item,
            SWITCH_STATUS: item.STATUS === "Active" ? true : false,
          }));
        })
      )
    );
    console.log(this.table_data_orig)
    // this.table_data_copy = [...this.table_data_orig];
    this.mainFilterValue = [...this.table_data_orig]
    this.uniqueValues = this.getDistinctValuesForColumns(this.table_data_orig, this.columnsArray);


    // calling main filter
    // console.log('showRuleSelector=>',this.showRuleSelector)
    this.mainFilterChangeHandler(this.showRuleSelector, this.COLUMNS.Status)
    console.log(this.activeFiltered)
    // calling table field filter
    this.table_data_copy = tableFilter(this.activeFiltered, this.mainFilterValue)
    // resetting filter options


    this.calculateCount(this.table_data_copy)
    this.rules_record_count = this.table_data_copy.length
    this.tablePageNumber = 0
    this.loaderService.hideLoader();
  }

  ngAfterViewInit() {
    this.filterComponents = [
      { selected: this.COLUMNS.Datazone, component: this.allDataZoneFilter },
      { selected: this.COLUMNS.DataSetName, component: this.allDataNameSetFilter },
      { selected: this.COLUMNS.FieldName, component: this.allFiledNameFilter },
      { selected: this.COLUMNS.DataType, component: this.dataTypeFilter },
      { selected: this.COLUMNS.RuleName, component: this.ruleNameFilter },
      { selected: this.COLUMNS.RuleType, component: this.ruleTypeFilter },
      { selected: this.COLUMNS.RuleDesc, component: this.ruleDescFilter },
    ];

  }

  mainFilterChangeHandler(filterArray, column) {
    console.log(filterArray)
    let tempFilterArray = filterArray
    // === "All" ? "" : filterArray;

    if (column === 'STATUS') {
      this.mainFilterValue = filterExactValuesFromText(this.table_data_orig, tempFilterArray, column)
      this.table_data_copy = [...this.mainFilterValue];
      this.uniqueValues = this.getDistinctValuesForColumns(this.table_data_copy, this.columnsArray);
      // this.activatedFilterReset(this.activeFiltered)
      this.rules_record_count = this.table_data_copy.length
    }
  }
  changeHandler(filterArray, column) {
    console.log(filterArray)
    let tempFilterArray = filterArray
    if (column === 'DATAZONE') {
      this.activeFiltered.dataZone.valArr = tempFilterArray
    } else if (column === 'DATASET_NM') {
      this.activeFiltered.dataSetName.valArr = tempFilterArray
    } else if (column === 'FIELD_NM') {
      this.activeFiltered.fieldName.valArr = tempFilterArray
    } else if (column === 'DATA_TYP') {
      this.activeFiltered.dataType.valArr = tempFilterArray
    } else if (column === 'RULE_NM') {
      this.activeFiltered.ruleName.valArr = tempFilterArray
    } else if (column === 'RULE_TYP') {
      this.activeFiltered.ruleType.valArr = tempFilterArray
    }
    const result = tableFilter(this.activeFiltered, this.mainFilterValue)
    if (result.length) {
      this.table_data_copy = result
      // this.setFilterOptions(this.activeFiltered,result)
    } else {
      if (!Object.keys(this.activeFiltered).some(field => this.activeFiltered[field].valArr.length)) {
        this.table_data_copy = this.mainFilterValue
        // this.setFilterOptions(this.activeFiltered,result)
      } else {
        this.table_data_copy = result
        // this.setFilterOptions(this.activeFiltered,result)
      }
    }
    this.setFilterOptions(this.activeFiltered, result)


    if (column === 'RULE_DESC') {
      // this.activeFiltered.ruleDec.valArr = tempFilterArray
      this.table_data_copy = filterIncludedValuesFromText(this.table_data_copy, tempFilterArray, column);
    }
    this.rules_record_count = this.table_data_copy.length
    this.tablePageNumber = 0

    console.log(this.activeFiltered)
  }

  setFilterOptions(fieldObject, result) {

    const filterFields = Object.keys(fieldObject)

    for (let field of filterFields) {
      if (!fieldObject[field].valArr.length) {
        this.uniqueValues[fieldObject[field].column] = getMappedArray(getUniqueValues(result, fieldObject[field].column));
      }
    }
  }

  activatedFilterReset(fieldObject: any) {
    const filterFields = Object.keys(fieldObject)
    filterFields.forEach(key => fieldObject[key].valArr = [])
  }
  setCurrentTab(e) {
    this.currentTab = this.tabs[e.index];
    this.tab_label = (this.currentTab === 'All') ? 'Data Quality' : this.currentTab
    this.rules_record_count = this.table_data_copy.length
    this._cdr.detectChanges();
    if (this.currentTab === 'All') {
      this.getResponseHandler();
    }
  }

  resetAll() {
    // resetting prime filter selectedValue to empty
    this.filterComponents.forEach((item) => {
      item.component.resetSelection();
    });
    this.showRuleSelector = ""
    this.table_data_copy = this.table_data_orig;
    this.mainFilterValue = this.table_data_orig
    // resetting local filter value to empty
    this.activatedFilterReset(this.activeFiltered)
    this.rules_record_count = this.table_data_copy.length

    // updating options
    this.uniqueValues = this.getDistinctValuesForColumns(this.table_data_copy, this.columnsArray);
  }

  getDistinctValuesForColumns = (tableData: any[], columns: string[]) => {
    const result: any = [];
    columns.forEach((column) => {
      const uniqueValues = getUniqueValues(tableData, column);
      const mappedArray = getMappedArray(uniqueValues);
      result[column] = [...mappedArray];
    });
    console.log(result)
    return result;
  };

  async updateRuleStatus(row) {
    // console.log(row)
    const formattedDate = format(new Date(), 'yyyy-MM-dd');

    let ruleType = replaceNumberWithEmptyString(row.RULE_ID)
    ruleType = (ruleType === 'THV') ? 'TH' : (ruleType === 'THT') ? 'TH' : (ruleType === 'CS') ? 'CT' : ruleType;
    const postData = {
      "status": row.SWITCH_STATUS,
      "identifier": ruleType,
      "ruleId": row.RULE_ID
    }
    // console.log(ruleType)
    // let query = {
    //   "query": `update HRMNY_CNF_ZN.DQ_${ruleType}_CONFIG set IS_ACTV = '${row.SWITCH_STATUS}', IS_RULE_ACTV = '${row.SWITCH_STATUS}', AUDIT_UPDT_DT = '${formattedDate}' where RULE_ID = '${row.RULE_ID}'`,
    //   "flag": "u"
    // }
    this.loaderService.showLoader()
    let response: any = await lastValueFrom(this.dataQualityApiService.updateRuleStatus(postData))
    let key = ""
    let message = ""
    if (response.status === 200) {
      key = 'suss'
      message = "Status Updated Successfully"
      this.getResponseHandler()
    } else {
      key = 'wrn'
      message = "Something went worng. Please try again."
    }
    this.loaderService.hideLoader()
    this.toastService.clear()
    // console.log('key=>',key,'message=>',message)
    this.toastService.add({ key: key, closable: false, sticky: false, severity: '', summary: '', detail: message });
    console.log(this.activeFiltered)
  }
  //calculate count
  calculateCount(table: any[]) {
    this.count = { enable: 0, disable: 0, total: 0 }
    table.forEach(row => {
      if (row.STATUS === 'Active') {
        this.count.enable++
      } else {
        this.count.disable++
      }
    })
    this.count.total = this.count.enable + this.count.disable
  }

  statusHandler(data) {
    // console.log(data)
    this.toastService.clear()
    this.toastService.add({ key: data.flag, closable: false, sticky: false, severity: '', summary: '', detail: data.message });
  }
  updateRuleCount(param) {
    this.resetAll()
    this.rules_record_count = param;
    // console.log(this.rules_record_count)

  }
  exportExcel() {
    this.excelservice.exportExcel('Threshold-Variation-Rules.xlsx', this.table_data_orig);
  }
}
