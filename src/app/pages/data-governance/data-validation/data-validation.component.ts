import {
  AfterViewInit,
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { PrimeMultiSelectComponent } from '@app/components/form-components/prime-multi-select/prime-multi-select.component';
import {
  getUniqueValues,
  getMappedArray,
  filterIncludedValuesFromArray,
  editDuplicateRecordChecker,
  addDuplicateRecordChecker,
  checkingDuplicationBetweentwoArray,
} from '@app/util/helper/arrayHandlers';
import {
  replaceNumberWithEmptyString,
  replaceParamsinJSON,
  replaceWordsWithEmptyString,
  tableFilter,
} from '@app/util/helper/harmonyUtils';
import { CommonDataService } from '@app/_services/common-data.service';
import { ExcelService } from '@app/_services/excel.service';
import { LoaderService } from '@app/_services/loading.service';
import { QueryService } from '@app/_services/query.service';
import { format } from 'date-fns';
import { MessageService } from 'primeng/api';
import { lastValueFrom, map } from 'rxjs';
import { DataValidationEditPopupComponent } from '@app/components/dg-components/data-validation-edit-popup/data-validation-edit-popup.component';
import { DataQualityApiService } from '@app/_services/dataQuality-api.service';
@Component({
  selector: 'app-data-validation',
  templateUrl: './data-validation.component.html',
  styleUrls: ['./data-validation.component.scss'],
})
export class DataValidationComponent implements OnInit, AfterViewInit {
  @ViewChild('dataZone') dataZone: PrimeMultiSelectComponent;
  @ViewChild('dataProvider') dataProvider: PrimeMultiSelectComponent;
  @ViewChild('dataSetName') dataSetName: PrimeMultiSelectComponent;
  @ViewChild('attribute') attribute: PrimeMultiSelectComponent;
  @ViewChild('ruleType') ruleType: PrimeMultiSelectComponent;
  @ViewChild('value_1') value_1: PrimeMultiSelectComponent;
  @ViewChild('value_2') value_2: PrimeMultiSelectComponent;
  @ViewChild('dqSeverity') dqSeverity: PrimeMultiSelectComponent;
  @ViewChild('status') status: PrimeMultiSelectComponent;
  @ViewChild('editPopup') editPopupComponent: DataValidationEditPopupComponent;
  @Output() record_count = new EventEmitter();
  buttonProps = {
    label: 'Export',
    click: '',
  };
  popupModelData: {
    addDisplayModal: boolean;
    editDisplayModal: boolean;
    header: string;
    tableData: any[];
    saveBtnProps: { label: string; click: string };
    cancelBtnProps: { label: string; click: string };
    editValue: string;
  };
  currentDeleteRuleId: any;
  table_data_orig: any;
  deletePopupModel: {
    displayModal: boolean;
    header: string;
    icon: string;
    content: { header: string; text: string };
    deleteBtnProps: { label: string; click: string };
    cancelBtnProps: { label: string; click: string };
  };
  table_data_copy: any[];
  uniqueValues: any;
  filterComponents: any;
  @Output() addApiStatusEmitter = new EventEmitter();
  isEdit: boolean;
  latestRuleId: any;
  editedRuleId: any;
  tablePageNumber: number = 0;
  constructor(
    private queryService: QueryService,
    private commonDataService: CommonDataService,
    public loaderService: LoaderService,
    private excelservice: ExcelService,
    private dataQualityApiService: DataQualityApiService
  ) { }
  COLUMNS = {
    dataZone: 'DATA_ZONE',
    dataProvider: 'DATA_PROVIDER',
    dataSetName: 'DATASET_NAME',
    attribute: 'ATTRIBUTE',
    ruleType: 'DQ_RULE_TYPE',
    value_1: 'VALUE_1',
    value_2: 'VALUE_2',
    dqSeverity: 'RULE_SEVERITY',
    status: 'STATUS',
  };
  activeFiltered = {
    dataZone: { valArr: [], column: 'DATA_ZONE' },
    dataProvider: { valArr: [], column: 'DATA_PROVIDER' },
    dataSetName: { valArr: [], column: 'DATASET_NAME' },
    attribute: { valArr: [], column: 'ATTRIBUTE' },
    ruleType: { valArr: [], column: 'DQ_RULE_TYPE' },
    value_1: { valArr: [], column: 'VALUE_1' },
    value_2: { valArr: [], column: 'VALUE_2' },
    dqSeverity: { valArr: [], column: 'RULE_SEVERITY' },
    status: { valArr: [], column: 'STATUS' },
  };
  columnsArray = [
    this.COLUMNS.dataZone,
    this.COLUMNS.dataSetName,
    this.COLUMNS.dataProvider,
    this.COLUMNS.attribute,
    this.COLUMNS.ruleType,
    this.COLUMNS.value_1,
    this.COLUMNS.value_2,
    this.COLUMNS.dqSeverity,
    this.COLUMNS.status,
  ];
  ngOnInit() {
    this.getResponseHandler();
    this.popupModelData = {
      addDisplayModal: false,
      editDisplayModal: false,
      tableData: [],
      header: '',
      saveBtnProps: {
        label: 'Save',
        click: '',
      },
      cancelBtnProps: {
        label: 'Cancel',
        click: '',
      },
      editValue: '',
    };
  }
  ngAfterViewInit() {
    this.filterComponents = [
      { selected: this.COLUMNS.dataZone, component: this.dataZone },
      { selected: this.COLUMNS.dataProvider, component: this.dataProvider },
      { selected: this.COLUMNS.dataSetName, component: this.dataSetName },
      { selected: this.COLUMNS.attribute, component: this.attribute },
      { selected: this.COLUMNS.ruleType, component: this.ruleType },
      { selected: this.COLUMNS.value_1, component: this.value_1 },
      { selected: this.COLUMNS.value_2, component: this.value_2 },
      { selected: this.COLUMNS.dqSeverity, component: this.dqSeverity },
      { selected: this.COLUMNS.status, component: this.status },
    ];
  }
  async getResponseHandler() {
    this.loaderService.showLoader();


    this.table_data_orig = await lastValueFrom(
      this.dataQualityApiService.getDataValidationRules().pipe(
        map((array: any) => {
          return array.map((item) => ({
            ...item,
            SWITCH_STATUS: item.STATUS === 'Active' ? true : false,
          }));
        })
      )
    );

    // this.table_data_copy = [...this.table_data_orig];
    this.table_data_copy = tableFilter(
      this.activeFiltered,
      this.table_data_orig
    );
    // console.log(this.table_data_copy)
    this.uniqueValues = this.getDistinctValuesForColumns(
      this.table_data_orig,
      this.columnsArray
    );
    // console.log(this.uniqueValues)
    this.loaderService.hideLoader();
    this.latestRuleId = this.table_data_copy[0].RULE_ID;
    this.record_count.emit(this.table_data_copy.length);
  }
  handleNewQueryClick() {
    this.popupModelData.addDisplayModal = true;
    this.isEdit = false;
    this.popupModelData.header = 'Data Validation New Rule';
    this.popupModelData.editValue = '';
    this.popupModelData.tableData = [...this.table_data_orig];
  }
  editBtnHandler(row) {
    this.popupModelData.header = 'Data Validation Edit Rule';
    this.popupModelData.editValue = row;
    this.popupModelData.editDisplayModal = true;
    this.editedRuleId = row.RULE_ID;
  }
  deleteBtnHandler(row) {
    this.currentDeleteRuleId = row.RULE_ID;

    this.deletePopupModel = {
      displayModal: true,
      header: 'Delete Rule',
      icon: 'pi-exclamation-triangle',
      content: {
        header: `Are you sure to delete the following rule ?`,
        text: 'This action will delete the rule permanently',
      },
      deleteBtnProps: {
        label: 'Delete',
        click: '',
      },
      cancelBtnProps: {
        label: 'Cancel',
        click: '',
      },
    };
  }
  async deleteApiCall() {
    this.deletePopupModel.displayModal = false;
    this.loaderService.showLoader();
    let ruleType = replaceNumberWithEmptyString(this.currentDeleteRuleId);
    const rule_info = {
      "identifier": ruleType,
      "ruleId": this.currentDeleteRuleId
    };
    const response = await lastValueFrom(this.dataQualityApiService.deleteRule(rule_info))
    let successMessage = 'Rule Deleted Successfully';
    let errMessages = 'Something went wrong. Please try again';
    if (response.status == 200) {
      // console.log(successMessage)
      this.addApiStatusEmitter.emit({ flag: 'err', message: successMessage });
      this.resetAll();
      this.getResponseHandler();
    } else {
      //calling toast
      // console.log(errMessages)
      this.addApiStatusEmitter.emit({ flag: 'wrn', message: errMessages });
    }
    this.loaderService.hideLoader();
  }
  async inputSwitchOnchange(row) {
    let ruleType = replaceNumberWithEmptyString(row.RULE_ID)
    ruleType = (ruleType === 'THV') ? 'TH' : ruleType;

    const postData = {
      "status": row.SWITCH_STATUS,
      "identifier": ruleType,
      "ruleId": row.RULE_ID
    }
    this.loaderService.showLoader();
    let response: any = await lastValueFrom(this.dataQualityApiService.updateRuleStatus(postData))
    let key = '';
    let message = '';
    console.log(response);

    if (response.status === 200) {
      key = 'suss';
      message = 'Status Updated Successfully';
      this.getResponseHandler();
    } else {
      key = 'wrn';
      message = 'Something went wrong. Please try again.';
    }
    this.addApiStatusEmitter.emit({ flag: key, message: message });
    this.loaderService.hideLoader();
  }
  async editPopupSubmitHandler(formValue) {
    let tableProperty = [
      'DATA_ZONE',
      'DATA_PROVIDER',
      'DATASET_NAME',
      'ATTRIBUTE',
      'DQ_RULE_TYPE',
    ];
    let formProperty = [
      'dataZone',
      'dataProvider',
      'datasetName',
      'attribute',
      'ruleType',
    ];
    let duplicateFound = editDuplicateRecordChecker(
      formValue,
      formProperty,
      this.table_data_orig,
      tableProperty,
      this.editedRuleId
    );

    console.log(duplicateFound);
    if (duplicateFound) {
      let errMessages = 'This Rule Already Inserted.Try Someother Rule';
      this.addApiStatusEmitter.emit({ flag: 'wrn', message: errMessages });
    } else {
      this.editPopupComponent.formGroup.reset();
      const formattedDate = format(new Date(), 'yyyy-MM-dd');
      const userId = localStorage.getItem('userId');
      let errMessages = 'Something went wrong. Please try again';
      let query = {};
      let value_1 = formValue.value_1.value
        ? formValue.value_1.value
        : formValue.value_1;
      let status = formValue.status.value === 'Disabled' ? false : true;

      const ruleUpdationData = {
        "ruletype": formValue.ruleType.value, //(MSTR_RULE_ID)
        "attri_value": formValue.attribute.value, //(colId)
        "dataset_value": formValue.datasetName.value,//(ENT_ID)
        "value_1": value_1,  // (ARG_1)
        "value_2": formValue.value_2,   // (ARG_2)
        "dq_severity": formValue.dqSeverity.value,   // (RULE_SVRT)
        "status": status,
        "user_id": userId,
        "rule_id": this.editedRuleId
      }
      let successMessage = 'Rule Updated Successfully';

      this.loaderService.showLoader();
      let response = await lastValueFrom(
        this.dataQualityApiService.updateDataValidationRule(ruleUpdationData)
      );
      this.loaderService.hideLoader();
      if (response.status == 200) {
        // calling toast
        this.addApiStatusEmitter.emit({
          flag: 'suss',
          message: successMessage,
        });
        this.getResponseHandler();
        this.popupModelData.editDisplayModal = false;
      } else {
        //calling toast
        this.addApiStatusEmitter.emit({ flag: 'wrn', message: errMessages });
      }
    }
  }

  async addPopupSubmitHandler(formValue) {
    const formattedDate = format(new Date(), 'yyyy-MM-dd');
    const userId = localStorage.getItem('userId');
    // let insertQuery =
    //   'INSERT INTO HRMNY_DB.HRMNY_CNF_ZN.DQ_DV_CONFIG (RULE_ID, MSTR_RULE_ID, ENT_ID,COL_ID_LST, BK_COL_ID, FLTR_CON, ARG_1, ARG_2, ARG_3, ARG_4, RULE_SVRT, RULE_DESC, IS_RULE_ACTV, EFFCTV_STRT_DT, EFFCTV_END_DT, IS_ACTV, AUDIT_BATCH_ID, AUDIT_INSRT_DT, AUDIT_INSRT_ID, AUDIT_UPDT_DT, AUDIT_UPDT_ID) VALUES';
    // //                                                         ('DV2167', 1,          19,       143,           0,      null,  'sq_test',  '', null, null,   'Critical', null,        true,      '2023-03-30',   null,            true,      '',          '2023-03-30',   2007,           '2023-03-30', 2007),

    // duplicate checking
    const attributeArr = formValue.attribute.filter(
      (record) => record.ruleType !== ''
    );

    const attributeDuplicateArr = attributeArr.filter((record) => {
      return this.table_data_orig.some((t_record) => {
        return (
          record.ruleType.value === t_record.MSTR_RULE_ID &&
          record.colId === t_record.COL_ID &&
          t_record.DATA_LAYER_ID === formValue.dataZone.value &&
          t_record.DATA_PROVIDER === formValue.dataProvider.value &&
          t_record.ENT_ID === formValue.datasetName.value
        );
      });
    });

    const rule_data = [];

    if (attributeDuplicateArr.length) {
      this.addApiStatusEmitter.emit({
        flag: 'wrn',
        message: `${attributeDuplicateArr[0].attribute} Already Inserted.Try someother rule`,
      });
    } else {
      attributeArr.forEach((element, i) => {
        if (element.ruleType) {
          let RuleId =
            replaceNumberWithEmptyString(this.latestRuleId) +
            (Number(replaceWordsWithEmptyString(this.latestRuleId)) + (1 + i));
          let status = element.status.value === 'Disabled' ? false : true;
          let value_1 = element.value_1.value
            ? element.value_1.value
            : element.value_1;
          let obj = {
            "ruletype": element.ruleType.value, //(MSTR_RULE_ID)
            "col_id": element.colId, //(colId)
            "dataset_value": formValue.datasetName.value,//(ENT_ID)
            "value_1": value_1,  // (ARG_1)
            "value_2": element.value_2 ? element.value_2 : '',   // (ARG_2)
            "dq_severity": element.dqSeverity.value,   // (RULE_SVRT)
            "status": status,
            "user_id": userId,
            "rule_id": RuleId
          }

          rule_data.push(obj)
        }
      });

      const ruleCreationData = {
        "attributeData": rule_data,
        "dataset": formValue.datasetName.value,
        "user_id": userId
      }
      let errMessages = 'Something went wrong. Please try again';
      let query = {};
      // insert query
      // query = {
      //   query: insertQuery.replace(/.$/, ';'),
      //   flag: 'i',
      // };
      let successMessage = 'Rule Inserted Successfully';
      // console.log(query)
      this.popupModelData.addDisplayModal = false;
      this.loaderService.showLoader();
      let response = await lastValueFrom(
        this.dataQualityApiService.createDataValidationRule(ruleCreationData)
      );
      this.loaderService.hideLoader();
      // console.log(response)

      if (response.status == 200) {
        this.addApiStatusEmitter.emit({
          flag: 'suss',
          message: successMessage,
        });
        this.resetAll();
        this.getResponseHandler();
      } else {
        this.addApiStatusEmitter.emit({ flag: 'wrn', message: errMessages });
      }
    }
  }
  getDistinctValuesForColumns(tableData: any[], columns: string[]) {
    const result: any = [];

    columns.forEach((column) => {
      const uniqueValues = getUniqueValues(tableData, column);

      const mappedArray = getMappedArray(uniqueValues);
      result[column] = [...mappedArray];
    });

    return result;
  }
  multiselectOnchange(filteredArr, column) {
    // console.log('filterdArr=>',filterdArr,'column=>',column)
    let tempFilterArray = filteredArr;
    if (column === 'DATA_ZONE') {
      this.activeFiltered.dataZone.valArr = tempFilterArray;
    } else if (column === 'DATA_PROVIDER') {
      this.activeFiltered.dataProvider.valArr = tempFilterArray;
    } else if (column === 'DATASET_NAME') {
      this.activeFiltered.dataSetName.valArr = tempFilterArray;
    } else if (column === 'ATTRIBUTE') {
      this.activeFiltered.attribute.valArr = tempFilterArray;
    } else if (column === 'DQ_RULE_TYPE') {
      this.activeFiltered.ruleType.valArr = tempFilterArray;
    } else if (column === 'VALUE_1') {
      this.activeFiltered.value_1.valArr = tempFilterArray;
    } else if (column === 'VALUE_2') {
      this.activeFiltered.value_2.valArr = tempFilterArray;
    } else if (column === 'RULE_SEVERITY') {
      this.activeFiltered.dqSeverity.valArr = tempFilterArray;
    } else if (column === 'STATUS') {
      this.activeFiltered.status.valArr = tempFilterArray;
    }

    console.log(this.activeFiltered);
    const result = tableFilter(this.activeFiltered, this.table_data_orig);
    if (result.length) {
      console.log(result);
      this.table_data_copy = result;
    } else {
      if (
        !Object.keys(this.activeFiltered).some(
          (field) => this.activeFiltered[field].valArr.length
        )
      ) {
        this.table_data_copy = this.table_data_orig;
      } else {
        this.table_data_copy = result;
      }
    }
    this.setFilterOptions(this.activeFiltered, result);
    this.record_count.emit(this.table_data_copy.length);
    this.tablePageNumber = 0;
  }
  setFilterOptions(fieldObject, result) {
    const filterFields = Object.keys(fieldObject);

    for (let field of filterFields) {
      if (!fieldObject[field].valArr.length) {
        this.uniqueValues[fieldObject[field].column] = getMappedArray(
          getUniqueValues(result, fieldObject[field].column)
        );
      }
    }
  }
  activatedFilterReset(fieldObject: any) {
    const filterFields = Object.keys(fieldObject);
    filterFields.forEach((key) => (fieldObject[key].valArr = []));
  }
  // resetBy(param) {
  //   this.filterComponents.forEach((item) => {
  //     if (item.selected !== param) {
  //       item.component.resetSelection();
  //     }
  //   });
  //   this.record_count.emit(this.table_data_copy.length)
  // }
  resetAll() {
    this.filterComponents.forEach((item) => {
      item.component.resetSelection();
    });
    this.activatedFilterReset(this.activeFiltered);
    this.table_data_copy = this.table_data_orig;
    this.record_count.emit(this.table_data_copy.length);
    this.uniqueValues = this.getDistinctValuesForColumns(
      this.table_data_copy,
      this.columnsArray
    );
  }

  exportExcel() {
    this.excelservice.exportExcel(
      'Data-Validation-Rules.xlsx',
      this.table_data_orig
    );
  }
}
