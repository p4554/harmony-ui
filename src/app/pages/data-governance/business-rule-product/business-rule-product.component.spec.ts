import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessRuleProductComponent } from './business-rule-product.component';

describe('BusinessRuleProductComponent', () => {
  let component: BusinessRuleProductComponent;
  let fixture: ComponentFixture<BusinessRuleProductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessRuleProductComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BusinessRuleProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
