import { Component,Input,EventEmitter,Output } from '@angular/core';
import { ExcelService } from '@app/_services/excel.service';

@Component({
  selector: 'app-business-rule-product',
  templateUrl: './business-rule-product.component.html',
  styleUrls: ['./business-rule-product.component.scss']
})
export class BusinessRuleProductComponent {
  @Input() savedData:string;
  
  @Output() ruleCountEmitter:EventEmitter<number> = new EventEmitter<number>()
  tabs=[
    "Market Definition",
    "Normalization Factor",
  ]
  currentTab:string="Market Definition";
  async ngOnInit() {
   }
   constructor(
    private excelService: ExcelService,

   ){}

   setCurrentTab(event){
    this.currentTab=this.tabs[event.index];
  }
  tableData:any;
  tableName:string;
  handleData(e){
    if(e.type=='market'){
      this.tableName='Market-Definition-Rule.xlsx';
      this.tableData=e.payload;
      this.ruleCountEmitter.emit(this.tableData.length)
    }
    else if(e.type=='Normal'){
      this.tableName='Normalization-factor-Rule.xlsx';
      this.tableData=e.payload;
      this.ruleCountEmitter.emit(this.tableData.length)
    }
    
  } 
exportExcel(){
  this.excelService.exportExcel(this.tableName, this.tableData);
}

}
