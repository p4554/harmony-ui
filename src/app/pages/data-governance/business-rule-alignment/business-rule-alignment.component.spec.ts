import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessRuleAlignmentComponent } from './business-rule-alignment.component';

describe('BusinessRuleAlignmentComponent', () => {
  let component: BusinessRuleAlignmentComponent;
  let fixture: ComponentFixture<BusinessRuleAlignmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessRuleAlignmentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BusinessRuleAlignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
