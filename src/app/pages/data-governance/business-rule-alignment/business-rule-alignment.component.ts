import { Component,Input,Output,EventEmitter} from '@angular/core';
import { ExcelService } from '@app/_services/excel.service';
@Component({
  selector: 'app-business-rule-alignment',
  templateUrl: './business-rule-alignment.component.html',
  styleUrls: ['./business-rule-alignment.component.scss']
})
export class BusinessRuleAlignmentComponent {
  @Input() savedData:string;
  @Output() ruleCountEmitter:EventEmitter<number> = new EventEmitter<number>()

  tabs=[
    "ZIP Territory Alignment",
    "Customer Territory Alignment",
    "Employee Territory Alignment"
  ]
  currentTab:string="ZIP Territory Alignment";
  constructor(
    private excelService: ExcelService,

   ){}
  setCurrentTab(event){
    this.currentTab=this.tabs[event.index];
  }
  tableData:any;
  tableName:string;
  handleData(e){
    if(e.type=='zip'){
      this.tableName='Zip-Territory-Alignment-Rule.xlsx';
      this.tableData=e.payload;
      this.ruleCountEmitter.emit(this.tableData.length)

    }
    else if(e.type=='customer'){
      this.tableName='customer-Territory-Alignment-Rule.xlsx';
      this.tableData=e.payload;
      this.ruleCountEmitter.emit(this.tableData.length)

    }
    else if(e.type=='employee'){
      this.tableName='Employee-Territory-Alignment-Rule.xlsx';
      this.tableData=e.payload;
      this.ruleCountEmitter.emit(this.tableData.length)

    }
    
  } 
exportExcel(){
  this.excelService.exportExcel(this.tableName, this.tableData);
}

}
