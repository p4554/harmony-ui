import {
  AfterViewInit,
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { PrimeMultiSelectComponent } from '@app/components/form-components/prime-multi-select/prime-multi-select.component';
import {
  addDuplicateRecordChecker,
  editDuplicateRecordChecker,
  filterIncludedValuesFromArray,
  getMappedArray,
  getUniqueValues,
} from '@app/util/helper/arrayHandlers';
import {
  replaceNumberWithEmptyString,
  replaceParamsinJSON,
  replaceWordsWithEmptyString,
} from '@app/util/helper/harmonyUtils';
import { CommonDataService } from '@app/_services/common-data.service';
import { ExcelService } from '@app/_services/excel.service';
import { LoaderService } from '@app/_services/loading.service';
import { QueryService } from '@app/_services/query.service';
import { format } from 'date-fns';
import { lastValueFrom, map } from 'rxjs';
import { tableFilter } from '@app/util/helper/harmonyUtils';
import { DataQualityApiService } from '@app/_services/dataQuality-api.service';

@Component({
  selector: 'app-referential-integrity',
  templateUrl: './referential-integrity.component.html',
  styleUrls: ['./referential-integrity.component.scss'],
})
export class ReferentialIntegrityComponent implements OnInit, AfterViewInit {
  @ViewChild('p_dataZone') p_dataZone: PrimeMultiSelectComponent;
  @ViewChild('P_dataProvider') P_dataProvider: PrimeMultiSelectComponent;
  @ViewChild('P_dataSetName') P_dataSetName: PrimeMultiSelectComponent;
  @ViewChild('P_attribute') P_attribute: PrimeMultiSelectComponent;
  @ViewChild('C_dataZone') C_dataZone: PrimeMultiSelectComponent;
  @ViewChild('C_dataProvider') C_dataProvider: PrimeMultiSelectComponent;
  @ViewChild('C_dataSetName') C_dataSetName: PrimeMultiSelectComponent;
  @ViewChild('C_attribute') C_attribute: PrimeMultiSelectComponent;
  @ViewChild('dqSeverity') dqSeverity: PrimeMultiSelectComponent;
  @ViewChild('status') status: PrimeMultiSelectComponent;
  @Output() addApiStatusEmitter = new EventEmitter();
  @Output() record_count = new EventEmitter();

  buttonPorps = {
    label: 'Export',
    click: '',
  };
  popupModelData: any;
  table_data_orig: any[] = [];
  table_data_copy: any[] = [];
  COLUMNS = {
    p_dataZone: 'PARENT_DATA_ZONE',
    P_dataProvider: 'PARENT_DATA_PROVIDER',
    P_dataSetName: 'PARENT_DATASET_NAME',
    P_attribute: 'PARENT_DATASET_ATTRIBUTE',
    C_dataZone: 'CHILD_DATA_ZONE',
    C_dataProvider: 'CHILD_DATA_PROVIDER',
    C_dataSetName: 'CHILD_DATASET_NAME',
    C_attribute: 'CHILD_DATASET_ATTRIBUTE',
    dqSeverity: 'RULE_SEVERITY',
    status: 'STATUS',
  };
  activeFiltered = {
    p_dataZone: { valArr: [], column: 'PARENT_DATA_ZONE' },
    P_dataProvider: { valArr: [], column: 'PARENT_DATA_PROVIDER' },
    P_dataSetName: { valArr: [], column: 'PARENT_DATASET_NAME' },
    P_attribute: { valArr: [], column: 'PARENT_DATASET_ATTRIBUTE' },
    C_dataZone: { valArr: [], column: 'CHILD_DATA_ZONE' },
    C_dataProvider: { valArr: [], column: 'CHILD_DATA_PROVIDER' },
    C_dataSetName: { valArr: [], column: 'CHILD_DATASET_NAME' },
    C_attribute: { valArr: [], column: 'CHILD_DATASET_ATTRIBUTE' },
    dqSeverity: { valArr: [], column: 'RULE_SEVERITY' },
    status: { valArr: [], column: 'STATUS' },
  };

  columnsArray = [
    this.COLUMNS.p_dataZone,
    this.COLUMNS.P_dataProvider,
    this.COLUMNS.P_dataSetName,
    this.COLUMNS.P_attribute,
    this.COLUMNS.C_dataZone,
    this.COLUMNS.C_dataProvider,
    this.COLUMNS.C_dataSetName,
    this.COLUMNS.C_attribute,
    this.COLUMNS.dqSeverity,
    this.COLUMNS.status,
  ];
  uniqueValues: any;
  filterComponents: any;
  isEdit: boolean;
  latestRuleId: any;
  editedRuleId: any;
  deletePopupModel: {
    displayModal: boolean;
    header: string;
    icon: string;
    content: { header: string; text: string };
    deleteBtnProps: { label: string; click: string };
    cancelBtnProps: { label: string; click: string };
  };
  deleteRuleId: any;
  tablePageNumber = 0;
  constructor(
    private queryService: QueryService,
    private commonDataService: CommonDataService,
    public loaderService: LoaderService,
    private excelservice: ExcelService,
    private dataQualityApiService: DataQualityApiService
  ) { }

  ngOnInit() {
    this.popupModelData = {
      displayModal: false,
      header: 'New Rule',
      saveBtnProps: {
        label: 'Save',
        click: '',
      },
      cancelBtnProps: {
        label: 'Cancel',
        click: '',
      },
      editValue: '',
    };
    this.getResponseHandler();
    this.loaderService.showLoader();
  }
  ngAfterViewInit() {
    this.filterComponents = [
      { selected: this.COLUMNS.p_dataZone, component: this.p_dataZone },
      { selected: this.COLUMNS.P_dataProvider, component: this.P_dataProvider },
      { selected: this.COLUMNS.P_dataSetName, component: this.P_dataSetName },
      { selected: this.COLUMNS.P_attribute, component: this.P_attribute },
      { selected: this.COLUMNS.C_dataZone, component: this.C_dataZone },
      { selected: this.COLUMNS.C_dataProvider, component: this.C_dataProvider },
      { selected: this.COLUMNS.C_dataSetName, component: this.C_dataSetName },
      { selected: this.COLUMNS.C_attribute, component: this.C_attribute },
      { selected: this.COLUMNS.dqSeverity, component: this.dqSeverity },
      { selected: this.COLUMNS.status, component: this.status },
    ];
  }
  async getResponseHandler() {
    this.table_data_orig = await lastValueFrom(
      this.dataQualityApiService.getReferentialIntegrityRules().pipe(
        map((array: any) => {
          return array.map((item) => ({
            ...item,
            SWITCH_STATUS: item.STATUS === 'Active' ? true : false,
          }));
        })
      )
    );

    this.loaderService.hideLoader();
    // this.table_data_copy = [...this.table_data_orig];
    this.table_data_copy = tableFilter(
      this.activeFiltered,
      this.table_data_orig
    );
    console.log(this.table_data_copy);
    this.loaderService.hideLoader();
    this.uniqueValues = this.getDistinctValuesForColumns(
      this.table_data_orig,
      this.columnsArray
    );
    this.latestRuleId = this.table_data_copy[0].RULE_ID;
    this.record_count.emit(this.table_data_copy.length);
  }

  handleNewRuleClick() {
    this.popupModelData.displayModal = true;
    this.popupModelData.editValue = '';
    this.popupModelData.header = 'Referential Integrity Add Rule';
    this.isEdit = false;
  }
  editBtnHandler(row) {
    console.log(row);
    this.isEdit = true;

    this.editedRuleId = row.RULE_ID;
    this.popupModelData.editValue = row;
    this.popupModelData.displayModal = true;
    this.popupModelData.header = 'Referential Integrity Edit Rule';
  }
  deleteBtnHandler(row) {
    console.log(row);
    this.deletePopupModel = {
      displayModal: true,
      header: 'Delete rule',
      icon: 'pi-exclamation-triangle',
      content: {
        header: `Are you sure to delete the following rule ?`,
        text: 'This action will delete the rule permanently',
      },
      deleteBtnProps: {
        label: 'Delete',
        click: '',
      },
      cancelBtnProps: {
        label: 'Cancel',
        click: '',
      },
    };
    this.deleteRuleId = row.RULE_ID;
  }
  async inputSwitchOnchange(row) {
    let ruleType = replaceNumberWithEmptyString(row.RULE_ID)
    ruleType = (ruleType === 'THV') ? 'TH' : ruleType;

    const postData = {
      "status": row.SWITCH_STATUS,
      "identifier": ruleType,
      "ruleId": row.RULE_ID
    }
    this.loaderService.showLoader();
    let response: any = await lastValueFrom(this.dataQualityApiService.updateRuleStatus(postData))
    let key = '';
    let message = '';
    console.log(response);

    if (response.status === 200) {
      key = 'suss';
      message = 'Status Updated Successfully';
      this.getResponseHandler();
    } else {
      key = 'wrn';
      message = 'Something went wrong. Please try again.';
    }
    this.addApiStatusEmitter.emit({ flag: key, message: message });
    this.loaderService.hideLoader();
  }
  getDistinctValuesForColumns(tableData: any[], columns: string[]) {
    const result: any = [];

    columns.forEach((column) => {
      const uniqueValues = getUniqueValues(tableData, column);

      const mappedArray = getMappedArray(uniqueValues);
      result[column] = [...mappedArray];
    });

    return result;
  }
  multiselectOnchange(filteredArr, column) {
    let tempFilterArray = filteredArr;
    if (column === 'PARENT_DATA_ZONE') {
      this.activeFiltered.p_dataZone.valArr = tempFilterArray;
    } else if (column === 'PARENT_DATA_PROVIDER') {
      this.activeFiltered.P_dataProvider.valArr = tempFilterArray;
    } else if (column === 'PARENT_DATASET_NAME') {
      this.activeFiltered.P_dataSetName.valArr = tempFilterArray;
    } else if (column === 'PARENT_DATASET_ATTRIBUTE') {
      this.activeFiltered.P_attribute.valArr = tempFilterArray;
    } else if (column === 'CHILD_DATA_ZONE') {
      this.activeFiltered.C_dataZone.valArr = tempFilterArray;
    } else if (column === 'CHILD_DATA_PROVIDER') {
      this.activeFiltered.C_dataProvider.valArr = tempFilterArray;
    } else if (column === 'CHILD_DATASET_NAME') {
      this.activeFiltered.C_dataSetName.valArr = tempFilterArray;
    } else if (column === 'CHILD_DATASET_ATTRIBUTE') {
      this.activeFiltered.C_attribute.valArr = tempFilterArray;
    } else if (column === 'RULE_SEVERITY') {
      this.activeFiltered.dqSeverity.valArr = tempFilterArray;
    } else if (column === 'STATUS') {
      this.activeFiltered.status.valArr = tempFilterArray;
    }
    console.log(this.activeFiltered);
    const result = tableFilter(this.activeFiltered, this.table_data_orig);
    if (result.length) {
      console.log(result);
      this.table_data_copy = result;
    } else {
      if (
        !Object.keys(this.activeFiltered).some(
          (field) => this.activeFiltered[field].valArr.length
        )
      ) {
        this.table_data_copy = this.table_data_orig;
      } else {
        this.table_data_copy = result;
      }
    }
    this.setFilterOptions(this.activeFiltered, result);

    this.record_count.emit(this.table_data_copy.length);
    this.tablePageNumber = 0;
  }
  setFilterOptions(fieldObject, result) {
    const filterFields = Object.keys(fieldObject);

    for (let field of filterFields) {
      if (!fieldObject[field].valArr.length) {
        this.uniqueValues[fieldObject[field].column] = getMappedArray(
          getUniqueValues(result, fieldObject[field].column)
        );
      }
    }
  }
  activatedFilterReset(fieldObject: any) {
    const filterFields = Object.keys(fieldObject);
    filterFields.forEach((key) => (fieldObject[key].valArr = []));
  }
  // resetBy(param) {
  //   this.filterComponents.forEach((item) => {
  //     if (item.selected !== param) {
  //       item.component.resetSelection();
  //     }
  //   });
  //   this.record_count.emit(this.table_data_copy.length)
  // }
  resetAll() {
    this.filterComponents.forEach((item) => {
      item.component.resetSelection();
    });
    this.table_data_copy = this.table_data_orig;
    this.activatedFilterReset(this.activeFiltered);
    this.record_count.emit(this.table_data_copy.length);
    this.uniqueValues = this.getDistinctValuesForColumns(
      this.table_data_copy,
      this.columnsArray
    );
  }

  // popup submit handler
  async handleFormSubmit(formValue) {
    console.log(formValue);
    const formattedDate = format(new Date(), 'yyyy-MM-dd');
    const userId = localStorage.getItem('userId');
    let status = formValue.status.value === 'Disabled' ? false : true;
    let RuleId =
      replaceNumberWithEmptyString(this.latestRuleId) +
      (Number(replaceWordsWithEmptyString(this.latestRuleId)) + 1);
    let successMessage = '';
    let errMessages = 'Something went wrong. Please try again';
    let query = {};
    console.log(status);

    const formProperty = [
      'P_dataZone',
      'P_dataProvider',
      'P_datasetName',
      'P_dataSetAttribute',
      'C_dataZone',
      'C_dataProvider',
      'C_datasetName',
      'C_dataSetAttribute',
    ];
    const tableProperty = [
      'PARENT_DATA_ZONE',
      'PARENT_DATA_PROVIDER',
      'PARENT_DATASET_NAME',
      'PARENT_DATASET_ATTRIBUTE',
      'CHILD_DATA_ZONE',
      'CHILD_DATA_PROVIDER',
      'CHILD_DATASET_NAME',
      'CHILD_DATASET_ATTRIBUTE',
    ];
    if (!this.isEdit) {
      // add handler
      const duplicateFound = addDuplicateRecordChecker(
        formValue,
        formProperty,
        this.table_data_orig,
        tableProperty
      );
      if (!duplicateFound) {
        const ruleCreationData = {
          "rule_id": RuleId,
          "p_dataset_value": formValue.P_datasetName.value,
          "p_dataSetAttr_value": formValue.P_dataSetAttribute.value,
          "c_dataset_value": formValue.C_datasetName.value,
          "c_dataSetAttr_value": formValue.C_dataSetAttribute.value,
          "dq_severity": formValue.dqSeverity.value,
          "status": status,
          "user_id": userId
        }
        successMessage = 'Rule Inserted Successfully';
        this.popupModelData.displayModal = false;
        this.loaderService.showLoader();
        const response = await lastValueFrom(
          this.dataQualityApiService.createReferentialIntegrityRule(ruleCreationData)
        );
        this.loaderService.hideLoader();
        if (response.status == 200) {
          // calling toast
          // console.log(successMessage)
          this.addApiStatusEmitter.emit({
            flag: 'suss',
            message: successMessage,
          });
          this.resetAll();
          this.getResponseHandler();
        } else {
          //calling toast
          // console.log(errMessages)
          this.addApiStatusEmitter.emit({ flag: 'wrn', message: errMessages });
        }
      } else {
        // duplicate error throwing
        this.addApiStatusEmitter.emit({
          flag: 'wrn',
          message: 'This Rule Already Inserted.Try Someother Rule',
        });
      }
    } else {
      // edit handler
      // duplicate checking
      const duplicateFound = editDuplicateRecordChecker(
        formValue,
        formProperty,
        this.table_data_orig,
        tableProperty,
        this.editedRuleId
      );
      console.log(duplicateFound);
      if (!duplicateFound) {
        const ruleUpdationData = {
          "rule_id": RuleId,
          "p_dataset_value": formValue.P_datasetName.value,
          "p_dataSetAttr_value": formValue.P_dataSetAttribute.value,
          "c_dataset_value": formValue.C_datasetName.value,
          "c_dataSetAttr_value": formValue.C_dataSetAttribute.value,
          "dq_severity": formValue.dqSeverity.value,
          "status": status,
          "user_id": userId
        }
        successMessage = 'Rule Updated Successfully';
        this.popupModelData.displayModal = false;
        let response = await lastValueFrom(
          this.dataQualityApiService.updateReferentialIntegrityRule(ruleUpdationData)
        );
        console.log(query);
        this.loaderService.hideLoader();
        if (response.status == 200) {
          // calling toast
          // console.log(successMessage)
          this.addApiStatusEmitter.emit({
            flag: 'suss',
            message: successMessage,
          });
          this.getResponseHandler();
        } else {
          //calling toast
          // console.log(errMessages)
          this.addApiStatusEmitter.emit({ flag: 'wrn', message: errMessages });
        }
      } else {
        // duplicate error throwing
        this.addApiStatusEmitter.emit({
          flag: 'wrn',
          message: 'This Rule Already Inserted.Try Someother Rule',
        });
      }
    }
  }
  async deletePopupDeleteHandler() {
    this.deletePopupModel.displayModal = false;
    this.loaderService.showLoader();
    let ruleType = replaceNumberWithEmptyString(this.deleteRuleId)
    const ruleData = {
      "identifier": ruleType,
      "ruleId": this.deleteRuleId
    }
    let response = await lastValueFrom(
      this.dataQualityApiService.deleteRule(ruleData)
    );
    let successMessage = 'Rule Deleted Successfully';
    let errMessages = 'Something went wrong. Please try again';
    if (response.status == 200) {
      // calling toast
      console.log(successMessage);
      this.addApiStatusEmitter.emit({ flag: 'err', message: successMessage });
      this.resetAll();
      this.getResponseHandler();
    } else {
      //calling toast
      console.log(errMessages);
      this.addApiStatusEmitter.emit({ flag: 'wrn', message: errMessages });
    }
    this.loaderService.hideLoader();
  }

  exportExcel() {
    this.excelservice.exportExcel(
      'Refrential-Integrity-Rules.xlsx',
      this.table_data_orig
    );
  }
}
