import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferentialIntegrityComponent } from './referential-integrity.component';

describe('ReferentialIntegrityComponent', () => {
  let component: ReferentialIntegrityComponent;
  let fixture: ComponentFixture<ReferentialIntegrityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReferentialIntegrityComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReferentialIntegrityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
