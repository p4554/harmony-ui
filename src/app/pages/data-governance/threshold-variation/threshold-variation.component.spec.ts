import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThresholdVariationComponent } from './threshold-variation.component';

describe('ThresholdVariationComponent', () => {
  let component: ThresholdVariationComponent;
  let fixture: ComponentFixture<ThresholdVariationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThresholdVariationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ThresholdVariationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
