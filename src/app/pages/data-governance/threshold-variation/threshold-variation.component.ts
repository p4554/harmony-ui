import { query } from '@angular/animations';
import {
  AfterViewInit,
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { PrimeMultiSelectComponent } from '@app/components/form-components/prime-multi-select/prime-multi-select.component';
import {
  addDuplicateRecordChecker,
  editDuplicateRecordChecker,
  filterIncludedValuesFromArray,
  getMappedArray,
  getUniqueValues,
} from '@app/util/helper/arrayHandlers';
import {
  editGroupByValueDuplicateChecker,
  groupByArrayToValueConverter,
  replaceNumberWithEmptyString,
  replaceParamsinJSON,
  replaceWordsWithEmptyString,
} from '@app/util/helper/harmonyUtils';
import { CommonDataService } from '@app/_services/common-data.service';
import { ExcelService } from '@app/_services/excel.service';
import { LoaderService } from '@app/_services/loading.service';
import { QueryService } from '@app/_services/query.service';
import { format } from 'date-fns';
import { lastValueFrom, map } from 'rxjs';
import { ThresholdVarientPopupComponent } from '@app/components/dg-components/threshold-varient-popup/threshold-varient-popup.component';
import { tableFilter } from '@app/util/helper/harmonyUtils';
import { DataQualityApiService } from '@app/_services/dataQuality-api.service';

export interface thresHoldTableColumns {
  dataZone: string;
  dataProvider: string;
  dataSetName: string;
  function: string;
  metricName: string;
  dqSeverity: string;
  thresholdValue: string;
  groupBy: string;
  filterClause: string;
  status: string;
}
@Component({
  selector: 'app-threshold-variation',
  templateUrl: './threshold-variation.component.html',
  styleUrls: ['./threshold-variation.component.scss'],
})
export class ThresholdVariationComponent implements OnInit, AfterViewInit {
  @ViewChild('dataZone') dataZone: PrimeMultiSelectComponent;
  @ViewChild('dataProvider') dataProvider: PrimeMultiSelectComponent;
  @ViewChild('dataSetName') dataSetName: PrimeMultiSelectComponent;
  @ViewChild('function') function: PrimeMultiSelectComponent;
  @ViewChild('metricName') metricName: PrimeMultiSelectComponent;
  @ViewChild('dqSeverity') dqSeverity: PrimeMultiSelectComponent;
  @ViewChild('thresholdValue') thresholdValue: PrimeMultiSelectComponent;
  @ViewChild('groupBy') groupBy: PrimeMultiSelectComponent;
  @ViewChild('filterClause') filterClause: PrimeMultiSelectComponent;
  @ViewChild('status') status: PrimeMultiSelectComponent;
  @ViewChild('popup') popup: ThresholdVarientPopupComponent;
  @Output() addApiStatusEmitter = new EventEmitter();
  @Output() record_count = new EventEmitter();

  buttonProps = {
    label: 'Export',
    click: '',
  };
  popupModelData: any;

  latestRuleId: any;
  table_data_orig: any[] = [];
  table_data_copy: any[] = [];

  isEdit: boolean = false;
  editedRuleId: string;
  filterComponents: { selected: any; component: any }[];
  deleteRuleId: any;
  deletePopupModel: {
    displayModal: boolean;
    header: string;
    icon: string;
    content: { header: string; text: string };
    deleteBtnProps: { label: string; click: string };
    cancelBtnProps: { label: string; click: string };
  };
  constructor(
    private queryService: QueryService,
    private commonDataService: CommonDataService,
    public loaderService: LoaderService,
    private excelservice: ExcelService,
    private dataQualityApiService: DataQualityApiService,
  ) { }
  activeFiltered = {
    dataZone: { valArr: [], column: 'DATA_ZONE' },
    dataProvider: { valArr: [], column: 'DATA_PROVIDER' },
    dataSetName: { valArr: [], column: 'DATASET_NAME' },
    function: { valArr: [], column: 'FUNCTION' },
    metricName: { valArr: [], column: 'METRIC_NAME' },
    dqSeverity: { valArr: [], column: 'RULE_SEVERITY' },
    thresholdValue: { valArr: [], column: 'THRESHOLD_VALUE' },
    groupBy: { valArr: [], column: 'GROUP_BY' },
    filterClause: { valArr: [], column: 'FILTER_CLAUSE' },
    status: { valArr: [], column: 'STATUS' },
  };
  COLUMNS: thresHoldTableColumns = {
    dataZone: 'DATA_ZONE',
    dataProvider: 'DATA_PROVIDER',
    dataSetName: 'DATASET_NAME',
    function: 'FUNCTION',
    metricName: 'METRIC_NAME',
    dqSeverity: 'RULE_SEVERITY',
    thresholdValue: 'THRESHOLD_VALUE',
    groupBy: 'GROUP_BY',
    filterClause: 'FILTER_CLAUSE',
    status: 'STATUS',
  };
  groupByOptions: any[] = [];
  columnsArray = [
    this.COLUMNS.dataZone,
    this.COLUMNS.dataProvider,
    this.COLUMNS.dataSetName,
    this.COLUMNS.function,
    this.COLUMNS.metricName,
    this.COLUMNS.dqSeverity,
    this.COLUMNS.thresholdValue,
    this.COLUMNS.filterClause,
    this.COLUMNS.groupBy,
    this.COLUMNS.status,
  ];
  tablePageNumber = 0;
  uniqueValues: any;
  // statusOptions = [{label:"Active",value:'Active'},{label:"Inactive",value:'Inactive'}]
  ngOnInit() {
    // this.loaderService.showLoader()
    this.getResponseHandler();
    this.popupModelData = {
      displayModal: false,
      header: 'Threshold Variation Add Rule',
      saveBtnProps: {
        label: 'Save',
        click: '',
      },
      cancelBtnProps: {
        label: 'Cancel',
        click: '',
      },
      editValue: '',
    };
  }

  async getResponseHandler() {
    this.table_data_orig = await lastValueFrom(
      this.dataQualityApiService.getThVariationRules().pipe(
        map((array: any) => {
          return array.map((item) => ({
            ...item,
            SWITCH_STATUS: item.STATUS === 'Active' ? true : false,
          }));
        })
      )
    );

    // this.table_data_copy = [...this.table_data_orig];

    this.table_data_copy = tableFilter(
      this.activeFiltered,
      this.table_data_orig
    );
    this.latestRuleId = this.table_data_copy[0].RULE_ID;
    console.log(this.table_data_copy);
    this.loaderService.hideLoader();
    this.uniqueValues = this.getDistinctValuesForColumns(
      this.table_data_orig,
      this.columnsArray
    );
    // console.log(this.uniqueValues)

    this.record_count.emit(this.table_data_copy.length);
  }
  ngAfterViewInit() {
    this.filterComponents = [
      { selected: this.COLUMNS.dataZone, component: this.dataZone },
      { selected: this.COLUMNS.dataProvider, component: this.dataProvider },
      { selected: this.COLUMNS.dataSetName, component: this.dataSetName },
      { selected: this.COLUMNS.function, component: this.function },
      { selected: this.COLUMNS.metricName, component: this.metricName },
      { selected: this.COLUMNS.dqSeverity, component: this.dqSeverity },
      { selected: this.COLUMNS.thresholdValue, component: this.thresholdValue },
      { selected: this.COLUMNS.groupBy, component: this.groupBy },
      { selected: this.COLUMNS.filterClause, component: this.filterClause },
      { selected: this.COLUMNS.status, component: this.status },
    ];
  }
  multiselectOnchange(filteredArr, column) {
    let tempFilterArray = filteredArr;
    if (column === 'DATA_ZONE') {
      this.activeFiltered.dataZone.valArr = tempFilterArray;
    } else if (column === 'DATA_PROVIDER') {
      this.activeFiltered.dataProvider.valArr = tempFilterArray;
    } else if (column === 'DATASET_NAME') {
      this.activeFiltered.dataSetName.valArr = tempFilterArray;
    } else if (column === 'FUNCTION') {
      this.activeFiltered.function.valArr = tempFilterArray;
    } else if (column === 'METRIC_NAME') {
      this.activeFiltered.metricName.valArr = tempFilterArray;
    } else if (column === 'RULE_SEVERITY') {
      this.activeFiltered.dqSeverity.valArr = tempFilterArray;
    } else if (column === 'THRESHOLD_VALUE') {
      this.activeFiltered.thresholdValue.valArr = tempFilterArray;
    } else if (column === 'GROUP_BY') {
      this.activeFiltered.groupBy.valArr = tempFilterArray;
    } else if (column === 'FILTER_CLAUSE') {
      this.activeFiltered.filterClause.valArr = tempFilterArray;
    } else if (column === 'STATUS') {
      this.activeFiltered.status.valArr = tempFilterArray;
    }

    console.log(this.activeFiltered);
    const result = tableFilter(this.activeFiltered, this.table_data_orig);
    if (result.length) {
      console.log(result);
      this.table_data_copy = result;
    } else {
      if (
        !Object.keys(this.activeFiltered).some(
          (field) => this.activeFiltered[field].valArr.length
        )
      ) {
        this.table_data_copy = this.table_data_orig;
      } else {
        this.table_data_copy = result;
      }
    }
    this.setFilterOptions(this.activeFiltered, result);
    this.record_count.emit(this.table_data_copy.length);
    this.tablePageNumber = 0;
  }
  setFilterOptions(fieldObject, result) {
    const filterFields = Object.keys(fieldObject);

    for (let field of filterFields) {
      if (!fieldObject[field].valArr.length) {
        this.uniqueValues[fieldObject[field].column] = getMappedArray(
          getUniqueValues(result, fieldObject[field].column)
        );
      }
    }
  }
  activatedFilterReset(fieldObject: any) {
    const filterFields = Object.keys(fieldObject);
    filterFields.forEach((key) => (fieldObject[key].valArr = []));
  }
  // resetBy(param) {
  //   // console.log(param)
  //   this.filterComponents.forEach((item) => {
  //     if (item.selected !== param) {
  //       // console.log(item.component)
  //       item.component.resetSelection();
  //     }
  //   });
  //   this.record_count.emit(this.table_data_copy.length)
  // }
  resetAll() {
    this.filterComponents.forEach((item) => {
      item.component.resetSelection();
    });
    this.activatedFilterReset(this.activeFiltered);

    this.table_data_copy = this.table_data_orig;

    this.record_count.emit(this.table_data_copy.length);
    this.uniqueValues = this.getDistinctValuesForColumns(
      this.table_data_copy,
      this.columnsArray
    );
  }
  editBtnHandler(row) {
    this.popupModelData.header = 'Threshold Variation Edit Rule';
    this.popupModelData.editValue = row;
    this.popupModelData.displayModal = true;
    // console.log(row)
    this.isEdit = true;
    this.editedRuleId = row.RULE_ID;
  }
  deleteBtnHandler(row) {
    // console.log(row)
    this.deletePopupModel = {
      displayModal: true,
      header: 'Delete Rule',
      icon: 'pi-exclamation-triangle',
      content: {
        header: `Are you sure to delete the following rule ?`,
        text: 'This action will delete the rule permanently',
      },
      deleteBtnProps: {
        label: 'Delete',
        click: '',
      },
      cancelBtnProps: {
        label: 'Cancel',
        click: '',
      },
    };
    this.deleteRuleId = row.RULE_ID;
  }

  // delete api handler
  async deletePopupDeleteHandler() {
    let ruleType = replaceNumberWithEmptyString(this.deleteRuleId)
    ruleType = (ruleType === 'THV') ? 'TH' : ruleType;

    const ruleData = {
      "identifier": ruleType,
      "ruleId": this.deleteRuleId
    }
    this.deletePopupModel.displayModal = false;
    this.loaderService.showLoader();
    let response: any = await lastValueFrom(
      this.dataQualityApiService.deleteRule(ruleData)
    );
    let successMessage = 'Rule Deleted Successfully';
    let errMessages = 'Something went wrong. Please try again';
    if (response.status == 200) {
      // calling toast
      // console.log(successMessage)
      this.addApiStatusEmitter.emit({ flag: 'err', message: successMessage });
      this.resetAll();
      this.getResponseHandler();
    } else {
      //calling toast
      // console.log(errMessages)
      this.addApiStatusEmitter.emit({ flag: 'wrn', message: errMessages });
    }
    this.loaderService.hideLoader();
  }
  handleNewRuleClick() {
    this.popupModelData.displayModal = true;
    this.popupModelData.editValue = '';
    this.popupModelData.header = 'Threshold Variation Add Rule';
    this.isEdit = false;
  }
  // hint popup submit handler
  async PopupSubmitHandler(formValue) {
    const formattedDate = format(new Date(), 'yyyy-MM-dd');
    const userId = localStorage.getItem('userId');
    let status = formValue.status.value === 'Disabled' ? false : true;

    let RuleId =
      replaceNumberWithEmptyString(this.latestRuleId) +
      (Number(replaceWordsWithEmptyString(this.latestRuleId)) + 1);
    console.log('latestRuleId', this.latestRuleId);
    console.log('RuleId', RuleId);
    let groupByValue = groupByArrayToValueConverter(formValue.groupBy);
    // console.log(groupByValue)
    let successMessage = '';
    let errMessages = 'Something went wrong. Please try again';
    let query = {};
    // console.log(formValue)
    // console.log(status)
    const formProperty = [
      'dataZone',
      'dataProvider',
      'datasetName',
      'metricName',
      'function',
    ];
    const tableProperty = [
      'DATA_ZONE',
      'DATA_PROVIDER',
      'DATASET_NAME',
      'METRIC_NAME',
      'FUNCTION',
    ];
    if (!this.isEdit) {
      const duplicateFound = addDuplicateRecordChecker(
        formValue,
        formProperty,
        this.table_data_orig,
        tableProperty
      );
      if (
        !duplicateFound ||
        !this.table_data_orig.some((obj) => obj.GROUP_BY_ID === groupByValue)
      ) {
        // insert 
        const insertDate = {
          "rule_id": RuleId,
          "dataset_value": formValue.datasetName.value,
          "group_by_value": groupByValue,
          "metric_value": formValue.metricName.value,
          "function_label": formValue.function.label,
          "function_value": formValue.function.value,
          "threshold_value": formValue.thresholdValue,
          "dq_severity": formValue.dqSeverity.label,
          "filter_clause": formValue.filterClause,
          "user_id": userId,
          "status": status
        }
        successMessage = 'Rule Inserted Successfully';
        this.popup.formGroup.reset();
        this.popupModelData.displayModal = false;
        this.loaderService.showLoader();
        let response: any = await lastValueFrom(
          this.dataQualityApiService.createThresholdVariationRule(insertDate)
        );
        this.loaderService.hideLoader();
        // console.log(response)
        if (response.status == 200) {
          // calling toast
          // console.log(successMessage)
          this.addApiStatusEmitter.emit({
            flag: 'suss',
            message: successMessage,
          });
          this.resetAll();
          this.getResponseHandler();
        } else {
          //calling toast
          // console.log(errMessages)
          this.addApiStatusEmitter.emit({ flag: 'wrn', message: errMessages });
        }
      } else {
        this.addApiStatusEmitter.emit({
          flag: 'wrn',
          message: 'This Rule Already Inserted.Try Someother Rule',
        });
      }
    } else {
      const duplicateFound = editDuplicateRecordChecker(
        formValue,
        formProperty,
        this.table_data_orig,
        tableProperty,
        this.editedRuleId
      );

      const groupByDuplicationFlag = editGroupByValueDuplicateChecker(
        groupByValue,
        this.table_data_orig,
        'GROUP_BY_ID',
        this.editedRuleId
      );

      console.log(groupByDuplicationFlag);
      if (!duplicateFound || groupByDuplicationFlag) {
        // //update
        // query = {
        //   query: `UPDATE HRMNY_DB.HRMNY_CNF_ZN.DQ_TH_CONFIG set ENT_ID='${formValue.datasetName.value}',MSTR_RULE_ID=${formValue.function.value}, COL_ID=${formValue.metricName.value}, BK_COL_ID='${groupByValue}', AGG_FN='${formValue.function.label}', TH_VAL='${formValue.thresholdValue}', RULE_SVRT='${formValue.dqSeverity.label}', FLTR_CON='${formValue.filterClause}', IS_RULE_ACTV='${status}', AUDIT_UPDT_DT='${formattedDate}', AUDIT_UPDT_ID=${userId} where RULE_ID='${this.editedRuleId}'`,
        //   flag: 'u',
        // };

        const updateDate = {
          "rule_id": this.editedRuleId,
          "dataset_value": formValue.datasetName.value,
          "group_by_value": groupByValue,
          "metric_value": formValue.metricName.value,
          "function_label": formValue.function.label,
          "function_value": formValue.function.value,
          "threshold_value": formValue.thresholdValue,
          "dq_severity": formValue.dqSeverity.label,
          "filter_clause": formValue.filterClause,
          "user_id": userId,
          "status": status
        }
        successMessage = 'Changes Updated Successfully';
        this.popup.formGroup.reset();
        this.popupModelData.displayModal = false;
        this.loaderService.showLoader();
        let response: any = await lastValueFrom(
          this.dataQualityApiService.updateThresholdVariationRule(updateDate)
        );
        this.loaderService.hideLoader();
        // console.log(response)
        if (response.status == 200) {
          // calling toast
          // console.log(successMessage)
          this.addApiStatusEmitter.emit({
            flag: 'suss',
            message: successMessage,
          });
          this.getResponseHandler();
        } else {
          //calling toast
          // console.log(errMessages)
          this.addApiStatusEmitter.emit({ flag: 'wrn', message: errMessages });
        }
      } else {
        this.addApiStatusEmitter.emit({
          flag: 'wrn',
          message: 'This Rule Already Inserted.Try Someother Rule',
        });
      }
    }
  }
  async inputSwitchOnchange(row) {
    let ruleType = replaceNumberWithEmptyString(row.RULE_ID)
    ruleType = (ruleType === 'THV') ? 'TH' : ruleType;

    const postData = {
      "status": row.SWITCH_STATUS,
      "identifier": ruleType,
      "ruleId": row.RULE_ID
    }
    this.loaderService.showLoader();
    let response: any = await lastValueFrom(this.dataQualityApiService.updateRuleStatus(postData))
    let key = '';
    let message = '';
    // console.log(response)

    if (response.status === 200) {
      key = 'suss';
      message = 'Status Updated Successfully';
      this.getResponseHandler();
    } else {
      key = 'wrn';
      message = 'Something went wrong. Please try again.';
    }
    this.addApiStatusEmitter.emit({ flag: key, message: message });
    this.loaderService.hideLoader();
  }
  getDistinctValuesForColumns(tableData: any[], columns: string[]) {
    const result: any = [];

    columns.forEach((column) => {
      const uniqueValues = getUniqueValues(tableData, column);

      const mappedArray = getMappedArray(uniqueValues);
      result[column] = [...mappedArray];
    });

    return result;
  }

  exportExcel() {
    this.excelservice.exportExcel(
      'Threshold-Variation-Rules.xlsx',
      this.table_data_orig
    );
  }
}
