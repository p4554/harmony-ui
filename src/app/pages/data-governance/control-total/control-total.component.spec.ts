import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlTotalComponent } from './control-total.component';

describe('ControlTotalComponent', () => {
  let component: ControlTotalComponent;
  let fixture: ComponentFixture<ControlTotalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ControlTotalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ControlTotalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
