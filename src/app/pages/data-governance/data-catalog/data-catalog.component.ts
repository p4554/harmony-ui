import {
	AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { replacePlaceholders } from '@app/util/helper/patterns';
import { CommonDataService } from '@app/_services/common-data.service';
import { LoaderService } from '@app/_services/loading.service';
import { QueryService } from '@app/_services/query.service';
import { lastValueFrom, map } from 'rxjs';
import { query } from '@angular/animations';
import { replaceParamsinJSON, tableFilter } from '@app/util/helper/harmonyUtils';
import { format, parse } from 'date-fns';
import { DatePipe } from '@angular/common';
import { PrimeMultiSelectComponent } from '@app/components/form-components/prime-multi-select/prime-multi-select.component';
import { getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-data-catalog',
  templateUrl: './data-catalog.component.html',
  styleUrls: ['./data-catalog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [MessageService]
})
export class DataCatalogComponent implements OnInit , AfterViewInit{
  @ViewChild('name') name: PrimeMultiSelectComponent;
  @ViewChild('owner') owner: PrimeMultiSelectComponent;
  @ViewChild('tag') tag: PrimeMultiSelectComponent;
  @ViewChild('dataZone') dataZone: PrimeMultiSelectComponent;
  @ViewChild('type') type: PrimeMultiSelectComponent;
  @ViewChild('addedDate') addedDate: PrimeMultiSelectComponent;

  activeFiltered = {
    name: { valArr: [], column: 'NAME' },
    owner: { valArr: [], column: 'OWNER' },
    tag: { valArr: [], column: 'TAGS' },
    dataZone: { valArr: [], column: 'DATAZONE' },
    type: { valArr: [], column: 'TYPE' },
    addedDate: { valArr: [], column: 'DATE_ADDED' },
  };
  COLUMNS = {
    name: 'NAME',
    owner: 'OWNER',
    tag: 'TAGS',
    dataZone: 'DATAZONE',
    type: 'TYPE',
    addedDate: 'DATE_ADDED',
  };
  groupByOptions: any[] = [];
  columnsArray = [
    this.COLUMNS.name,
    this.COLUMNS.owner,
    this.COLUMNS.tag,
    this.COLUMNS.dataZone,
    this.COLUMNS.type,
    this.COLUMNS.addedDate,
  ];
  tablePageNumber:any = 0;
  form: FormGroup;
  dataZoneOptions: any;
  dataProviderOptions: any;
  datasetNameOptions: any;
  searchCheckBox = {
    tag: false,
    description: false,
    field : false,
    entity : false 
  }
  tab_label = 'Data Catalog';
  isLoaded = true;
  autocompleteResults = [];
  autocompleteText: any = '';
  table_data_orig: any;
  table_data_copy: any[];
  tagInput = '';
  uniqueValues: any;
  tabs = ['ATRRIBUTE LEVEL', 'ENTITY LEVEL'];
  currentTab: string = 'ATRRIBUTE LEVEL';

  openAdvanceSearch = false;

  tagPopupFlag = false;
  notifyPopupFlag = false;
  currentCatalog:any = '';
  filterComponents: {
    selected: string;
    component: PrimeMultiSelectComponent;
  }[];
  databaseOptions = [
    { label: 'Database', value: 'database' },
    { label: 'S3', value: 's3' },
    { label: 'Local', value: 'local' },
  ];
  cancelBtnProps = {
    label: "Cancel",
    click: "",
  }
  saveBtnProps = {
    label: "Add",
    click : ""
  }

 menuItems = [
	{
		label: 'Data Profile',
		command: () => {
			console.log('Data Profile')
		}
	},
	{
		label: 'Add Tag',
		command: () => {
			console.log('Add Tag')
      this.tagPopupFlag = true;
		}
	},
	{
		label: 'Notify on Refresh',
		command: (e) => {
			console.log('Notify on Refresh',e)
      this.notifyPopupFlag = true;
		}
	}
];
  loader: boolean;


  constructor(
    private fb: FormBuilder,
    private queryService: QueryService,
    private commonDataService: CommonDataService,
    public loaderService: LoaderService,
    private _cdr: ChangeDetectorRef,
    private toastService: MessageService,
  ) {
    this.getDataSet();
    this.getResponseHandler();

    //this.getDatasetName();
    this.form = this.fb.group({
      database: ['', Validators.required],
      data_zone: [''],
      data_provider: [''],
      dataset_name: [''],
    });
  }
  ngOnInit():void{
  }
  ngAfterViewInit() {
    this.filterComponents = [
      { selected: this.COLUMNS.name, component: this.name },
      { selected: this.COLUMNS.owner, component: this.owner },
      { selected: this.COLUMNS.tag, component: this.tag },
      { selected: this.COLUMNS.dataZone, component: this.dataZone },
      { selected: this.COLUMNS.type, component: this.type },
      { selected: this.COLUMNS.addedDate, component: this.addedDate },
    ];
    console.log('this.name',this.name)
  }
  multiselectOnchange(filteredArr, column) {
    let tempFilterArray = filteredArr;
    if (column === 'NAME') {
      this.activeFiltered.name.valArr = tempFilterArray;
    } else if (column === 'OWNER') {
      this.activeFiltered.owner.valArr = tempFilterArray;
    } else if (column === 'TAGS') {
      this.activeFiltered.tag.valArr = tempFilterArray;
    } else if (column === 'DATAZONE') {
      this.activeFiltered.dataZone.valArr = tempFilterArray;
    } else if (column === 'TYPE') {
      this.activeFiltered.type.valArr = tempFilterArray;
    } else if (column === 'DATE_ADDED') {
      this.activeFiltered.addedDate.valArr = tempFilterArray;
    }
    console.log(this.activeFiltered);
    const result = tableFilter(this.activeFiltered, this.table_data_orig);
    if (result.length) {
      console.log(result);
      this.table_data_copy = result;
    } else {
      if (
        !Object.keys(this.activeFiltered).some(
          (field) => this.activeFiltered[field].valArr.length
        )
      ) {
        this.table_data_copy = this.table_data_orig;
      } else {
        this.table_data_copy = result;
      }
    }
    this.setFilterOptions(this.activeFiltered, result);
    // this.record_count.emit(this.table_data_copy.length);
    this.tablePageNumber = 0;
  }
  setFilterOptions(fieldObject, result) {
    const filterFields = Object.keys(fieldObject);

    for (let field of filterFields) {
      if (!fieldObject[field].valArr.length) {
        this.uniqueValues[fieldObject[field].column] = getMappedArray(
          getUniqueValues(result, fieldObject[field].column)
        );
      }
    }
  }
  activatedFilterReset(fieldObject: any) {
    const filterFields = Object.keys(fieldObject);
    filterFields.forEach((key) => (fieldObject[key].valArr = []));
  }
  resetAll() {
    this.filterComponents.forEach((item) => {
      console.log('item',item)
      item.component.resetSelection();
    });
    this.activatedFilterReset(this.activeFiltered);
    this.table_data_copy = this.table_data_orig;
    this.uniqueValues = this.getDistinctValuesForColumns(
      this.table_data_copy,
      this.columnsArray
    );
  }
  getDistinctValuesForColumns(tableData:any[],columns:string[]){

    const result: any = [];

    columns.forEach((column) => {
      const uniqueValues = getUniqueValues(tableData, column);
      
      const mappedArray = getMappedArray(uniqueValues);
      result[column] = [...mappedArray];
    });

    return result;
  }
  // this.getDataProvider();
  async getDataSet() {
    const query = await lastValueFrom(
      this.queryService.fetchQuery('data_profile_data_zone')
    );

    this.dataZoneOptions = await lastValueFrom(
      this.commonDataService.getSourceConnectorTableData(query).pipe(
        map((array) => {
          return array.map((item) => ({
            label: item.DATA_ZONE_NM,
            value: item.DATA_ZONE_ID,
          }));
        })
      )
    );
	this.dataZoneOptions = this.dataZoneOptions.sort((a,b)=> a.label.localeCompare(b.label))
    this._cdr.detectChanges();
  }

  getControlByName(controlName: string) {
    return this.form.get(controlName);
  }

  changeEventHandler(param) {
    if (param === 'datazone') {
      let paramArray: string[] = [this.form.value.data_zone];
      console.log(paramArray);
      // this.getDataProvider(paramArray);
    } else if (param === 'dataprovider') {
      let paramArray: string[] = [
        this.form.value.data_provider,
        this.form.value.data_zone,
      ];
      // this.getDatasetName(paramArray);
    } else if (param === 'dataname') {
      // let paramArray: string[] = [this.form.value.data_zone, this.form.value.data_name];
      // this.getDatasetName(paramArray);
    }
  }

  submit() {
    this.isLoaded = true;
  }

  setCurrentTab(e) {
    this.currentTab = this.tabs[e.index];
    this._cdr.detectChanges();
    // if( this.currentTab === 'All'){
    //   this.calculateCount(this.table_data_orig)
    // }
  }

  async getResponseHandler() {
    this.loader = true;
    // const formattedDate = format(, 'yyyy-MM-dd');
    console.log();
    // .pipe(
    // 	map((array) => {
    // 	  const datePipe = new DatePipe('en-US');
    // 	  return array.map((item) => ({
    // 		...item,
    // 		date: datePipe.transform(item.DATE_ADDED, 'MM/dd/yyyy')
    // 	  }));
    // 	})
    //   )

    const query = await lastValueFrom(
      this.queryService.fetchQuery('data_catalog_table')
    );
    this.table_data_orig = await lastValueFrom(
      this.commonDataService.getSourceConnectorTableData(query).pipe(
		map((array)=>{
			return array.map((item)=>({
				...item,
				DATE_ADDED: this.convertDate(item.DATE_ADDED)
			}))
		})
	  )
    );
    // this.table_data_copy = [...this.table_data_orig];
	this.table_data_copy = tableFilter(this.activeFiltered,this.table_data_orig)
    this.uniqueValues = this.getDistinctValuesForColumns(this.table_data_copy, this.columnsArray)
	console.log(this.table_data_copy);
  this.loader = false;
  this._cdr.detectChanges();

  }
  convertDate(dateString:string){
	const date = parse(dateString, "EEE, dd MMM yyyy HH:mm:ss 'GMT'", new Date());
	const formattedDate = format(date, 'dd-MM-yyyy'); // Example: "22/12/2020"
	return formattedDate
  }
  advanceSearch() {
    this.openAdvanceSearch = !this.openAdvanceSearch;
  }

  search() {
    // this.getResponseHandler();
  }

  tagRejectHAndler(){
    this.tagPopupFlag = false;
    this.tagInput = '';
  }
  async tagAddHandler(){
    console.log(this.tagInput.trim())
    if(this.tagInput.trim()){
      this.tagPopupFlag = false;
    this.currentCatalog
    const query = {
                       query:`COMMENT ON TABLE ${this.currentCatalog.DATAZONE}.${this.currentCatalog.NAME} IS '${this.tagInput.trim()}'`,
                        flag: "u"
                    }
    this.loader = true;
    const response = await lastValueFrom(this.commonDataService.getSourceConnectorTableData(query))
    if(response.status === 200){
      console.log('Updated Sucessfully');
      this.toastService.add({key:'suss', closable:false, sticky:false,  severity:'', summary:'', detail:"Tag updated sucessfully"});
      this.getResponseHandler()
    }else {
      this.toastService.add({key:'wrn', closable:false, sticky:false,  severity:'', summary:'', detail:"something went wrong try again"});
    }
    }  
  }
  notifyClickHandler(){
    this.notifyPopupFlag = false;
  }
  setCurrentDataCatalouge(data){
    this.currentCatalog = data

  }
}
