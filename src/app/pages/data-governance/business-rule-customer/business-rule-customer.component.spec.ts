import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessRuleCustomerComponent } from './business-rule-customer.component';

describe('BusinessRuleCustomerComponent', () => {
  let component: BusinessRuleCustomerComponent;
  let fixture: ComponentFixture<BusinessRuleCustomerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessRuleCustomerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BusinessRuleCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
