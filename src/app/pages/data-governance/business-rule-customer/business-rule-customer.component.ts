import { Component,ViewChild,Input,Output,EventEmitter} from '@angular/core';
import { QueryService } from '@app/_services/query.service';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import { LoaderService } from '@app/_services/loading.service';
import { BusinessRuleApiService } from '@app/_services/business-rule-api.service';
import { getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { tableFilter } from '@app/util/helper/harmonyUtils';
import { PrimeMultiSelectComponent } from '@app/components/form-components/prime-multi-select/prime-multi-select.component';
import { DialogService  } from 'primeng/dynamicdialog';
import  {EditCustomerRuleComponent} from '../../../components/business-rule-components/edit-customer-rule/edit-customer-rule.component'
import { MessageService } from 'primeng/api';
import { ExcelService } from '@app/_services/excel.service';
@Component({
  selector: 'app-business-rule-customer',
  templateUrl: './business-rule-customer.component.html',
  styleUrls: ['./business-rule-customer.component.scss']
})
export class BusinessRuleCustomerComponent {
  @ViewChild('customerTypeFilter') customerTypeFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('ruleNameFilter') ruleNameFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('customerIdFilter') customerIdFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('businessUnitFilter') businessUnitFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('businessProcessFilters') businessProcessFiltersComponent: PrimeMultiSelectComponent;
  @ViewChild('ruleTypeFilter') ruleTypeFilterComponent: PrimeMultiSelectComponent;  
  @ViewChild('oldValueFilter') oldValueFilterComponent: PrimeMultiSelectComponent; 
  @ViewChild('newValueFilter') newValueFilterComponent: PrimeMultiSelectComponent; 
  @ViewChild('startDateFilter') startDateFilterComponent: PrimeMultiSelectComponent;  
  @ViewChild('endDateFilter') endDateFilterComponent: PrimeMultiSelectComponent;  
 
@Output() ruleCountEmitter = new EventEmitter<number>() 
 @Input() savedData:string;

  tablePageNumber=0;
  tableData:any;
  tableDataCopy:any;
  uniqueValues:any;
  filterComponents: any;
  FormateDatePipe
Column={
   customerType:"CUST_TYP",
   ruleName:"CUST_BR_NM",
   customerId:"CUST_ID",
   businessUnit: "CUST_BU_CD",
   businessProcess: "CUST_BP_CD",
   ruleType: "CUST_BR_TYP",
   oldValue: "CUST_OLD_VAL",
   newValue: "CUST_NEW_VAL",
   startDate:"CUST_EFFCTV_STRT_DT",
   endDate:"CUST_EFFCTV_END_DT"
}
columnArray=[
  this.Column.customerType,
  this.Column.ruleName,
  this.Column.customerId,
  this.Column.businessUnit,
  this.Column.businessProcess,
  this.Column.ruleType,
  this.Column.oldValue,
  this.Column.newValue,
  this.Column.startDate,
  this.Column.endDate,
]
activeFilter={
  customerType: { valArr:[],column:'CUST_TYP'},
  ruleName: { valArr:[],column:'CUST_BR_NM'},
  customerId:{valArr:[],column:'CUST_ID'},
  businessUnit: { valArr:[],column:'CUST_BU_CD'},
  businessProcess:{ valArr:[],column:'CUST_BP_CD'},
  ruleType:{ valArr:[],column:'CUST_BR_TYP'},
  oldValue:{ valArr:[],column:'CUST_OLD_VAL'},
  newValue:{ valArr:[],column:'CUST_NEW_VAL'},
  startDate:{ valArr:[],column:'CUST_EFFCTV_STRT_DT'},
  endDate:{ valArr:[],column:'CUST_EFFCTV_END_DT'}
}
  constructor(
    private BusinessRuleApiService: BusinessRuleApiService,
    public loaderService: LoaderService,
    private queryService: QueryService,
    private dialogService: DialogService,
    private toastService: MessageService,
    private excelService: ExcelService,
  ) { }
  ngOnChanges(changes:any){
    if(this.savedData=='saved'){
     this.getResponseHandler();
    }
  }
  async ngOnInit() {
    this.loaderService.showLoader();
     await this.getResponseHandler();
   }
   async getResponseHandler(){
    this.tableData = await lastValueFrom(
      this.BusinessRuleApiService.getCustomerTable()
    );
    this.tableData.reverse();
    console.log(this.tableData)
    this.tableDataCopy=[...this.tableData]
    this.tablePageNumber = 0
    this.uniqueValues=this.getDistinctValuesForColumns(this.tableData,this.columnArray)
    setTimeout(() => {
     this.filterComponents = [
       { selected: 'customerTypeFilterSelected', component: this.customerTypeFilterComponent },
       { selected: 'ruleNameFilterSelected', component: this.ruleNameFilterComponent },
       { selected: 'customerIdFilterSelected', component: this.customerIdFilterComponent },
       { selected: 'businessUnitFilterSelected', component: this.businessUnitFilterComponent },
       { selected: 'businessProcessFiltersSelected', component: this.businessProcessFiltersComponent },
       { selected: 'ruleTypeFilterSelected', component: this.ruleTypeFilterComponent },
       { selected: 'oldValueFilterSelected', component: this.oldValueFilterComponent },
       { selected: 'newValueFilterSelected', component: this.newValueFilterComponent },
       { selected: 'startDateFilterSelected', component: this.startDateFilterComponent },
       { selected: 'endDateFilterSelected', component: this.endDateFilterComponent },
     ];
   }, 300);
   if (this.activeFilter) {
     this.tableData = tableFilter(this.activeFilter,this.tableDataCopy);
   }
   this.loaderService.hideLoader();
   this.ruleCountEmitter.emit(this.tableData?.length)
   }
   getDistinctValuesForColumns = (tableData: any[], columns: string[]) => {
		const result: any = [];
		columns.forEach((column) => {
		  const uniqueValues = getUniqueValues(tableData, column);
		  const mappedArray = getMappedArray(uniqueValues);
		  result[column] = [...mappedArray];
		});
		return result;
	  };
    filterChangeHandler(filterArray, column) {
      const filterArrayCopy = filterArray;
      if (column === 'CUST_TYP') {
        this.activeFilter.customerType.valArr = filterArrayCopy;
      }
      else if (column === 'CUST_BR_NM') {
        this.activeFilter.ruleName.valArr = filterArrayCopy;
      }
      else if (column === 'CUST_ID') {
        this.activeFilter.customerId.valArr = filterArrayCopy;
      }
      else if (column === 'CUST_BU_CD') {
        this.activeFilter.businessUnit.valArr = filterArrayCopy;
      }
      else if (column === 'CUST_BP_CD') {
        this.activeFilter.businessProcess.valArr = filterArrayCopy;
      }
      else if (column === 'CUST_BR_TYP') {
        this.activeFilter.ruleType.valArr = filterArrayCopy;
      } else if (column === 'CUST_OLD_VAL') {
        this.activeFilter.oldValue.valArr = filterArrayCopy;
      } else if (column === 'CUST_NEW_VAL') {
        this.activeFilter.newValue.valArr = filterArrayCopy;
      }
      else if (column === 'CUST_EFFCTV_STRT_DT') {
        this.activeFilter.startDate.valArr = filterArrayCopy;
      }
      else if (column === 'CUST_EFFCTV_END_DT') {
        this.activeFilter.endDate.valArr = filterArrayCopy;
      }
      const result=tableFilter(this.activeFilter,this.tableDataCopy)
  
      if (result.length){
      this.tableData=result
      this.tablePageNumber = 0
     }
     else{
      if(!Object.keys(this.activeFilter).some(field=>this.activeFilter[field].valArr.length)){
        this.tableData = this.tableDataCopy
      }else {
        this.tableData = result
      }
     }
     this.ruleCountEmitter.emit(this.tableData?.length);
     this.setFilterOptions(this.activeFilter,result)  
    }
    setFilterOptions(fieldObject,result){

      const filterFields = Object.keys(fieldObject)
    
      for(let field of filterFields){
        if(!fieldObject[field].valArr.length){
        this.uniqueValues[fieldObject[field].column] = getMappedArray(getUniqueValues(result, fieldObject[field].column));
        }
      }
      }
      resetAll() {
        this.filterComponents.forEach((item) => {
          item.component.resetSelection();
        });
        this.tableData = this.tableDataCopy;
        this.activatedFilterReset(this.activeFilter)
        this.uniqueValues = this.getDistinctValuesForColumns(this.tableData, this.columnArray);
        this.ruleCountEmitter.emit(this.tableData?.length);
      }
      activatedFilterReset(fieldObject:any){
        const filterFields = Object.keys(fieldObject)
        filterFields.forEach(key=> fieldObject[key].valArr = [])
      }
  
      exportExcel(){
        this.excelService.exportExcel('Customer-Rule.xlsx', this.tableDataCopy);
      }
  editHandler(editData:string){
    this.loaderService.showLoader();

    const dialogRef = this.dialogService.open(EditCustomerRuleComponent, {
      header: 'Edit Customer Rule',
      width: '60%',
      height: '80%',
      data: {
        editData: editData,
        tableData: this.tableDataCopy
      },
    });
    dialogRef.onClose.subscribe(result => {
      if (result === 'saved') {
        this.getResponseHandler();
      }
    });
    window.addEventListener('popstate', () => {
      dialogRef.close();
    });
  }
  deletePopupModel: any;
  tableSelectedRowIndex: number;
  deleteHandler(index: number) {
    this.deletePopupModel = {
      displayModal: true,
      header: 'Delete Customer Rule',
      icon: 'pi-exclamation-triangle',
      content: {
        header: `Delete Customer Rule`,
        text: 'This action will delete the customer permanently',
      },
      deleteBtnProps: {
        label: 'Delete',
        click: '',
      },
      cancelBtnProps: {
        label: 'Cancel',
        click: '',
      },
    };

    this.tableSelectedRowIndex = index;
  }
  deletePopupDeleteHandler = () => {
    let brCustomerId = this.tableData[this.tableSelectedRowIndex].BR_CUST_ID;
    const postData = {
      brCustomerId: brCustomerId
    };
    this.deletePopupModel.displayModal = false;
    this.loaderService.showLoader();
    this.BusinessRuleApiService.deleteCustomer(postData).subscribe((response) => {
      if (response.status == 200) {
        this.toastService.clear();
        this.toastService.add({
          key: 'err',
          closable: false,
          sticky: false,
          severity: '',
          summary: '',
          detail: `Customer Rule Deleted Successfully`,
        });
        this.getResponseHandler();
      } else {
        console.log(response);
        this.toastService.clear();
        this.toastService.add({
          key: 'wrn',
          closable: false,
          sticky: false,
          severity: '',
          summary: '',
          detail: `Connection Error TryAgain`,
        });
      }
    });
  };
}
