import {
	AfterViewInit,
	ChangeDetectionStrategy,
	ChangeDetectorRef,
	Component,
	ElementRef,
	ViewChild,
} from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { replacePlaceholders } from '@app/util/helper/patterns';
import { CommonDataService } from '@app/_services/common-data.service';
import { LoaderService } from '@app/_services/loading.service';
import { QueryService } from '@app/_services/query.service';
import { lastValueFrom, map } from 'rxjs';
import { query } from '@angular/animations';
import { replaceParamsinJSON } from '@app/util/helper/harmonyUtils';

// import { Network } from 'vis-network/esnext';
// import { DataSet } from 'vis-data/esnext';

// import { DataSet } from 'vis-data';

// import {vis} from 'vis-network';

declare var vis: any;

@Component({
	selector: 'app-data-lineage',
	templateUrl: './data-lineage.component.html',
	styleUrls: ['./data-lineage.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DataLineageComponent implements AfterViewInit {
	@ViewChild('mynetwork') mynetwork: ElementRef;

	networkInstance: any;

	form: FormGroup;
	dataZoneOptions: any;
	dataProviderOptions: any[]= [];
	datasetNameOptions: any;

	tab_label = 'Data Lineage' 

	isLoaded = true;

	databaseOptions = [
		{ label: 'Database', value: 'database' },
		{ label: 'S3', value: 's3' },
		{ label: 'Local', value: 'local' },
	];

	tabs = ['ATRRIBUTE LEVEL', 'ENTITY LEVEL'];
	currentTab: string = 'ATRRIBUTE LEVEL';

	data: any;

	constructor(
		private fb: FormBuilder,
		private queryService: QueryService,
		private commonDataService: CommonDataService,
		public loaderService: LoaderService,
		private _cdr: ChangeDetectorRef
	) {
		this.getDataSet();

		//this.getDatasetName();
		this.form = this.fb.group({
			database: ['', Validators.required],
			data_zone: ['', Validators.required],
			data_provider: ['', Validators.required],
			dataset_name: ['', Validators.required],
		});
	}

	ngAfterViewInit() {
		// create an array with nodes
		var nodes = new vis.DataSet([
			{ id: 1, label: 'Node 1' },
			{ id: 2, label: 'Node 2' },
			{ id: 3, label: 'Node 3' },
			{ id: 4, label: 'Node 4' },
			{ id: 5, label: 'Node 5' },
		]);

		// create an array with edges
		var edges = new vis.DataSet([
			{ from: 1, to: 3 },
			{ from: 1, to: 2 },
			{ from: 2, to: 4 },
			{ from: 2, to: 5 },
			{ from: 3, to: 3 },
		]);

		// create a network
		// var container = document.getElementById('mynetwork');
		var container = this.mynetwork.nativeElement;

		var data = {
			nodes: nodes,
			edges: edges,
		};
		var options = {};
		var network = new vis.Network(container, data, options);
	}

	// this.getDataProvider();
	async getDataSet() {
		const query = await lastValueFrom(this.queryService.fetchQuery('data_profile_data_zone'));

		this.dataZoneOptions = await lastValueFrom(
			this.commonDataService.getSourceConnectorTableData(query).pipe(
				map((array) => {
					return array.map((item) => ({
						label: item.DATA_ZONE_NM,
						value: item.DATA_ZONE_ID,
					}));
				})
			)
		);
		this._cdr.detectChanges();
	}

	async getDataProvider(paramArray) {
		const jsonStr = await lastValueFrom(this.queryService.fetchQuery('data_profile_data_provider'));
		const newJSONStr = replaceParamsinJSON(jsonStr, paramArray);
		this.dataProviderOptions = await lastValueFrom(
			this.commonDataService.getSourceConnectorTableData(newJSONStr).pipe(
				map((array) => {
					return array.map((item) => ({
						label: item.DATA_PROVIDER,
						value: item.DATA_PROVIDER,
					}));
				})
			)
		);
		this._cdr.detectChanges();
		console.log(this.dataProviderOptions)
	}

	async getDatasetName(paramArray) {
		const jsonStr = await lastValueFrom(this.queryService.fetchQuery('data_profile_dataset_name'));

		const newJSONStr = replaceParamsinJSON(jsonStr, paramArray);

		this.datasetNameOptions = await lastValueFrom(
			this.commonDataService.getSourceConnectorTableData(newJSONStr).pipe(
				map((array) => {
					return array.map((item) => ({
						label: item.ENT_NM,
						value: item.ENT_ID,
					}));
				})
			)
		);
		this._cdr.detectChanges();
	}

	getControlByName(controlName: string) {
		return this.form.get(controlName);
	}

	changeEventHandler(param) {
		if (param === 'datazone') {
			let paramArray: string[] = [this.form.value.data_zone];
			console.log(paramArray);
			this.getDataProvider(paramArray);
		} else if (param === 'dataprovider') {
			let paramArray: string[] = [this.form.value.data_provider, this.form.value.data_zone];
			this.getDatasetName(paramArray);
		} else if (param === 'dataname') {
			// let paramArray: string[] = [this.form.value.data_zone, this.form.value.data_name];
			// this.getDatasetName(paramArray);
		}
	}

	submit() {
		this.isLoaded = true;
	}

	setCurrentTab(e) {
		this.currentTab = this.tabs[e.index];
		this._cdr.detectChanges();
		// if( this.currentTab === 'All'){
		//   this.calculateCount(this.table_data_orig)
		// }
	}

	getTreeData() {
		var nodes = [
			{ id: 1, label: 'Node 1', title: 'I am node 1!' },
			{ id: 2, label: 'Node 2', title: 'I am node 2!' },
			{ id: 3, label: 'Node 3' },
			{ id: 4, label: 'Node 4' },
			{ id: 5, label: 'Node 5' },
		];

		// create an array with edges
		var edges = [
			{ from: 1, to: 3 },
			{ from: 1, to: 2 },
			{ from: 2, to: 4 },
			{ from: 2, to: 5 },
		];
		var treeData = {
			nodes: nodes,
			edges: edges,
		};
		return treeData;
	}
}
