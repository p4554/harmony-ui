import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlSumComponent } from './control-sum.component';

describe('ControlSumComponent', () => {
  let component: ControlSumComponent;
  let fixture: ComponentFixture<ControlSumComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ControlSumComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ControlSumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
