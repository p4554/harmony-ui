import {
  AfterViewInit,
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { PrimeMultiSelectComponent } from '@app/components/form-components/prime-multi-select/prime-multi-select.component';
import {
  getUniqueValues,
  getMappedArray,
  filterIncludedValuesFromArray,
  addDuplicateRecordChecker,
  editDuplicateRecordChecker,
} from '@app/util/helper/arrayHandlers';
import {
  editGroupByValueDuplicateChecker,
  groupByArrayToValueConverter,
  replaceNumberWithEmptyString,
  replaceParamsinJSON,
  replaceWordsWithEmptyString,
} from '@app/util/helper/harmonyUtils';
import { CommonDataService } from '@app/_services/common-data.service';
import { ExcelService } from '@app/_services/excel.service';
import { LoaderService } from '@app/_services/loading.service';
import { QueryService } from '@app/_services/query.service';
import { format } from 'date-fns';
import { MessageService } from 'primeng/api';
import { lastValueFrom, map } from 'rxjs';
import { tableFilter } from '@app/util/helper/harmonyUtils';
import { DataQualityApiService } from '@app/_services/dataQuality-api.service';

@Component({
  selector: 'app-control-sum',
  templateUrl: './control-sum.component.html',
  styleUrls: ['./control-sum.component.scss'],
})
export class ControlSumComponent implements OnInit, AfterViewInit {
  @ViewChild('T_dataZone') T_dataZone: PrimeMultiSelectComponent;
  @ViewChild('T_dataProvider') T_dataProvider: PrimeMultiSelectComponent;
  @ViewChild('T_dataSetName') T_dataSetName: PrimeMultiSelectComponent;
  @ViewChild('T_metricName') T_metricName: PrimeMultiSelectComponent;
  @ViewChild('T_groupBy') T_groupBy: PrimeMultiSelectComponent;
  @ViewChild('T_filterClause') T_filterClause: PrimeMultiSelectComponent;
  @ViewChild('S_dataZone') S_dataZone: PrimeMultiSelectComponent;
  @ViewChild('S_dataProvider') S_dataProvider: PrimeMultiSelectComponent;
  @ViewChild('S_dataSetName') S_dataSetName: PrimeMultiSelectComponent;
  @ViewChild('S_metricName') S_metricName: PrimeMultiSelectComponent;
  @ViewChild('S_groupBy') S_groupBy: PrimeMultiSelectComponent;
  @ViewChild('S_filterClause') S_filterClause: PrimeMultiSelectComponent;
  @ViewChild('dqSeverity') dqSeverity: PrimeMultiSelectComponent;
  @ViewChild('status') status: PrimeMultiSelectComponent;
  @Output() record_count = new EventEmitter();

  @Output() addApiStatusEmitter = new EventEmitter();
  buttonPorps = {
    label: 'Export',
    click: '',
  };
  COLUMNS = {
    T_dataZone: 'TARGET_DATA_ZONE',
    T_dataProvider: 'TARGET_DATA_PROVIDER',
    T_dataSetName: 'TARGET_DATASET_NAME',
    T_metricName: 'TARGET_METRIC_NAME',
    T_groupBy: 'TARGET_GROUP_BY',
    T_filterClause: 'TARGET_FILTER',
    S_dataZone: 'SOURCE_DATA_ZONE',
    S_dataProvider: 'SOURCE_DATA_PROVIDER',
    S_dataSetName: 'SOURCE_DATASET_NAME',
    S_metricName: 'SOURCE_METRIC_NAME',
    S_groupBy: 'SOURCE_GROUP_BY',
    S_filterClause: 'SOURCE_FILTER',
    dqSeverity: 'RULE_SEVERITY',
    status: 'STATUS',
  };
  activeFiltered = {
    T_dataZone: { valArr: [], column: 'TARGET_DATA_ZONE' },
    T_dataProvider: { valArr: [], column: 'TARGET_DATA_PROVIDER' },
    T_dataSetName: { valArr: [], column: 'TARGET_DATASET_NAME' },
    T_metricName: { valArr: [], column: 'TARGET_METRIC_NAME' },
    T_groupBy: { valArr: [], column: 'TARGET_GROUP_BY' },
    T_filterClause: { valArr: [], column: 'TARGET_FILTER' },
    S_dataZone: { valArr: [], column: 'SOURCE_DATA_ZONE' },
    S_dataProvider: { valArr: [], column: 'SOURCE_DATA_PROVIDER' },
    S_dataSetName: { valArr: [], column: 'SOURCE_DATASET_NAME' },
    S_metricName: { valArr: [], column: 'SOURCE_METRIC_NAME' },
    S_groupBy: { valArr: [], column: 'SOURCE_GROUP_BY' },
    S_filterClause: { valArr: [], column: 'SOURCE_FILTER' },
    dqSeverity: { valArr: [], column: 'RULE_SEVERITY' },
    status: { valArr: [], column: 'STATUS' },
  };

  columnsArray = [
    this.COLUMNS.T_dataZone,
    this.COLUMNS.T_dataProvider,
    this.COLUMNS.T_dataSetName,
    this.COLUMNS.T_metricName,
    this.COLUMNS.T_groupBy,
    this.COLUMNS.T_filterClause,
    this.COLUMNS.S_dataZone,
    this.COLUMNS.S_dataProvider,
    this.COLUMNS.S_dataSetName,
    this.COLUMNS.S_metricName,
    this.COLUMNS.S_groupBy,
    this.COLUMNS.S_filterClause,
    this.COLUMNS.dqSeverity,
    this.COLUMNS.status,
  ];

  table_data_orig: any;
  table_data_copy: any[];
  uniqueValues: any;
  filterComponents: any;
  popupModelData: {
    displayModal: boolean;
    header: string;
    saveBtnProps: { label: string; click: string };
    cancelBtnProps: { label: string; click: string };
    editValue: string;
  };
  deletePopupModel: {
    displayModal: boolean;
    header: string;
    icon: string;
    content: { header: string; text: string };
    deleteBtnProps: { label: string; click: string };
    cancelBtnProps: { label: string; click: string };
  };
  currentDeleteRuleId: any;
  isEdit: boolean;
  latestRuleId: any;
  editedRuleId: any;
  tablePageNumber = 0;
  constructor(
    private queryService: QueryService,
    private commonDataService: CommonDataService,
    public loaderService: LoaderService,
    private toastService: MessageService,
    private excelservice: ExcelService,
    private dataQualityApiService: DataQualityApiService
  ) { }
  ngOnInit() {
    this.getResponseHandler();
    this.popupModelData = {
      displayModal: false,
      header: 'Control Sum New Rule',
      saveBtnProps: {
        label: 'Save',
        click: '',
      },
      cancelBtnProps: {
        label: 'Cancel',
        click: '',
      },
      editValue: '',
    };
  }
  ngAfterViewInit() {
    this.filterComponents = [
      { selected: this.COLUMNS.T_dataZone, component: this.T_dataZone },
      { selected: this.COLUMNS.T_dataProvider, component: this.T_dataProvider },
      { selected: this.COLUMNS.T_dataSetName, component: this.T_dataSetName },
      { selected: this.COLUMNS.T_metricName, component: this.T_metricName },
      { selected: this.COLUMNS.T_groupBy, component: this.T_groupBy },
      { selected: this.COLUMNS.T_filterClause, component: this.T_filterClause },
      { selected: this.COLUMNS.S_dataZone, component: this.S_dataZone },
      { selected: this.COLUMNS.S_dataProvider, component: this.S_dataProvider },
      { selected: this.COLUMNS.S_dataSetName, component: this.S_dataSetName },
      { selected: this.COLUMNS.S_metricName, component: this.S_metricName },
      { selected: this.COLUMNS.S_groupBy, component: this.S_groupBy },
      { selected: this.COLUMNS.S_filterClause, component: this.S_filterClause },
      { selected: this.COLUMNS.dqSeverity, component: this.dqSeverity },
      { selected: this.COLUMNS.status, component: this.status },
    ];
  }
  async getResponseHandler() {
    this.table_data_orig = await lastValueFrom(
      this.dataQualityApiService.getControlSumRules().pipe(
        map((array: any) => {
          return array.map((item) => ({
            ...item,
            SWITCH_STATUS: item.STATUS === 'Active' ? true : false,
          }));
        })
      )
    );

    // this.table_data_copy = [...this.table_data_orig];
    this.table_data_copy = tableFilter(
      this.activeFiltered,
      this.table_data_orig
    );
    console.log(tableFilter(this.activeFiltered, this.table_data_orig));
    this.loaderService.hideLoader();
    this.uniqueValues = this.getDistinctValuesForColumns(
      this.table_data_orig,
      this.columnsArray
    );
    // console.log(this.uniqueValues)
    this.latestRuleId = this.table_data_copy[0].RULE_ID;
    // sending table data to the quality page for count calculation
    // this.controlTotalTableEmitter.emit(this.table_data_copy)
    this.record_count.emit(this.table_data_copy.length);
  }
  getDistinctValuesForColumns(tableData: any[], columns: string[]) {
    const result: any = [];

    columns.forEach((column) => {
      const uniqueValues = getUniqueValues(tableData, column);

      const mappedArray = getMappedArray(uniqueValues);
      result[column] = [...mappedArray];
    });

    return result;
  }
  resetBy(param) {
    this.filterComponents.forEach((item) => {
      if (item.selected !== param) {
        // console.log(item.component)
        item.component.resetSelection();
      }
    });
    this.record_count.emit(this.table_data_copy.length);
  }
  resetAll() {
    this.filterComponents.forEach((item) => {
      item.component.resetSelection();
    });
    this.activatedFilterReset(this.activeFiltered);
    this.table_data_copy = this.table_data_orig;
    this.record_count.emit(this.table_data_copy.length);
    this.uniqueValues = this.getDistinctValuesForColumns(
      this.table_data_copy,
      this.columnsArray
    );
  }
  multiselectOnchange(filteredArr, column) {
    let tempFilterArray = filteredArr;
    if (column === 'TARGET_DATA_ZONE') {
      this.activeFiltered.T_dataZone.valArr = tempFilterArray;
    } else if (column === 'TARGET_DATA_PROVIDER') {
      this.activeFiltered.T_dataProvider.valArr = tempFilterArray;
    } else if (column === 'TARGET_DATASET_NAME') {
      this.activeFiltered.T_dataSetName.valArr = tempFilterArray;
    } else if (column === 'TARGET_METRIC_NAME') {
      this.activeFiltered.T_metricName.valArr = tempFilterArray;
    } else if (column === 'TARGET_GROUP_BY') {
      this.activeFiltered.T_groupBy.valArr = tempFilterArray;
    } else if (column === 'TARGET_FILTER') {
      this.activeFiltered.T_filterClause.valArr = tempFilterArray;
    } else if (column === 'SOURCE_DATA_ZONE') {
      this.activeFiltered.S_dataZone.valArr = tempFilterArray;
    } else if (column === 'SOURCE_DATA_PROVIDER') {
      this.activeFiltered.S_dataProvider.valArr = tempFilterArray;
    } else if (column === 'SOURCE_DATASET_NAME') {
      this.activeFiltered.S_dataSetName.valArr = tempFilterArray;
    } else if (column === 'SOURCE_METRIC_NAME') {
      this.activeFiltered.S_metricName.valArr = tempFilterArray;
    } else if (column === 'SOURCE_GROUP_BY') {
      this.activeFiltered.S_groupBy.valArr = tempFilterArray;
    } else if (column === 'SOURCE_FILTER') {
      this.activeFiltered.S_filterClause.valArr = tempFilterArray;
    } else if (column === 'RULE_SEVERITY') {
      this.activeFiltered.dqSeverity.valArr = tempFilterArray;
    } else if (column === 'STATUS') {
      this.activeFiltered.status.valArr = tempFilterArray;
    }

    console.log(this.activeFiltered);
    const result = tableFilter(this.activeFiltered, this.table_data_orig);
    if (result.length) {
      console.log(result);
      this.table_data_copy = result;
    } else {
      if (
        !Object.keys(this.activeFiltered).some(
          (field) => this.activeFiltered[field].valArr.length
        )
      ) {
        this.table_data_copy = this.table_data_orig;
      } else {
        this.table_data_copy = result;
      }
    }
    this.setFilterOptions(this.activeFiltered, result);
    this.record_count.emit(this.table_data_copy.length);
    this.tablePageNumber = 0;
  }
  setFilterOptions(fieldObject, result) {
    const filterFields = Object.keys(fieldObject);

    for (let field of filterFields) {
      if (!fieldObject[field].valArr.length) {
        this.uniqueValues[fieldObject[field].column] = getMappedArray(
          getUniqueValues(result, fieldObject[field].column)
        );
      }
    }
  }
  activatedFilterReset(fieldObject: any) {
    const filterFields = Object.keys(fieldObject);
    filterFields.forEach((key) => (fieldObject[key].valArr = []));
  }

  handleNewQueryClick() {
    this.popupModelData.displayModal = true;
    this.isEdit = false;
    this.popupModelData.header = 'Control Sum New Rule';
    this.popupModelData.editValue = '';
  }
  editBtnHandler(row) {
    this.popupModelData.header = 'Control Sum Edit Rule';
    this.popupModelData.editValue = row;
    this.editedRuleId = row.RULE_ID;
    this.popupModelData.displayModal = true;
    // console.log(row)
    this.isEdit = true;
  }
  deleteBtnHandler(row) {
    this.currentDeleteRuleId = row.RULE_ID;
    // console.log(this.currentDeleteRuleId)
    this.deletePopupModel = {
      displayModal: true,
      header: 'Delete Rule',
      icon: 'pi-exclamation-triangle',
      content: {
        header: `Are you sure to delete the following rule ?`,
        text: 'This action will delete the rule permanently',
      },
      deleteBtnProps: {
        label: 'Delete',
        click: '',
      },
      cancelBtnProps: {
        label: 'Cancel',
        click: '',
      },
    };
  }

  async inputSwitchOnchange(row) {
    let ruleType = replaceNumberWithEmptyString(row.RULE_ID)
    ruleType = (ruleType === 'CS') ? 'CT' : ruleType;

    const postData = {
      "status": row.SWITCH_STATUS,
      "identifier": ruleType,
      "ruleId": row.RULE_ID
    }
    this.loaderService.showLoader();
    let response: any = await lastValueFrom(this.dataQualityApiService.updateRuleStatus(postData))
    let key = '';
    let message = '';
    // console.log(response)

    if (response.status === 200) {
      key = 'suss';
      message = 'Status Updated Successfully';
      this.getResponseHandler();
    } else {
      key = 'wrn';
      message = 'Something went wrong. Please try again.';
    }
    this.addApiStatusEmitter.emit({ flag: key, message: message });
    this.loaderService.hideLoader();
  }
  async PopupSubmitHandler(formValue) {
    // console.log(formValue)
    const formattedDate = format(new Date(), 'yyyy-MM-dd');
    const userId = localStorage.getItem('userId');
    let status = formValue.status.value === 'Disabled' ? false : true;
    let RuleId =
      replaceNumberWithEmptyString(this.latestRuleId) +
      (Number(replaceWordsWithEmptyString(this.latestRuleId)) + 1);
    const T_groupByValue = groupByArrayToValueConverter(formValue.T_groupBy);
    const S_groupByValue = groupByArrayToValueConverter(formValue.S_groupBy);
    let successMessage = '';
    let errMessages = 'Something went wrong. Please try again';
    let query = {};
    const formProperty = [
      'T_dataZone',
      'T_dataProvider',
      'T_dataSetName',
      'T_metricName',
      'S_dataZone',
      'S_dataProvider',
      'S_dataSetName',
      'S_metricName',
    ];
    const tableProperty = [
      'TARGET_DATA_ZONE',
      'TARGET_DATA_PROVIDER',
      'TARGET_DATASET_NAME',
      'TARGET_METRIC_NAME',
      'SOURCE_DATA_ZONE',
      'SOURCE_DATA_PROVIDER',
      'SOURCE_DATASET_NAME',
      'SOURCE_METRIC_NAME',
    ];
    if (!this.isEdit) {
      // add handler
      const duplicateFound = addDuplicateRecordChecker(
        formValue,
        formProperty,
        this.table_data_orig,
        tableProperty
      );
      if (
        !duplicateFound ||
        !this.table_data_orig.some(
          (obj) => obj.TARGET_GROUP_BY_ID === T_groupByValue
        ) ||
        !this.table_data_orig.some(
          (obj) => obj.SOURCE_GROUP_BY_ID === S_groupByValue
        )
      ) {
        // duplicate checking

        const ruleCreationData = {
          "s_dataset_value": formValue.S_dataSetName.value,
          "s_metric_value": formValue.S_metricName.value,
          "s_group_by_value": S_groupByValue,
          "s_filter_clause": formValue.S_filterClause,
          "t_dataset_value": formValue.T_dataSetName.value,
          "t_metric_value": formValue.T_metricName.value,
          "t_group_by_value": T_groupByValue,
          "t_filter_clause": formValue.T_filterClause,
          "dq_severity": formValue.dqSeverity.value,
          "status": status,
          "rule_id": RuleId,
          "user_id": userId
        }
        // query = {
        //   query: `INSERT INTO HRMNY_DB.HRMNY_CNF_ZN.DQ_CT_CONFIG (RULE_ID, MSTR_RULE_ID, SRC_ENT_ID, SRC_COL_ID, SRC_BK_COL_ID, SRC_FLTR_CON, TGT_ENT_ID, TGT_COL_ID, TGT_BK_COL_ID, TGT_FLTR_CON, RULE_SVRT, RULE_DESC, IS_RULE_ACTV, EFFCTV_STRT_DT, EFFCTV_END_DT, IS_ACTV, AUDIT_BATCH_ID, AUDIT_INSRT_DT, AUDIT_INSRT_ID, AUDIT_UPDT_DT, AUDIT_UPDT_ID) VALUES 
        //     ('${RuleId}',27,${formValue.S_dataSetName.value},
        //     ${formValue.S_metricName.value}, '${S_groupByValue},', 
        //     '${formValue.S_filterClause}',${formValue.T_dataSetName.value},
        //     ${formValue.T_metricName.value} ,'${T_groupByValue}', '${formValue.T_filterClause}', 
        //     '${formValue.dqSeverity.value}', null,  ${status},'${formattedDate}', null, ${status}, '',  
        //     '${formattedDate}', ${userId}, '${formattedDate}', ${userId})`,
        //   flag: 'i',
        // };
        successMessage = 'Rule Inserted Successfully';
        this.popupModelData.displayModal = false;
        this.loaderService.showLoader();
        let response = await lastValueFrom(
          this.dataQualityApiService.createControlSumRules(ruleCreationData)
        );
        if (response.status == 200) {
          // calling toast
          this.addApiStatusEmitter.emit({
            flag: 'suss',
            message: successMessage,
          });
          this.resetAll();
          this.getResponseHandler();
        } else {
          //calling toast
          this.addApiStatusEmitter.emit({ flag: 'wrn', message: errMessages });
        }
      } else {
        this.addApiStatusEmitter.emit({
          flag: 'wrn',
          message: 'This Rule Already Inserted.Try Someother Rule',
        });
      }
    } else {
      //  edit handler
      const duplicateFound = editDuplicateRecordChecker(
        formValue,
        formProperty,
        this.table_data_orig,
        tableProperty,
        this.editedRuleId
      );
      const T_groupByDuplicateFlag = editGroupByValueDuplicateChecker(
        T_groupByValue,
        this.table_data_orig,
        'TARGET_GROUP_BY_ID',
        this.editedRuleId
      );
      const S_groupByDuplicateFlag = editGroupByValueDuplicateChecker(
        S_groupByValue,
        this.table_data_orig,
        'SOURCE_GROUP_BY_ID',
        this.editedRuleId
      );
      if (!duplicateFound || T_groupByDuplicateFlag || S_groupByDuplicateFlag) {


        const ruleUpdationData = {
          "s_dataset_value": formValue.S_dataSetName.value,
          "s_metric_value": formValue.S_metricName.value,
          "s_group_by_value": S_groupByValue,
          "s_filter_clause": formValue.S_filterClause,
          "t_dataset_value": formValue.T_dataSetName.value,
          "t_metric_value": formValue.T_metricName.value,
          "t_group_by_value": T_groupByValue,
          "t_filter_clause": formValue.T_filterClause,
          "dq_severity": formValue.dqSeverity.value,
          "status": status,
          "rule_id": RuleId,
          "user_id": userId
        }
        successMessage = 'Rule Updated Successfully';
        this.popupModelData.displayModal = false;
        this.loaderService.showLoader();
        let response = await lastValueFrom(
          this.dataQualityApiService.updateControlSumRules(ruleUpdationData)
        );
        if (response.status === 200) {
          // calling toast
          this.addApiStatusEmitter.emit({
            flag: 'suss',
            message: successMessage,
          });
          this.getResponseHandler();
        } else {
          //calling toast
          this.addApiStatusEmitter.emit({ flag: 'wrn', message: errMessages });
        }
      } else {
        this.addApiStatusEmitter.emit({
          flag: 'wrn',
          message: 'This Rule Already Inserted.Try Someother Rule',
        });
      }
    }
  }

  async deleteApiCall() {
    this.deletePopupModel.displayModal = false;
    // console.log(this.currentDeleteRuleId)
    this.loaderService.showLoader();
    let ruleType = replaceNumberWithEmptyString(this.currentDeleteRuleId)
    ruleType = (ruleType === 'CS') ? 'CT' : ruleType;
    const deleteRuleData = {
      "identifier": ruleType,
      "ruleId": this.currentDeleteRuleId
    }
    const response = await lastValueFrom(
      this.dataQualityApiService.deleteRule(deleteRuleData)
    );
    let successMessage = 'Rule Deleted Successfully';
    let errMessages = 'Something went wrong. Please try again';
    // console.log(response)
    // console.log(newJSONStr)
    if (response.status == 200) {
      console.log(successMessage);
      this.addApiStatusEmitter.emit({ flag: 'err', message: successMessage });
      this.resetAll();
      this.getResponseHandler();
    } else {
      //calling toast
      // console.log(errMessages)
      this.addApiStatusEmitter.emit({ flag: 'wrn', message: errMessages });
    }
    this.loaderService.hideLoader();
  }

  exportExcel() {
    this.excelservice.exportExcel(
      'Control-Sum-Rules.xlsx',
      this.table_data_orig
    );
  }
}
