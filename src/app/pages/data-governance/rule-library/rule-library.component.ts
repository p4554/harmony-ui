import { Component,  ViewChild,  OnInit } from '@angular/core';
import { QueryService } from '@app/_services/query.service';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import { CommonDataService } from '@app/_services/common-data.service';
import { LoaderService } from '@app/_services/loading.service';
import { filterIncludedValuesFromArray, getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { DialogService  } from 'primeng/dynamicdialog';
import { PrimeMultiSelectComponent } from '@app/components/form-components/prime-multi-select/prime-multi-select.component';
import { MessageService } from 'primeng/api';
import { tableFilter } from '@app/util/helper/harmonyUtils';
import { ChangeDetectorRef } from '@angular/core';
@Component({
  selector: 'app-rule-library',
  templateUrl: './rule-library.component.html',
  styleUrls: ['./rule-library.component.scss']
})
export class RuleLibraryComponent {
  
  @ViewChild('ruleIdFilter') ruleIdFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('ruleTypeFilter') ruleTypeFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('ruleCategoryFilter') ruleCategoryFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('dataProviderFilter') dataProviderFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('dataSetFilter') dataSetFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('dimensionFilter') dimensionFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('metricsFilter') metricsFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('recommendedFilter') recommendedFilterComponent: PrimeMultiSelectComponent;



  table_data: any = []
  table_data_copy: any = []
  filterComponents: any;
  tenantName: any
  region: any
  country: any
  project: any
  productId: any
  language: any
  popupModelData: any
  popupModelTrigger: boolean = false;
  uniqueValues:any;
  tablePageNumber:number = 0
  tab_label = 'Rule Library'
  Column = {
    ruleId: "RULE_ID",
    ryleType: "RULE_TYPE",
    ruleCategory: "RULE_CATEGORY",
    dataProvider: "DATA_PROVIDER",
    dataSet: "DATASET",
    dimensionsGrains: "DIMENSION_GRAINS",
    metrics: "METRICS",
    recommendedRule: "RECOMMENDED_RULE"
  }

  columnArrays = [
    this.Column.ruleId,
    this.Column.ryleType,
    this.Column.ruleCategory,
    this.Column.dataProvider,
    this.Column.dataSet,
    this.Column.dimensionsGrains,
    this.Column.metrics,
    this.Column.recommendedRule
  ];
  
  activeFiltered = {
    ruleId: { valArr: [], column: 'RULE_ID' },
    ryleType: { valArr: [], column: 'RULE_TYPE' },
    ruleCategory: { valArr: [], column: 'RULE_CATEGORY' },
    dataProvider: { valArr: [], column: 'DATA_PROVIDER' },
    dataSet: { valArr: [], column: 'DATASET' },
    dimensionsGrains: { valArr: [], column: 'DIMENSION_GRAINS' },
    metrics: { valArr: [], column: 'METRICS' },
    recommendedRule: { valArr: [], column: 'RECOMMENDED_RULE'}
  }
  constructor(
    private commonDataService: CommonDataService,
    public loaderService: LoaderService,
    private queryService: QueryService,
    private dialogService: DialogService,
    private toastService: MessageService,
    private cdRef: ChangeDetectorRef,
  ) { }
  async ngOnInit() {
    await this.getResponseHandler();

  }
  async getResponseHandler() {
    this.loaderService.showLoader();
      this.table_data = await lastValueFrom(
      this.commonDataService.getRuleLibraryData()
    );
     console.log(this.table_data);
    this.table_data_copy = [...this.table_data];
    this.uniqueValues=this.getDistinctValuesForColumns(this.table_data,this.columnArrays)
    this.tablePageNumber = 0
    

    setTimeout(() => {
      this.filterComponents = [
        { selected: 'ruleIdFilterSelected', component: this.ruleIdFilterComponent },
        { selected: 'ruleTypeFilterSelected', component: this.ruleTypeFilterComponent },
        { selected: 'ruleCategoryFilterSelected', component: this.ruleCategoryFilterComponent },
        { selected: 'dataProviderFilterSelected', component: this.dataProviderFilterComponent },
        { selected: 'dataSetFilterSelected', component: this.dataSetFilterComponent },
        { selected: 'dimensionFilterSelected', component: this.dimensionFilterComponent },
        { selected: 'metricsFilterSelected', component: this.metricsFilterComponent },
        { selected: 'recommendedFilterSelected', component: this.recommendedFilterComponent },

      ];
    }, 300);
    
    if (this.activeFiltered) {
      this.table_data = tableFilter(this.activeFiltered,this.table_data_copy);
    }
    this.loaderService.hideLoader();
  }
  getDistinctValuesForColumns = (tableData: any[], columns: string[]) => {
		const result: any = [];
		columns.forEach((column) => {
		  const uniqueValues = getUniqueValues(tableData, column);
		  const mappedArray = getMappedArray(uniqueValues);
		  result[column] = [...mappedArray];
		});
		return result;
	  };


  filterChangeHandler(filterArray, column) {
    const filterArrayCopy = filterArray;
    if (column === 'RULE_ID') {
      this.activeFiltered.ruleId.valArr = filterArrayCopy;
    }
    else if (column === 'RULE_TYPE') {
      this.activeFiltered.ryleType.valArr = filterArrayCopy;
    }
    else if (column === 'RULE_CATEGORY') {
      this.activeFiltered.ruleCategory.valArr = filterArrayCopy;
    }
    else if (column === 'DATA_PROVIDER') {
      this.activeFiltered.dataProvider.valArr = filterArrayCopy;
    }
    else if (column === 'DATASET') {
      this.activeFiltered.dataSet.valArr = filterArrayCopy;
    }
    else if (column === 'DIMENSION_GRAINS') {
      this.activeFiltered.dimensionsGrains.valArr = filterArrayCopy;
    }
    else if (column === 'METRICS') {
      this.activeFiltered.metrics.valArr = filterArrayCopy;
    }
    else if (column === 'RECOMMENDED_RULE') {
      this.activeFiltered.recommendedRule.valArr = filterArrayCopy;
    }
    const result=tableFilter(this.activeFiltered,this.table_data_copy)

    if (result.length){
    this.table_data=result
    this.tablePageNumber = 0
   }
   else{
    if(!Object.keys(this.activeFiltered).some(field=>this.activeFiltered[field].valArr.length)){
      this.table_data = this.table_data_copy
    }else {
      this.table_data = result
    }
   }
   this.setFilterOptions(this.activeFiltered,result)  

  }
  setFilterOptions(fieldObject,result){

		const filterFields = Object.keys(fieldObject)
	
		for(let field of filterFields){
		  if(!fieldObject[field].valArr.length){
			this.uniqueValues[fieldObject[field].column] = getMappedArray(getUniqueValues(result, fieldObject[field].column));
		  }
		}
	  }
    resetAll() {
      this.filterComponents.forEach((item) => {
        item.component.resetSelection();
      });
      this.table_data = this.table_data_copy;
      this.activatedFilterReset(this.activeFiltered)
      this.uniqueValues = this.getDistinctValuesForColumns(this.table_data, this.columnArrays);
    }
    activatedFilterReset(fieldObject:any){
		  const filterFields = Object.keys(fieldObject)
		  filterFields.forEach(key=> fieldObject[key].valArr = [])
		}

} 
