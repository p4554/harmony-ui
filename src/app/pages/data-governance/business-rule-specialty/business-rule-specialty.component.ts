import { Component,Input,Output,EventEmitter} from '@angular/core';
import { ExcelService } from '@app/_services/excel.service';

@Component({
  selector: 'app-business-rule-specialty',
  templateUrl: './business-rule-specialty.component.html',
  styleUrls: ['./business-rule-specialty.component.scss']
})
export class BusinessRuleSpecialtyComponent {
  tabs=[
    "Specialty Group Map",
    "Specialty Update",
    "Specialty Inclusion"
  ]
  currentTab:string="Specialty Group Map";
  @Output() ruleCountEmitter:EventEmitter<number> = new EventEmitter<number>()

  @Input() savedData:string;

  constructor(
    private excelService: ExcelService,

   ){}
  setCurrentTab(event){
    this.currentTab=this.tabs[event.index];
  }
  tableData:any;
  tableName:string;
  handleData(e){
    if(e.type=='groupMap'){
      this.tableName='Specialty-Group-Map-Rule.xlsx';
      this.tableData=e.payload;
      this.ruleCountEmitter.emit(this.tableData.length)

    }
    else if(e.type=='update'){
      this.tableName='Specialty-Update-Rule.xlsx';
      this.tableData=e.payload;
      this.ruleCountEmitter.emit(this.tableData.length)

    }
    else if(e.type=='inclusion'){
      this.tableName='Specialty-Inclusion-Rule.xlsx';
      this.tableData=e.payload;
      this.ruleCountEmitter.emit(this.tableData.length)

    }
    
  } 
exportExcel(){
  this.excelService.exportExcel(this.tableName, this.tableData);
}
}
