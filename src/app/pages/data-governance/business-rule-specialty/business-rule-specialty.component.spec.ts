import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessRuleSpecialtyComponent } from './business-rule-specialty.component';

describe('BusinessRuleSpecialtyComponent', () => {
  let component: BusinessRuleSpecialtyComponent;
  let fixture: ComponentFixture<BusinessRuleSpecialtyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessRuleSpecialtyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BusinessRuleSpecialtyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
