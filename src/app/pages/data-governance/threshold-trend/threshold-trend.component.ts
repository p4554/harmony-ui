import {
  AfterViewInit,
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { PrimeMultiSelectComponent } from '@app/components/form-components/prime-multi-select/prime-multi-select.component';
import {
  getUniqueValues,
  getMappedArray,
  filterIncludedValuesFromArray,
  addDuplicateRecordChecker,
  editDuplicateRecordChecker,
} from '@app/util/helper/arrayHandlers';
import {
  replaceNumberWithEmptyString,
  replaceWordsWithEmptyString,
  groupByArrayToValueConverter,
  replaceParamsinJSON,
  editGroupByValueDuplicateChecker,
} from '@app/util/helper/harmonyUtils';
import { CommonDataService } from '@app/_services/common-data.service';
import { ExcelService } from '@app/_services/excel.service';
import { LoaderService } from '@app/_services/loading.service';
import { QueryService } from '@app/_services/query.service';
import { format } from 'date-fns';
import { lastValueFrom, map } from 'rxjs';
import { tableFilter } from '@app/util/helper/harmonyUtils';
import { DataQualityApiService } from '@app/_services/dataQuality-api.service';

@Component({
  selector: 'app-threshold-trend',
  templateUrl: './threshold-trend.component.html',
  styleUrls: ['./threshold-trend.component.scss'],
})
export class ThresholdTrendComponent implements OnInit, AfterViewInit {
  @ViewChild('dataZone') dataZone: PrimeMultiSelectComponent;
  @ViewChild('dataProvider') dataProvider: PrimeMultiSelectComponent;
  @ViewChild('dataSetName') dataSetName: PrimeMultiSelectComponent;
  @ViewChild('function') function: PrimeMultiSelectComponent;
  @ViewChild('metricName') metricName: PrimeMultiSelectComponent;
  @ViewChild('dqSeverity') dqSeverity: PrimeMultiSelectComponent;
  @ViewChild('thresholdValue') thresholdValue: PrimeMultiSelectComponent;
  @ViewChild('timeLineAttribute') timeLineAttribute: PrimeMultiSelectComponent;
  @ViewChild('groupBy') groupBy: PrimeMultiSelectComponent;
  @ViewChild('filterClause') filterClause: PrimeMultiSelectComponent;
  @ViewChild('status') status: PrimeMultiSelectComponent;
  @Output() addApiStatusEmitter = new EventEmitter();
  @Output() record_count = new EventEmitter();
  buttonProps = {
    label: 'Export',
    click: '',
  };
  popupModelData: {
    displayModal: boolean;
    header: string;
    saveBtnProps: { label: string; click: string };
    cancelBtnProps: { label: string; click: string };
    editValue: string;
  };
  table_data_orig: any;
  table_data_copy: any[];
  uniqueValues: any;
  COLUMNS = {
    dataZone: 'DATA_ZONE',
    dataProvider: 'DATA_PROVIDER',
    dataSetName: 'DATASET_NAME',
    function: 'FUNCTION',
    metricName: 'METRIC_NAME',
    dqSeverity: 'RULE_SEVERITY',
    thresholdValue: 'THRESHOLD_VALUE',
    timeLineAttribute: 'TMLN_COL_ID',
    groupBy: 'GROUP_BY',
    filterClause: 'FILTER_CLAUSE',
    status: 'STATUS',
  };
  activeFiltered = {
    dataZone: { valArr: [], column: 'DATA_ZONE' },
    dataProvider: { valArr: [], column: 'DATA_PROVIDER' },
    dataSetName: { valArr: [], column: 'DATASET_NAME' },
    function: { valArr: [], column: 'FUNCTION' },
    metricName: { valArr: [], column: 'METRIC_NAME' },
    dqSeverity: { valArr: [], column: 'RULE_SEVERITY' },
    thresholdValue: { valArr: [], column: 'THRESHOLD_VALUE' },
    timeLineAttribute: { valArr: [], column: 'TMLN_COL_ID' },
    groupBy: { valArr: [], column: 'GROUP_BY' },
    filterClause: { valArr: [], column: 'FILTER_CLAUSE' },
    status: { valArr: [], column: 'STATUS' },
  };
  columnsArray = [
    this.COLUMNS.dataZone,
    this.COLUMNS.dataProvider,
    this.COLUMNS.dataSetName,
    this.COLUMNS.function,
    this.COLUMNS.metricName,
    this.COLUMNS.dqSeverity,
    this.COLUMNS.thresholdValue,
    this.COLUMNS.timeLineAttribute,
    this.COLUMNS.groupBy,
    this.COLUMNS.filterClause,
    this.COLUMNS.status,
  ];
  filterComponents: any[] = [];
  tablePageNumber: number = 0;
  isEdit: boolean;
  latestRuleId: any;
  editedRuleId: any;
  deletePopupModel: {
    displayModal: boolean;
    header: string;
    icon: string;
    content: { header: string; text: string };
    deleteBtnProps: { label: string; click: string };
    cancelBtnProps: { label: string; click: string };
  };
  deleteRuleId: any;

  constructor(
    private queryService: QueryService,
    private commonDataService: CommonDataService,
    public loaderService: LoaderService,
    private excelservice: ExcelService,
    private dataQualityApiService: DataQualityApiService
  ) { }

  ngOnInit() {
    // this.loaderService.showLoader()
    this.getResponseHandler();
    this.popupModelData = {
      displayModal: false,
      header: 'Threshold Trend New Rule',
      saveBtnProps: {
        label: 'Save',
        click: '',
      },
      cancelBtnProps: {
        label: 'Cancel',
        click: '',
      },
      editValue: '',
    };
  }
  ngAfterViewInit() {
    this.filterComponents = [
      { selected: this.COLUMNS.dataZone, component: this.dataZone },
      { selected: this.COLUMNS.dataProvider, component: this.dataProvider },
      { selected: this.COLUMNS.dataSetName, component: this.dataSetName },
      { selected: this.COLUMNS.function, component: this.function },
      { selected: this.COLUMNS.metricName, component: this.metricName },
      { selected: this.COLUMNS.dqSeverity, component: this.dqSeverity },
      { selected: this.COLUMNS.thresholdValue, component: this.thresholdValue },
      {
        selected: this.COLUMNS.timeLineAttribute,
        component: this.timeLineAttribute,
      },
      { selected: this.COLUMNS.groupBy, component: this.groupBy },
      { selected: this.COLUMNS.filterClause, component: this.filterClause },
      { selected: this.COLUMNS.status, component: this.status },
    ];
  }
  async getResponseHandler() {
    this.table_data_orig = await lastValueFrom(
      this.dataQualityApiService.getThTrendRules().pipe(
        map((array: any) => {
          return array.map((item) => ({
            ...item,
            SWITCH_STATUS: item.STATUS === 'Active' ? true : false,
          }));
        })
      )
    );

    // this.table_data_copy = [...this.table_data_orig];
    this.table_data_copy = tableFilter(
      this.activeFiltered,
      this.table_data_orig
    );
    console.log(this.table_data_copy);
    this.loaderService.hideLoader();
    this.latestRuleId = this.table_data_copy[0].RULE_ID;
    this.uniqueValues = this.getDistinctValuesForColumns(
      this.table_data_orig,
      this.columnsArray
    );
    console.log(this.uniqueValues);
    this.record_count.emit(this.table_data_copy.length);
  }

  handleNewRuleClick() {
    this.popupModelData.displayModal = true;
    this.popupModelData.editValue = '';
    this.popupModelData.header = 'Threshold Trend Add Rule';
    this.isEdit = false;
  }

  getDistinctValuesForColumns(tableData: any[], columns: string[]) {
    const result: any = [];

    columns.forEach((column) => {
      const uniqueValues = getUniqueValues(tableData, column);

      const mappedArray = getMappedArray(uniqueValues);
      result[column] = [...mappedArray];
    });

    return result;
  }
  multiselectOnchange(filteredArr, column) {
    let tempFilterArray = filteredArr;
    if (column === 'DATA_ZONE') {
      this.activeFiltered.dataZone.valArr = tempFilterArray;
    } else if (column === 'DATA_PROVIDER') {
      this.activeFiltered.dataProvider.valArr = tempFilterArray;
    } else if (column === 'DATASET_NAME') {
      this.activeFiltered.dataSetName.valArr = tempFilterArray;
    } else if (column === 'FUNCTION') {
      this.activeFiltered.function.valArr = tempFilterArray;
    } else if (column === 'METRIC_NAME') {
      this.activeFiltered.metricName.valArr = tempFilterArray;
    } else if (column === 'RULE_SEVERITY') {
      this.activeFiltered.dqSeverity.valArr = tempFilterArray;
    } else if (column === 'THRESHOLD_VALUE') {
      this.activeFiltered.thresholdValue.valArr = tempFilterArray;
    } else if (column === 'TMLN_COL_ID') {
      this.activeFiltered.timeLineAttribute.valArr = tempFilterArray;
    } else if (column === 'GROUP_BY') {
      this.activeFiltered.groupBy.valArr = tempFilterArray;
    } else if (column === 'FILTER_CLAUSE') {
      this.activeFiltered.filterClause.valArr = tempFilterArray;
    } else if (column === 'STATUS') {
      this.activeFiltered.status.valArr = tempFilterArray;
    }

    console.log(this.activeFiltered);
    const result = tableFilter(this.activeFiltered, this.table_data_orig);
    if (result.length) {
      console.log(result);
      this.table_data_copy = result;
    } else {
      if (
        !Object.keys(this.activeFiltered).some(
          (field) => this.activeFiltered[field].valArr.length
        )
      ) {
        this.table_data_copy = this.table_data_orig;
      } else {
        this.table_data_copy = result;
      }
    }
    this.setFilterOptions(this.activeFiltered, result);
    this.record_count.emit(this.table_data_copy.length);
    this.tablePageNumber = 0;
  }
  setFilterOptions(fieldObject, result) {
    const filterFields = Object.keys(fieldObject);

    for (let field of filterFields) {
      if (!fieldObject[field].valArr.length) {
        this.uniqueValues[fieldObject[field].column] = getMappedArray(
          getUniqueValues(result, fieldObject[field].column)
        );
      }
    }
  }
  activatedFilterReset(fieldObject: any) {
    const filterFields = Object.keys(fieldObject);
    filterFields.forEach((key) => (fieldObject[key].valArr = []));
  }
  // resetBy(param) {
  //   this.filterComponents.forEach((item) => {
  //     if (item.selected !== param) {
  //       item.component.resetSelection();
  //     }
  //   });
  //   this.record_count.emit(this.table_data_copy.length)
  // }
  resetAll() {
    this.filterComponents.forEach((item) => {
      item.component.resetSelection();
    });
    this.table_data_copy = this.table_data_orig;
    this.activatedFilterReset(this.activeFiltered);
    this.record_count.emit(this.table_data_copy.length);
    this.uniqueValues = this.getDistinctValuesForColumns(
      this.table_data_copy,
      this.columnsArray
    );
  }

  async inputSwitchOnchange(row) {
    let ruleType = replaceNumberWithEmptyString(row.RULE_ID)
    ruleType = (ruleType === 'THT') ? 'TH' : ruleType;

    const postData = {
      "status": row.SWITCH_STATUS,
      "identifier": ruleType,
      "ruleId": row.RULE_ID
    }
    this.loaderService.showLoader();
    let response: any = await lastValueFrom(this.dataQualityApiService.updateRuleStatus(postData))
    let key = '';
    let message = '';
    console.log(response);

    if (response.status === 200) {
      key = 'suss';
      message = 'Status Updated Successfully';
      this.getResponseHandler();
    } else {
      key = 'wrn';
      message = 'Something went wrong. Please try again.';
    }
    this.addApiStatusEmitter.emit({ flag: key, message: message });
    this.loaderService.hideLoader();
  }

  editBtnHandler(row) {
    this.popupModelData.header = 'Threshold Trend Edit Rule';
    this.popupModelData.editValue = row;
    this.popupModelData.displayModal = true;
    console.log(row);
    this.isEdit = true;
    this.editedRuleId = row.RULE_ID;
  }
  deleteBtnHandler(row) {
    console.log(row);
    this.deletePopupModel = {
      displayModal: true,
      header: 'Delete Rule',
      icon: 'pi-exclamation-triangle',
      content: {
        header: `Are you sure to delete the following rule ?`,
        text: 'This action will delete the rule permanently',
      },
      deleteBtnProps: {
        label: 'Delete',
        click: '',
      },
      cancelBtnProps: {
        label: 'Cancel',
        click: '',
      },
    };

    this.deleteRuleId = row.RULE_ID;
  }

  // delete api handler
  async deletePopupDeleteHandler() {
    this.deletePopupModel.displayModal = false;
    this.loaderService.showLoader();
    let ruleType = replaceNumberWithEmptyString(this.deleteRuleId)
    ruleType = (ruleType === 'THV') ? 'TH' : (ruleType === 'THT') ? 'TH' : ruleType;

    const deleteRuleData = {
      "identifier": ruleType,
      "ruleId": this.deleteRuleId
    }

    let response = await lastValueFrom(
      this.dataQualityApiService.deleteRule(deleteRuleData)
    );
    let successMessage = 'Rule Deleted Successfully';
    let errMessages = 'Something went wrong. Please try again';
    if (response.status == 200) {
      // calling toast
      console.log(successMessage);
      this.addApiStatusEmitter.emit({ flag: 'err', message: successMessage });
      this.resetAll();
      this.getResponseHandler();
    } else {
      //calling toast
      console.log(errMessages);
      this.addApiStatusEmitter.emit({ flag: 'wrn', message: errMessages });
    }
    this.loaderService.hideLoader();
  }
  // hint popup submit handler
  async PopupSubmitHandler(formValue) {
    const formattedDate = format(new Date(), 'yyyy-MM-dd');
    const userId = localStorage.getItem('userId');
    let status = formValue.status.value === 'Disabled' ? false : true;
    let RuleId =
      replaceNumberWithEmptyString(this.latestRuleId) +
      (Number(replaceWordsWithEmptyString(this.latestRuleId)) + 1);

    let groupByValue = groupByArrayToValueConverter(formValue.groupBy);
    console.log(groupByValue);
    let successMessage = '';
    let errMessages = 'Something went wrong. Please try again';
    let query = {};
    console.log(formValue);
    console.log(status);
    const formProperty = [
      'dataZone',
      'dataProvider',
      'datasetName',
      'metricName',
      'function',
    ];
    const tableProperty = [
      'DATA_ZONE',
      'DATA_PROVIDER',
      'DATASET_NAME',
      'METRIC_NAME',
      'FUNCTION',
    ];
    if (!this.isEdit) {
      // insert query

      // duplicate Validation

      const duplicateFound = addDuplicateRecordChecker(
        formValue,
        formProperty,
        this.table_data_orig,
        tableProperty
      );
      if (
        !duplicateFound ||
        !this.table_data_orig.some((obj) => obj.GROUP_BY_ID === groupByValue)
      ) {
        const ruleCreationData = {
          "rule_id": RuleId,
          "function_value": formValue.function.value,
          "function_label": formValue.function.label,
          "dataset_value": formValue.datasetName.value,
          "metric_value": formValue.metricName.value,
          "group_by_value": groupByValue,
          "threshold_value": formValue.thresholdValue,
          "filter_clause": formValue.filterClause,
          "time_line_attr": formValue.timeLineAttribute.value,
          "dq_severity": formValue.dqSeverity.label,
          "status": status,
          "user_id": userId
        }

        successMessage = 'Rule Inserted Successfully';
        //  this.formGroup.reset()
        this.popupModelData.displayModal = false;
        this.loaderService.showLoader();
        let response = await lastValueFrom(
          this.dataQualityApiService.createThTrendRules(ruleCreationData)
        );
        this.loaderService.hideLoader();
        console.log(response);
        if (response.status == 200) {
          // calling toast
          console.log(successMessage);
          this.addApiStatusEmitter.emit({
            flag: 'suss',
            message: successMessage,
          });
          this.resetAll();
          this.getResponseHandler();
        } else {
          //calling toast
          console.log(errMessages);
          this.addApiStatusEmitter.emit({ flag: 'wrn', message: errMessages });
        }
      } else {
        this.addApiStatusEmitter.emit({
          flag: 'wrn',
          message: 'This Rule Already Inserted.Try Someother Rule',
        });
      }
    } else {
      // update

      const duplicateFound = editDuplicateRecordChecker(
        formValue,
        formProperty,
        this.table_data_orig,
        tableProperty,
        this.editedRuleId
      );

      const groupByDuplicationFlag = editGroupByValueDuplicateChecker(
        groupByValue,
        this.table_data_orig,
        'GROUP_BY_ID',
        this.editedRuleId
      );

      if (!duplicateFound || groupByDuplicationFlag) {
        const ruleUpdationData = {
          "rule_id": this.editedRuleId,
          "function_value": formValue.function.value,
          "function_label": formValue.function.label,
          "dataset_value": formValue.datasetName.value,
          "metric_value": formValue.metricName.value,
          "group_by_value": groupByValue,
          "threshold_value": formValue.thresholdValue,
          "filter_clause": formValue.filterClause,
          "time_line_attr": formValue.timeLineAttribute.value,
          "dq_severity": formValue.dqSeverity.label,
          "status": status,
          "user_id": userId
        }
        successMessage = 'Changes Updated Successfully';
        this.popupModelData.displayModal = false;
        this.loaderService.showLoader();
        let response = await lastValueFrom(
          this.dataQualityApiService.updateThTrendRules(ruleUpdationData)
        );
        this.loaderService.hideLoader();
        console.log(response);

        if (response.status == 200) {
          // calling toast
          console.log(successMessage);
          this.addApiStatusEmitter.emit({
            flag: 'suss',
            message: successMessage,
          });
          this.getResponseHandler();
        } else {
          //calling toast
          console.log(errMessages);
          this.addApiStatusEmitter.emit({ flag: 'wrn', message: errMessages });
        }
      } else {
        this.addApiStatusEmitter.emit({
          flag: 'wrn',
          message: 'This Rule Already Inserted.Try Someother Rule',
        });
      }
    }
  }

  exportExcel() {
    this.excelservice.exportExcel(
      'Threshold-Trend-Rules.xlsx',
      this.table_data_orig
    );
  }
}
