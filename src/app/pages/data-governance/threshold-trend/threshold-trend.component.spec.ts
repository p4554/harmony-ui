import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThresholdTrendComponent } from './threshold-trend.component';

describe('ThresholdTrendComponent', () => {
  let component: ThresholdTrendComponent;
  let fixture: ComponentFixture<ThresholdTrendComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThresholdTrendComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ThresholdTrendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
