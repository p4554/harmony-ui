import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CommonDataService } from '@app/_services/common-data.service';
import { ExcelService } from '@app/_services/excel.service';
import { LoaderService } from '@app/_services/loading.service';
import { QueryService } from '@app/_services/query.service';
import { PrimeMultiSelectComponent } from '@app/components/form-components/prime-multi-select/prime-multi-select.component';
import { getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { replaceParamsinJSON, tableFilter } from '@app/util/helper/harmonyUtils';
import { format, parse } from 'date-fns';
import { lastValueFrom, map } from 'rxjs';

@Component({
  selector: 'app-file-monitoring',
  templateUrl: './file-monitoring.component.html',
  styleUrls: ['./file-monitoring.component.scss']
})
export class FileMonitoringComponent implements OnInit, AfterViewInit {
  @ViewChild('dataProvider') dataProvider: PrimeMultiSelectComponent
  @ViewChild('dataSet') dataSet: PrimeMultiSelectComponent
  @ViewChild('fileName') fileName: PrimeMultiSelectComponent
  @ViewChild('fileFrequency') fileFrequency: PrimeMultiSelectComponent
  @ViewChild('runDate') runDate: PrimeMultiSelectComponent
  @ViewChild('status') status: PrimeMultiSelectComponent
  rules_record_count = 0;
  tabs = [
    "Dashboard",
    "History"
  ];
  fileDetailsFormGroup: FormGroup;
  currentTab = 'Dashboard';
  dataProviderOptions: any[] = [];
  dataSetOptions: any[] = [];
  frequencyOptions: any[] = [];
  fileNameOptions: any[] = [];
  runDateOptions: any[] = [];
  table_data_orig: any[] = [];
  table_data_copy: any[] = [];
  activeFiltered = {
    dataProvider: { valArr: [], column: 'ENT_PRVDR_NM' },
    dataSet: { valArr: [], column: 'ENT_SRC_NM' },
    fileName: { valArr: [], column: 'FILE_NAME' },
    fileFrequency: { valArr: [], column: 'ENT_FRE' },
    runDate: { valArr: [], column: 'PRCS_END_TM' },
    status: { valArr: [], column: 'STATUS' },
  }
  COLUMNS = {
    dataProvider: 'ENT_PRVDR_NM',
    dataSet: 'ENT_SRC_NM',
    fileName: 'FILE_NAME',
    fileFrequency: 'ENT_FRE',
    runDate: 'PRCS_END_TM',
    status: 'STATUS',
  }
  groupByOptions: any[] = []
  columnsArray = [
    this.COLUMNS.dataProvider,
    this.COLUMNS.dataSet,
    this.COLUMNS.fileName,
    this.COLUMNS.fileFrequency,
    this.COLUMNS.runDate,
    this.COLUMNS.status,
  ];
  tablePageNumber = 0
  uniqueValues: any;
  filterComponents: { selected: string; component: PrimeMultiSelectComponent; }[];
  dataProviderValChangeFlag = false;
  constructor(
    private fb: FormBuilder,
    private queryService: QueryService,
    private commonDataService: CommonDataService,
    private excelService: ExcelService,
    public loaderService: LoaderService,
  ) {

  }
  ngOnInit(): void {
    console.log('init')
    this.fileDetailsFormGroup = this.fb.group({
      dataProvider: [''],
      dataSet: [''],
      frequency: [''],
      fileName: [''],
      runDate: ['']
    })
    this.getTableValues()
    this.getDataProviderOptions()
    this.getFileFrequencyOptions()
    this.getFileNameOptions()
    this.getRunDateOptions()
  }
  ngAfterViewInit() {
    this.filterComponents = [
      { selected: this.COLUMNS.dataProvider, component: this.dataProvider },
      { selected: this.COLUMNS.dataSet, component: this.dataSet },
      { selected: this.COLUMNS.fileName, component: this.fileName },
      { selected: this.COLUMNS.fileFrequency, component: this.fileFrequency },
      { selected: this.COLUMNS.runDate, component: this.runDate },
      { selected: this.COLUMNS.status, component: this.status },
    ];
  }

  async getTableValues() {
    const query = await lastValueFrom(this.queryService.fetchQuery('file_monitoring_table'));
    this.table_data_orig = await lastValueFrom(this.commonDataService.getSourceConnectorTableData(query).pipe(
      map((array) => {
        return array.map((item) => ({
          ...item,
          PRCS_END_TM: this.convertDate(item.PRCS_END_TM)
        }))
      })
    ));
    this.table_data_copy = tableFilter(this.activeFiltered, this.table_data_orig)
    this.uniqueValues = this.getDistinctValuesForColumns(this.table_data_orig, this.columnsArray)
  }

  multiselectOnchange(filteredArr, column) {
    let tempFilterArray = filteredArr
    if (column === 'ENT_PRVDR_NM') {
      this.activeFiltered.dataProvider.valArr = tempFilterArray
    } else if (column === 'ENT_SRC_NM') {
      this.activeFiltered.dataSet.valArr = tempFilterArray
    } else if (column === 'FILE_NAME') {
      this.activeFiltered.fileName.valArr = tempFilterArray
    } else if (column === 'ENT_FRE') {
      this.activeFiltered.fileFrequency.valArr = tempFilterArray
    } else if (column === 'PRCS_END_TM') {
      this.activeFiltered.runDate.valArr = tempFilterArray
    } else if (column === 'STATUS') {
      this.activeFiltered.status.valArr = tempFilterArray
    }

    console.log(this.activeFiltered)
    const result = tableFilter(this.activeFiltered, this.table_data_orig)
    if (result.length) {
      console.log(result)
      this.table_data_copy = result
    } else {
      if (!Object.keys(this.activeFiltered).some(field => this.activeFiltered[field].valArr.length)) {
        this.table_data_copy = this.table_data_orig
      } else {
        this.table_data_copy = result
      }
    }
    this.setFilterOptions(this.activeFiltered, result)
    this.tablePageNumber = 0
  }
  setFilterOptions(fieldObject, result) {
    const filterFields = Object.keys(fieldObject)
    for (let field of filterFields) {
      if (!fieldObject[field].valArr.length) {
        this.uniqueValues[fieldObject[field].column] = getMappedArray(getUniqueValues(result, fieldObject[field].column));
      }
    }
  }
  activatedFilterReset(fieldObject: any) {
    const filterFields = Object.keys(fieldObject)
    filterFields.forEach(key => fieldObject[key].valArr = [])
  }
  resetAll() {
    this.filterComponents.forEach((item) => {
      item.component.resetSelection();
    });
    this.activatedFilterReset(this.activeFiltered)
    this.table_data_copy = this.table_data_orig;
    this.uniqueValues = this.getDistinctValuesForColumns(this.table_data_copy, this.columnsArray);
  }
  async getDataProviderOptions() {
    this.loaderService.showLoader()
    const query = await lastValueFrom(this.queryService.fetchQuery('file_monitoring_data_provider'));
    this.dataProviderOptions = await lastValueFrom(this.commonDataService.getSourceConnectorTableData(query).pipe(
      map((array) => {
        return array.map((item) => ({
          label: item.OBJ_SRC_NM,
          value: item.OBJ_SRC_NM
        }))
      })
    ));
    this.dataProviderOptions = this.dataProviderOptions.sort((a, b) => a.label.localeCompare(b.label))
    this.loaderService.hideLoader()
  }
  async getDataSetOptions(dataProvider) {

    if (dataProvider.length > 0) {

      let query = `SELECT DISTINCT ENT_ID, ENT_NM FROM HRMNY_CNF_ZN.ENTITY_META T WHERE IS_ACTV = TRUE AND ENT_PRVDR_NM in (?)`
      let value = ''
      dataProvider.forEach((element, i) => {
        if (i === 0) {
          value = `'${element.toUpperCase()}'`;
        } else {
          value = `${value},'${element.toUpperCase()}'`
        }
      });
      query = query.replace('?', value)

      let finalQuery = {
        query: query,
        flag: 's'
      }
      console.log('finalQuery=>', finalQuery)
      this.loaderService.showLoader()
      this.dataSetOptions = await lastValueFrom(this.commonDataService.getSourceConnectorTableData(finalQuery).pipe(
        map((array) => {
          return array.map((item) => {
            if (item.ENT_NM !== null && item.ENT_NM !== '') {
              return {
                label: item.ENT_NM,
                value: item.ENT_ID
              }
            } else {
              return null;
            }
          }).filter(item => item !== null)
        })
      ));
      this.dataSetOptions = this.dataSetOptions.sort((a, b) => a.label.localeCompare(b.label))
      this.loaderService.hideLoader()
    }
  }
  async getFileFrequencyOptions() {
    const query = await lastValueFrom(this.queryService.fetchQuery('file_monitoring_file_frequency'));
    this.frequencyOptions = await lastValueFrom(this.commonDataService.getSourceConnectorTableData(query).pipe(
      map((array) => {
        return array.map((item) => ({
          label: item.LOV_CD,
          value: item.LOV_CD
        }))
      })
    ));
    this.frequencyOptions = this.frequencyOptions.sort((a, b) => a.label.localeCompare(b.label))
  }
  async getFileNameOptions() {
    const query = await lastValueFrom(this.queryService.fetchQuery('file_monitoring_file_name'));
    this.fileNameOptions = await lastValueFrom(this.commonDataService.getSourceConnectorTableData(query).pipe(
      map((array) => {
        return array.map((item) => ({
          label: item.FILE_NAME,
          value: item.FILE_NAME
        }))
      })
    ));
    this.fileNameOptions = this.fileNameOptions.sort((a, b) => a.label.localeCompare(b.label))
  }
  async getRunDateOptions() {
    const query = await lastValueFrom(this.queryService.fetchQuery('file_monitoring_run_date'));
    this.runDateOptions = await lastValueFrom(this.commonDataService.getSourceConnectorTableData(query).pipe(
      map((array) => {
        return array.map((item) => ({
          label: this.convertDate(item.RUN_DATE),
          value: this.convertDate(item.RUN_DATE)
        }))
      })
    ));
    // this.runDateOptions = this.runDateOptions.sort((a,b)=>a.label.localeCompare(b.label))
  }
  setCurrentTab(e) {
    this.currentTab = this.tabs[e.index];
    // this.rules_record_count = this.table_data_copy.length
  }

  dataProviderPanelHide() {
    if (this.dataProviderValChangeFlag) {
      const dataProvider = this.fileDetailsFormGroup.get('dataProvider').value
      this.getDataSetOptions(dataProvider)
      this.dataProviderValChangeFlag = false;
    }
  }
  convertDate(dateString: string) {
    const date = parse(dateString, "EEE, dd MMM yyyy HH:mm:ss 'GMT'", new Date());
    const formattedDate = format(date, 'dd-MM-yyyy'); // Example: "22/12/2020"
    return formattedDate
  }
  handleshowClick() {
    console.log(this.fileDetailsFormGroup.value)
  }

  exportExcel() {
    this.excelService.exportExcel('File-Monitor.xlsx', this.table_data_copy);
  }
  getDistinctValuesForColumns(tableData: any[], columns: string[]) {

    const result: any = [];

    columns.forEach((column) => {
      const uniqueValues = getUniqueValues(tableData, column);

      const mappedArray = getMappedArray(uniqueValues);
      result[column] = [...mappedArray];
    });

    return result;
  }
  dataProviderOnchange() {
    this.dataProviderValChangeFlag = true;
  }
}
