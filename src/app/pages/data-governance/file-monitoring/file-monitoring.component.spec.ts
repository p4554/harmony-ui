import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FileMonitoringComponent } from './file-monitoring.component';

describe('FileMonitoringComponent', () => {
  let component: FileMonitoringComponent;
  let fixture: ComponentFixture<FileMonitoringComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FileMonitoringComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FileMonitoringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
