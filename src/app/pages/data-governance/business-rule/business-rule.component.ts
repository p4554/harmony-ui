import { Component, ViewChild } from '@angular/core';
import { OverlayPanel } from 'primeng/overlaypanel';
import { DialogService } from 'primeng/dynamicdialog';
import { LoaderService } from '@app/_services/loading.service';
import { NewCustomerRuleComponent } from '../../../components/business-rule-components/new-customer-rule/new-customer-rule.component';
import { NewMarketRuleComponent } from '@app/components/business-rule-components/new-market-rule/new-market-rule.component';
import { NewNormalizationRuleComponent } from '@app/components/business-rule-components/new-normalization-rule/new-normalization-rule.component';
import { NewZipRuleComponent } from '@app/components/business-rule-components/new-zip-rule/new-zip-rule.component';
import { NewCustomerTerritoryRuleComponent } from '@app/components/business-rule-components/new-customer-territory-rule/new-customer-territory-rule.component';
import { NewEmployeeTerritoryRuleComponent } from '@app/components/business-rule-components/new-employee-territory-rule/new-employee-territory-rule.component';
import { NewGroupMapRuleComponent } from '@app/components/business-rule-components/new-group-map-rule/new-group-map-rule.component';
import { NewInclusionRuleComponent } from '@app/components/business-rule-components/new-inclusion-rule/new-inclusion-rule.component';
import { NewUpdateRuleComponent } from '@app/components/business-rule-components/new-update-rule/new-update-rule.component';
import {BusinessRuleApiService} from '../../../_services/business-rule-api.service'
@Component({
  selector: 'app-business-rule',
  templateUrl: './business-rule.component.html',
  styleUrls: ['./business-rule.component.scss'],
})
export class BusinessRuleComponent {
  showOverlayPanel: boolean;
  tab_label: string = 'Business Rules';
  customerTableData: any;
  productMarketTableData: any;
  productNormalTableData: any;
  savedCustomer:string;
  savedProduct:string;
  savedAlignment:string;
  savedSpecialty:string;
  tabs = ['Customer', 'Product', 'Alignment', 'Specialty'];
  rules: number ;
  constructor(
    private dialogService: DialogService,
    public loaderService: LoaderService,
    public BusinessRuleApiService:BusinessRuleApiService
  ) {}
  currentTab: string = 'Customer';

  setCurrentTab(event) {
    this.currentTab = this.tabs[event.index];
    console.log(event, this.currentTab);
  }
  newRuleHandler(event: Event) {
    this.showOverlayPanel = true;
  }
  newCustomerHandler(){
    this.loaderService.showLoader();

    const dialogRef = this.dialogService.open(NewCustomerRuleComponent,{
      header: 'Add Customer Rule',
      width: '60%',
      height: '80%',
    });
    dialogRef.onClose.subscribe(result => {
      
      if (result === 'saved') {
        this.savedCustomer='saved';
      }
    });
    window.addEventListener('popstate', () => {
      dialogRef.close();
    });
  }
  newMarketRuleHandler(){
    this.loaderService.showLoader();
    const dialogRef = this.dialogService.open(NewMarketRuleComponent,{
      header: 'Add Market Definition Rule',
      width: '60%',
      height: '80%',
    });
    dialogRef.onClose.subscribe(result => {

      if (result === 'savedMarket') {
        this.savedProduct='savedMarket';
      }
    });
    window.addEventListener('popstate', () => {
      dialogRef.close();
    });
  }
  newNormaliZationRuleHandler(){
    this.loaderService.showLoader();
    const dialogRef = this.dialogService.open(NewNormalizationRuleComponent,{
      header: 'Add Normalization Factor Rule',
      width: '60%',
      height: '80%',
    });
    dialogRef.onClose.subscribe(result => {

      if (result === 'savedNormal') {
        this.savedProduct='savedNormal';
      }
    });
    window.addEventListener('popstate', () => {
      dialogRef.close();
    });
  }
  newZipRuleHandler(){
    this.loaderService.showLoader();
    const dialogRef = this.dialogService.open(NewZipRuleComponent,{
      header: 'Add Zip Territory Alignment Rule',
      width: '60%',
      height: '80%',
    });
    dialogRef.onClose.subscribe(result => {

      if (result === 'savedZip') {
        this.savedAlignment='savedZip';

      }
    });
    window.addEventListener('popstate', () => {
      dialogRef.close();
    });
  }
  newCustomerTerritoryRuleHandler(){
    this.loaderService.showLoader();
    const dialogRef = this.dialogService.open(NewCustomerTerritoryRuleComponent,{
      header: 'Add Customer Territory Alignment Rule',
      width: '60%',
      height: '80%',
    });
    dialogRef.onClose.subscribe(result => {

      if (result === 'savedCustomer') {
        this.savedAlignment='savedCustomer';

      }
    });
    window.addEventListener('popstate', () => {
      dialogRef.close();
    });
  }
  newEmployeeRuleRuleHandler(){
    this.loaderService.showLoader();
    const dialogRef = this.dialogService.open(NewEmployeeTerritoryRuleComponent,{
      header: 'Add Employee Territory Alignment Rule',
      width: '60%',
      height: '80%',
    });
    dialogRef.onClose.subscribe(result => {

      if (result === 'savedEmployee') {
        this.savedAlignment='savedEmployee';

      }
    });
    window.addEventListener('popstate', () => {
      dialogRef.close();
    });
  }
  newGroupMapRuleHandler(){
    this.loaderService.showLoader();
    const dialogRef = this.dialogService.open(NewGroupMapRuleComponent,{
      header: 'Add Specialty Group Map Rule',
      width: '60%',
      height: '80%',
    });
    dialogRef.onClose.subscribe(result => {

      if (result === 'savedGroupMapRule') {
        this.savedSpecialty='savedGroupMapRule';

      }
    });
    window.addEventListener('popstate', () => {
      dialogRef.close();
    });
  }
  newUpdateRuleHandler(){
    this.loaderService.showLoader();
    const dialogRef = this.dialogService.open(NewUpdateRuleComponent,{
      header: 'Add Specialty Update Rule',
      width: '60%',
      height: '80%',
    });
    dialogRef.onClose.subscribe(result => {

      
      if (result === 'savedUpdateRule') {
        this.savedSpecialty='savedUpdateRule';

      }
    });
    window.addEventListener('popstate', () => {
      dialogRef.close();
    });
  }
  newInclusionRuleRuleHandler(){
    this.loaderService.showLoader();
    const dialogRef = this.dialogService.open(NewInclusionRuleComponent,{
      header: 'Add specialty Inclusion Rule',
      width: '60%',
      height: '80%',
    });
    dialogRef.onClose.subscribe(result => {

     
      if (result === 'savedInclusions') {
        this.savedSpecialty='savedInclusions';

      }
    });
    window.addEventListener('popstate', () => {
      dialogRef.close();
    });
  }
  exportExcel() {}

  ruleCountSetter(count) {
    this.rules = count;
  }
}
