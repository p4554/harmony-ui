import { Component,  ViewChild,  OnInit } from '@angular/core';
import { LoaderService } from '@app/_services/loading.service';
import { CommonDataService } from '@app/_services/common-data.service';
@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent {

  notificationData: any = []
  table_data_copy: any = []
  dateTimeArray: any = []
  beforeTime:any=[]
  constructor(
    public loaderService: LoaderService,
    private commonDataService: CommonDataService,

  ) { }
 
  async ngOnInit() {
    this.loaderService.showLoader();
   await this.commonDataService.fetchNotifications().toPromise()
    .then((data: any) => {
      this.notificationData = data;
    })
    console.log('data',this.notificationData)
   for(let i=0;i<this.notificationData.length;i++) {
    this.dateTimeArray[i] = await this.extractDateTime(this.notificationData[i].JOB_NOTIFICATION);
   }
   this.beforeTime = await this.calculateTimeBefore(this.dateTimeArray)
   this.loaderService.hideLoader();
  }
   extractDateTime(data) {
    this.loaderService.showLoader();
    const dateTimeString = data.split(" at ")[1]; 
    const dateTime = new Date(dateTimeString); 
  
    const date = dateTime.toDateString(); 
    const time = dateTime.toLocaleTimeString(); 
  
    return { date, time };
  }
   calculateTimeBefore(messages: { date: string, time: string }[]): string[] {
    this.loaderService.showLoader();

    const now = new Date(); 
  
    const timeBefore: string[] = [];
  
    for (const message of messages) {
      const messageDateTime = new Date(message.date + ' ' + message.time);
      const timeDifference = now.getTime() - messageDateTime.getTime(); 
  
      if (timeDifference >= 24 * 60 * 60 * 1000) {
        const days = Math.floor(timeDifference / (24 * 60 * 60 * 1000));
        timeBefore.push(days === 1 ? '1 day' : days + ' days');
      } else {
        const hours = Math.floor(timeDifference / (60 * 60 * 1000));
        timeBefore.push(hours === 1 ? '1 hour' : hours + ' hours');
      }
    }
  
    return timeBefore;
  }
  
}
