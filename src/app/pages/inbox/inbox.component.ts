import { Component, ViewChild, OnInit } from '@angular/core';
import { LoaderService } from '@app/_services/loading.service';
import { CommonDataService } from '@app/_services/common-data.service';
@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss'],
})
export class InboxComponent {
  inboxData: any = [];
  table_data_copy: any = [];
  dateTimeArray: any = [];
  beforeTime: any = [];
  constructor(
    public loaderService: LoaderService,
    private commonDataService: CommonDataService
  ) {}
  async ngOnInit() {
    this.loaderService.showLoader();
    await this.commonDataService
      .fetchInbox()
      .toPromise()
      .then((data: any) => {
        this.inboxData = data;
        console.log(this.inboxData);
      });
      this.inboxData.reverse();
    for (let i = 0; i < this.inboxData.length; i++) {
      this.dateTimeArray[i] = this.inboxData[i].CREATD_DT;
    }

    this.beforeTime = await this.calculateTimeBefore(this.dateTimeArray);
    console.log('date', this.dateTimeArray, this.beforeTime);

    this.loaderService.hideLoader();
  }
  calculateTimeBefore(dateArray: string[]): string[] {
    const now = new Date();
    const timeBefore: string[] = [];

    for (const dateStr of dateArray) {
      const messageDateTime = new Date(dateStr);
      const timeDifference = now.getTime() - messageDateTime.getTime();

      if (timeDifference >= 24 * 60 * 60 * 1000) {
        const days = Math.floor(timeDifference / (24 * 60 * 60 * 1000));
        timeBefore.push(days === 1 ? '1 day' : days + ' days');
      } else {
        const hours = Math.floor(timeDifference / (60 * 60 * 1000));
        timeBefore.push(hours === 1 ? '1 hour' : hours + ' hours');
      }
    }

    return timeBefore;
  }
}
