import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared.module';
import { DataSourcePageComponent } from './data-source-page.component';

@NgModule({
  declarations: [DataSourcePageComponent],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [DataSourcePageComponent],
})
export class DataSourcePageModule {}
