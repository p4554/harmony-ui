import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonDataService } from '@app/_services/common-data.service';

@Component({
	selector: 'app-data-source-page',
	templateUrl: './data-source-page.component.html',
	styleUrls: ['./data-source-page.component.scss'],
})
export class DataSourcePageComponent implements OnInit {
	tabledata: { head: string[]; body: any };
	menudata = [
		{
			items: [
				{
					label: 'Edit Metadata',
					icon: '',
					command: (e: Event) => {
						this.editMetadata();
						console.log();
					},
				},
				{
					label: 'View Summary',
					icon: '',
					command: () => {
						this.viewSummary();
					},
				},
				{
					label: 'View Insights',
					icon: '',
					command: () => {
						this.viewInsights();
					},
				},
				{
					label: 'Approve Request',
					icon: '',
					command: () => {
						this.approveRequest();
					},
				},
				{
					label: 'Send Reminder to Approvers',
					icon: '',
					command: () => {
						this.approverReminder();
					},
				},
				{
					label: 'Delete',
					icon: '',
					command: () => {
						this.delete();
					},
				},
			],
		},
	];

	selectedRow: any;
	editMetadata() {
		console.log('editMetadata', this.selectedRow);
	}
	viewSummary() {
		console.log('View Summary', this.selectedRow);
	}
	viewInsights() {
		console.log('View Insights', this.selectedRow);
	}
	approveRequest() {
		console.log('Approve Request', this.selectedRow);
	}
	approverReminder() {
		console.log('Send Reminder to Approvers', this.selectedRow);
	}
	delete() {
		console.log('Delete', this.selectedRow);
	}
	constructor(private commonDataService: CommonDataService, private router: Router) {}
	ngOnInit() {
		let tableCell = ['id', 'requestor', 'datasource', 'sourcetype', 'reason', 'Approvers'];

		this.commonDataService.getOnBoardData().subscribe((data: any) => {
			let onboardBody = data;
			// this.tabledata.body = onboardBody
			let tableBodyData = [];
			onboardBody.forEach((data) => {
				let Obj = {};
				tableCell.forEach((element) => {
					Obj[element] = data[element];
				});

				tableBodyData.push(Obj);
			});

			this.tabledata = {
				head: ['ID', 'Requestor Info', 'Data Source', 'Source Type', 'Reason', 'Approvers', 'Actions'],
				body: tableBodyData,
			};
			console.log(tableBodyData);
		});
	}

	setSelectedRow(data) {
		this.selectedRow = data;
	}
	setCurrentTab(data) {
		console.log(data);
	}
	navigationHandler() {
		this.router.navigateByUrl('/advanced-search');
	}
}
