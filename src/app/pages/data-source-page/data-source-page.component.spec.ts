import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataSourcePageComponent } from './data-source-page.component';

describe('DataSourcePageComponent', () => {
  let component: DataSourcePageComponent;
  let fixture: ComponentFixture<DataSourcePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataSourcePageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DataSourcePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
