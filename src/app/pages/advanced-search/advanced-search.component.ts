import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { CommonDataService } from '@app/_services/common-data.service';
import { MessageService } from 'primeng/api';

@Component({
	selector: 'app-advanced-search',
	templateUrl: './advanced-search.component.html',
	styleUrls: ['./advanced-search.component.scss'],
	providers: [MessageService],
})
export class AdvancedSearchComponent implements OnInit {
	searchInput: any = '';
	sub_searchInput: any;
	tabledata: {
		head: string[];
		body: any;
	};
	tableSelectedInput: any[] = [];
	btnStatus: boolean = true;
	toastData: any;
	source_listBoxData = {
		options: [
			{
				lable: 'one',
				code: 'l-1',
			},
			{
				lable: 'two',
				code: 'l-2',
			},
			{
				lable: 'three',
				code: 'l-3',
			},
			{
				lable: 'four',
				code: 'l-4',
			},
			{
				lable: 'five',
				code: 'l-5',
			},
			{
				lable: 'six',
				code: 'l-6',
			},
			{
				lable: 'seven',
				code: 'l-7',
			},
			{
				lable: 'eight',
				code: 'l-8',
			},
		],
		head_text: 'Source Type ',
	};
	owners_listBoxData = {
		options: [
			{
				lable: 'one',
				code: 'l-1',
			},
			{
				lable: 'two',
				code: 'l-2',
			},
			{
				lable: 'three',
				code: 'l-3',
			},
			{
				lable: 'four',
				code: 'l-4',
			},
			{
				lable: 'five',
				code: 'l-5',
			},
			{
				lable: 'six',
				code: 'l-6',
			},
			{
				lable: 'seven',
				code: 'l-7',
			},
			{
				lable: 'eight',
				code: 'l-8',
			},
		],
		head_text: 'Owners',
	};
	popupModelData = {
		displayModal: false,
		header: 'Share to',
	};
	//empty / inter / done
	status = 'empty';

	// inputform values

	formcontrolValue = {
		formControl: new FormControl('', [Validators.required]),
		type: 'text',
		placeholder: 'Placeholder',
		icon: 'pi pi-check',
		direction: 'left',
	};
	constructor(private commonDataService: CommonDataService, private toast: MessageService) { }

	ngOnInit() {
		this.status = 'empty';

		let tableCellData = ['id', 'datasource', 'sourcetype', 'owners'];
		this.tabledata = {
			head: ['Select', 'ID', 'Data Source', 'Source Type', 'Owners', 'Action'],
			body: [],
		};

		// this.commonDataService.getOnBoardData().subscribe((data: any) => {
		// 	let arr = [];
		// 	data.forEach((data: any, i: number) => {
		// 		let obj = {};
		// 		let input = {
		// 			// lable: 'Item ' + (i + 1),
		// 			selected: false,
		// 			type: 'checkbox',
		// 		};
		// 		obj['input'] = input;
		// 		tableCellData.forEach((element) => {
		// 			obj[element] = data[element];
		// 		});

		// 		arr.push(obj);
		// 	});
		// 	this.tabledata.body = arr;
		// });
	}

	search() {
		this.status = 'inter';

		setTimeout(() => {
			this.status = 'done';
		}, 2000);

		console.log(this.searchInput);
	}
	sub_search() {
		console.log(this.sub_searchInput);
	}
	checkBoxInputHandler(data) {
		if (data.length > 0) {
			this.tableSelectedInput = data;
			this.btnStatus = false;
		} else {
			this.btnStatus = true;
		}
		console.log(data);
	}
	shareBtnHandler() {
		this.popupModelData.displayModal = true;
	}
	singleSelectValHandler(data) {
		console.log(data);
		this.toast.add({
			severity: 'success',
			summary: 'Message Sent Successfully',
			detail: 'this is details Ofthe Message',
		});
	}
}
