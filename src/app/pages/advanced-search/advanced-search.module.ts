import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ToastModule } from 'primeng/toast';
import { AdvancedSearchComponent } from './advanced-search.component';
import { SharedModule } from '@app/shared.module';
@NgModule({
  declarations: [AdvancedSearchComponent],
  imports: [
    CommonModule,
    SharedModule,
    ToastModule,
  ],
  exports: [AdvancedSearchComponent],
})
export class AdvancedSearchModule { }
