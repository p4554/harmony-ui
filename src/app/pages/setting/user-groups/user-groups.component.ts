import { Component,ViewChild,OnInit } from '@angular/core';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import {AdminApiService} from '../../../_services/admin-api.service'
import { LoaderService } from '@app/_services/loading.service';
import {  getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { DialogService } from 'primeng/dynamicdialog';
import { PrimeMultiSelectComponent } from '@app/components/form-components/prime-multi-select/prime-multi-select.component';
import { NewGroupConfigComponent } from '@app/components/new-group-config/new-group-config.component';
import { EditGroupConfigComponent } from '@app/components/edit-group-config/edit-group-config.component';
import { MessageService } from 'primeng/api';
import { tableFilter } from '@app/util/helper/harmonyUtils';
@Component({
  selector: 'app-user-groups',
  templateUrl: './user-groups.component.html',
  styleUrls: ['./user-groups.component.scss']
})
export class UserGroupsComponent {
  @ViewChild('username') usernameFilterComponent: PrimeMultiSelectComponent;
	@ViewChild('groupname') groupnameFilterComponent: PrimeMultiSelectComponent;
	@ViewChild('active') activeFilterComponent: PrimeMultiSelectComponent;

table_data:any=[]
table_data_copy:any=[]
filterComponents: any;
user_Name:any
group_Name:any
active_status:any
popupModelData:any
popupModelTrigger: boolean = false;
uniqueValues:any;
tablePageNumber:number = 0
tab_label = 'Users Group Configuration'
Column = {
  userName: "USER_NM",
  groupName: "USRGRP_NM",
  activeStatus: "ACTV_FLG",
}

columnArrays = [
  this.Column.userName,
  this.Column.groupName,
  this.Column.activeStatus,
];

activeFiltered = {
  userName: { valArr: [], column: 'USER_NM' },
  groupName: { valArr: [], column: 'USRGRP_NM' },
  activeStatus: { valArr: [], column: 'ACTV_FLG' },
}
transformedActiveValues: any[] = [];

constructor(
private AdminApiService: AdminApiService,
public loaderService: LoaderService,
private dialogService: DialogService,
private toastService: MessageService,
){}
 async ngOnInit(){
   
     this.loaderService.showLoader();
      await this.getResponseHandler();

  }
async getResponseHandler(){
  this.loaderService.showLoader();
  this.table_data=await lastValueFrom(
     this.AdminApiService.userGroupTable()
  )
  this.table_data_copy = [...this.table_data];
  this.uniqueValues=this.getDistinctValuesForColumns(this.table_data,this.columnArrays)
  this.tablePageNumber = 0
 
  setTimeout(() => {
    this.filterComponents = [
      { selected: 'usernameFilterSelected', component: this.usernameFilterComponent },
      { selected: 'groupnameFilterFilterSelected', component: this.groupnameFilterComponent },
      { selected: 'activeFilterSelected', component: this.activeFilterComponent },      
    ];
  }, 300);
  if (this.activeFiltered) {
    this.table_data = tableFilter(this.activeFiltered,this.table_data_copy);
  }
  this.transformedActiveValues = this.getTransformedValues(this.uniqueValues?.[this.Column.activeStatus]);
  this.loaderService.hideLoader();
}

getTransformedValues(values: any[]) {
  const transformedValues = [];
  for (const value of values) {
     if(value.value === 'Y'){
      transformedValues.push({ label: 'Yes', value: 'Y' });
    } else if(value.value === 'N'){
      transformedValues.push({ label: 'No', value: 'N' });
    }else{
      transformedValues.push(value);
        }
  }
  return transformedValues;
}
getDistinctValuesForColumns = (tableData: any[], columns: string[]) => {
  const result: any = [];
  columns.forEach((column) => {
    const uniqueValues = getUniqueValues(tableData, column);
    const mappedArray = getMappedArray(uniqueValues);
    result[column] = [...mappedArray];
  });
  return result;
  };


filterChangeHandler(filterArray, column) {
  const filterArrayCopy = filterArray;
  if (column === 'USER_NM') {
    this.activeFiltered.userName.valArr = filterArrayCopy;
  }
  else if (column === 'USRGRP_NM') {
    this.activeFiltered.groupName.valArr = filterArrayCopy;
  }
  else if (column === 'ACTV_FLG') {
    this.activeFiltered.activeStatus.valArr = filterArrayCopy;
  }
  const result=tableFilter(this.activeFiltered,this.table_data_copy)

  if (result.length){
  this.table_data=result
  this.tablePageNumber = 0
 }
 else{
  if(!Object.keys(this.activeFiltered).some(field=>this.activeFiltered[field].valArr.length)){
    this.table_data = this.table_data_copy
  }else {
    this.table_data = result
  }
 }
 this.setFilterOptions(this.activeFiltered,result)  

}
setFilterOptions(fieldObject,result){

  const filterFields = Object.keys(fieldObject)

  for(let field of filterFields){
    if(!fieldObject[field].valArr.length){
    this.uniqueValues[fieldObject[field].column] = getMappedArray(getUniqueValues(result, fieldObject[field].column));
    }
  }
  }

  resetAll() {
    this.filterComponents.forEach((item) => {
      item.component.resetSelection();
    });
    this.table_data = this.table_data_copy;
    this.activatedFilterReset(this.activeFiltered)
    this.uniqueValues = this.getDistinctValuesForColumns(this.table_data, this.columnArrays);
  }
  activatedFilterReset(fieldObject:any){
    const filterFields = Object.keys(fieldObject)
    filterFields.forEach(key=> fieldObject[key].valArr = [])
  }


newUserHandler(){
  this.loaderService.showLoader();
  const dialogRef = this.dialogService.open(NewGroupConfigComponent, {
    header: 'New Group Configuration',
    width: '60%',
    height: '65%',
  });
  dialogRef.onClose.subscribe(result => {

    if (result === 'saved') {
      this.getResponseHandler();
    }

  });
  window.addEventListener('popstate', () => {
    dialogRef.close();
  });
}
editUserHandler(editData:any){
  this.loaderService.showLoader();
  const dialogRef = this.dialogService.open(EditGroupConfigComponent, {
    header: 'Edit Group Configuration',
    width: '60%',
    height: '65%',
    data:editData,
  });
  dialogRef.onClose.subscribe(result => {

    if (result === 'saved') {
      this.getResponseHandler();
    }

  });
  window.addEventListener('popstate', () => {
    dialogRef.close();
  });
}
 
deletePopupModel: any;
tableSelectedRowIndex: number;
deleteHandler(index: number) {
  this.deletePopupModel = {
    displayModal: true,
    header: 'Delete Configuration',
    icon: 'pi-exclamation-triangle',
    content: {
      header: `Delete Configuration`,
      text: 'This action will delete the configuration permanently',
    },
    deleteBtnProps: {
      label: 'Delete',
      click: '',
    },
    cancelBtnProps: {
      label: 'Cancel',
      click: '',
    },
  };

  this.tableSelectedRowIndex = index;
}
deletePopupDeleteHandler = () => {
  console.log(this.tableSelectedRowIndex)
  let user_group_id = this.table_data[this.tableSelectedRowIndex].USRGRP_USR_ID;
  const postData={
    userGroupUserId:user_group_id
  }
  this.deletePopupModel.displayModal = false;
  this.loaderService.showLoader();
  this.AdminApiService.deleteUserGroup(postData).subscribe((response) => {
    this.loaderService.hideLoader();
    if (response.status == 200) {
      this.toastService.clear();
      this.toastService.add({
        key: 'err',
        closable: false,
        sticky: false,
        severity: '',
        summary: '',
        detail: `Configuration Deleted Successfully`,
      });
      this.getResponseHandler();
    } else {
      console.log(response);
      this.toastService.clear();
      this.toastService.add({
        key: 'wrn',
        closable: false,
        sticky: false,
        severity: '',
        summary: '',
        detail: `Connection Error TryAgain`,
      });
    }
  });
};
}
