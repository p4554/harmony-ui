import { Component,ViewChild,OnInit } from '@angular/core';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import {AdminApiService} from '../../../_services/admin-api.service'
import { LoaderService } from '@app/_services/loading.service';
import { getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { DialogService } from 'primeng/dynamicdialog';
import { PrimeMultiSelectComponent } from '@app/components/form-components/prime-multi-select/prime-multi-select.component';
import {NewUserAccessConfigComponent} from '../../../components/new-user-access-config/new-user-access-config.component';
import { EditUserAccessConfigComponent } from '@app/components/edit-user-access-config/edit-user-access-config.component';
import { MessageService } from 'primeng/api';
import { tableFilter } from '@app/util/helper/harmonyUtils';
@Component({
  selector: 'app-user-access',
  templateUrl: './user-access.component.html',
  styleUrls: ['./user-access.component.scss']
})
export class UserAccessComponent {
  @ViewChild('GroupNameFilter') userGroupNameFilterComponent: PrimeMultiSelectComponent;
	@ViewChild('tenantNameFilter') tenantNameFilterComponent: PrimeMultiSelectComponent;
	@ViewChild('moduleNameFilter') modulenameFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('activeFilter') activeFilterComponent: PrimeMultiSelectComponent;

tab_label = 'User Access Configuration' 
table_data:any=[]
table_data_copy:any=[]
filterComponents: any;

popupModelData:any
popupModelTrigger: boolean = false;
module:any
uniqueValues:any;
tablePageNumber:number = 0
transformedActiveValues: any[] = [];
Column = {
  userGroupName: "USRGRP_NM",
  tenantName: "TNANT_NM",
  module: "LOV_DESC",
  activeStatus: "ACTV_FLG",
}

columnArrays = [
  this.Column.userGroupName,
  this.Column.tenantName,
  this.Column.module,
  this.Column.activeStatus,
];

activeFiltered = {
  userGroupName: { valArr: [], column: 'USRGRP_NM' },
  tenantName: { valArr: [], column: 'TNANT_NM' },
  module: { valArr: [], column: 'LOV_DESC' },
  activeStatus: { valArr: [], column: 'ACTV_FLG' },
  
}

constructor(
private AdminApiService: AdminApiService,
public loaderService: LoaderService,
private dialogService: DialogService,
private toastService: MessageService,
){}
 async ngOnInit(){
   
     this.loaderService.showLoader();
      await this.getResponseHandler();

  }
async getResponseHandler(){
  this.loaderService.showLoader();
  this.table_data=await lastValueFrom(
     this.AdminApiService.userAccessTable()
  )
  console.log(this.table_data);
  this.table_data_copy = [...this.table_data];
  this.uniqueValues=this.getDistinctValuesForColumns(this.table_data,this.columnArrays)
  this.tablePageNumber = 0
 
  setTimeout(() => {
    this.filterComponents = [
      { selected: 'userGroupNameFilterSelected', component: this.userGroupNameFilterComponent },
      { selected: 'tenantNameFilterFilterSelected', component: this.tenantNameFilterComponent },
      { selected: 'modulenameFilterSelected', component: this.modulenameFilterComponent },      
      { selected: 'activeFilterSelected', component: this.activeFilterComponent },      
    ];
  }, 300);
  if (this.activeFiltered) {
    this.table_data = tableFilter(this.activeFiltered,this.table_data_copy);
  }
  this.transformedActiveValues = this.getTransformedValues(this.uniqueValues?.[this.Column.activeStatus]);
  this.loaderService.hideLoader();
}
getTransformedValues(values: any[]) {
  const transformedValues = [];
  for (const value of values) {
     if(value.value === 'Y'){
      transformedValues.push({ label: 'Yes', value: 'Y'});
    } else if(value.value === 'N'){
      transformedValues.push({ label: 'No', value: 'N' });
    }else{
      transformedValues.push(value);
        }
  }
  return transformedValues;
}
getDistinctValuesForColumns = (tableData: any[], columns: string[]) => {
  const result: any = [];
  columns.forEach((column) => {
    const uniqueValues = getUniqueValues(tableData, column);
    const mappedArray = getMappedArray(uniqueValues);
    result[column] = [...mappedArray];
  });
  return result;
  };


filterChangeHandler(filterArray, column) {
  const filterArrayCopy = filterArray;
  if (column === 'USRGRP_NM') {
    this.activeFiltered.userGroupName.valArr = filterArrayCopy;
  }
  else if (column === 'TNANT_NM') {
    this.activeFiltered.tenantName.valArr = filterArrayCopy;
  }
  else if (column === 'LOV_DESC') {
    this.activeFiltered.module.valArr = filterArrayCopy;
  }
  else if (column === 'ACTV_FLG') {
    this.activeFiltered.activeStatus.valArr = filterArrayCopy;
  }
  const result=tableFilter(this.activeFiltered,this.table_data_copy)

  if (result.length){
  this.table_data=result
  this.tablePageNumber = 0
 }
 else{
  if(!Object.keys(this.activeFiltered).some(field=>this.activeFiltered[field].valArr.length)){
    this.table_data = this.table_data_copy
  }else {
    this.table_data = result
  }
 }
 this.setFilterOptions(this.activeFiltered,result)  

}
setFilterOptions(fieldObject,result){

  const filterFields = Object.keys(fieldObject)

  for(let field of filterFields){
    if(!fieldObject[field].valArr.length){
    this.uniqueValues[fieldObject[field].column] = getMappedArray(getUniqueValues(result, fieldObject[field].column));
    }
  }
  }
  resetAll() {
    this.filterComponents.forEach((item) => {
      console.log(item)
      item.component.resetSelection();
    });
    this.table_data = this.table_data_copy;
    this.activatedFilterReset(this.activeFiltered)
    this.uniqueValues = this.getDistinctValuesForColumns(this.table_data, this.columnArrays);
  }
  activatedFilterReset(fieldObject:any){
    const filterFields = Object.keys(fieldObject)
    filterFields.forEach(key=> fieldObject[key].valArr = [])
  }





newUserHandler(){
  this.loaderService.showLoader();
  const dialogRef = this.dialogService.open(NewUserAccessConfigComponent, {
    header: 'New User Access Configuration',
    width: '60%',
    height: '60%',
  });
  dialogRef.onClose.subscribe(result => {

    if (result === 'saved') {
      this.getResponseHandler();
    }

  });
  window.addEventListener('popstate', () => {
    dialogRef.close();
  });
}
editUserHandler(editData:any){
  this.loaderService.showLoader();
  const dialogRef = this.dialogService.open(EditUserAccessConfigComponent, {
    header: 'Edit User Access Configuration',
    width: '60%',
    height: '60%',
    data:editData
  });
  dialogRef.onClose.subscribe(result => {
    if (result === 'saved') {
      this.getResponseHandler();
    }
  });
  window.addEventListener('popstate', () => {
    dialogRef.close();
  });
}
 
deletePopupModel: any;
tableSelectedRowIndex: number;
deleteHandler(index: number) {
  this.deletePopupModel = {
    displayModal: true,
    header: 'Delete Configuration',
    icon: 'pi-exclamation-triangle',
    content: {
      header: `Delete Configuration`,
      text: 'This action will delete the configuration permanently',
    },
    deleteBtnProps: {
      label: 'Delete',
      click: '',
    },
    cancelBtnProps: {
      label: 'Cancel',
      click: '',
    },
  };

  this.tableSelectedRowIndex = index;
}
deletePopupDeleteHandler = () => {
  let user_id = this.table_data[this.tableSelectedRowIndex].USRGRP_MDUL_ID;
  const postData={
    userGroupModuleId:user_id
  }
  this.deletePopupModel.displayModal = false;
  this.loaderService.showLoader();
  this.AdminApiService.deleteUserAccess(postData).subscribe((response) => {
    this.loaderService.hideLoader();
    if (response.status == 200) {
      this.toastService.clear();
      this.toastService.add({
        key: 'err',
        closable: false,
        sticky: false,
        severity: '',
        summary: '',
        detail: `Configuration Deleted Successfully`,
      });
      this.getResponseHandler();
    } else {
      console.log(response);
      this.toastService.clear();
      this.toastService.add({
        key: 'wrn',
        closable: false,
        sticky: false,
        severity: '',
        summary: '',
        detail: `Connection Error TryAgain`,
      });
    }
  });
};


}
