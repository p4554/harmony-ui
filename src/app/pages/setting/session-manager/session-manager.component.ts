import { Component,ViewChild,OnInit } from '@angular/core';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import {AdminApiService} from '../../../_services/admin-api.service'
import { LoaderService } from '@app/_services/loading.service';
import { getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { PrimeMultiSelectComponent } from '@app/components/form-components/prime-multi-select/prime-multi-select.component';
import { tableFilter } from '@app/util/helper/harmonyUtils';
@Component({
  selector: 'app-session-manager',
  templateUrl: './session-manager.component.html',
  styleUrls: ['./session-manager.component.scss']
})
export class SessionManagerComponent {
  @ViewChild('sessionIdFilter') sessionidFilterComponent: PrimeMultiSelectComponent;
	@ViewChild('userNameFilter') usernameFilterComponent: PrimeMultiSelectComponent;
	@ViewChild('sessionActivityFilter') sessionActivityFilterComponent: PrimeMultiSelectComponent;
	@ViewChild('sessionStartFilter')  sessionStartFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('sessionStopFilter') sessionStopFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('ipAddressFilter') ipAddressFilterComponent: PrimeMultiSelectComponent;


tab_label = 'Session Manager' 
table_data:any=[]
table_data_copy:any=[]
filterComponents: any;
popupModelData:any
popupModelTrigger: boolean = false;
uniqueValues:any;
tablePageNumber:number = 0
Column = {
  sessionId: "SESSN_ID",
  userName: "USER_NM",
  activity: "USERSESSN_ACTVTY_NM",
  sessionStart: "SESSN_START",
  sessionStop: "SESSN_END",
  ipAddress: "IP_ADD"
}

columnArrays = [
  this.Column.sessionId,
  this.Column.userName,
  this.Column.activity,
  this.Column.sessionStart,
  this.Column.sessionStop,
  this.Column.ipAddress
];

activeFiltered = {
  sessionId: { valArr: [], column: 'SESSN_ID' },
  userName: { valArr: [], column: 'USER_NM' },
  activity: { valArr: [], column: 'USERSESSN_ACTVTY_NM' },
  sessionStart: { valArr: [], column: 'SESSN_START' },
  sessionStop: { valArr: [], column: 'SESSN_END' },
  ipAddress: { valArr: [], column: 'IP_ADD' }
}

constructor(
private AdminApiService: AdminApiService,
public loaderService: LoaderService,
){}
 async ngOnInit(){
   
     this.loaderService.showLoader();
      await this.getResponseHandler();

  }
async getResponseHandler(){
  this.loaderService.showLoader();
  this.table_data=await lastValueFrom(
     this.AdminApiService.sessionManagerTable()
  )
  this.table_data_copy = [...this.table_data];
  this.uniqueValues=this.getDistinctValuesForColumns(this.table_data,this.columnArrays)
  this.tablePageNumber = 0 
  
  setTimeout(() => {
    this.filterComponents = [
      { selected: 'sessionidFilterSelected', component: this.sessionidFilterComponent },
      { selected: 'usernameFilterFilterSelected', component: this.usernameFilterComponent },
      { selected: 'sessionActivityFilterSelected', component: this.sessionActivityFilterComponent },
      { selected: 'sessionStartFilterSelected', component: this.sessionStartFilterComponent },
      { selected: 'sessionStopFilterSelected', component: this.sessionStopFilterComponent },
      { selected: 'ipAddressFilterSelected', component: this.ipAddressFilterComponent },
    ];
  }, 300);
  if (this.activeFiltered) {
    this.table_data = tableFilter(this.activeFiltered,this.table_data_copy);
  }
  this.loaderService.hideLoader();
}
getDistinctValuesForColumns = (tableData: any[], columns: string[]) => {
  const result: any = [];
  columns.forEach((column) => {
    const uniqueValues = getUniqueValues(tableData, column);
    const mappedArray = getMappedArray(uniqueValues);
    result[column] = [...mappedArray];
  });
  return result;
  };


filterChangeHandler(filterArray, column) {
  console.log(filterArray, column);
  const filterArrayCopy = filterArray;
  if (column === 'SESSN_ID') {
    this.activeFiltered.sessionId.valArr = filterArrayCopy;
  }
  else if (column === 'USER_NM') {
    this.activeFiltered.userName.valArr = filterArrayCopy;
  }
  else if (column === 'USERSESSN_ACTVTY_NM') {
    this.activeFiltered.activity.valArr = filterArrayCopy;
  }
  else if (column === 'SESSN_START') {
    this.activeFiltered.sessionStart.valArr = filterArrayCopy;
  }
  else if (column === 'SESSN_END') {
    this.activeFiltered.sessionStop.valArr = filterArrayCopy;
  }
  else if (column === 'IP_ADD') {
    this.activeFiltered.ipAddress.valArr = filterArrayCopy;
  }
  const result=tableFilter(this.activeFiltered,this.table_data_copy)

  if (result.length){
  this.table_data=result
  this.tablePageNumber = 0
 }
 else{
  if(!Object.keys(this.activeFiltered).some(field=>this.activeFiltered[field].valArr.length)){
    this.table_data = this.table_data_copy
  }else {
    this.table_data = result
  }
 }
 this.setFilterOptions(this.activeFiltered,result)  

}
setFilterOptions(fieldObject,result){

  const filterFields = Object.keys(fieldObject)

  for(let field of filterFields){
    if(!fieldObject[field].valArr.length){
    this.uniqueValues[fieldObject[field].column] = getMappedArray(getUniqueValues(result, fieldObject[field].column));
    }
  }
  }
  resetAll() {
    this.filterComponents.forEach((item) => {
      item.component.resetSelection();
    });
    this.table_data = this.table_data_copy;
    this.activatedFilterReset(this.activeFiltered)
    this.uniqueValues = this.getDistinctValuesForColumns(this.table_data, this.columnArrays);
  }
  activatedFilterReset(fieldObject:any){
    const filterFields = Object.keys(fieldObject)
    filterFields.forEach(key=> fieldObject[key].valArr = [])
  }


}
