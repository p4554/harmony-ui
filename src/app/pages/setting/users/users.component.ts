import { Component,ViewChild,OnInit } from '@angular/core';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import {AdminApiService} from '../../../_services/admin-api.service'
import { LoaderService } from '@app/_services/loading.service';
import {  getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { DialogService } from 'primeng/dynamicdialog';
import { PrimeMultiSelectComponent } from '@app/components/form-components/prime-multi-select/prime-multi-select.component';
import { NewUserComponent } from '@app/components/new-user/new-user.component';
import { EditUserComponent } from '@app/components/edit-user/edit-user.component';
import { MessageService } from 'primeng/api';
import { tableFilter } from '@app/util/helper/harmonyUtils';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent {
  @ViewChild('userFirstName') userFirstNameFilterComponent: PrimeMultiSelectComponent;
	@ViewChild('userLastName') userLastNameFilterComponent: PrimeMultiSelectComponent;
	@ViewChild('userName') userNameFilterComponent: PrimeMultiSelectComponent;
	@ViewChild('Reporting')  ReportingFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('email') emailFilterComponent: PrimeMultiSelectComponent;



table_data:any=[]
table_data_copy:any=[]
filterComponents: any;
popupModelData:any
popupModelTrigger: boolean = false;
uniqueValues:any;
tablePageNumber:number = 0

tab_label = 'Users'
Column = {
  firstName: "FIRST_NM",
  lastName: "LAST_NM",
  userName: "USER_NM",
  reportingManager: "REPORTING_MANAGER",
  emailId: "USER_EMAIL_ID"
}
columnArrays=[
  this.Column.firstName,
  this.Column.lastName,
  this.Column.userName,
  this.Column.reportingManager,
  this.Column.emailId
]
activeFiltered={
  firstName:{valArr:[],column:'FIRST_NM'},
  lastName:{valArr:[],column:'LAST_NM'},
  userName:{valArr:[],column:'USER_NM'},
  reportingManager:{valArr:[],column:'REPORTING_MANAGER'},
  emailId:{valArr:[],column:'USER_EMAIL_ID'}
}
constructor(
private AdminApiService: AdminApiService,
public loaderService: LoaderService,
private dialogService: DialogService,
private toastService: MessageService,
){}
 async ngOnInit(){
   
     this.loaderService.showLoader();
      await this.getResponseHandler();

  }
async getResponseHandler(){
  this.loaderService.showLoader();
  this.table_data=await lastValueFrom(
     this.AdminApiService.userTable()
  )  
  
    this.table_data_copy = [...this.table_data];
    this.uniqueValues=this.getDistinctValuesForColumns(this.table_data,this.columnArrays)
    this.tablePageNumber = 0
  setTimeout(() => {
    this.filterComponents = [
      { selected: 'userFirstNameFilterSelected', component: this.userFirstNameFilterComponent },
      { selected: 'userLastNameFilterFilterSelected', component: this.userLastNameFilterComponent },
      { selected: 'userNameFilterSelected', component: this.userNameFilterComponent },
      { selected: 'ReportingFilterSelected', component: this.ReportingFilterComponent },
      { selected: 'emailFilterSelected', component: this.emailFilterComponent },
      
    ];
  }, 300);
  if (this.activeFiltered) {
    this.table_data = tableFilter(this.activeFiltered,this.table_data_copy);
  }
  this.loaderService.hideLoader();
}
getDistinctValuesForColumns = (tableData: any[], columns: string[]) => {
  const result: any = [];
  columns.forEach((column) => {
    const uniqueValues = getUniqueValues(tableData, column);
    const mappedArray = getMappedArray(uniqueValues);
    result[column] = [...mappedArray];
  });
  return result;
  };
  filterChangeHandler(filterArray, column) {
    console.log(filterArray, column);
    const filterArrayCopy = filterArray;
    if (column === 'FIRST_NM') {
      this.activeFiltered.firstName.valArr = filterArrayCopy;
    }
    else if (column === 'LAST_NM') {
      this.activeFiltered.lastName.valArr = filterArrayCopy;
    }
    else if (column === 'USER_NM') {
      this.activeFiltered.userName.valArr = filterArrayCopy;
    }
    else if (column === 'REPORTING_MANAGER') {
      this.activeFiltered.reportingManager.valArr = filterArrayCopy;
    }
    else if (column === 'USER_EMAIL_ID') {
      this.activeFiltered.emailId.valArr = filterArrayCopy;
    }
    
    const result=tableFilter(this.activeFiltered,this.table_data_copy)
   if (result.length){
    this.table_data=result
    this.tablePageNumber = 0
   }
   else{
    if(!Object.keys(this.activeFiltered).some(field=>this.activeFiltered[field].valArr.length)){
      this.table_data = this.table_data_copy
    }else {
      this.table_data = result
    }
   }
   this.setFilterOptions(this.activeFiltered,result)  

  }
  setFilterOptions(fieldObject,result){

		const filterFields = Object.keys(fieldObject)
	
		for(let field of filterFields){
		  if(!fieldObject[field].valArr.length){
			this.uniqueValues[fieldObject[field].column] = getMappedArray(getUniqueValues(result, fieldObject[field].column));
		  }
		}
	  }
    resetAll() {
      console.log(this.filterComponents);
      this.filterComponents.forEach((item) => {
        console.log(item)
        item.component.resetSelection();
      });
      this.table_data = this.table_data_copy;
      this.activatedFilterReset(this.activeFiltered)
      this.uniqueValues = this.getDistinctValuesForColumns(this.table_data, this.columnArrays);
    }
    activatedFilterReset(fieldObject:any){
		  const filterFields = Object.keys(fieldObject)
		  filterFields.forEach(key=> fieldObject[key].valArr = [])
		}




newUserHandler(){
  this.loaderService.showLoader(); 
  const dialogRef = this.dialogService.open(NewUserComponent, {
    header: 'New User',
    width: '60%',
    height: '85%',
    data: {
      tableData: this.table_data_copy
    }
  });
  dialogRef.onClose.subscribe(result => {
    if (result === 'saved') {
      this.getResponseHandler();
    }
  });
  window.addEventListener('popstate', () => {
    dialogRef.close();
  });
}
editUserHandler(editData:any){
  this.loaderService.showLoader(); 
  const dialogRef = this.dialogService.open(EditUserComponent, {
    header: 'Edit User',
    width: '60%',
    height: '80%',
    data: {
      editData: editData,
      tableData: this.table_data_copy
    },
  });
  dialogRef.onClose.subscribe(result => {
    if (result === 'saved') {
      this.getResponseHandler();
    }


  });
  window.addEventListener('popstate', () => {
    dialogRef.close();
  });
}

deletePopupModel: any;
tableSelectedRowIndex: number;
deleteHandler(index: number) {
  this.deletePopupModel = {
    displayModal: true,
    header: 'Delete User',
    icon: 'pi-exclamation-triangle',
    content: {
      header: `Delete ${this.table_data[index].USER_NM}?`,
      text: 'This action will delete the User permanently',
    },
    deleteBtnProps: {
      label: 'Delete',
      click: '',
    },
    cancelBtnProps: {
      label: 'Cancel',
      click: '',
    },
  };

  this.tableSelectedRowIndex = index;
}
deletePopupDeleteHandler = () => {
  console.log(this.tableSelectedRowIndex)
  let user_id = this.table_data[this.tableSelectedRowIndex].USER_ID;
  let user_name = this.table_data[this.tableSelectedRowIndex].USER_NM;
  let imageUrl = this.table_data[this.tableSelectedRowIndex].IMG_URL;
  const postData={
    userId: user_id,
    existing_file_name:imageUrl
  }
  this.deletePopupModel.displayModal = false;
  this.loaderService.showLoader();
  this.AdminApiService.deleteUser(postData).subscribe((response) => {
    this.loaderService.hideLoader();
    if (response.status == 200) {
      this.toastService.clear();
      this.toastService.add({
        key: 'err',
        closable: false,
        sticky: false,
        severity: '',
        summary: '',
        detail: `${user_name} Deleted Successfully`,
      });
      this.getResponseHandler();
    } else {
      console.log(response);
      this.toastService.clear();
      this.toastService.add({
        key: 'wrn',
        closable: false,
        sticky: false,
        severity: '',
        summary: '',
        detail: `Connection Error TryAgain`,
      });
    }
  });
};
}
