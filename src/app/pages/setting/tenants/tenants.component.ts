import { Component, ViewChild, OnInit } from '@angular/core';
import { QueryService } from '@app/_services/query.service';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import { LoaderService } from '@app/_services/loading.service';
import { getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { DialogService } from 'primeng/dynamicdialog';
import { NewTenantComponent } from '../../../components/new-tenant/new-tenant.component'
import { PrimeMultiSelectComponent } from '@app/components/form-components/prime-multi-select/prime-multi-select.component';
import { EditTenantComponent } from '../../../components/edit-tenant/edit-tenant.component';
import { MessageService } from 'primeng/api';
import { tableFilter } from '@app/util/helper/harmonyUtils';
import { ChangeDetectorRef } from '@angular/core';
import {AdminApiService} from '../../../_services/admin-api.service'
@Component({
  selector: 'app-tenants',
  templateUrl: './tenants.component.html',
  styleUrls: ['./tenants.component.scss']
})
export class TenantsComponent {
  @ViewChild('tenantNameFilter') tenantNameFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('regionFilter') regionFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('countryFilter') countryFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('projectFilter') projectFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('productIdFilter') productIdFilterComponent: PrimeMultiSelectComponent;
  @ViewChild('languageFilter') languageFilterComponent: PrimeMultiSelectComponent;



  table_data: any = []
  table_data_copy: any = []
  filterComponents: any;
  tenantName: any
  region: any
  country: any
  project: any
  productId: any
  language: any
  popupModelData: any
  popupModelTrigger: boolean = false;
  uniqueValues: any;
  tablePageNumber: number = 0
  tab_label = 'Tenants'
  Column = {
    tenantName: "TNANT_NM",
    region: "RGN_NM",
    country: "CNTRY_NM",
    project: "PROJ_NM",
    productId: "PROD_ID",
    language: "LANG_NM"
  }

  columnArrays = [
    this.Column.tenantName,
    this.Column.region,
    this.Column.country,
    this.Column.project,
    this.Column.productId,
    this.Column.language
  ];

  activeFiltered = {
    tenantName: { valArr: [], column: 'TNANT_NM' },
    region: { valArr: [], column: 'RGN_NM' },
    country: { valArr: [], column: 'CNTRY_NM' },
    project: { valArr: [], column: 'PROJ_NM' },
    productId: { valArr: [], column: 'PROD_ID' },
    language: { valArr: [], column: 'LANG_NM' }
  }
  constructor(
    public loaderService: LoaderService,
    private queryService: QueryService,
    private dialogService: DialogService,
    private toastService: MessageService,
    private cdRef: ChangeDetectorRef,
    private AdminApiService: AdminApiService,
  ) { }
  async ngOnInit() {
    await this.getResponseHandler();

  }
  async getResponseHandler() {
    this.loaderService.showLoader();
    this.table_data = await lastValueFrom(
      this.AdminApiService.tenantTable()
    );
    this.table_data.reverse();
    this.table_data_copy = [...this.table_data];
    this.uniqueValues = this.getDistinctValuesForColumns(this.table_data, this.columnArrays)
    this.tablePageNumber = 0


    setTimeout(() => {
      this.filterComponents = [
        { selected: 'tenantNameFilterSelected', component: this.tenantNameFilterComponent },
        { selected: 'regionFilterFilterSelected', component: this.regionFilterComponent },
        { selected: 'countryFilterSelected', component: this.countryFilterComponent },
        { selected: 'projectFilterSelected', component: this.projectFilterComponent },
        { selected: 'productIdFilterSelected', component: this.productIdFilterComponent },
        { selected: 'languageFilterSelected', component: this.languageFilterComponent },

      ];
    }, 300);

    if (this.activeFiltered) {
      this.table_data = tableFilter(this.activeFiltered, this.table_data_copy);
    }
    this.loaderService.hideLoader();
  }
  getDistinctValuesForColumns = (tableData: any[], columns: string[]) => {
    const result: any = [];
    columns.forEach((column) => {
      const uniqueValues = getUniqueValues(tableData, column);
      const mappedArray = getMappedArray(uniqueValues);
      result[column] = [...mappedArray];
    });
    return result;
  };


  filterChangeHandler(filterArray, column) {
    const filterArrayCopy = filterArray;
    if (column === 'TNANT_NM') {
      this.activeFiltered.tenantName.valArr = filterArrayCopy;
    }
    else if (column === 'RGN_NM') {
      this.activeFiltered.region.valArr = filterArrayCopy;
    }
    else if (column === 'CNTRY_NM') {
      this.activeFiltered.country.valArr = filterArrayCopy;
    }
    else if (column === 'PROJ_NM') {
      this.activeFiltered.project.valArr = filterArrayCopy;
    }
    else if (column === 'PROD_ID') {
      this.activeFiltered.productId.valArr = filterArrayCopy;
    }
    else if (column === 'LANG_NM') {
      this.activeFiltered.language.valArr = filterArrayCopy;
    }
    const result = tableFilter(this.activeFiltered, this.table_data_copy)

    if (result.length) {
      this.table_data = result
      this.tablePageNumber = 0
    }
    else {
      if (!Object.keys(this.activeFiltered).some(field => this.activeFiltered[field].valArr.length)) {
        this.table_data = this.table_data_copy
      } else {
        this.table_data = result
      }
    }
    this.setFilterOptions(this.activeFiltered, result)

  }
  setFilterOptions(fieldObject, result) {

    const filterFields = Object.keys(fieldObject)

    for (let field of filterFields) {
      if (!fieldObject[field].valArr.length) {
        this.uniqueValues[fieldObject[field].column] = getMappedArray(getUniqueValues(result, fieldObject[field].column));
      }
    }
  }
  resetAll() {
    this.filterComponents.forEach((item) => {
      item.component.resetSelection();
    });
    this.table_data = this.table_data_copy;
    this.activatedFilterReset(this.activeFiltered)
    this.uniqueValues = this.getDistinctValuesForColumns(this.table_data, this.columnArrays);
  }
  activatedFilterReset(fieldObject: any) {
    const filterFields = Object.keys(fieldObject)
    filterFields.forEach(key => fieldObject[key].valArr = [])
  }
  newTenantHandler() {
    this.loaderService.showLoader();
    const dialogRef = this.dialogService.open(NewTenantComponent, {
      header: 'New Tenant',
      width: '60%',
      height: '75%',
      data: {
        tableData: this.table_data_copy
      }
    });

    dialogRef.onClose.subscribe(result => {

      if (result === 'saved') {
        this.getResponseHandler();
      }

    });
    window.addEventListener('popstate', () => {
      dialogRef.close();
    });
  }
  editTenantHandler(editData: any) {
    this.loaderService.showLoader();
    const dialogRef = this.dialogService.open(EditTenantComponent, {
      header: 'Edit Tenant',
      width: '60%',
      height: '75%',
      data: {
        editData: editData,
        tableData: this.table_data_copy
      },
    });
    dialogRef.onClose.subscribe(result => {
      if (result === 'saved') {
        this.getResponseHandler();
      }
    });
    window.addEventListener('popstate', () => {
      dialogRef.close();
    });
  }
  deletePopupModel: any;
  selectedData: any;
  deleteHandler(data) {
    this.deletePopupModel = {
      displayModal: true,
      header: 'Delete Tenant',
      icon: 'pi-exclamation-triangle',
      content: {
        header: `Delete ${data.TNANT_NM}?`,
        text: 'This action will delete the tenant permanently',
      },
      deleteBtnProps: {
        label: 'Delete',
        click: '',
      },
      cancelBtnProps: {
        label: 'Cancel',
        click: '',
      },
    };

    this.selectedData = data;
  }
  deletePopupDeleteHandler = async () => {
    console.log(this.selectedData)
    let tenant_id = this.selectedData.TNANT_ID;
    let tenantName = this.selectedData.TNANT_NM;
    let imageUrl = this.selectedData.IMG_URL;
    const postData={
      existing_file_name:imageUrl,
      tenantId:tenant_id
    }
    this.deletePopupModel.displayModal = false;
    this.loaderService.showLoader();
    this.AdminApiService.deleteTenant(postData).subscribe((response) => {
      this.loaderService.hideLoader();
      if (response.status == 200) {
        this.toastService.clear();
        this.toastService.add({
          key: 'err',
          closable: false,
          sticky: false,
          severity: '',
          summary: '',
          detail: `${tenantName} Deleted Successfully`,
        });
        this.getResponseHandler();
      } else {
        console.log(response);
        this.toastService.clear();
        this.toastService.add({
          key: 'wrn',
          closable: false,
          sticky: false,
          severity: '',
          summary: '',
          detail: `Connection Error TryAgain`,
        });
      }
    });
  };

}
