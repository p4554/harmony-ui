import { Component,ViewChild,OnInit } from '@angular/core';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import {AdminApiService} from '../../../_services/admin-api.service'
import { LoaderService } from '@app/_services/loading.service';
import { getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { DialogService } from 'primeng/dynamicdialog';
import { PrimeMultiSelectComponent } from '@app/components/form-components/prime-multi-select/prime-multi-select.component';
import { NewUserZoneConfigComponent } from '@app/components/new-user-zone-config/new-user-zone-config.component';
import { EditUserZoneConfigComponent } from '@app/components/edit-user-zone-config/edit-user-zone-config.component';
import { MessageService } from 'primeng/api';
import { tableFilter } from '@app/util/helper/harmonyUtils';
@Component({
  selector: 'app-user-zone',
  templateUrl: './user-zone.component.html',
  styleUrls: ['./user-zone.component.scss']
})
export class UserZoneComponent {
  @ViewChild('groupname') groupnameFilterComponent: PrimeMultiSelectComponent;
	@ViewChild('zonename') zonenameFilterComponent: PrimeMultiSelectComponent;
	@ViewChild('active') activeFilterComponent: PrimeMultiSelectComponent;

table_data:any=[]
table_data_copy:any=[]
filterComponents: any;
user_Name:any
group_Name:any
active_status:any
popupModelData:any
popupModelTrigger: boolean = false;
uniqueValues:any;
tablePageNumber:number = 0
transformedActiveValues: any[] = [];

tab_label = 'Users Zone Configuration' 
Column = {
  groupName: "USRGRP_NM",
  userZone: "DATA_LYR_NAME",
  activeStatus: "ACTV_FLG",
}

columnArrays = [
  this.Column.groupName,
  this.Column.userZone,
  this.Column.activeStatus,
];

activeFiltered = {
  groupName: { valArr: [], column: 'USRGRP_NM' },
  userZone: { valArr: [], column: 'DATA_LYR_NAME' },
  activeStatus: { valArr: [], column: 'ACTV_FLG' },
}
constructor(
private AdminApiService: AdminApiService,
public loaderService: LoaderService,
private dialogService: DialogService,
private toastService: MessageService,
){}
 async ngOnInit(){
   
     this.loaderService.showLoader();
      await this.getResponseHandler();

  }
async getResponseHandler(){
  this.loaderService.showLoader();
  this.table_data=await lastValueFrom(
     this.AdminApiService.userZoneTable()
  )
  this.table_data.reverse();
    this.table_data_copy = [...this.table_data];
    this.uniqueValues=this.getDistinctValuesForColumns(this.table_data,this.columnArrays)
    this.tablePageNumber = 0
  setTimeout(() => {
    this.filterComponents = [
      { selected: 'groupnameFilterSelected', component: this.groupnameFilterComponent },
      { selected: 'zonenameFilterFilterSelected', component: this.zonenameFilterComponent },
      { selected: 'activeFilterSelected', component: this.activeFilterComponent },      
    ];
  }, 300);
  if (this.activeFiltered) {
    this.table_data = tableFilter(this.activeFiltered,this.table_data_copy);
  }
  this.transformedActiveValues = this.getTransformedValues(this.uniqueValues?.[this.Column.activeStatus]);

  this.loaderService.hideLoader();
}
getDistinctValuesForColumns = (tableData: any[], columns: string[]) => {
  const result: any = [];
  columns.forEach((column) => {
    const uniqueValues = getUniqueValues(tableData, column);
    const mappedArray = getMappedArray(uniqueValues);
    result[column] = [...mappedArray];
  });
  return result;
  };

  getTransformedValues(values: any[]) {
    const transformedValues = [];
    for (const value of values) {
       if(value.value === 'Y'){
        transformedValues.push({ label: 'Yes', value: 'Y' });
      } else if(value.value === 'N'){
        transformedValues.push({ label: 'No', value: 'N' });
      }else{
        transformedValues.push(value);
          }
    }
    return transformedValues;
  }
filterChangeHandler(filterArray, column) {
  console.log(filterArray, column);
  const filterArrayCopy = filterArray;
  if (column === 'USRGRP_NM') {
    this.activeFiltered.groupName.valArr = filterArrayCopy;
  }
  else if (column === 'DATA_LYR_NAME') {
    this.activeFiltered.userZone.valArr = filterArrayCopy;
  }
  else if (column === 'ACTV_FLG') {
    this.activeFiltered.activeStatus.valArr = filterArrayCopy;
  }
  const result=tableFilter(this.activeFiltered,this.table_data_copy)

  if (result.length){
  this.table_data=result
  this.tablePageNumber = 0
 }
 else{
  if(!Object.keys(this.activeFiltered).some(field=>this.activeFiltered[field].valArr.length)){
    this.table_data = this.table_data_copy
  }else {
    this.table_data = result
  }
 }
 this.setFilterOptions(this.activeFiltered,result)  

}
setFilterOptions(fieldObject,result){

  const filterFields = Object.keys(fieldObject)

  for(let field of filterFields){
    if(!fieldObject[field].valArr.length){
    this.uniqueValues[fieldObject[field].column] = getMappedArray(getUniqueValues(result, fieldObject[field].column));
    }
  }
  }
  resetAll() {
    this.filterComponents.forEach((item) => {
      item.component.resetSelection();
    });
    this.table_data = this.table_data_copy;
    this.activatedFilterReset(this.activeFiltered)
    this.uniqueValues = this.getDistinctValuesForColumns(this.table_data, this.columnArrays);
  }
  activatedFilterReset(fieldObject:any){
    const filterFields = Object.keys(fieldObject)
    filterFields.forEach(key=> fieldObject[key].valArr = [])
  }




newUserHandler(){
  const dialogRef = this.dialogService.open(NewUserZoneConfigComponent, {
    header: 'New User Zone Configuration',
    width: '60%',
    height: '62%',
  });
  dialogRef.onClose.subscribe(result => {

    if (result === 'saved') {
      this.getResponseHandler();
    }

  });
  window.addEventListener('popstate', () => {
    dialogRef.close();
  });
}
editUserHandler(editData:any){
  const dialogRef = this.dialogService.open(EditUserZoneConfigComponent, {
    header: 'Edit User Zone Configuration',
    width: '60%',
    height: '65%',
    data:editData,
  });
  dialogRef.onClose.subscribe(result => {

    if (result === 'saved') {
      this.getResponseHandler();
    }

  });
  window.addEventListener('popstate', () => {
    dialogRef.close();
  });
}
deletePopupModel: any;
tableSelectedRowIndex: number;
deleteHandler(index: number) {
  this.deletePopupModel = {
    displayModal: true,
    header: 'Delete Configuration',
    icon: 'pi-exclamation-triangle',
    content: {
      header: `Delete Configuration`,
      text: 'This action will delete the configuration permanently',
    },
    deleteBtnProps: {
      label: 'Delete',
      click: '',
    },
    cancelBtnProps: {
      label: 'Cancel',
      click: '',
    },
  };

  this.tableSelectedRowIndex = index;
}
deletePopupDeleteHandler = () => {
  console.log(this.tableSelectedRowIndex)
  let user_zone_id = this.table_data[this.tableSelectedRowIndex].USRGRP_LYR_ID;
  const postData={
    userGroupLayerId: user_zone_id
  }
  this.deletePopupModel.displayModal = false;
  this.loaderService.showLoader();
  this.AdminApiService.deleteUserZone(postData).subscribe((response) => {
    this.loaderService.hideLoader();
    if (response.status == 200) {
      this.toastService.clear();
      this.toastService.add({
        key: 'err',
        closable: false,
        sticky: false,
        severity: '',
        summary: '',
        detail: `Configuration Deleted Successfully`,
      });
      this.getResponseHandler();
    } else {
      console.log(response);
      this.toastService.clear();
      this.toastService.add({
        key: 'wrn',
        closable: false,
        sticky: false,
        severity: '',
        summary: '',
        detail: `Connection Error TryAgain`,
      });
    }
  });
};
}
