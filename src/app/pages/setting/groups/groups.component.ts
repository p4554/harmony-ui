import { Component,ViewChild,OnInit } from '@angular/core';
import { QueryService } from '@app/_services/query.service';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import {AdminApiService} from '../../../_services/admin-api.service'
import { LoaderService } from '@app/_services/loading.service';
import { filterIncludedValuesFromArray, getMappedArray, getUniqueValues } from '@app/util/helper/arrayHandlers';
import { DialogService } from 'primeng/dynamicdialog';
import { PrimeMultiSelectComponent } from '@app/components/form-components/prime-multi-select/prime-multi-select.component';
import {NewGroupComponent} from '../../../components/new-group/new-group.component'
import { EditGroupComponent } from '@app/components/edit-group/edit-group.component';
import { MessageService } from 'primeng/api';
import { tableFilter } from '@app/util/helper/harmonyUtils';
@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss']
})
export class GroupsComponent {
  @ViewChild('groupName') groupNameFilterComponent: PrimeMultiSelectComponent;
	@ViewChild('descriptionName') descriptionNameFilterComponent: PrimeMultiSelectComponent;
	@ViewChild('groupType') groupTypeFilterComponent: PrimeMultiSelectComponent;
	@ViewChild('active')  activeFilterComponent: PrimeMultiSelectComponent;

table_data:any=[]
table_data_copy:any=[]
filterComponents: any;
uniqueValues:any
tablePageNumber:number = 0
tab_label = 'Groups'

popupModelData:any
popupModelTrigger: boolean = false;
Column={
  groupName:"USRGRP_NM",
  description:"USRGRP_DESC",
  groupType:"GRP_TYP",
  active:"ACTV_FLG"
}
columnArrays=[
  this.Column.groupName,
  this.Column.description,
  this.Column.groupType,
  this.Column.active 
]
activeFiltered={
  groupName:{valArr:[],column:'USRGRP_NM'},
  description:{valArr:[],column:'USRGRP_DESC'},
  groupType:{valArr:[],column:'GRP_TYP'},
  active:{valArr:[],column:'ACTV_FLG'} 
}
constructor(
private AdminApiService: AdminApiService,
public loaderService: LoaderService,
private queryService: QueryService,
private dialogService: DialogService,
private toastService: MessageService,
){}

transformedGroupTypeValues: any[] = [];
transformedActiveValues: any[] = [];
 async ngOnInit(){
   
     this.loaderService.showLoader();
     await this.getResponseHandler();
     
    
  }
async getResponseHandler(){
  this.loaderService.showLoader();
  this.table_data=await lastValueFrom(
     this.AdminApiService.groupTable()
  )
    this.table_data.reverse();
    this.table_data_copy = [...this.table_data];
    this.uniqueValues=this.getDistinctValuesForColumns(this.table_data,this.columnArrays)
    this.tablePageNumber = 0
  setTimeout(() => {
    this.filterComponents = [
      { selected: 'groupNameFilterSelected', component: this.groupNameFilterComponent },
      { selected: 'descriptionNameFilterFilterSelected', component: this.descriptionNameFilterComponent },
      { selected: 'groupTypeFilterSelected', component: this.groupTypeFilterComponent },
      { selected: 'activeFilterSelected', component: this.activeFilterComponent },
      
    ];
  }, 300);
  if (this.activeFiltered) {
    this.table_data = tableFilter(this.activeFiltered,this.table_data_copy);
  }
  this.transformedGroupTypeValues = this.getTransformedValues(this.uniqueValues?.[this.Column.groupType]);
  this.transformedActiveValues = this.getTransformedValues(this.uniqueValues?.[this.Column.active]);

  this.loaderService.hideLoader();
}
getTransformedValues(values: any[]) {
  const transformedValues = [];
  for (const value of values) {
    if (value.value === 'F') {
      transformedValues.push({ label: 'Functional', value: 'F' });
    } else if (value.value === 'T') {
      transformedValues.push({ label: 'Technical', value: 'T' });
    } else if(value.value === 'Y'){
      transformedValues.push({ label: 'Yes', value: 'Y' });
    } else if(value.value === 'N'){
      transformedValues.push({ label: 'No', value: 'N' });
    }else{
      transformedValues.push(value);
        }
  }
  return transformedValues;
}


getDistinctValuesForColumns = (tableData: any[], columns: string[]) => {
  const result: any = [];
  columns.forEach((column) => {
    const uniqueValues = getUniqueValues(tableData, column);
    const mappedArray = getMappedArray(uniqueValues);
    result[column] = [...mappedArray];
  });
  return result;
  };


  filterChangeHandler(filterArray, column) {
    const filterArrayCopy = filterArray;
    if (column === 'USRGRP_NM') {
      this.activeFiltered.groupName.valArr = filterArrayCopy;
    }
    else if (column === 'USRGRP_DESC') {
      this.activeFiltered.description.valArr = filterArrayCopy;
    }
    else if (column === 'GRP_TYP') {
      this.activeFiltered.groupType.valArr = filterArrayCopy;
      console
    }
    else if (column === 'ACTV_FLG') {
      this.activeFiltered.active.valArr = filterArrayCopy;
    }
    const result=tableFilter(this.activeFiltered,this.table_data_copy)
    if (result.length){
    this.table_data=result
    this.tablePageNumber = 0
   }
   else{
    if(!Object.keys(this.activeFiltered).some(field=>this.activeFiltered[field].valArr.length)){
      this.table_data = this.table_data_copy
    }else {
      this.table_data = result
    }
   }
   this.setFilterOptions(this.activeFiltered,result)  

  }
  setFilterOptions(fieldObject,result){

		const filterFields = Object.keys(fieldObject)
		for(let field of filterFields){
		  if(!fieldObject[field].valArr.length){
			this.uniqueValues[fieldObject[field].column] = getMappedArray(getUniqueValues(result, fieldObject[field].column));  
      this.transformedGroupTypeValues = this.getTransformedValues(this.uniqueValues?.[this.Column.groupType]);
      this.transformedActiveValues = this.getTransformedValues(this.uniqueValues?.[this.Column.active]);
    }
		}
	  }
    resetAll() {
      this.filterComponents.forEach((item) => {
        item.component.resetSelection();
      });
      this.table_data = this.table_data_copy;
      this.activatedFilterReset(this.activeFiltered)
      this.uniqueValues = this.getDistinctValuesForColumns(this.table_data, this.columnArrays);
      this.transformedGroupTypeValues = this.getTransformedValues(this.uniqueValues?.[this.Column.groupType]);
      this.transformedActiveValues = this.getTransformedValues(this.uniqueValues?.[this.Column.active]);
    }
    activatedFilterReset(fieldObject:any){
		  const filterFields = Object.keys(fieldObject)
		  filterFields.forEach(key=> 
        fieldObject[key].valArr = [],
        )
		}



newGroupHandler(){
  this.loaderService.showLoader();

  const dialogRef = this.dialogService.open(NewGroupComponent, {
    header: 'New Group',
    width: '60%',
    height: '70%',
    data: {
      tableData: this.table_data_copy
    }
  });
  dialogRef.onClose.subscribe(result => {

    if (result === 'saved') {
      this.getResponseHandler();
    }

  });
  window.addEventListener('popstate', () => {
    dialogRef.close();
  });
  
}
editGroupHandler(editData:any){
  console.log(editData);
  const dialogRef = this.dialogService.open(EditGroupComponent, {
    header: 'Edit Group',
    width: '60%',
    height: '70%',
    data: {
      editData: editData,
      tableData: this.table_data_copy
    },
  });
  dialogRef.onClose.subscribe(result => {
    if (result === 'saved') {
      this.getResponseHandler();
    }
  });
  window.addEventListener('popstate', () => {
    dialogRef.close();
  });
}
deletePopupModel: any;
tableSelectedRowIndex: number;
deleteHandler(index: number) {
  this.deletePopupModel = {
    displayModal: true,
    header: 'Delete Group',
    icon: 'pi-exclamation-triangle',
    content: {
      header: `Delete ${this.table_data[index].USRGRP_NM}?`,
      text: 'This action will delete the Group permanently',
    },
    deleteBtnProps: {
      label: 'Delete',
      click: '',
    },
    cancelBtnProps: {
      label: 'Cancel',
      click: '',
    },
  };

  this.tableSelectedRowIndex = index;
}
deletePopupDeleteHandler = () => {
  let group_id = this.table_data[this.tableSelectedRowIndex].USRGRP_ID;

  let group_Name = this.table_data[this.tableSelectedRowIndex].USRGRP_NM;
  const postData={
    userGroupId:group_id,
  }
  this.deletePopupModel.displayModal = false;
  this.loaderService.showLoader();
  this.AdminApiService.deleteGroup(postData).subscribe((response) => {
    this.loaderService.hideLoader();
    if (response.status == 200) {
      this.toastService.clear();
      this.toastService.add({
        key: 'err',
        closable: false,
        sticky: false,
        severity: '',
        summary: '',
        detail: `${group_Name} Deleted Successfully`,
      });
      this.getResponseHandler();
    } else {
      console.log(response);
      this.toastService.clear();
      this.toastService.add({
        key: 'wrn',
        closable: false,
        sticky: false,
        severity: '',
        summary: '',
        detail: `Connection Error TryAgain`,
      });
    }
  });
};


}
