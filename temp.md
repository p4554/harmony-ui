import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  constructor(private http: HttpClient) {}

  getDatabaseProfilingPath(SchemaName: string, TableName: string) {
    const lPath = '/DataProfile/' + TableName + '.html';
    let fileName = '';

    if (window.document.location.protocol === 'file:' || System.IO.File.Exists(lPath)) {
      fileName = TableName;
    } else {
      const endpoint = 'https://harmony.datazymes.com:5000/profiling';
      const request = new FormData();
      request.append('schemaName', SchemaName);
      request.append('tableName', TableName);

      this.http.post(endpoint, request).subscribe((response) => {
        if (response.status === 200) {
          fileName = TableName;
          const path = '/DataProfile/' + TableName + '.html';
          const file = new Blob([response.body], { type: 'text/html' });
          const fileURL = URL.createObjectURL(file);
          const a = document.createElement('a');
          a.href = fileURL;
          a.download = TableName + '.html';
          document.body.appendChild(a);
          a.click();
        }
      });
    }
  }
}



System.IO.File.WriteAllText(path, response.Content);


<iframe [src]="pathToHtmlFile"></iframe>
